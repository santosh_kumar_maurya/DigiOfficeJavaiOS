//
//  LocationCell.h
//  iWork
//
//  Created by Shailendra on 01/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UIImageView *CheckUnCheckImageView;
@property(nonatomic,strong)IBOutlet UIImageView *CrossImageView;
@property(nonatomic,strong)IBOutlet UILabel *LocationNameLabel;
@property(nonatomic,strong)IBOutlet UILabel *AvibilityLabel;
@end
