//
//  NewTaskDetailTableViewCell.h
//  iWork
//
//  Created by Himanshu  Goyal on 30/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewTaskDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *detailStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailValueLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deatilValueLabelLeadingConstraints;
@property (weak, nonatomic) IBOutlet UILabel *sepratorLabel;

@end
