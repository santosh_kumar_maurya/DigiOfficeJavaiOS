//
//  AskProcessFilterViewController.m
//  iWork
//
//  Created by Shailendra on 08/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "AskProcessFilterViewController.h"
#import "Header.h"

@interface AskProcessFilterViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    
    NSMutableArray *arrayForBool;
    NSMutableArray *FilterArray;
    
    IBOutlet UITableView *FilterTableView;
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *CancelFilterBtn;
    IBOutlet UIButton *ApplyFilterBtn;
    IBOutlet UIView *BottomView;
    NSMutableArray *PlusImageArray;
    NSMutableArray *MinusImageArray;
    
    NSIndexPath *indexPath3;
    RatingCell *Cell3;
    NSIndexPath *indexPath4;
    MyWorkLocationCell *Cell4;
    NSIndexPath *indexPath5;
    DurationCell *Cell5;
    NSIndexPath *indexPath6;
    MyWorkEmployeeCell *Cell6;

    IBOutlet UIView *DatePickerViewBg;
    IBOutlet UIDatePicker *DatePicker;
    
    NSString *DatePickerSelectionStr;
    NSString *StartDateStr;
    NSString *EndDateStr;
    
    NSString *LocationSting;
    NSString *TrackID;
    NSString *StartDate;
    NSString *EndDate;
    NSDictionary *UserDic;
    NSString *RatingSelect;
    BOOL RadioSelect;
    NSString *Emp_IDStr;
    NSString *Emp_NameStr;
    NSDateFormatter *dateFormat;
    
    AppDelegate *delegate;
    NSUserDefaults *userDefault;
}

@end
@implementation AskProcessFilterViewController

- (void)viewDidLoad {
    userDefault = [NSUserDefaults standardUserDefaults];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    RadioSelect = YES;
    TrackID = @"";
    StartDate = @"";
    EndDate = @"";
    Emp_IDStr = @"";
    Emp_NameStr = @"";
    RatingSelect = @"";
    DatePickerSelectionStr = @"";
    arrayForBool = [[NSMutableArray alloc] init];
    PlusImageArray = [[NSMutableArray alloc] initWithObjects:@"Plus",@"Plus",@"Plus",@"Plus", nil];
    MinusImageArray = [[NSMutableArray alloc] initWithObjects:@"Minus",@"Minus",@"Minus",@"Minus", nil];
    FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"DURATION_FILTER", nil),NSLocalizedString(@"TASK_ID", nil),NSLocalizedString(@"TASK_TYPE", nil),NSLocalizedString(@"EMPLOYEE_FILTER", nil), nil];
    
    UINib *SelectionNib = [UINib nibWithNibName:@"DurationCell" bundle:nil];
    [FilterTableView registerNib:SelectionNib forCellReuseIdentifier:@"CELL2"];
    
    UINib *LocationNib = [UINib nibWithNibName:@"MyWorkLocationCell" bundle:nil];
    [FilterTableView registerNib:LocationNib forCellReuseIdentifier:@"CELL1"];

    UINib *SearchNib = [UINib nibWithNibName:@"RatingCell" bundle:nil];
    [FilterTableView registerNib:SearchNib forCellReuseIdentifier:@"CELL3"];

    UINib *MyWorkEmployeeNib = [UINib nibWithNibName:@"MyWorkEmployeeCell" bundle:nil];
    [FilterTableView registerNib:MyWorkEmployeeNib forCellReuseIdentifier:@"CELL"];
    
    arrayForBool=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[FilterArray count]; i++) {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    FilterTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, FilterTableView.bounds.size.width, 0.01f)];

    CancelBtn.layer.borderWidth = 1.0;
    CancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    CancelBtn.layer.cornerRadius = 3.0;
    CancelBtn.clipsToBounds = YES;
    
    CancelFilterBtn.layer.borderWidth = 1.0;
    CancelFilterBtn.layer.borderColor = delegate.redColor.CGColor;
    CancelFilterBtn.layer.cornerRadius = 3.0;
    CancelFilterBtn.clipsToBounds = YES;
    CancelFilterBtn.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    
    ApplyFilterBtn.layer.cornerRadius = 3.0;
    
    BottomView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    FilterTableView.backgroundColor = [UIColor whiteColor];
    [super viewDidLoad];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [FilterArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict = [userDefault objectForKey:@"ASKPROGRESS"];
    NSLog(@"%@",dict);
    if(indexPath.section == 0)
    {
        DurationCell * Cell = (DurationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
        [Cell.StartButton addTarget:self action:@selector(StartButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [Cell.EndButton addTarget:self action:@selector(EndButtonAction) forControlEvents:UIControlEventTouchUpInside];
        Cell.DateView.hidden = NO;
        if (dict != nil)
        {
            if ([[dict valueForKey:@"fromDate"] isEqualToString:@""] || [dict valueForKey:@"fromDate"] == nil)
            {
                if ([StartDateStr isEqualToString:@""])
                {
                    Cell.StartDateTextField.text = StartDateStr;
                }
                else
                {
                    Cell.StartDateTextField.text = StartDateStr;
                }
                
                if ([EndDateStr isEqualToString:@""])
                {
                    Cell.EndDateTextField.text = EndDateStr;
                }
                else
                {
                    Cell.EndDateTextField.text = EndDateStr;
                }
            }
            else
            {
                if ([StartDateStr isEqualToString:@""] || StartDateStr == nil)
                {
                    Cell.StartDateTextField.text = [dict valueForKey:@"fromDate"];
                    StartDateStr = Cell.StartDateTextField.text;
                }
                else
                {
                    Cell.StartDateTextField.text = StartDateStr;
                }
                
                if ([EndDateStr isEqualToString:@""] || EndDateStr == nil)
                {
                    Cell.EndDateTextField.text = [dict valueForKey:@"toDate"];
                    EndDateStr = Cell.EndDateTextField.text;
                }
                else
                {
                    Cell.EndDateTextField.text = EndDateStr;
                }
            }
        }
        else
        {
            if ([StartDateStr isEqualToString:@""])
            {
                Cell.StartDateTextField.text = StartDateStr;
            }
            else
            {
                Cell.StartDateTextField.text = StartDateStr;
            }
            
            if ([EndDateStr isEqualToString:@""])
            {
                Cell.EndDateTextField.text = EndDateStr;
            }
            else
            {
                Cell.EndDateTextField.text = EndDateStr;
            }
        }
        return Cell;
    }
    else if (indexPath.section == 1){
        MyWorkLocationCell * Cell = (MyWorkLocationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        Cell.LocationTextField.delegate = self;
        Cell.LocationTextField.keyboardType = UIKeyboardTypeNumberPad;
        if (dict != nil)
        {
            if ([[dict valueForKey:@"taskId"] integerValue] == 0 || [dict valueForKey:@"taskId"] == nil)
            {
                if ([TrackID isEqualToString:@""])
                {
                    Cell.LocationTextField.text = TrackID;
                    Cell.LocationTextField.placeholder = NSLocalizedString(@"SEARCH_TASK_ID", nil);
                }
                else
                {
                    Cell.LocationTextField.text = TrackID;
                }
            }
            else
            {
                if ([TrackID isEqualToString:@""])
                {
                    if ([[dict valueForKey:@"taskId"] integerValue] == -1)
                    {
                        Cell.LocationTextField.text = [userDefault valueForKey:@"taskId"];
                    }
                    else
                    {
                        Cell.LocationTextField.text = [NSString stringWithFormat:@"%@", [dict valueForKey:@"taskId"]];
                    }
                    TrackID = Cell.LocationTextField.text;
                    
                }
                else
                {
                    Cell.LocationTextField.text = TrackID;
                }
            }
        }
        else
        {
            if ([TrackID isEqualToString:@""])
            {
                Cell.LocationTextField.text = TrackID;
                Cell.LocationTextField.placeholder = NSLocalizedString(@"SEARCH_TASK_ID", nil);
            }
            else
            {
                Cell.LocationTextField.text = TrackID;
            }
        }
        return Cell;
    }
    else if (indexPath.section == 2){
        RatingCell * Cell = (RatingCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
        Cell.LowestImageLabel.text = NSLocalizedString(@"MY_TASK", nil);
        Cell.HigestImageLabel.text = NSLocalizedString(@"ASSIGNMENT", nil);
        Cell.AllImageLabel.text = NSLocalizedString(@"BOTH", nil);
        
        if (dict != nil)
        {
            if ([[dict valueForKey:@"taskType"] isEqualToString:@"BOTH"] || [[dict valueForKey:@"taskType"] isEqualToString:@"both"] || [dict valueForKey:@"taskType"] == nil)
            {
                if ([RatingSelect isEqualToString:@""])
                {
                    RatingSelect = [dict valueForKey:@"taskType"];
                }
                else
                {
                    NSLog(@"%@",RatingSelect);
                }
            }
            else
            {
                if ([RatingSelect isEqualToString:@""])
                {
                    RatingSelect = [dict valueForKey:@"taskType"];
                }
                else
                {
                    NSLog(@"%@",RatingSelect);
                }
            }
        }
        else
        {
            if ([RatingSelect isEqualToString:@""])
            {
                RatingSelect = @"BOTH";
            }
            else
            {
                
            }
        }
        
        
        if([RatingSelect isEqualToString:@"MYTASK"] || [RatingSelect isEqualToString:@"task"]){
            Cell.LowestImageView.image = [UIImage imageNamed:@"RadioOn"];
        }
        else{
            Cell.LowestImageView.image = [UIImage imageNamed:@"RadioOff"];
        }
        if([RatingSelect isEqualToString:@"ASSIGNMENT"] || [RatingSelect isEqualToString:@"assignment"]){
            
            Cell.HigestImageView.image = [UIImage imageNamed:@"RadioOn"];
        }
        else{
            Cell.HigestImageView.image = [UIImage imageNamed:@"RadioOff"];
        }
        if([RatingSelect isEqualToString:@"BOTH"] || [RatingSelect isEqualToString:@"both"]){
            Cell.AllImageView.image = [UIImage imageNamed:@"RadioOn"];
        }
        else{
            Cell.AllImageView.image = [UIImage imageNamed:@"RadioOff"];
        }
        [Cell.LowestBtn addTarget:self action:@selector(RatingSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.HigestBtn addTarget:self action:@selector(RatingSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.AllBtn addTarget:self action:@selector(RatingSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
        return Cell;
    }
    else{
        MyWorkEmployeeCell * Cell = (MyWorkEmployeeCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        if (dict != nil)
        {
            
            if ([[dict valueForKey:@"employeeName"] isEqualToString:@""])
            {
                if ([Emp_NameStr isEqualToString:@""])
                {
                }
                else
                {
                }
            }
            else
            {
                if ([Emp_NameStr isEqualToString:@""])
                {
                    Emp_NameStr = [NSString stringWithFormat:@"%@", [dict valueForKey:@"employeeName"]];
                    RadioSelect = YES;
                }
                else
                {
                }
            }
            
            if ([[dict valueForKey:@"employeeId"] isEqualToString:@""])
            {
                if ([Emp_IDStr isEqualToString:@""])
                {
                }
                else
                {
                }
            }
            else
            {
                if ([Emp_IDStr isEqualToString:@""])
                {
                    Emp_IDStr = [NSString stringWithFormat:@"%@", [dict valueForKey:@"employeeId"]];
                    RadioSelect = NO;
                }
                else
                {
                }
            }
        }
        else
        {
          
        }
        if(RadioSelect==YES)
        {
            Cell.EmployeeNameRadioImage.image = [UIImage imageNamed:@"RadioOn"];
            Cell.EmployeeIDRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            Cell.EmployeeIDTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME_FILTER", nil);
            Cell.EmployeeIDTextField.keyboardType = UIKeyboardTypeDefault;
            Cell.EmployeeIDTextField.text = Emp_NameStr;
        }
        else
        {
            Cell.EmployeeNameRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            Cell.EmployeeIDRadioImage.image = [UIImage imageNamed:@"RadioOn"];
            Cell.EmployeeIDTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID_FILTER", nil);
            Cell.EmployeeIDTextField.keyboardType = UIKeyboardTypeNumberPad;
            Cell.EmployeeIDTextField.text = Emp_IDStr;
        }
        [Cell.EmployeeNameBtn addTarget:self action:@selector(RadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.EmployeeIDBtn addTarget:self action:@selector(RadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
        return Cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0 ||indexPath.section == 1){
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return 60;
        }
        else{
            return 0;
        }
    }
    else if(indexPath.section == 2 ){
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return 117;
        }
        else{
            return 0;
        }
    }
    else if(indexPath.section == 3 ){
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return 145;
        }
        else{
            return 0;
        }
    }
    return 0;
    
}
-(void)StartButtonAction{
    DatePickerViewBg.hidden = NO;
    DatePickerSelectionStr = @"Start";
    [self.view endEditing:YES];
}
-(void)EndButtonAction{
    DatePickerViewBg.hidden = NO;
    DatePickerSelectionStr = @"End";
    [self.view endEditing:YES];
}
-(void)RadioBtnOnOff:(UIButton*)Sender{
    switch (Sender.tag) {
        case 0:
            RadioSelect = YES;
            [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        case 1:
            RadioSelect = NO;
            [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        default:
            break;
    }
}
-(void)RatingSelectBtn:(UIButton*)Sender{
    switch (Sender.tag) {
        case 0:
            RatingSelect = @"MYTASK";
            [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        case 1:
            RatingSelect = @"ASSIGNMENT";
            [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        case 2:
            RatingSelect = @"BOTH";
            [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        default:
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
#pragma mark - Creating View for TableView Section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *Views=[[UIView alloc]initWithFrame:CGRectMake(0, 0, FilterTableView.frame.size.width,0)];
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, FilterTableView.frame.size.width,44)];
    sectionView.tag=section;
    sectionView.backgroundColor  = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
    UILabel *viewLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, FilterTableView.frame.size.width, 44)];
    viewLabel.backgroundColor=[UIColor clearColor];
    viewLabel.textAlignment = NSTextAlignmentLeft;
    viewLabel.textColor=[UIColor blackColor];
    viewLabel.font=[UIFont systemFontOfSize:14];
    viewLabel.text=[NSString stringWithFormat:@"%@",[FilterArray objectAtIndex:section]];
    [sectionView addSubview:viewLabel];
    
    UIImageView *ImageViews = [[UIImageView alloc] initWithFrame:CGRectMake(FilterTableView.frame.size.width-40, 17, 10, 10)];
    if ([[arrayForBool objectAtIndex:section] boolValue]){
        ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[MinusImageArray objectAtIndex:section]]];
    }
    else{
        ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[PlusImageArray objectAtIndex:section]]];
    }
    [sectionView addSubview:ImageViews];
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    return  sectionView;
    return Views;
}
#pragma mark - Table header gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[FilterArray count]; i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)CancelAction:(id)sender{
    [self GetCellValue];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)ClearFilterAction:(id)sender
{
    RadioSelect = YES;
    EndDateStr = @"";
    StartDateStr = @"";
    TrackID = @"";
    Emp_IDStr = @"";
    Emp_NameStr = @"";
    RatingSelect = @"BOTH";
    Cell4.LocationTextField.text = @"";
    Cell6.EmployeeIDTextField.text = @"";
    [FilterTableView reloadData];
    [userDefault removeObjectForKey:@"ASKPROGRESS"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearFilteriPM" object:nil userInfo:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)MyTeamApplyFilter:(id)sender
{
    [self GetCellValue];
    
    StartDate  = Cell5.StartDateTextField.text;
    EndDate  = Cell5.EndDateTextField.text;
    TrackID = Cell4.LocationTextField.text;
    
    NSString *TaskType = @"";
    NSString *EmployeeType = @"";
    if([RatingSelect isEqualToString:@"MYTASK"] || [RatingSelect isEqualToString:@"task"]){
        TaskType = @"task";
    }
    else if([RatingSelect isEqualToString:@"ASSIGNMENT"] || [RatingSelect isEqualToString:@"assignment"]){
        TaskType = @"assignment";
    }
    else if([RatingSelect isEqualToString:@"BOTH"] || [RatingSelect isEqualToString:@"both"]){
        TaskType = @"BOTH";
    }
    if(RadioSelect==YES){
        EmployeeType = @"empName";
        Emp_NameStr = Cell6.EmployeeIDTextField.text;
        Emp_IDStr = @"";
        
    }
    else{
        EmployeeType = @"empId";
        Emp_IDStr = Cell6.EmployeeIDTextField.text;
        Emp_NameStr = @"";
    }
    
    
    UserDic = @ {
        @"employeeId": Emp_IDStr,
        @"employeeName":Emp_NameStr,
        @"fromDate": StartDate,
        @"taskId": TrackID,
        @"taskType": TaskType,
        @"toDate": EndDate,
        @"user_Id"  : [ApplicationState userId],
        @"offset" : [NSNumber numberWithInt:0],
        @"limit" : [NSNumber numberWithInt:50]
    };
    NSLog(@"UserDic==%@",UserDic);
    [userDefault setObject:UserDic forKey:@"ASKPROGRESS"];
    [userDefault synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ASKPROGRESS" object:nil userInfo:UserDic];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    DatePickerViewBg.hidden = YES;
}
-(IBAction)DatePickerDoneAction{
    DatePickerViewBg.hidden = YES;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    if([DatePickerSelectionStr isEqualToString:@"Start"]){
        StartDateStr = [dateFormat stringFromDate: DatePicker.date];
        EndDateStr = @"";
    }
    else if([DatePickerSelectionStr isEqualToString:@"End"]){
        EndDateStr = [dateFormat stringFromDate: DatePicker.date];
    }
    [FilterTableView reloadData];
    if(![EndDateStr isEqualToString:@""]){
        NSComparisonResult result;
        NSDate *startDate = [dateFormat dateFromString:StartDateStr];
        NSDate *endDate = [dateFormat dateFromString:EndDateStr];
        result = [startDate compare:endDate];
        if(result==NSOrderedAscending){
            [self ReloadSection];
        }
        else if(result==NSOrderedDescending){
            EndDateStr = @"";
            [self ShowAlert:@"You cannot select date earlier to start date" ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
    }
}
-(void)ReloadSection{
    [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void)GetCellValue{
    indexPath5 = [NSIndexPath indexPathForRow:0 inSection:0];
    Cell5 = [FilterTableView cellForRowAtIndexPath:indexPath5];
    [Cell5.StartDateTextField resignFirstResponder];
    [Cell5.EndDateTextField resignFirstResponder];
    indexPath4 = [NSIndexPath indexPathForRow:0 inSection:1];
    Cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
    [Cell4.LocationTextField resignFirstResponder];
    
    indexPath6 = [NSIndexPath indexPathForRow:0 inSection:3];
    Cell6 = [FilterTableView cellForRowAtIndexPath:indexPath6];
    [Cell6.EmployeeIDTextField resignFirstResponder];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}


@end
