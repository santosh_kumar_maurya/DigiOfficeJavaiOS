//
//  NeedRevisionCell.m
//  iWork
//
//  Created by Shailendra on 21/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "NeedRevisionCell.h"
#import "Header.h"
#import "iWork-Swift.h"
@implementation NeedRevisionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.layer.cornerRadius =6;
//    self.layer.masksToBounds =YES;
//    
//    self.layer.cornerRadius = 7.0f;
//    self.layer.shadowRadius = 3.0f;
//    self.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
//    self.layer.shadowOpacity = 0.7f;
//    self.layer.masksToBounds = NO;
    
//    CAShapeLayer * maskLayer = [CAShapeLayer layer];
//    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.RjectBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){6.0}].CGPath;
//    self.RjectBtn.layer.mask = maskLayer;
//    
//    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
//    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: self.ApproveBtn
//                       .bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
//    self.ApproveBtn.layer.mask = maskLayer1;
//    
//    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
//    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: self.NeedRevisionBtn
//                       .bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){6.0}].CGPath;
//    self.NeedRevisionBtn.layer.mask = maskLayer2;
//    
//    CAShapeLayer * maskLayer3 = [CAShapeLayer layer];
//    maskLayer3.path = [UIBezierPath bezierPathWithRoundedRect: self.CompleteApproveBtn
//                       .bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
//    self.CompleteApproveBtn.layer.mask = maskLayer3;
//    
//    CAShapeLayer * maskLayer4 = [CAShapeLayer layer];
//    maskLayer4.path = [UIBezierPath bezierPathWithRoundedRect: self.StartTaskBtn
//                       .bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
//    self.StartTaskBtn.layer.mask = maskLayer4;
    
    self.RjectBtn.backgroundColor = [UIColor clearColor];
    self.ApproveBtn.backgroundColor = [UIColor clearColor];
    self.CompleteApproveBtn.backgroundColor = [UIColor clearColor];
    self.StartTaskBtn.backgroundColor = [UIColor clearColor];
    self.NeedRevisionBtn.backgroundColor = [UIColor clearColor];
    
    [self.RjectBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [self.ApproveBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    [self.CompleteApproveBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    [self.StartTaskBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    [self.NeedRevisionBtn setTitleColor:[UIColor NeedRevisionBtnColor] forState:UIControlStateNormal];
}

- (void)configureCell:(NSDictionary *)info{
   
    if ([[info objectForKey:@"type"] isEqualToString:@"APPROVE"]){
        _StatusImageView.image = [UIImage imageNamed:@"assigned"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"Assigned"]){
        _StatusImageView.image = [UIImage imageNamed:@"assigned"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"EMP_NEWCOMMENT"]){
        _StatusImageView.image = [UIImage imageNamed:@"comment"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"FAIL"]){
        _StatusImageView.image = [UIImage imageNamed:@"cancelled_new"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"LM_NEWCOMMENT"]){
        _StatusImageView.image = [UIImage imageNamed:@"comment"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"SUSPEND"]){
        _StatusImageView.image = [UIImage imageNamed:@"cancelled_new"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"COMPLETE"]){
        _StatusImageView.image = [UIImage imageNamed:@"submitted_new"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"START"]){
        _StatusImageView.image = [UIImage imageNamed:@"inprogressblue"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"NEWTASK"]){
        _StatusImageView.image = [UIImage imageNamed:@"taskrequest"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"CANCEL"]){
        _StatusImageView.image = [UIImage imageNamed:@"cancelled"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"REJECT"]){
        _StatusImageView.image = [UIImage imageNamed:@"declined_new"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"CLOSE"]){
        _StatusImageView.image = [UIImage imageNamed:@"confirmed"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"NEED REVISION"]){
        _StatusImageView.image = [UIImage imageNamed:@"NeedRevision"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"S_CLOSE"]
             || [[info objectForKey:@"type"] isEqualToString:@"STACKFEED"] || [[info objectForKey:@"type"] isEqualToString:@"LM_STACKFEED"]){
        _StatusImageView.image = [UIImage imageNamed:@"review"];
    }
    
    if(IsSafeStringPlus(TrToString(info[@"notification"]))){
        _NotificationLabel.text  = info[@"notification"];
    }
    else{
        _NotificationLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"creationDate"]))){
        NSNumber *datenumber = info[@"creationDate"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _DateLabel.text = @" ";
    }

}

-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm aa"];
    return [dateformate stringFromDate:Newdate];
}
@end
