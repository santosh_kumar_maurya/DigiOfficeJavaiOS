

#import "LoadingAgent.h"
#import "QuartzCore/QuartzCore.h"
#import "AppDelegate.h"

static LoadingAgent *agent;

@implementation LoadingAgent
- (id)init{
	return nil;
}

- (id)myinit{
	delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    if( self == [super init]){
        loadingCount = 0;
		UIWindow* keywindow = [[UIApplication sharedApplication] keyWindow];
        CGRect keyFrame=[keywindow frame];
        CGRect frame=CGRectMake(keyFrame.origin.x, keyFrame.origin.y+delegate.headerheight, keyFrame.size.width, keyFrame.size.height-delegate.headerheight-delegate.headerheight);
		main_view = [[UIView alloc] initWithFrame:frame];
		main_view.backgroundColor =[UIColor clearColor];
		main_view.alpha =1.0;
		
		wait = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		wait.hidesWhenStopped = NO;
		frame=CGRectMake((delegate.devicewidth-265)/2, (delegate.deviceheight-136)/2, 265.0, 136.0);
		UIView *loadingView=[[UIView alloc]initWithFrame:frame];
		loadingView.backgroundColor=[UIColor clearColor];
        frame=CGRectMake(3.0, 3.0, 259.0,130.0);
        UIView *iv=[[UIView alloc]initWithFrame:frame];
        iv.backgroundColor=delegate.BackgroudColor;
        [loadingView addSubview:iv];
        frame=CGRectMake(99.0, 47.0, 146.0,42.0);
		UILabel *loadingLabel = [[UILabel alloc] initWithFrame:frame];
		loadingLabel.textColor = [UIColor whiteColor];
		loadingLabel.backgroundColor = [UIColor clearColor];
		loadingLabel.text = @"Please wait...";
        //loadingLabel.lineBreakMode=UILineBreakModeWordWrap;
		loadingLabel.numberOfLines=0;		
		[loadingView addSubview:loadingLabel];
		[loadingView addSubview:wait];
		
		frame=CGRectMake(27.0, 49.0, 37.0,37.0);
		wait.frame=frame;
		
		CALayer *l=[loadingView layer];
		[l setCornerRadius:7.0];
		[l setBorderWidth:3.0];
		[l setBorderColor:[[UIColor whiteColor]CGColor]];  
        
        [main_view addSubview:loadingView];
		[wait startAnimating];
		
		return self;
	}	
	return nil;
}

-(BOOL) isBusy{
   if(loadingCount!=0)
	   return YES;
  return NO;
}

- (void) makeBusy:(BOOL)yesOrno{
    
	if(yesOrno){
		loadingCount=1;
	}else {
		loadingCount=0;
		if(loadingCount<=0){
			loadingCount = 0;
		}
	}
	
	if(loadingCount == 1){
		[[[UIApplication sharedApplication] keyWindow] addSubview:main_view];
		[[[UIApplication sharedApplication] keyWindow] bringSubviewToFront:main_view];
	}else if(loadingCount == 0) {
		[main_view removeFromSuperview];
	}else {
		[[[UIApplication sharedApplication] keyWindow] bringSubviewToFront:main_view];
	}
	
}

- (void) forceRemoveBusyState{
	loadingCount = 0;
	[main_view removeFromSuperview];
}

+ (LoadingAgent*)defaultAgent{
   if(!agent){
		agent =[[LoadingAgent alloc] myinit];
	}
	return agent;
}


@end
