#import "SearchDetailsScreen.h"
#import "Header.h"
@interface SearchDetailsScreen ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *SearchTableView;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UILabel *NoDataLabel;

    NSMutableArray *SearchArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
   
    int TagValue;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
    UIView *CoomentView;
    UITextView *CommentTextView;
}
@end

@implementation SearchDetailsScreen
@synthesize query;

- (void)viewDidLoad{
    offset = 0;
    limit = 50;
    HeaderLabel.text = query;
    SearchArray = [[NSMutableArray alloc] init];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    SearchTableView.estimatedRowHeight = 50;
    SearchTableView.backgroundColor = delegate.BackgroudColor;
    SearchTableView.rowHeight = UITableViewAutomaticDimension;
    SearchTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, SearchTableView.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [SearchTableView addSubview:refreshControl];
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [SearchTableView registerNib:Nib forCellReuseIdentifier:@"CELL"];
}
- (void)reloadData{
    offset = 0;
    limit = 50;
    SearchArray = [[NSMutableArray alloc] init];
    [self fetchDetail];
}
-(void)viewWillAppear:(BOOL)animated{
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
}
-(void) fetchDetail
{
    if(delegate.isInternetConnected){
        params = @ {
            @"taskName": query,
            @"userId"  : [ApplicationState userId],
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
            
        };
        ResponseDic =  [Api WebApi:params Url:@"taskNameList"];
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            NSLog(@"SearchDetailsResponseDic==%@",ResponseDic);
            NoDataLabel.hidden = YES;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskList"]))){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"taskList"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"taskList"]];
                    NoDataLabel.hidden = YES;
                    [SearchTableView reloadData];
                }
                else if(ResponseArrays.count == 0 && SearchArray.count == 0){
                  [self NoDataFound];
                }
            }
            else{
               [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(SearchArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Record Found";
    }
    [SearchTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskList"]))){
        NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"taskList"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self fetchDetail];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [SearchArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [SearchArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//    Cell.DetailsBtn.tag = indexPath.section;
//    [Cell.DetailsBtn addTarget:self action:@selector(StartCancelDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    if(SearchArray.count>0){
        NSDictionary * responseData = [SearchArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
    }
//    if ([[[SearchArray objectAtIndex:indexPath.section] objectForKey:@"taskStatus"] isEqualToString:@"New"]||[[[SearchArray objectAtIndex:indexPath.section] objectForKey:@"taskStatus"] isEqualToString:@"new"]){
//        [Cell.DetailsBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
//        Cell.RatingView.hidden = YES;
//        Cell.StausImageView.hidden = NO;
//        Cell.StatusLabel.hidden = NO;
//        Cell.DetailsBtn.hidden =NO;
//    }
//    else if ([[[SearchArray objectAtIndex:indexPath.section] objectForKey:@"taskStatus"]  isEqualToString:@"Assigned"]){
//        [Cell.DetailsBtn setTitle:NSLocalizedString(@"START", nil) forState:UIControlStateNormal];
//        Cell.RatingView.hidden = YES;
//        Cell.StausImageView.hidden = NO;
//        Cell.StatusLabel.hidden = NO;
//        Cell.DetailsBtn.hidden =NO;
//    }
//    else if ([[[SearchArray objectAtIndex:indexPath.section] objectForKey:@"taskStatus"] isEqualToString:@"Declined"] ||([[[SearchArray objectAtIndex:indexPath.section] objectForKey:@"taskStatus"] isEqualToString:@"Discarded"])){
//        Cell.RatingView.hidden = YES;
//        Cell.StausImageView.hidden = NO;
//        Cell.StatusLabel.hidden = NO;
//        Cell.DetailsBtn.hidden =YES;
//    }
//    else if ([[[SearchArray objectAtIndex:indexPath.section] objectForKey:@"taskStatus"] isEqualToString:@"Approved"] ||([[[SearchArray objectAtIndex:indexPath.section] objectForKey:@"taskStatus"] isEqualToString:@"Rejected"])){
//        Cell.RatingView.hidden = NO;
//        Cell.StausImageView.hidden = YES;
//        Cell.StatusLabel.hidden = YES;
//        Cell.DetailsBtn.hidden =NO;
//        [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
//    }
//    else{
//        Cell.RatingView.hidden = YES;
//        Cell.StausImageView.hidden = NO;
//        Cell.StatusLabel.hidden = NO;
//        Cell.DetailsBtn.hidden =NO;
//        [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
//    }
    return Cell;
}
-(void)CommentView{
    
    CoomentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
    CoomentView.backgroundColor = [UIColor clearColor];
    UIImageView *ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    ImageView.backgroundColor = [UIColor blackColor];
    ImageView.alpha = 0.3;
    UIView *InnerView = [[UIView alloc] initWithFrame:CGRectMake(10, delegate.deviceheight/2 - 100, delegate.devicewidth -20, 150)];
    CGPoint center = InnerView.center;
    center.x = CoomentView.center.x;
    InnerView.center = center;
    InnerView.layer.cornerRadius = 5.0;
    InnerView.backgroundColor = [UIColor whiteColor];
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(0, InnerView.frame.size.height-40, InnerView.frame.size.width, 40)];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: BtnView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    BtnView.layer.mask = maskLayer1;
    
    UILabel *AddCommentLabel;
    AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 45)];
    AddCommentLabel.backgroundColor = delegate.yellowColor;
    AddCommentLabel.textColor = [UIColor blackColor];
    AddCommentLabel.font = delegate.headFont;
    AddCommentLabel.textAlignment = NSTextAlignmentCenter;
    if([[[SearchArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
        AddCommentLabel.text = NSLocalizedString(@"START_TASK", nil);
    }
    else{
        AddCommentLabel.text = NSLocalizedString(@"CANCEL_TASK_SMALL", nil);
    }
    
    CAShapeLayer * AddCommentmaskLayer1 = [CAShapeLayer layer];
    AddCommentmaskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: AddCommentLabel.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){5.0}].CGPath;
    AddCommentLabel.layer.mask = AddCommentmaskLayer1;
    
    UILabel *AddCommentStaticLabel;
    AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, AddCommentLabel.frame.origin.y+AddCommentLabel.frame.size.height + 10, delegate.devicewidth-40, 40)];
    AddCommentStaticLabel.textColor = [UIColor blackColor];
    AddCommentStaticLabel.font = delegate.normalFont;
    AddCommentStaticLabel.numberOfLines = 0;
    AddCommentStaticLabel.textAlignment = NSTextAlignmentCenter;
    if([[[SearchArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
        AddCommentStaticLabel.text = NSLocalizedString(@"ARE_YOU_SURE_START", nil);
    }
    else{
        AddCommentStaticLabel.text = NSLocalizedString(@"ARE_YOU_SURE_CANCEL", nil);
    }
    
    UIButton *SubmitBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, BtnView.frame.size.width/2, 40)];
    [SubmitBtn addTarget:self action:@selector(SubmitAction) forControlEvents:UIControlEventTouchUpInside];
    SubmitBtn.backgroundColor = delegate.yellowColor;
    SubmitBtn.titleLabel.font = delegate.normalFont;
    [SubmitBtn setTitle:NSLocalizedString(@"OKAY", nil) forState:UIControlStateNormal];
    
    UIButton *CancelBtn=[[UIButton alloc] initWithFrame:CGRectMake(BtnView.frame.size.width/2, 0, BtnView.frame.size.width/2, 40)];
    [CancelBtn addTarget:self action:@selector(CancelAction) forControlEvents:UIControlEventTouchUpInside];
    CancelBtn.backgroundColor = delegate.redColor;
    CancelBtn.titleLabel.font = delegate.normalFont;
    [CancelBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    [BtnView addSubview:SubmitBtn];
    [BtnView addSubview:CancelBtn];
    [InnerView addSubview:BtnView];
    [InnerView addSubview:AddCommentLabel];
    [InnerView addSubview:AddCommentStaticLabel];
    [CoomentView addSubview:ImageView];
    [CoomentView addSubview:InnerView];
    
    [self.view addSubview:CoomentView];
}
-(void)CancelAction{
    CoomentView.hidden = YES;
}
-(void)SubmitAction{
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(CancelTaskSubmitAction) withObject:nil afterDelay:0.5];
}
-(void)CancelTaskSubmitAction{
    if(delegate.isInternetConnected){
        NSString* TaskID = [[SearchArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        if([[[SearchArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
            ResponseDic =  [Api WebApi:nil Url:[NSString stringWithFormat:@"startButton?employeeId=%@&taskId=%@",[ApplicationState userId],TaskID]];
        }
        else{
            params = @ {
                @"comment" : @"",
                @"status" : @"4",
                @"actionBy" : @"3",   // Cancel By Employee
                @"taskId" : TaskID,
                @"user_id" : [ApplicationState userId]
            };
            ResponseDic =  [Api WebApi:params Url:@"cancelTask"];
        }
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            CoomentView.hidden = YES;
            [self fetchDetail];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else{
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)StartCancelDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]||[sender.titleLabel.text isEqualToString:NSLocalizedString(@"START", nil)]){
        [self CommentView];
    }
    else{
        NSString* str = [[SearchArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        if ([[[SearchArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
            TaskDetailManagerScreen *viewController = [[TaskDetailManagerScreen alloc] init];
            viewController.taskID = str;
            viewController.isManager = TRUE;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else{
            HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
            viewController.taskID = str;
            viewController.isManager = TRUE;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
    }
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)HomeAction:(id)sender{
    [self GotobackContainer];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
