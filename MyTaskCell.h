//
//  MyTaskCell.h
//  iWork
//
//  Created by Shailendra on 04/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTaskCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UIView *TaskView;
@property(nonatomic,strong)IBOutlet UIView *TopView;
@property(nonatomic,strong)IBOutlet UILabel *MyTaskStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *TaskCreationStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *TaskCreationLabel;
@property(nonatomic,strong)IBOutlet UILabel *TaskCompletionStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *TaskCompletionLabel;
@property(nonatomic,strong)IBOutlet UIButton *TaskCreationBtn;
@property(nonatomic,strong)IBOutlet UIButton *TaskCompletionBtn;
@property(nonatomic,strong)IBOutlet UIButton *ViewAllTasksBtn;
@end
