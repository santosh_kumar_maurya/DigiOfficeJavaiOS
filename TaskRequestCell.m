//
//  TaskRequestCell.m
//  iWork
//
//  Created by Shailendra on 17/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "TaskRequestCell.h"
#import "Header.h"
#import "iWork-Swift.h"
@implementation TaskRequestCell


- (void)awakeFromNib {
    [super awakeFromNib];
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.TaskView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    self.TaskView.layer.mask = maskLayer;

    [self.CompleteApproveBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    [self.ApproveBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    [self.RejectBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [self.CompleteViewDetailsBtn setTitleColor:[UIColor ButtonSkyColor] forState:UIControlStateNormal];
    [self.ApproveRejectViewDetailsBtn setTitleColor:[UIColor ButtonSkyColor] forState:UIControlStateNormal];
    [self.NeedRevisionBtn setTitleColor:[UIColor NeedRevisionBtnColor] forState:UIControlStateNormal];
    
    
}
- (void)configureCell:(NSDictionary *)info{
    
    if(IsSafeStringPlus(TrToString(info[@"taskId"]))){
        _TaskIdLabel.text  = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),info[@"taskId"]];
    }
    else{
        _TaskIdLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"creationDate"]))){
        NSNumber *datenumber = info[@"creationDate"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _DateLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskName"]))){
        _TaskNameLabel.text  = info[@"taskName"];
    }
    else{
        _TaskNameLabel.text  = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"empName"]))){
        _CreatedbyLabel.text  = info[@"empName"];
    }
    else{
        _CreatedbyLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"duration"]))){
        _DurationLabel.text  = info[@"duration"];
    }
    else{
        _DurationLabel.text  = @" ";
    }    
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm aa"];
    return [dateformate stringFromDate:Newdate];
}
@end
