//
//  FeedbackStackHolderViewController.m
//  iWork
//
//  Created by Shailendra on 01/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "FeedbackStackHolderViewController.h"
#import "Header.h"

@interface FeedbackStackHolderViewController (){
    IBOutlet UITableView *FeedbackStackHolderTableView;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UIView *OuterView;
    IBOutlet UIView *InnerView;
    IBOutlet UILabel *FeedbackLabel;
    IBOutlet UIButton *OkayBtn;
    NSMutableArray *FeedbackStackHoderArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    WebApiService *Api;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
    NSDictionary *params;
    IBOutlet UIView *NoDataView;
}
@end

@implementation FeedbackStackHolderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self clearfilterUserdefaultData];
    offset = 0;
    limit = 50;
    CheckValue = @"STAKEHOLDER";
    Api = [[WebApiService alloc] init];
    FeedbackStackHoderArray = [[NSMutableArray alloc]init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor whiteColor];
    FeedbackStackHolderTableView.estimatedRowHeight = 50;
    FeedbackStackHolderTableView.backgroundColor = [UIColor whiteColor];
    FeedbackStackHolderTableView.rowHeight = UITableViewAutomaticDimension;
    FeedbackStackHolderTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, FeedbackStackHolderTableView.bounds.size.width, 10.f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [FeedbackStackHolderTableView addSubview:refreshControl];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(FeedbackAsStakeHolderApi) withObject:nil afterDelay:0.5];
    [self FeedbackView];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"STACKHOLDER" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ClearFilterData:) name:@"ClearFilteriPM" object:nil];
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    offset = 0;
    limit = 50;
    FeedbackStackHoderArray = [[NSMutableArray alloc]init];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(FeedbackAsStakeHolderApi) withObject:nil afterDelay:0.5];
}
-(void)ClearFilterData:(NSNotification*)notification{
    CheckValue = @"STAKEHOLDER";
    offset = 0;
    limit = 50;
    FeedbackStackHoderArray = [[NSMutableArray alloc]init];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(FeedbackAsStakeHolderApi) withObject:nil afterDelay:0.5];
}
-(void)FeedbackView{
    InnerView.layer.cornerRadius = 5;
    InnerView.clipsToBounds = YES;
    [OkayBtn setTitle:NSLocalizedString(@"OKAY", nil) forState:UIControlStateNormal];
}
- (void)reloadData
{
    if(![CheckValue isEqualToString:@"FILTER"])
    {
        CheckValue = @"Refresh";
        offset = 0;
        limit = 50;
        FeedbackStackHoderArray = [[NSMutableArray alloc]init];
        [self FeedbackAsStakeHolderApi];
    }
}
- (void)FeedbackAsStakeHolderApi {
    
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"STAKEHOLDER"]||[CheckValue isEqualToString:@"Refresh"] || [CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"employeeName": @"",
                    @"endDate": @"",
                    @"startDate": @"",
                    @"taskId": @0,
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            ResponseDic = [Api WebApi:params Url:[NSString stringWithFormat:@"FeedbackAsStakeCountList?employeeID=%@",[ApplicationState userId]]];
            [refreshControl endRefreshing];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self FeedbackAsStakeHolderApi];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
                [LoadingManager hideLoadingView:self.view];
                NSLog(@"FeedbackStackHolderDic==%@",ResponseDic);
                NoDataLabel.hidden = YES;
                if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                    NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                    if(ResponseArrays.count>0){
                        [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                        NoDataLabel.hidden = YES;
                    }
                    else if(ResponseArrays.count == 0 && FeedbackStackHoderArray.count == 0){
                        [self NoDataFound];
                    }
                    [FeedbackStackHolderTableView reloadData];
                }
                else{
                   [self NoDataFound];
                }
            }
            else{
                [LoadingManager hideLoadingView:self.view];
                [self NoDataFound];
            }
        }
    }
    else{
       [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(FeedbackStackHoderArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataView.hidden = NO;
        NoDataLabel.text = @"No Data Found";
    }
     [FeedbackStackHolderTableView reloadData];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [FeedbackStackHoderArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FeedbackStackholderCell *Cell = (FeedbackStackholderCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.FeedBackBtn.tag = indexPath.row;
    [Cell.FeedBackBtn addTarget:self action:@selector(OpenPopUp:) forControlEvents:UIControlEventTouchUpInside];
    if(FeedbackStackHoderArray.count>0){
        NSDictionary * responseData = [FeedbackStackHoderArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
    }
    return Cell;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
    if(ArraysValue.count>0){
        offset = offset + 50;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                   [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                  [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self FeedbackAsStakeHolderApi];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
         [FeedbackStackHoderArray addObject:BindDataDic];
    }
}
-(void)OpenPopUp:(UIButton*)sender{
    int Tagvalue = sender.tag;
    NSDictionary *Dic = [FeedbackStackHoderArray objectAtIndex:Tagvalue];
    if(IsSafeStringPlus(TrToString(Dic[@"feedback"]))){
         FeedbackLabel.text  = Dic[@"feedback"];
         OuterView.hidden = NO;
    }
    else{
         FeedbackLabel.text  = @" ";
         OuterView.hidden = YES;
    }
}
-(IBAction)OkayBtnAction:(id)sender{
    OuterView.hidden = YES;
}
-(IBAction)FilterAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    GiveFeedbacFilterViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"GiveFeedbacFilterViewController"];
    ObjController.isComeFrom =@"STACKHOLDER";
    [[self navigationController] presentViewController:ObjController animated:YES completion:nil];
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
-(void)clearfilterUserdefaultData
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"STACKHOLDER"];
    [userDefault synchronize];
}
@end
