//
//  OnScheduleViewController.m
//  iWork
//
//  Created by Shailendra on 07/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "OnScheduleViewController.h"
#import "Header.h"

@interface OnScheduleViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *OnSchudleTableView;
    NSMutableArray *OnScheduleArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UIView *NoDataView;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
}
@end

@implementation OnScheduleViewController
@synthesize OnScheduleDics;
- (void)viewDidLoad {
//    [self clearfilterUserdefaultData];
    Api = [[WebApiService alloc] init];
    [self EmptyArray];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    OnSchudleTableView.estimatedRowHeight = 50;
    OnSchudleTableView.rowHeight = UITableViewAutomaticDimension;
    OnSchudleTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, OnSchudleTableView.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [OnSchudleTableView addSubview:refreshControl];
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [OnSchudleTableView registerNib:Nib forCellReuseIdentifier:@"CELL"];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"ONSCHUDLE" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ClearFilterData:) name:@"ClearFilteriPM" object:nil];
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated
{
    if(![CheckValue isEqualToString:@"FILTER"]){
        [self EmptyArray];
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(OnScheduleApi) withObject:nil afterDelay:0.5];
    }
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    OnScheduleArray = [[NSMutableArray alloc] init];
    [self OnScheduleApi];
}
- (void)reloadData
{
    if(![CheckValue isEqualToString:@"FILTER"]){
        CheckValue = @"Refresh";
        [self OnScheduleApi];
    }
    [self EmptyArray];
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    OnScheduleArray = [[NSMutableArray alloc] init];
}
-(void)ClearFilterData:(NSNotification*)notification{
    CheckValue = @"";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(OnScheduleApi) withObject:nil afterDelay:0.5];
}
- (void)OnScheduleApi {
    if(delegate.isInternetConnected){
        
        if(![CheckValue isEqualToString:@"FILTER"]){
            params = @ {
                @"fromDate": @"",
                @"handsetId": @"",
                @"status": @"2",
                @"taskId": @"",
                @"taskType": @"",
                @"toDate": @"",
                @"user_id"  : [OnScheduleDics valueForKey:@"empId"],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        ResponseDic =  [Api WebApi:params Url:@"getEmployeeTaskList"];
        [refreshControl endRefreshing];
        if(ResponseDic==nil){
            [LoadingManager hideLoadingView:self.view];
            [self OnScheduleApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            NSLog(@"OnScheduleResponseDic==%@",ResponseDic);
            [LoadingManager hideLoadingView:self.view];
            NoDataView.hidden = YES;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]];
                }
                else{
                    
                    [self NoRecordDound];
                }
                [OnSchudleTableView reloadData];
                
            }
            else{
                [LoadingManager hideLoadingView:self.view];
                [self NoRecordDound];
            }
            [OnSchudleTableView reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self NoRecordDound];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoRecordDound{
    if(OnScheduleArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Record Found";
    }
    [OnSchudleTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
        NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            if([CheckValue isEqualToString:@"FILTER"]){
                NSMutableDictionary *NewDic = [params mutableCopy];
                for (int i = 0 ; i< [NewDic count];i++){
                    if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                        [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                    }
                    else{
                        [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                    }
                }
                params = [NewDic mutableCopy];
            }
            [self OnScheduleApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *keys in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:keys] forKey:keys];
        }
        [OnScheduleArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [OnScheduleArray count];;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.tag = indexPath.row;
    Cell.ViewDetailsBtn.tag = indexPath.row;
    Cell.RatingView.hidden = YES;
    Cell.RateStaticLabel.hidden = YES;
    Cell.DurationStaticLabel.hidden = NO;
    Cell.DurationLabel.hidden = NO;
    [Cell.ViewDetailsBtn addTarget:self action:@selector(GotoEmployeeDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    if(OnScheduleArray.count>0){
        NSDictionary * responseData = [OnScheduleArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
        Cell.StatusLabel.text = @"In-Progress";
        Cell.StausImageView.image = [UIImage imageNamed:@"WatingForApprove"];
    }
    return Cell;
}
-(void)GotoEmployeeDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    NSString* str = [[OnScheduleArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    if ([[[OnScheduleArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
        TaskDetailManagerScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailManagerScreen"];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
        
    }
    else{
        HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
-(void)clearfilterUserdefaultData
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"OnSchudel"];
    [userDefault synchronize];
}
@end
