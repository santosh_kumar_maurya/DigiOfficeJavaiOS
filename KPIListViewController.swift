//
//  KPIListViewController.swift
//  iWork
//
//  Created by Shailendra on 09/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class KPIListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var KPIListTableView : UITableView!
    var ResponseDic : NSDictionary!
    var ObjAppDelegate = AppDelegate()
    var Api: WebApiService =  WebApiService()
    var KPIArray = NSArray()
    @IBOutlet var NoDataLabel : UILabel!
    @IBOutlet var NoDataView : UIView!
    var remainingWeightage : NSNumber!
    
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        ObjAppDelegate = UIApplication.shared.delegate as! AppDelegate
        self.view.backgroundColor = ObjAppDelegate.backgroudColor
        KPIListTableView.estimatedRowHeight = 200
        KPIListTableView.rowHeight = UITableViewAutomaticDimension;
        self.KPIListTableView.register(UINib(nibName: "ApprovedKPICell", bundle: nil), forCellReuseIdentifier: "ApprovedKPICell")
        LoadingManager.showLoading(self.view)
        self.perform(#selector(KPIListViewController.GetKPIWebApi), with: nil, afterDelay: 0.5)
    }

    func numberOfSections(in tableView: UITableView) -> Int{
        return 1;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return  KPIArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let Cell : ApprovedKPICell = tableView.dequeueReusableCell(
                withIdentifier: "ApprovedKPICell", for: indexPath) as! ApprovedKPICell
            let Dic = KPIArray[indexPath.row] as! NSDictionary
        Cell.tag = indexPath.row
            Cell.ApprovedKPILabel.text = Dic.value(forKey: "kpiDesc") as? String
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapButton(_:)))
        Cell.addGestureRecognizer(tapGesture)
        
            return Cell
    }
    func tapButton(_ sender: UITapGestureRecognizer) {
        let tagValue = sender.view?.tag
        let  KPIDic = KPIArray[tagValue!] as! NSDictionary
        let  kpiId =  KPIDic.value(forKey: "kpiId") as? String
        let  kpiDesc =  KPIDic.value(forKey: "kpiDesc") as? String
        let ObjController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "MyKPIDetailsScreen") as! MyKPIDetailsScreen
        ObjController.kpiId = kpiId
        ObjController.kpiName = kpiDesc
        self.navigationController?.pushViewController(ObjController, animated: true)
    }
    func GetKPIWebApi(){
        if (ObjAppDelegate.isInternetConnected()){
            let userId = UserDefaults.standard.string(forKey: "kUserId")
            ResponseDic = Api.webApi(nil, url:"getKPI?user_id=\(userId!)") as NSDictionary!
            if ResponseDic != nil{
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    LoadingManager.hideLoading(view)
                    let Dic = ResponseDic.value(forKey: "object") as! NSDictionary
                    if (Dic["kpi"] as? NSArray) != nil {
                        KPIArray = Dic.value(forKey: "kpi") as! NSArray
                        if(KPIArray.count == 0){
                            NoDataFound()
                        }
                    }
                    else{
                        NoDataFound()
                    }
                    self.KPIListTableView.reloadData()
                    
                   remainingWeightage = Dic.value(forKey: "remainingWeightage") as! NSNumber
//                   print("remainingWeightage====\(remainingWeightage!)")
//                    RemainDic.setValue("\(remainingWeightage!)", forKey: "REMAIN")
                   let RemainDic = ["REMAIN": remainingWeightage!,] as [String: Any]
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "REMAIN_WEIGHT"), object: nil, userInfo: RemainDic)
                }
                else{
                    LoadingManager.hideLoading(view)
                }
            }
            else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(KPIListViewController.GetKPIWebApi), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func NoDataFound(){
        if(KPIArray.count == 0){
            NoDataView.isHidden = false
            NoDataLabel.isHidden = false
            NoDataLabel.text = "No Record Found"
        }
    }
   func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: false, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}
