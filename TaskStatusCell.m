//
//  TaskStatusCell.m
//  iWork
//
//  Created by Shailendra on 05/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "TaskStatusCell.h"
#import "Header.h"
#import "iWork-Swift.h"
@implementation TaskStatusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    _MyTaskStatusView.layer.cornerRadius = 2.0;
    _MyTaskStatusView.maskView.layer.cornerRadius = 7.0f;
    _MyTaskStatusView.layer.shadowRadius = 3.0f;
    _MyTaskStatusView.layer.shadowColor = [UIColor blackColor].CGColor;
    _MyTaskStatusView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _MyTaskStatusView.layer.shadowOpacity = 0.7f;
    _MyTaskStatusView.layer.masksToBounds = NO;
    
    self.MyTaskView.backgroundColor = [UIColor TopBarYellowColor];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: self.MyTaskView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){2.0}].CGPath;
    self.MyTaskView.layer.mask = maskLayer1;
    
    _CompleteStaticLabel.textColor = [UIColor TextBlueColor];
    _OnScheduleStaticLabel.textColor = [UIColor TextBlueColor];
    _BehindStaticLabel.textColor = [UIColor TextBlueColor];
    _UserNameLabel.textColor = [UIColor TextBlueColor];
    _DesignationLabel.textColor = [UIColor TextBlueColor];
    
    [self ProfileImageShow];
}

-(void)ProfileImageShow{
    
    NSDictionary *Dic = [ApplicationState getUserLoginData];
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]))){
        _UserNameLabel.text = [NSString stringWithFormat:@"%@", [[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]];
    }
    else{
        _UserNameLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"title"]))){
        _DesignationLabel.text = [NSString stringWithFormat:@"%@", [[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"title"]];
    }
    else{
        _DesignationLabel.text = @" ";
    }
    NSString *URL = [NSString stringWithFormat:@"%@",[[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]];
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]))){
        NSURL *Url = [NSURL URLWithString:URL];
        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL:Url]];
        if(images == nil){
            NSLog(@"image nil");
            _ProfileImageView.image = [UIImage imageNamed:@"iPMProfile"];
        }
        else{
            
            [_ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
        }
    }
    else{
        _ProfileImageView.image = [UIImage imageNamed:@"iPMProfile"];
    }
    _ProfileImageView.clipsToBounds = YES;
    _ProfileImageView.layer.borderColor =delegate.redColor.CGColor;
    _ProfileImageView.layer.borderWidth = 1.5;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
