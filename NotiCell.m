//
//  NotiCell.m
//  iWork
//
//  Created by Shailendra on 21/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "NotiCell.h"
#import "Header.h"
@implementation NotiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.layer.cornerRadius =6;
//    self.layer.masksToBounds =YES;
//    
//    self.layer.cornerRadius = 7.0f;
//    self.layer.shadowRadius = 3.0f;
//    self.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
//    self.layer.shadowOpacity = 0.7f;
//    self.layer.masksToBounds = NO;
    
}

- (void)configureCell:(NSDictionary *)info{

    if ([[info objectForKey:@"type"] isEqualToString:@"Assigned"]||[[info objectForKey:@"type"] isEqualToString:@"APPROVE"]){
        _StatusImageView.image = [UIImage imageNamed:@"assigned"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"EMP_NEWCOMMENT"]){
        _StatusImageView.image = [UIImage imageNamed:@"comment"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"FAIL"]){
        _StatusImageView.image = [UIImage imageNamed:@"cancelled_new"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"LM_NEWCOMMENT"]){
        _StatusImageView.image = [UIImage imageNamed:@"comment"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"SUSPEND"]){
        _StatusImageView.image = [UIImage imageNamed:@"cancelled_new"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"COMPLETE"]){
        _StatusImageView.image = [UIImage imageNamed:@"submitted_new"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"START"]){
        _StatusImageView.image = [UIImage imageNamed:@"inprogressblue"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"NEWTASK"]){
        _StatusImageView.image = [UIImage imageNamed:@"taskrequest"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"CANCEL"]){
        _StatusImageView.image = [UIImage imageNamed:@"CancelledStatus"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"REJECT"]){
        _StatusImageView.image = [UIImage imageNamed:@"declined_new"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"CLOSE"]){
        _StatusImageView.image = [UIImage imageNamed:@"confirmed"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"NEED REVISION"]||[[info objectForKey:@"type"] isEqualToString:@"COACHING"]){
        _StatusImageView.image = [UIImage imageNamed:@"NeedRevision"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"NEW_KPI"]){
        _StatusImageView.image = [UIImage imageNamed:@"taskrequest"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"KPI_APPROVED"]){
        _StatusImageView.image = [UIImage imageNamed:@"Approved"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"KPI_REJECTED"]){
        _StatusImageView.image = [UIImage imageNamed:@"declined_new"];
    }
    else if ([[info objectForKey:@"type"] isEqualToString:@"S_CLOSE"]
             || [[info objectForKey:@"type"] isEqualToString:@"STACKFEED"] || [[info objectForKey:@"type"] isEqualToString:@"LM_STACKFEED"]){
        _StatusImageView.image = [UIImage imageNamed:@"review"];
    }
    if(IsSafeStringPlus(TrToString(info[@"notification"]))){
        _NotificationLabel.text  = info[@"notification"];
    }
    else{
        _NotificationLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"creationDate"]))){
        NSNumber *datenumber = info[@"creationDate"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _DateLabel.text = @" ";
    }
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm aa"];
    return [dateformate stringFromDate:Newdate];
}
@end
