//
//  KPIApprovalHistoryCell.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class KPIApprovalHistoryCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {

   @IBOutlet var btnViewMoreOrLess : UIButton!
   @IBOutlet var TitleLabel : UILabel!
   @IBOutlet var CreateDateLabel : UILabel!
   @IBOutlet var ToStaticLabel : UILabel!
   @IBOutlet var ToValueLabel : UILabel!
   @IBOutlet var KPIStaticLabel : UILabel!
   @IBOutlet var KPIValueLabel : UILabel!
   @IBOutlet var TargatStaticLabel : UILabel!
   @IBOutlet var WeightageStaticLabel : UILabel!
   @IBOutlet var WeightageValueLabel : UILabel!
   @IBOutlet var SubmittedbyStaticLabel : UILabel!
   @IBOutlet var SubmittedbyValueLabel : UILabel!
   @IBOutlet var StatusValueLabel : UILabel!
   @IBOutlet var StatusImageView : UIImageView!
   @IBOutlet var RejectionRemark : UILabel!
   @IBOutlet var Hight : NSLayoutConstraint!
   @IBOutlet var HightRejectionRemark : NSLayoutConstraint!
    
    var GateStaticArray = NSMutableArray()    
    var GateValueArray = NSArray()
   
    @IBOutlet var TargetView : UIView!
    @IBOutlet var TargetTableView : UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        TargetTableView.estimatedRowHeight = 150;
        TargetTableView.backgroundColor = UIColor.white
        TargetTableView.rowHeight = UITableViewAutomaticDimension;
        TargetTableView.layoutIfNeeded()
    }
    func CellConfigure(info:NSDictionary){
        if (info["title"] as? String) != nil{
            TitleLabel.text = "\(String(describing: info["title"]!))"
        }
        else{
            TitleLabel.text = " "
        }
        if (info["createdDate"] as? NSNumber) != nil{
            let Number = info["createdDate"] as! NSNumber
            CreateDateLabel.text = GetDate(datenumber: Number)
        }
        else{
            CreateDateLabel.text = " "
        }
        if (info["to"] as? String) != nil{
            ToValueLabel.text = "\(String(describing: info["to"]!))"
        }
        else{
            ToValueLabel.text = " "
        }
        if (info["kpi"] as? String) != nil{
            KPIValueLabel.text = "\(String(describing: info["kpi"]!))"
        }
        else{
            KPIValueLabel.text = " "
        }
        
        if (info["weightage"] as? NSNumber) != nil{
            WeightageValueLabel.text = "\(String(describing: info["weightage"]!))"
        }
        else{
            WeightageValueLabel.text = " "
        }
        
        if (info["submittedBy"] as? String) != nil{
            SubmittedbyValueLabel.text = "\(String(describing: info["submittedBy"]!))"
        }
        else{
            SubmittedbyValueLabel.text = " "
        }
        if (info["rejectionRemark"] as? String) != nil{
            RejectionRemark.text = "\(String(describing: info["rejectionRemark"]!))"
        }
        else{
            RejectionRemark.text = " "
        }
        if( info["status"] as? String  == "Approved"){
            StatusValueLabel.text = "\(String(describing: info["status"]!))"
            StatusImageView.image = UIImage(named: "Completes");
        }
        else if(info["status"] as? String  == "Rejected"){
            StatusValueLabel.text = "\(String(describing: info["status"]!))"
            StatusImageView.image = UIImage(named: "StatusRejected");
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return GateValueArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let Cell : KPITargetCell = tableView.dequeueReusableCell(
            withIdentifier: "CELL", for: indexPath) as! KPITargetCell
        if(GateValueArray.count > 0){
            Cell.GateStaticLabel.text = "\(GateStaticArray[indexPath.row])"
            Cell.GateValueLabel.text = "\(String(describing: GateValueArray[indexPath.row]))"
        }
        return Cell
    }
    func GetDate(datenumber:NSNumber) -> String{
        
        let DateStr = NSString(format: "%@", datenumber)
        let miliSec = Double(DateStr as String)
        let TakeOffDate = NSDate(timeIntervalSince1970: miliSec!/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm aa"
        let dateStr = dateFormatter.string(from: TakeOffDate as Date)
        let Newdate = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = "dd MMM YYYY hh:mm aa"
        return dateFormatter.string(from: Newdate!)
        
    }
}
