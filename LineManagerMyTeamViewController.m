//
//  LineManagerMyTeamViewController.m
//  iWork
//
//  Created by Shailendra on 31/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LineManagerMyTeamViewController.h"
#import "Header.h"
#import "iWork-Swift.h"

@interface LineManagerMyTeamViewController ()<UITableViewDelegate,UITextViewDelegate,SelectTeamMember,RainbowColorSource>{
    IBOutlet UITableView * MyTeamTableView;
    NSMutableArray *TeamArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    int IndexNumber;
    WebApiService *Api;
    NSDictionary *MyTaskDic;
    NSDictionary *ResponseDic;
    NSString *KPICascading;
    
    RainbowNavigation *rainbowNavigation;
    UINavigationBar* navbar;
    UILabel *notificationLabel;
}

@end

@implementation LineManagerMyTeamViewController

- (void)viewDidLoad {

    //[self configureNavigationBar];
    KPICascading = [ApplicationState GetKPICascading];
    CheckValue = @"MYTEAM";
    Api = [[WebApiService alloc] init];
    IndexNumber = 2;
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor clearColor];
    MyTeamTableView.estimatedRowHeight = 200;
    MyTeamTableView.backgroundColor = [UIColor clearColor];
    MyTeamTableView.rowHeight = UITableViewAutomaticDimension;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    //[MyTeamTableView addSubview:refreshControl];
    MyTeamTableView.bounces = NO;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"MYTEAM" object:nil];
    [super viewDidLoad];
    
}
-(void)configureNavigationBar{
    navbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 44)];
    [navbar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    navbar.shadowImage = [UIImage new];
    navbar.translucent = YES;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 20.0f, self.view.frame.size.width, navbar.frame.size.height)];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(-5.0f, 17.0f, 15.0f, 15.0f)];
    [backButton addTarget:self action:@selector(BackAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"BackBtn"] forState:UIControlStateNormal];
    backButton.imageView.contentMode = UIViewContentModeCenter;
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 100.0f, 30.0f)];
    [title setFont:[UIFont boldSystemFontOfSize:17]];
    
    title.text = NSLocalizedString(@"IPM", nil);
    title.textColor = [UIColor whiteColor];
    
    UITapGestureRecognizer* titleGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(BackAction:)];
    [title  setUserInteractionEnabled:YES];
    [title addGestureRecognizer:titleGesture];
    
    [customView addSubview:backButton];
    [customView addSubview:title];
    
    UIButton *notificationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [notificationButton setFrame:CGRectMake(self.view.frame.size.width - 52, 10.0f, 20.0f, 20.0f)];
    [notificationButton addTarget:self action:@selector(notificationTapped:) forControlEvents:UIControlEventTouchUpInside];
    [notificationButton setImage:[UIImage imageNamed:@"new_ic_notification"] forState:UIControlStateNormal];
    notificationButton.imageView.contentMode = UIViewContentModeCenter;
    
    notificationLabel = [[UILabel alloc] initWithFrame:CGRectMake(notificationButton.frame.origin.x + 8, 5.0f, 20.0f, 20.0f)];
    notificationLabel.text = @"10";
    notificationLabel.layer.cornerRadius = 10;
    notificationLabel.clipsToBounds = YES;
    notificationLabel.backgroundColor = delegate.yellowColor;
    notificationLabel.font = [UIFont systemFontOfSize:10];
    notificationLabel.textAlignment = NSTextAlignmentCenter;
    notificationLabel.textColor = [UIColor whiteColor];
    
    UIButton *HeaderProfileButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [HeaderProfileButton setFrame:CGRectMake(self.view.frame.size.width - 87, 10.0f, 20.0f, 20.0f)];
    [HeaderProfileButton addTarget:self action:@selector(openProfile) forControlEvents:UIControlEventTouchUpInside];
    [HeaderProfileButton setImage:[UIImage imageNamed:@"New_Profile"] forState:UIControlStateNormal];
    HeaderProfileButton.imageView.contentMode = UIViewContentModeCenter;
    
    UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [homeButton setFrame:CGRectMake(self.view.frame.size.width - 120, 10.0f, 20.0f, 20.0f)];
    [homeButton addTarget:self action:@selector(BackAction:) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setImage:[UIImage imageNamed:@"iPMhome"] forState:UIControlStateNormal];
    homeButton.imageView.contentMode = UIViewContentModeCenter;
    
    [customView addSubview:homeButton];
    [customView addSubview:HeaderProfileButton];
    [customView addSubview:notificationButton];
    [customView addSubview:notificationLabel];
    
    UINavigationItem* navItem = [[UINavigationItem alloc] init];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:customView];
    
    navItem.leftBarButtonItem = leftButton;
    [navbar setItems:@[navItem]];
    
    [self.view addSubview:navbar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateNotifications) name:@"Notification" object:nil];
    
    rainbowNavigation = [[RainbowNavigation alloc] init];
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIImageView *imageView =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.width * 0.65)];
    imageView.image=[UIImage imageNamed:@"iPMDashboardBanner"];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    MyTeamTableView.tableHeaderView = imageView;
    
}
- (IBAction)notificationTapped:(id)sender {
    [delegate DeleteNotificationCounter:@"IPM"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NotificationScreen *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"NotificationScreen"];
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(void)openProfile{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    ObjViewController.isComeFrom = @"IPM";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)UpdateNotifications{
    notificationLabel.alpha=0.0;
    notificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IPM"]];
    if ([notificationLabel.text intValue]>0) {
        notificationLabel.alpha=1.0;
        notificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IPM"]];
    }
}
-(void)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self UpdateNotifications];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TeamWebApi) withObject:nil afterDelay:0.1];
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    TeamArray = [[NSMutableArray alloc] init];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TeamWebApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    TeamArray = [[NSMutableArray alloc] init];
    [self TeamWebApi];
}
- (void)TeamWebApi {
    
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"MYTEAM"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"empId" : @"",
                    @"empName" : @"",
                    @"handset_id" :@"",
                    @"user_id" :[ApplicationState userId]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"getTeamDashData"];
        }
        NSLog(@"ResponseDic===%@",ResponseDic);
        [refreshControl endRefreshing];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self TeamWebApi];
        }
        else if([[ResponseDic objectForKey:@"statusCode"]intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            MyTaskDic = ResponseDic;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"empTaskDetails"]))){
               TeamArray = [[ResponseDic valueForKey:@"object"] valueForKey:@"empTaskDetails"];
            }
            [MyTeamTableView reloadData];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([KPICascading isEqualToString:@"1"]){
        if([TeamArray count]>0){
            return 6;
        }
        return 5;
    }
    else{
        if([TeamArray count]>0){
            return 5;
        }
        return  4;
    }
    return  0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([KPICascading isEqualToString:@"1"]){
        if(indexPath.row == 0){
            BannerCell *Cell = (BannerCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
            return Cell;
        }
        else if(indexPath.row == 1){
            TaskStatusCell *Cell = (TaskStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL4" forIndexPath:indexPath];
            UITapGestureRecognizer* Gestures = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoProfilePage)];
            [Cell.ProfileImageView  setUserInteractionEnabled:YES];
            [Cell.ProfileImageView addGestureRecognizer:Gestures];
            return Cell;
        }
        else if(indexPath.row == 2){
            AssignmentCell *Cell = (AssignmentCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            return Cell;
        }
        else if(indexPath.row == 3){
            MyTaskCell *Cell = (MyTaskCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            if(IsSafeStringPlus(TrToString([MyTaskDic[@"object"] valueForKey:@"taskCreationCount"]))){
                Cell.TaskCreationLabel.text = [NSString stringWithFormat:@"%@", [MyTaskDic[@"object"] valueForKey:@"taskCreationCount"]];
            }
            else{
                Cell.TaskCreationLabel.text = @"0";
            }
            if(IsSafeStringPlus(TrToString([MyTaskDic[@"object"] valueForKey:@"taskCompletionCount"]))){
                Cell.TaskCompletionLabel.text = [NSString stringWithFormat:@"%@", [MyTaskDic[@"object"] valueForKey:@"taskCompletionCount"]];
            }
            else{
                Cell.TaskCompletionLabel.text = @"0";
            }
            return Cell;
        }
        else if(indexPath.row == 4){
            TeamKPICell *Cell = (TeamKPICell *)[tableView dequeueReusableCellWithIdentifier:@"CELL5" forIndexPath:indexPath];
            return Cell;
        }
        else{
            TeamCell *Cell = (TeamCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2"];
            Cell = nil;
            if(Cell == nil){
                Cell = (TeamCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2"];
            }
            Cell.TeamMemberArray = TeamArray;
            Cell.delegate = self;
            [Cell configureCell:TeamArray];
            [Cell.MyTeamTableView reloadData];
            Cell.hight.constant = 60 * TeamArray.count - 5;
            Cell.MyTeamTableView.scrollEnabled = NO;
            return Cell;
        }
    }
    else{
        if(indexPath.row == 0){
            BannerCell *Cell = (BannerCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
            return Cell;
        }
        else if(indexPath.row == 1){
            TaskStatusCell *Cell = (TaskStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL4" forIndexPath:indexPath];
            return Cell;
        }
        else if(indexPath.row == 2){
            AssignmentCell *Cell = (AssignmentCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            return Cell;
        }
        else if(indexPath.row == 3){
            MyTaskCell *Cell = (MyTaskCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            if(IsSafeStringPlus(TrToString([MyTaskDic[@"object"] valueForKey:@"taskCreationCount"]))){
                Cell.TaskCreationLabel.text = [NSString stringWithFormat:@"%@", [MyTaskDic[@"object"] valueForKey:@"taskCreationCount"]];
            }
            else{
                Cell.TaskCreationLabel.text = @"0";
            }
            if(IsSafeStringPlus(TrToString([MyTaskDic[@"object"] valueForKey:@"taskCompletionCount"]))){
                Cell.TaskCompletionLabel.text = [NSString stringWithFormat:@"%@", [MyTaskDic[@"object"] valueForKey:@"taskCompletionCount"]];
            }
            else{
                Cell.TaskCompletionLabel.text = @"0";
            }
            return Cell;
        }
        else{
            TeamCell *Cell = (TeamCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2"];
            Cell = nil;
            if(Cell == nil){
                Cell = (TeamCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2"];
            }
            Cell.TeamMemberArray = TeamArray;
            Cell.delegate = self;
            [Cell configureCell:TeamArray];
            [Cell.MyTeamTableView reloadData];
            Cell.hight.constant = 60 * TeamArray.count - 30;
            return Cell;
        }
    }
}
-(IBAction)GotoProfilePage{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    ObjViewController.isComeFrom = @"PMS";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)ViewAllTask:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskRequestScreen *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"TaskRequestScreen"];
    ObjController.isNewReq = TRUE;
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(void)SelectTeamMemberIndex:(NSDictionary *)Dic{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EMPLOYEEINFO" object:nil userInfo:Dic];
    UIStoryboard *Objstoryboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TeamMemberViewController *ObjController = [Objstoryboard instantiateViewControllerWithIdentifier:@"TeamMemberViewController"];
    ObjController.EmployeeDic = Dic;
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(IBAction)GoToAssignment:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    CreateTaskScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateTaskScreen"];
    ObjViewController.isAssignTask = YES;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)GoToAskForProgress:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    AskForProgressViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"AskForProgressViewController"];
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(IBAction)GotoTaskCreationAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskRequestScreen *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"TaskRequestScreen"];
    ObjController.isNewReq = TRUE;
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(IBAction)GotoTaskCompletionAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskRequestScreen *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"TaskRequestScreen"];
    ObjController.isNewReq = FALSE;
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(IBAction)GotoFilterControllerAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    GiveFeedbacFilterViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"GiveFeedbacFilterViewController"];
    ObjController.isComeFrom =@"MYTEAM";
    [[self navigationController] presentViewController:ObjController animated:YES completion:nil];
}
-(IBAction)GotoApprovalTaskControllerAction:(id)sender{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    KPIApprovalParentViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"KPIApprovalParentViewController"];
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(IBAction)GotoHistoryControllerAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    KPIApprovalParentViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"KPIApprovalParentViewController"];
    ObjController.isComeFrom = @"HISTORY";
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(IBAction)GotoAllTeamKpiControllerAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    KPIApprovalParentViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"KPIApprovalParentViewController"];
    [[self navigationController] pushViewController:ObjController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
