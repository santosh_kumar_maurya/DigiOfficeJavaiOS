//
//  TaskRequestCell.h
//  iWork
//
//  Created by Shailendra on 17/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskRequestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *TaskView;
@property (weak, nonatomic) IBOutlet UILabel *TaskIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *DateLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *DurationStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *DurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *CreatedbyStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *CreatedbyLabel;
@property (weak, nonatomic) IBOutlet UIButton *RejectBtn;
@property (weak, nonatomic) IBOutlet UIButton *ApproveBtn;
@property (weak, nonatomic) IBOutlet UIButton *NeedRevisionBtn;
@property (weak, nonatomic) IBOutlet UIButton *CompleteApproveBtn;
@property (weak, nonatomic) IBOutlet UIButton *ApproveRejectViewDetailsBtn;
@property (weak, nonatomic) IBOutlet UIButton *CompleteViewDetailsBtn;
@property (weak, nonatomic) IBOutlet UIImageView *ApproveImageView;

- (void)configureCell:(NSDictionary *)info;
@end
