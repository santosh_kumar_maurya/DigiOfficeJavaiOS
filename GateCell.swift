//
//  GateCell.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class GateCell: UITableViewCell {

    //MARK: - Properties
    @IBOutlet weak var gateLabel: UILabel!
    @IBOutlet weak var txtGate: UITextField!
    //end
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.updateUI()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateUI()
    {
        let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
        txtGate.leftView = paddingView
        txtGate.leftViewMode = UITextFieldViewMode.always
    }
    
    func configureCellWithData(index: Int)
    {
        let placeHolder = "Gate " + index.description + " "
        let text = placeHolder + "*"
        let range = (text as NSString).range(of: "*")
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: self.txtGate.textColor , range: range)
        self.txtGate.attributedPlaceholder = attributedString
    }
}
