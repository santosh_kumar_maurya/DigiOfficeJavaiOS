//
//  GiveRecognitionsViewController.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit


class GiveRecognitionsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var GiveRecognitionsTableView: UITableView!
    @IBOutlet weak var NodataLabel: UILabel!
    @IBOutlet weak var NodataView: UIView!
    var delegate = AppDelegate()
    var ResponseDic : NSDictionary!
    var refreshControl = UIRefreshControl()
    var Api: WebApiService =  WebApiService()
    var EmployeeID : String! = ""
    var taskID : String! = ""
    var ResponseArray = NSArray()
    var RatingValue : String! = ""
    var Tagvalue : Int = 0
    
    var attributedString : NSMutableAttributedString!
    var CommingSoon : String = ""
    
  
    @IBOutlet var ConfrimationView :UIView!
    @IBOutlet var ConfrimationTitleLabel :UILabel!
    @IBOutlet var YesButton :UIButton!
    var viewController = HistoryDetailScreen()
    let RecognitionEnable = UserDefaults.standard.string(forKey: "kRecognitionEnable")

    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        IQKeyboardManager.shared().isEnabled = true
        delegate = UIApplication.shared.delegate as! AppDelegate
        GiveRecognitionsTableView.estimatedRowHeight = 50;
        GiveRecognitionsTableView.backgroundColor = UIColor.clear;
        GiveRecognitionsTableView.rowHeight = UITableViewAutomaticDimension;
        GiveRecognitionsTableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: GiveRecognitionsTableView.bounds.size.width, height: 10.0))
        refreshControl.addTarget(self, action: #selector(GiveRecognitionsViewController.reloadData), for: UIControlEvents.valueChanged)
        GiveRecognitionsTableView.addSubview(refreshControl)
        self.perform(#selector(GiveRecognitionsViewController.GiveRecognitionsWebApi), with: nil, afterDelay: 0.5)
        ConfrimationTitleLabel.textColor = UIColor.TextBlueColor()
        YesButton.setTitleColor(UIColor.ApproveGreenColor(), for: UIControlState.normal)
        
    }
    func reloadData(){
        self.perform(#selector(GiveRecognitionsViewController.GiveRecognitionsWebApi), with: nil, afterDelay: 0.5)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ResponseArray.count;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell : GiveRecognitionsCell = tableView.dequeueReusableCell(
                withIdentifier: "CELL", for: indexPath) as! GiveRecognitionsCell
        Cell.DetailsBtn.tag = indexPath.section
        Cell.GiveRecognitionsBtn.tag = indexPath.row
        if(ResponseArray.count>0){
            let Dic = ResponseArray[indexPath.row] as! NSDictionary
            Cell.ConfigurationCell(info: Dic)
            RatingValue = Dic.value(forKey: "rating") as! String
            if(RatingValue != nil || RatingValue != ""){
                let FloatValue = NSNumber(value: Float(RatingValue)!)
                Cell.RatingView.value = FloatValue as! CGFloat
                Cell.RatingView.tintColor = UIColor.NeedRevisionBtnColor()
            }
            else{
                Cell.RatingView.value = 0
            }
        }
        return Cell
        
    }
    func GiveRecognitionsWebApi(){
        if (delegate.isInternetConnected()){
            let userId = UserDefaults.standard.string(forKey: "kUserId")
            ResponseDic = Api.webApi(nil, url:"recognitionTaskList?userId=\(userId!)&employeeId=\(EmployeeID!)") as NSDictionary!
            refreshControl.endRefreshing()
            NodataView.isHidden = true
            if ResponseDic != nil{
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    LoadingManager.hideLoading(view)
                    let Dic = ResponseDic.value(forKey: "object") as! NSDictionary
                    if (Dic["recognitionTask"] as? NSArray) != nil{
                       ResponseArray = Dic.value(forKey: "recognitionTask") as! NSArray
                    }
                    if(ResponseArray.count == 0){
                        NoDataFound()
                    }
                    self.GiveRecognitionsTableView.reloadData()
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: ResponseDic.value(forKey: "message")!))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }
            else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(GiveRecognitionsViewController.GiveRecognitionsWebApi), with: nil, afterDelay: 0.2)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func NoDataFound(){
        if(ResponseArray.count == 0){
            NodataView.isHidden = false
            NodataLabel.isHidden = false
            NodataLabel.text = "No Record Found"
        }
    }
    @IBAction func GotoDetailsPageAction(sender:UIButton){
        Tagvalue = sender.tag;
        let Dic = ResponseArray[Tagvalue] as! NSDictionary
        taskID = Dic.value(forKey: "taskId") as! String
        viewController.taskID = taskID
        viewController.isManager = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func GiveRecognitions(sender:UIButton){
        ConfrimationView.isHidden = false
    }

    func open(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(scheme): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
            }
        }
    }
    @IBAction func ConfrimationCancelOKAction(){
        ConfrimationView.isHidden = true
    }
    @IBAction func ConfrimationYesAction(){
        ConfrimationView.isHidden = true
        if(Int(RecognitionEnable!) == 0){
            CommingSoon = "COMMINGSOON"
            let normalText = "Comming soon...\n Please give recognition using iConnect."
            attributedString = NSMutableAttributedString(string:normalText)
            self.ShowAlert(MsgTitle: "", BtnTitle:"Close")
        }
        else{
            let Dic = ResponseArray[Tagvalue] as! NSDictionary
            taskID = Dic.value(forKey: "taskId") as! String
            EmployeeID = Dic.value(forKey: "assignedEmployeeId") as! String
            let userId = UserDefaults.standard.string(forKey: "kUserId")
            let Token = UserDefaults.standard.string(forKey: "kToken")
            let RegcognitionsUrl = "iconnect://?token=\(Token!)&user_key=\(userId!)&givenTo=\(EmployeeID!)&taskId=\(taskID!)"
            open(scheme: RegcognitionsUrl)
        }
    }

    @IBAction func BackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func HomeAction(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContainerParentViewController") as! ContainerParentViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        if(CommingSoon == "COMMINGSOON"){
            ObjAlertController.isCome = "KPI"
            ObjAlertController.attributedString = attributedString
        }
        else{
            ObjAlertController.messageTitleStr = MsgTitle as String!
        }
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: false, completion: nil)
    }
}
