//
//  FeedbackListViewController.swift
//  iWork
//
//  Created by Shailendra on 16/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class FeedbackListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var FeedBackListTableView: UITableView!
    var Api: WebApiService =  WebApiService()
    var refreshControl = UIRefreshControl()
    var delegate = AppDelegate()
    @IBOutlet var NoDataView : UIView!
    @IBOutlet var NoDataLabel : UILabel!
    var ResponseDic = NSDictionary()
    var FeedbackArray  = NSArray()
    var param : NSDictionary!
    var offset = 0
    var limit = 50
    var Dic = NSDictionary()
    var CheckValue = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearUserDefaultFilterData()
        delegate = UIApplication.shared.delegate as! AppDelegate
        FeedBackListTableView.estimatedRowHeight = 150;
        FeedBackListTableView.backgroundColor = UIColor.white
        FeedBackListTableView.rowHeight = UITableViewAutomaticDimension;
        refreshControl.addTarget(self, action: #selector(FeedbackListViewController.reloadData), for: UIControlEvents.valueChanged)
        FeedBackListTableView.addSubview(refreshControl)
        NotificationCenter.default.addObserver(self, selector: #selector(notificationData(_:)), name: Notification.Name("GIVEFEEDBACK"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(clearFilterData(_:)), name: Notification.Name("ClearFilteriPM"), object: nil)
        LoadingManager.showLoading(self.view)
        self.perform(#selector(FeedbackListViewController.GetFeedbackList), with: nil, afterDelay: 0.5)
    }
    override func viewWillAppear(_ animated: Bool) {
//        LoadingManager.showLoading(self.view)
//        self.perform(#selector(FeedbackListViewController.GetFeedbackList), with: nil, afterDelay: 0.5)
    }
    func notificationData(_ notification: Notification) {
        param = notification.userInfo as NSDictionary!
        CheckValue = "FILTER"
        EmptyArray()
        LoadingManager.showLoading(view)
        self.perform(#selector(GetFeedbackList), with: nil, afterDelay: 0.5)
    }
    func clearFilterData(_ notification: Notification)
    {
        CheckValue = ""
        EmptyArray()
        LoadingManager.showLoading(view)
        self.perform(#selector(GetFeedbackList), with: nil, afterDelay: 0.5)
    }

    func reloadData(){
        EmptyArray()
        self.perform(#selector(FeedbackListViewController.GetFeedbackList), with: nil, afterDelay: 0.5)
    }
    func EmptyArray(){
        if (CheckValue != "FILTER")
        {
            FeedbackArray  = NSArray()
            offset = 0
            limit = 50
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
       return FeedbackArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell : FeedbackCell = tableView.dequeueReusableCell(
            withIdentifier: "CELL", for: indexPath) as! FeedbackCell
        Cell.GiveFeedbackButton.tag = indexPath.row
        if(FeedbackArray.count > 0){
            Dic = FeedbackArray[indexPath.row] as! NSDictionary
            Cell.CellConfigure(info: Dic)
        }
        return Cell
    }
    @IBAction func GiveFeedbackAction(sender:UIButton){
       let TagValue = sender.tag;
       Dic = FeedbackArray[TagValue] as! NSDictionary
       let viewController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "GiveFeedbackViewController") as! GiveFeedbackViewController
        viewController.TaskID = "\(String(describing: Dic.value(forKey: "taskId")!))"
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    @IBAction func HomeAction(sender:UIButton){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContainerParentViewController") as! ContainerParentViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func BackAction(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func FilterAction(sender:UIButton){
        let viewController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "GiveFeedbacFilterViewController") as! GiveFeedbacFilterViewController
        viewController.isComeFrom = "GIVEFEEDBACK"
        self.navigationController?.present(viewController, animated: true, completion: nil)
    }
    
    func  GetFeedbackList()
    {
        if (delegate.isInternetConnected())
        {
            if (CheckValue != "FILTER")
            {
                param  = ["offset": NSNumber(value: offset),"limit": NSNumber(value: limit)]
            }
            print(param);
            let userId = UserDefaults.standard.string(forKey: "kUserId")
//            param  = ["offset": NSNumber(value: offset),"limit": NSNumber(value: limit)]
            ResponseDic = Api.webApi(param as! [AnyHashable : Any]!, url: "giveFeedbackTask?employeeId=\(userId!)") as NSDictionary!
            refreshControl.endRefreshing()
            if ResponseDic != nil {
                LoadingManager.hideLoading(view)
                NoDataView.isHidden = true
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    FeedbackArray = ResponseDic.value(forKey: "object") as! NSArray
                    if(FeedbackArray.count == 0){
                         NoDataFound()
                    }
                    self.FeedBackListTableView.reloadData()
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: ResponseDic.value(forKey: "message")))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }
            else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(TeamMemberViewController.GetAverageFeedback), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func NoDataFound(){
        if(FeedbackArray.count==0){
            NoDataView.isHidden = false
            NoDataLabel.isHidden = false
            NoDataLabel.text = "No Record found"
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: true, completion: nil)
    }
    func clearUserDefaultFilterData()
    {
        UserDefaults.standard.removeObject(forKey: "GIVEFEEDBACK")
    }
}
