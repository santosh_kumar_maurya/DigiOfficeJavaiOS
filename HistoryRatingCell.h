//
//  HistoryRatingCell.h
//  iWork
//
//  Created by Shailendra on 23/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryRatingCell : UITableViewCell

@property(nonatomic,retain)IBOutlet UIButton *LowestButton;
@property(nonatomic,retain)IBOutlet UIButton *HighestButton;
@property(nonatomic,retain)IBOutlet UIButton *AllButton;
@property(nonatomic,retain)IBOutlet UIImageView *LowestImageView;
@property(nonatomic,retain)IBOutlet UIImageView *HighestImageView;
@property(nonatomic,retain)IBOutlet UIImageView *AllImageView;
@end
