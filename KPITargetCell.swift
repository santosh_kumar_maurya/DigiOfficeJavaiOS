//
//  KPITargetCell.swift
//  iWork
//
//  Created by Shailendra on 22/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class KPITargetCell: UITableViewCell {

    @IBOutlet var GateStaticLabel : UILabel!
    @IBOutlet var GateValueLabel : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
