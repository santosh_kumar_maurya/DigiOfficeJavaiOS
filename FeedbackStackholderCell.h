//
//  FeedbackStackholderCell.h
//  iWork
//
//  Created by Shailendra on 01/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackStackholderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *TaskView;
@property (weak, nonatomic) IBOutlet UILabel *TaskIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *DateLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *EmployeeNameStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *EmployeeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *StakeHolderStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *StakeHolderLabel;
@property (weak, nonatomic) IBOutlet UILabel *EffectiveStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *EffectiveLabel;
@property (weak, nonatomic) IBOutlet UILabel *LackingStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *LackingLabel;
@property (weak, nonatomic) IBOutlet UILabel *FeedBackStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *FeedBackLabel;
@property (weak, nonatomic) IBOutlet UILabel *FeedBackDateStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *FeedBackDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *FeedBackBtn;

- (void)configureCell:(NSDictionary *)info;


@end
