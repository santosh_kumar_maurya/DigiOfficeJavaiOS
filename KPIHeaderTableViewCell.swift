//
//  KPIHeaderTableViewCell.swift
//  iWork
//
//  Created by mac book pro on 21/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class KPIHeaderTableViewCell: UITableViewCell {

    //MARK: - Properties
    @IBOutlet weak var headerLabel: UILabel!
    // end
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
