//
//  AssignmentCell.h
//  iWork
//
//  Created by Shailendra on 04/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignmentCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UIView *AssignmentView;
@property(nonatomic,strong)IBOutlet UIView *AskProgressView;
@property(nonatomic,strong)IBOutlet UILabel *AssignmentLabel;
@property(nonatomic,strong)IBOutlet UILabel *AskProgressLabel;
@end
