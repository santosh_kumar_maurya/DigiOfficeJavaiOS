//
//  SubTaskCell.swift
//  iWork
//
//  Created by Shailendra on 12/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class SubTaskCell: UITableViewCell {

    @IBOutlet var SubTaskTextField : UITextField!
    @IBOutlet var DeleteButton : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
