//
//  MyKpiCell.h
//  iWork
//
//  Created by Shailendra on 05/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyKpiCell : UITableViewCell<UIScrollViewDelegate>

@property(nonatomic,strong)IBOutlet UIView *TopView;
@property(nonatomic,strong)IBOutlet UIView *BgView;
@property(nonatomic,strong)IBOutlet UILabel *MyKPIStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *KPIStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *KPILabel;
@property(nonatomic,strong)IBOutlet UILabel *MyFeedbackStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *FeedbackStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *FeedbackLabel;
@property(nonatomic,strong)IBOutlet UILabel *FeedbackAsStackHolderStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *FeedbackAsStackStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *FeedbackAsStackLabel;

@end
