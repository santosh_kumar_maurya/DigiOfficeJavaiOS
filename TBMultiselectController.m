//
//  MultiSelectViewController.m
//  MultiSelectUIXibVersion
//`
//  Created by Terry Bu on 10/28/14.
//  Copyright (c) 2014 TurnToTech. All rights reserved.
//

#import "TBMultiselectController.h"
#import "AlertController.h"
#import "AppContainerViewController.h"
@interface TBMultiselectController ()

@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation TBMultiselectController
//@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0.94 green:0.97 blue:1.00 alpha:1.0];
    self.navigationController.navigationBarHidden=TRUE;
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    savedArrayOfEmployees = [[NSMutableArray alloc]init];
    StatckHolderID = [[NSMutableArray alloc] init];
//    NSString* name = @"Mayank123";
//    Contact *data = [self createRecord:name];
//    [savedArrayOfEmployees addObject:data];
//    data = [self createRecord:@"SUDESH123"];
//    [savedArrayOfEmployees addObject:data];
//    data = [self createRecord:@"LINEMANAGERDEMO"];
//    [savedArrayOfEmployees addObject:data];
//    data = [self createRecord:@"DALJEET123"];
//    [savedArrayOfEmployees addObject:data];
//    data = [self createRecord:@"Praveen123"];
//    [savedArrayOfEmployees addObject:data];
//    data = [self createRecord:@"Saransh123"];
//    [savedArrayOfEmployees addObject:data];
//    data = [self createRecord:@"Mohit123"];
//    [savedArrayOfEmployees addObject:data];
//    data = [self createRecord:@"LINEMANAGERDEMO5"];
//    [savedArrayOfEmployees addObject:data];
//    data = [self createRecord:@"EMPLOYEEDEMO3"];
//    [savedArrayOfEmployees addObject:data];
//    data = [self createRecord:@"EMPLOYEEDEMO2"];
//    [savedArrayOfEmployees addObject:data];
//    data = [self createRecord:@"LINEMANAGERDEMO3"];
//    [savedArrayOfEmployees addObject:data];
    
    NSData *data1 = [[NSUserDefaults standardUserDefaults]objectForKey:@"Key"];
    delegate.dataFull = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
    NSLog(@"delegate.dataFull===%@",delegate.dataFull);

    Contact *data;
    NSMutableArray *stackholderArray = [delegate.dataFull valueForKey:@"stackholder"];
    for (int i = 0;i<stackholderArray.count; i++){
        NSString *stackholderStr = [stackholderArray objectAtIndex:i];
        NSArray *Split = [stackholderStr componentsSeparatedByString:@","];
        NSString *StackHolderName = Split[0];
        data = [self createRecord:StackHolderName];
        [savedArrayOfEmployees addObject:data];
         NSString *StackHolderID = Split[1];
        [StatckHolderID addObject:StackHolderID];
    }
    
    self.searchResults1 = [[NSMutableArray array]init];
    self.selectedContacts = [[NSMutableArray array]init];
    [self ScreenDesign];
}
- (void) ScreenDesign{
    UIView *screenView=[[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    screenView.backgroundColor=delegate.BackgroudColor;
    
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor=delegate.headerbgColler;
    [screenView addSubview:headerview];
    
    UIButton *backButton=[[UIButton alloc] initWithFrame:CGRectMake(5, 35, 30, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackBtn"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(sendButton:) forControlEvents:UIControlEventTouchUpInside];
    CGPoint center = backButton.center;
    center.y = headerview.frame.size.height / 2 +3;
    [backButton setCenter:center];
    backButton.tag=30;
    [headerview addSubview:backButton];
    
    headerlab = [[UILabel alloc] initWithFrame:CGRectMake(45, backButton.frame.origin.y+12, 200, 16)];
    headerlab.textColor=delegate.yellowColor;
    headerlab.font=delegate.headFont;
    headerlab.text=NSLocalizedString(@"ADD_STAKE_HOLDER_WITHOUT_ASTRIC", nil);    //
    [headerview addSubview:headerlab];
    
    tab=[[UITableView alloc] initWithFrame:CGRectMake(0, delegate.headerheight, screenView.frame.size.width,screenView.frame.size.height-80)];
    tab.delegate=self;
    tab.dataSource=self;
    tab.backgroundColor=[UIColor whiteColor];
    tab.showsVerticalScrollIndicator = NO;
    tab.estimatedRowHeight = 200.0;
    tab.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
    tab.estimatedRowHeight = 100.0;
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.allowsMultipleSelection = YES;
    [screenView addSubview:tab];
    
    //searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 160, 44)];
    searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    searchController.searchResultsUpdater = self;
    searchController.searchBar.delegate = self;
    //searchController.
    [searchController.searchBar sizeToFit];
    [searchController.searchBar setValue:@"DONE" forKey:@"_cancelButtonText"];
    searchController.dimsBackgroundDuringPresentation = NO;
    tab.tableHeaderView = searchController.searchBar;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, screenView.frame.size.height-40, screenView.frame.size.width, 40)];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: footerView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    //footerView.layer.mask = maskLayer1;
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width, 40)];
    submitButton.backgroundColor = delegate.redColor;
    submitButton.clipsToBounds = YES;
    submitButton.titleLabel.font = delegate.contentBigFont;
    [submitButton setTitle:NSLocalizedString(@"DONE_BTN", nil) forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(sendButton:) forControlEvents:UIControlEventTouchUpInside];
    submitButton.tag=20;
    [footerView addSubview:submitButton];
    [screenView addSubview:footerView];
    [self.view addSubview:screenView];
}

#pragma mark Search Bar Methods
- (void)updateSearchResultsForSearchController:(UISearchController *)aSearchController {
    [self.searchResults1 removeAllObjects];
    NSString *searchString = aSearchController.searchBar.text;
    Contact *group;
    for(group in savedArrayOfEmployees)
    {
        //NSMutableArray *newGroup = [[NSMutableArray alloc] init];
        //for(element in group)
        {
            NSRange range = [group.firstName rangeOfString:searchString options:NSCaseInsensitiveSearch];
            if (range.length > 0) {
                //[newGroup addObject:group];
                [self.searchResults1 addObject:group];
            }
        }
    }
    [tab reloadData];
    return;
}
#pragma mark Tableview Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (searchController.active && ![searchController.searchBar.text isEqualToString:@""]) {
        int count = (int)[self.searchResults1 count];
        return count;
    }
    else {
        return (savedArrayOfEmployees.count);
    }
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(!cell){
        cell =
        [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    Contact *selectedContact;
    if (searchController.active && ![searchController.searchBar.text isEqualToString:@""]) {
        selectedContact = [self.searchResults1 objectAtIndex:indexPath.row];
        if (selectedContact.checkmarkFlag == YES) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else if (selectedContact.checkmarkFlag == NO) {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else {
      selectedContact = [savedArrayOfEmployees objectAtIndex:indexPath.row];
        if (selectedContact.checkmarkFlag == YES) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [tab selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        else if (selectedContact.checkmarkFlag == NO) {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *fullName = selectedContact.firstName;
    cell.textLabel.text = fullName;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    Contact *selectedContact;
    selectedCounter = (int)self.selectedContacts.count;
    if (selectedCounter >= 3){
        [self ShowAlert:NSLocalizedString(@"LIMIT_EXCEED", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    if (searchController.active && ![searchController.searchBar.text isEqualToString:@""]) {
        selectedContact = [self.searchResults1 objectAtIndex:indexPath.row];
            if (selectedContact.checkmarkFlag == YES) {
            selectedContact.checkmarkFlag = NO;
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self.selectedContacts removeObject:selectedContact];
        }
        else {
            selectedContact.checkmarkFlag = YES;
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [self.selectedContacts addObject:selectedContact];
        }
    }
    else {
        selectedContact = [savedArrayOfEmployees objectAtIndex:indexPath.row];
        selectedContact.checkmarkFlag = YES;
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.selectedContacts addObject:selectedContact];
    }
    selectedCounter = (int)self.selectedContacts.count;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    Contact *selectedContact = [savedArrayOfEmployees objectAtIndex:indexPath.row];
    selectedContact.checkmarkFlag = NO;
    cell.accessoryType = UITableViewCellAccessoryNone;
    [self.selectedContacts removeObject:selectedContact];
    selectedCounter = (int)self.selectedContacts.count;
    selectedContactsCounterButton.title = [NSString stringWithFormat:@"sel: %d", selectedCounter];

}
#pragma mark IBActions and custom methods
- (IBAction) sendButton: (id) sender {
    UIButton *btn=(UIButton *)sender;
    if(20 == btn.tag){
        NSMutableString *myString = [[NSMutableString alloc]initWithString:@""];
        for (int i=0; i < self.selectedContacts.count; i++) {
            Contact *myContact = self.selectedContacts[i];
            if (i+1 == self.selectedContacts.count)
                [myString appendString:[NSString stringWithFormat:@"%@", myContact.firstName]];
            else
                [myString appendString:[NSString stringWithFormat:@"%@,", myContact.firstName]];
        }
        if ([self.delegate1 respondsToSelector:@selector(multiSelectController:didFinishPickingSelections1:)]) {
            [self.delegate1 multiSelectController:self didFinishPickingSelections1:myString];
        }
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    else if (30 == btn.tag){
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}
- (Contact *) createRecord:(NSString*) name{
    Contact *myContactObject = [[Contact alloc]init];
    myContactObject.firstName = name;
    myContactObject.checkmarkFlag = NO;
    return myContactObject;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
