//
//  SelectKpiCell.swift
//  iWork
//
//  Created by Shailendra on 12/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class SelectKpiCell: UITableViewCell {

    @IBOutlet var SelectKPITextField : UITextField!
    @IBOutlet var CustomView : UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        CustomView.layer.cornerRadius = 2.0
        CustomView.layer.borderWidth = 1.0;
        CustomView.layer.borderColor = UIColor.darkGray.cgColor;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
