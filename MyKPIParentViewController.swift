//
//  MyKPIParentViewController.swift
//  iWork
//
//  Created by Shailendra on 09/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

//let SCREEN_WIDTH = UIScreen.main.bounds.size.width

class MyKPIParentViewController: UIViewController ,UIScrollViewDelegate {
    
    var ObjAppDelegate = AppDelegate()
    var isComeFrom : String! = ""
    @IBOutlet var containerScrollView : UIScrollView!
    @IBOutlet var menuScrollView : UIScrollView!
    @IBOutlet var AddKPIBtn : UIButton!
    var btnArray : [String] = []
    @IBOutlet var HeaderLabel : UILabel!
    var BtnTagValue : Int = 0
    let KPICascading = UserDefaults.standard.string(forKey: "kKPICascading")
    var RemainingValue : Int  = 0
    var attributedString : NSMutableAttributedString!
    var KPILimit : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddKPIBtn.isHidden = false
        ObjAppDelegate = UIApplication.shared.delegate as! AppDelegate
        HeaderLabel.text = "\(NSLocalizedString("MY_KPI", comment: "") as NSString)"
        btnArray.append((NSLocalizedString("KPI_LIST", comment: "") as NSString) as String)
        btnArray.append((NSLocalizedString("KPI_APPROVAL", comment: "") as NSString) as String)
        addButtonsInScrollMenu(buttonArray: btnArray as NSArray)
        
        let storyBoard : UIStoryboard  = UIStoryboard(name: "PMS", bundle: nil)
        let OneVC : KPIListViewController = storyBoard.instantiateViewController(withIdentifier: "KPIListViewController") as! KPIListViewController
        let TwoVC : ApproveKPIViewController = storyBoard.instantiateViewController(withIdentifier: "ApproveKPIViewController") as! ApproveKPIViewController
        
        let controllerArray  = [OneVC,TwoVC] as NSArray
        addChildViewControllersOntoContainer(controllersArr: controllerArray)
        AddKPIBtn.backgroundColor = UIColor.ButtonGreenColor()
        
        let notificationName = Notification.Name("REMAIN_WEIGHT")
        NotificationCenter.default.addObserver(self, selector: #selector(MyKPIParentViewController.ReceivedNotification), name: notificationName, object: nil)
    }
    override func viewDidLayoutSubviews() {
//        if(KPICascading == "1"){
//            AddKPIBtn.isHidden = false
//        }
//        else{
//            AddKPIBtn.isHidden = true
//        }
    }
    
    func addButtonsInScrollMenu(buttonArray:NSArray){
        
        let buttonHeight = self.menuScrollView.frame.size.height as! CGFloat
        var cWidth = 0.0 as! Double
        
        for i in 0 ..< buttonArray.count {
            
            let tagTitle = buttonArray[i] as! NSString
            let button = UIButton(frame: CGRect(x: cWidth, y: 0, width: Double(SCREEN_WIDTH/2), height: Double(buttonHeight)))
            button.titleLabel?.numberOfLines = 2
            button.setTitle("\(tagTitle)", for: UIControlState.normal)
            button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 15)
            button.setTitleColor(UIColor.white, for: UIControlState.selected)
            button.setTitleColor(UIColor(red: (248.0/255.0), green: (117.0/255.0), blue: (177.0/255.0), alpha: 1.0), for: UIControlState.normal)
            button.tag = i
            button.addTarget(self, action: #selector(KPIApprovalParentViewController.buttonPressed(sender:)), for: UIControlEvents.touchUpInside)
            self.menuScrollView.addSubview(button)
            
            let bottomView : UIView = UIView(frame: CGRect(x: 0, y: button.frame.size.height-2, width: SCREEN_WIDTH/2, height: 2))
            bottomView.backgroundColor = UIColor.white
            bottomView.tag = 1001
            button.addSubview(bottomView)
            
            if(i==0){
                button.isSelected = true
                bottomView.isHidden = false
            }
            else{
                bottomView.isHidden = true
            }
            cWidth += Double(SCREEN_WIDTH/2)
        }
        self.menuScrollView.contentSize = CGSize(width: cWidth, height: Double(self.menuScrollView.frame.size.height))
    }
    
    func buttonPressed (sender:UIButton){
        
        let buttonWidth = 0.0 as! Double
        for subview in self.menuScrollView.subviews {
            if let btn  = subview as? UIButton {
                let bottomView = btn.viewWithTag(1001)
                if(btn.tag == sender.tag){
                    btn.isSelected = true
                    bottomView?.isHidden = false
                }
                else{
                    btn.isSelected = false
                    bottomView?.isHidden = true
                }
                
            }
        }
        let Points = CGPoint(x: SCREEN_WIDTH * CGFloat (sender.tag), y: 0)
        self.containerScrollView.setContentOffset(Points, animated: true)
        let xx = SCREEN_WIDTH * CGFloat(sender.tag) * CGFloat(CGFloat(buttonWidth)/CGFloat(SCREEN_WIDTH)) - CGFloat(buttonWidth)
        
        let Rects = CGRect(x: xx, y: 0.0, width: SCREEN_WIDTH, height: self.menuScrollView.frame.size.height)
        self.menuScrollView.scrollRectToVisible(Rects, animated: true)
        
        
        
    }
    func widthForMenuTitle(title:NSString,buttonEdgeInsets:(UIEdgeInsets)) -> CGFloat {
        
        return 0.0
    }
    func addChildViewControllersOntoContainer(controllersArr:NSArray){
        
        for i in 0 ..< controllersArr.count {
            let vc = controllersArr[i] as! UIViewController
            var frame = CGRect(x: 0.0, y: 0.0, width: self.containerScrollView.frame.size.width, height: containerScrollView.frame.size.height)
            frame.origin.x = SCREEN_WIDTH * CGFloat(i);
            vc.view.frame = frame;
            self.addChildViewController(vc)
            self.containerScrollView.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
        }
        self.containerScrollView.contentSize = CGSize(width: CGFloat(SCREEN_WIDTH) * CGFloat(controllersArr.count) + CGFloat(1), height: CGFloat (self.containerScrollView.frame.size.height))
        
        self.containerScrollView.isPagingEnabled = true;
        self.containerScrollView.delegate = self;
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = ( CGFloat(scrollView.contentOffset.x) / CGFloat(SCREEN_WIDTH)) as CGFloat
        
        let buttonWidth = 0.0 as! Double
        for subview in self.menuScrollView.subviews {
            if let btn  = subview as? UIButton {
                let bottomView = btn.viewWithTag(1001)
                if( CGFloat(btn.tag) == CGFloat(page)){
                    btn.isSelected = true
                    bottomView?.isHidden = false
                }
                else{
                    btn.isSelected = false
                    bottomView?.isHidden = true
                }
            }
        }
        let xx = scrollView.contentOffset.x * CGFloat(buttonWidth) / CGFloat(SCREEN_WIDTH) - CGFloat(buttonWidth)
        
        let Rects = CGRect(x: xx, y: 0.0, width: SCREEN_WIDTH, height: self.menuScrollView.frame.size.height)
        self.menuScrollView.scrollRectToVisible(Rects, animated: true)
    }
    func ReceivedNotification(notification:NSNotification){
        if let userInfo = notification.userInfo as? [String: Any] {
            if let MinimumValue = userInfo["REMAIN"] as? NSNumber {
                RemainingValue = Int(NSNumber(integerLiteral: Int(MinimumValue)));
            }
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func AddKPIAction(_ sender: UIButton){
        
        if(KPICascading == "0"){
            KPILimit = "KPILimit"
            let normalText = "Please develop your KPI through"
            let boldText  = " MyInfo."
            attributedString = NSMutableAttributedString(string:normalText)
            let attrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 12)]
            let boldString = NSMutableAttributedString(string:boldText, attributes:attrs)
            attributedString.append(boldString)
            
            self.ShowAlert(MsgTitle: "", BtnTitle: NSLocalizedString("OKAY", comment: "")as NSString)
        }
        else{
            if(RemainingValue == 0 || RemainingValue < 0){
                KPILimit = ""
                self.ShowAlert(MsgTitle:  NSLocalizedString("WEIGHTAGE_LIMIT", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
            }
            else{
                let viewController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "CreateKPIViewController") as! CreateKPIViewController
                viewController.remainingWeightage = RemainingValue as NSNumber
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        
        
    }
    @IBAction func HomeAction(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContainerParentViewController") as! ContainerParentViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func BackBtnAction(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        if(KPILimit == "KPILimit"){
            ObjAlertController.isCome = "KPI"
            ObjAlertController.attributedString = attributedString
        }
        else{
           ObjAlertController.messageTitleStr = MsgTitle as String!
        }
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: false, completion: nil)
    }

}
