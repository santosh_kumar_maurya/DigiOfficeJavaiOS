//
//  ApproveKPIViewController.swift
//  iWork
//
//  Created by Shailendra on 09/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class ApproveKPIViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet var KPIApprovalTableView : UITableView!
    var ResponseDic : NSDictionary!
    var ObjAppDelegate = AppDelegate()
    var Api: WebApiService =  WebApiService()
    var KPISentForApprovals = NSArray()
    var selectedIndex: Int = 0
    @IBOutlet var NoDataLabel : UILabel!
    @IBOutlet var NoDataView : UIView!
    var remainingWeightage : NSNumber!
    
    override func viewDidLoad(){
        super.viewDidLoad()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        LoadingManager.showLoading(self.view)
        ObjAppDelegate = UIApplication.shared.delegate as! AppDelegate
        KPIApprovalTableView.estimatedRowHeight = 200
        KPIApprovalTableView.rowHeight = UITableViewAutomaticDimension;
        self.KPIApprovalTableView.register(UINib(nibName: "EditKPICell", bundle: nil), forCellReuseIdentifier: "EditKPICell")
        self.perform(#selector(ApproveKPIViewController.GetKPIApprovalWebApi), with: nil, afterDelay: 0.5)
        
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        print("KPISentForApprovals.count===\(KPISentForApprovals.count)")
        if (KPISentForApprovals.count>0){
            return KPISentForApprovals.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell : EditKPICell = tableView.dequeueReusableCell(
            withIdentifier: "EditKPICell", for: indexPath) as! EditKPICell
        Cell.EditKPIBtn.tag = indexPath.row
        if(KPISentForApprovals.count>0){
            let Dic = KPISentForApprovals[indexPath.row] as! NSDictionary
            Cell.EditKPILabel.text = Dic.value(forKey: "kpi") as? String
        }
        Cell.EditKPIBtn.addTarget(self, action: #selector(EditAction(sender:)), for: UIControlEvents.touchUpInside)
        return Cell
    }
    func callWebServiceEditKPILoggedStatus(kpiId: String, dict: NSDictionary!){
        if (ObjAppDelegate.isInternetConnected())
        {
            let response = Api.webApi(nil, url:"editKpiLoggedStatus?kpiId=\(kpiId)") as NSDictionary!
            if response != nil{
                if(response?.value(forKey: "statusCode") as! Int == 5){
                    LoadingManager.hideLoading(view)
                    let viewController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "CreateKPIViewController") as! CreateKPIViewController
                    viewController.ResponseDic = dict.mutableCopy() as! NSMutableDictionary
                    viewController.isComeFrom = "EDIT"
                    viewController.remainingWeightage = remainingWeightage
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }else{
                LoadingManager.showLoading(self.view)
                self.GetDataForLoogedStatusApi()
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func GetKPIApprovalWebApi(){
        if (ObjAppDelegate.isInternetConnected()){
            let userId = UserDefaults.standard.string(forKey: "kUserId")
            ResponseDic = Api.webApi(nil, url:"getKPI?user_id=\(userId!)") as NSDictionary!
            
            if ResponseDic != nil{
                NoDataView.isHidden = true
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    LoadingManager.hideLoading(view)
                    let Dic = ResponseDic.value(forKey: "object") as! NSDictionary
                    print("Dic===\(Dic)")
                    if (Dic["kpiSentForApprovals"] as? NSArray) != nil {
                        KPISentForApprovals = Dic.value(forKey: "kpiSentForApprovals") as! NSArray
                        remainingWeightage = Dic.value(forKey: "remainingWeightage") as! NSNumber
                        if(KPISentForApprovals.count == 0){
                            NoDataFound()
                        }
                        self.KPIApprovalTableView.reloadData()
                    }
                    else{
                        NoDataFound()
                    }
                    
                }
                else{
                    LoadingManager.hideLoading(view)
                }
            }
            else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(ApproveKPIViewController.GetKPIApprovalWebApi), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func NoDataFound(){
        if(KPISentForApprovals.count == 0){
            NoDataView.isHidden = false
            NoDataLabel.isHidden = false
            NoDataLabel.text = "No Record Found"
        }
        //self.KPIApprovalTableView.reloadData()
    }
    func GetDataForLoogedStatusApi(){
        let Dic = KPISentForApprovals[self.selectedIndex] as! NSDictionary
        let kpiId = Dic["id"] as! Int
        LoadingManager.showLoading(self.view)
        self.callWebServiceEditKPILoggedStatus(kpiId: kpiId.description, dict: Dic)
    }
    func EditAction(sender:UIButton){
        self.selectedIndex = sender.tag
        self.GetDataForLoogedStatusApi()
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: false, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}
