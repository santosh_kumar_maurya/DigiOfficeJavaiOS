//
//  MyTaskCell.m
//  iWork
//
//  Created by Shailendra on 04/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyTaskCell.h"
#import "iWork-Swift.h"

@implementation MyTaskCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    _TaskView.layer.cornerRadius = 2.0;
    _TaskView.maskView.layer.cornerRadius = 7.0f;
    _TaskView.layer.shadowRadius = 3.0f;
    _TaskView.layer.shadowColor = [UIColor blackColor].CGColor;
    _TaskView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _TaskView.layer.shadowOpacity = 0.7f;
    _TaskView.layer.masksToBounds = NO;
    
    self.TopView.backgroundColor = [UIColor TopBarYellowColor];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: self.TopView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){2.0}].CGPath;
    self.TopView.layer.mask = maskLayer1;
    
    
    _MyTaskStaticLabel.textColor = [UIColor TextBlueColor];
    _TaskCreationStaticLabel.textColor = [UIColor TextBlueColor];
    _TaskCompletionStaticLabel.textColor = [UIColor TextBlueColor];
    _TaskCreationLabel.textColor = [UIColor TextBlueColor];
    _TaskCompletionLabel.textColor = [UIColor TextBlueColor];
    _ViewAllTasksBtn.backgroundColor = [UIColor ButtonSkyColor];
    
    
    
    _ViewAllTasksBtn.layer.cornerRadius = 18.0;
    _ViewAllTasksBtn.clipsToBounds = YES;
    _ViewAllTasksBtn.layer.shadowRadius = 3.0f;
    _ViewAllTasksBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _ViewAllTasksBtn.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _ViewAllTasksBtn.layer.shadowOpacity = 0.7f;
    _ViewAllTasksBtn.layer.masksToBounds = NO;
    
    
    
}

@end
