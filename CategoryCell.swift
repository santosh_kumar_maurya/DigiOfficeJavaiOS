//
//  CategoryCell.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    //MARK: - Properties
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var txtCategory: UITextField!
    //end
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.updateUI()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateUI(){
        self.txtCategory.layer.borderWidth = 1.0
        self.txtCategory.layer.cornerRadius = 2.0
        self.txtCategory.layer.borderColor = UIColor.lightGray.cgColor
        self.txtCategory.clipsToBounds = true
        let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
        txtCategory.leftView = paddingView
        txtCategory.leftViewMode = UITextFieldViewMode.always
    }

}
