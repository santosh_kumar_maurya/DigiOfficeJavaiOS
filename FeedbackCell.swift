//
//  FeedbackCell.swift
//  iWork
//
//  Created by Shailendra on 16/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class FeedbackCell: UITableViewCell {

    @IBOutlet var TaskIdLabel : UILabel!
    @IBOutlet var CompletedByStaticLabel : UILabel!
    @IBOutlet var CompletedByLabel : UILabel!
    @IBOutlet var CompletedDateStaticLabel : UILabel!
    @IBOutlet var CompletedDateLabel : UILabel!
    @IBOutlet var TaskNameStaticLabel : UILabel!
    @IBOutlet var TaskNameLabel : UILabel!
    @IBOutlet var GiveFeedbackButton : UIButton!
    @IBOutlet var ProfileImageView : UIImageView!
    
    override func awakeFromNib() {
        ProfileImageView.layer.borderColor = UIColor.white.cgColor
        ProfileImageView.layer.borderWidth = 1;
        ProfileImageView.layer.cornerRadius = 25;
        ProfileImageView.clipsToBounds = true
        
        GiveFeedbackButton.setTitleColor(UIColor.ApproveGreenColor(), for: UIControlState.normal)
        super.awakeFromNib()
        // Initialization code
    }
    func CellConfigure(info:NSDictionary){
        if (info["taskId"] as? NSNumber) != nil{
            TaskIdLabel.text = "Task ID : \(String(describing: info["taskId"]!))"
        }
        else{
            TaskIdLabel.text = " "
        }
        if (info["completedBy"] as? String) != nil{
            CompletedByLabel.text = "\(String(describing: info["completedBy"]!))"
        }
        else{
            CompletedByLabel.text = " "
        }
        if (info["taskName"] as? String) != nil{
            TaskNameLabel.text = "\(String(describing: info["taskName"]!))"
        }
        else{
            TaskNameLabel.text = " "
        }
        
        if (info["completionDate"] as? NSNumber) != nil{
            let datenumber = info["completionDate"] as! NSNumber
            CompletedDateLabel.text = convertDateFormater(datenumber: datenumber)
        }
        else{
            CompletedDateLabel.text = " "
        }
        if (info["userImg"] as? String) != nil{
            let Urls = URL(string: "\(String(describing: info["userImg"]!))")
            ProfileImageView.sd_setImage(with: Urls!, placeholderImage: UIImage(named: "profile_image_default"))
        }
        else{
            ProfileImageView.image = UIImage(named: "profile_image_default")
        }
    }
    func convertDateFormater(datenumber: NSNumber) -> String{
        let DateStr = NSString(format: "%@",datenumber);
        let milisec = Double(DateStr as String)
        let takeOffDate = NSDate(timeIntervalSince1970: milisec!/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateStr = dateFormatter.string(from: takeOffDate as Date)
        let date = dateFormatter.date(from: dateStr as String)
        dateFormatter.dateFormat = "dd MMM yyyy"
        return  dateFormatter.string(from: date!)
    }
}
