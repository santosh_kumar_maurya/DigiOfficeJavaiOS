#import "NewTaskDetailScreen.h"
#import "Header.h"
#import "iWork-Swift.h"
const UIEdgeInsets textInsetsMine10 = {5, 10, 11, 17};
const UIEdgeInsets texttextInsetsSomeone10 = {5, 15, 11, 10};

@interface NewTaskDetailScreen ()<UITextViewDelegate>{
   
    UIButton *screenView;
    WebApiService *Api;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    UIView *CoomentView;
    NSString *SelectBtn;
    UIRefreshControl *refreshControl;
    NSString *SubTask;
    NSArray* subTaskData;
    NSMutableArray *stakeHolderData;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UITextView *FeedTextView;
    IBOutlet UIView *OuterView;
    
    IBOutlet HCSStarRatingView *RatingView;
    IBOutlet UIButton *SubmitBtn;
    float RateValue;
    IBOutlet UILabel *RatingLabel;
    IBOutlet UILabel *ApproveTaskStaticLabel;
    IBOutlet UIButton *CloseBtn;
    
    IBOutlet UIView *NeedRevisionView;
    IBOutlet UITextView *CommentTextView;
    IBOutlet UILabel *NeedRevisionStaticLabel;
    
    IBOutlet UIView *ConfirmationView;
    IBOutlet UILabel *ConfirmationTitle;
    IBOutlet UILabel *ConfirmationDescription;
    
    IBOutlet UIView *ConfirmationConfirmView;
    IBOutlet UILabel *ConfirmationConfirmTitle;
    IBOutlet UILabel *ConfirmationConfirmDescription;
    IBOutlet UIImageView *ConfirmationConfirmImageView;
    
    NSString *CommingSoon;
    NSMutableAttributedString *attributedString;
    
    int tagValue;
    NSString *SelectBtnString;
    IBOutlet UIButton *ApproveRejectBtn;
    IBOutlet UIButton *CancelBtn;
    
    __weak IBOutlet UILabel *createdOnStaticLabel;
    __weak IBOutlet UILabel *createdOnValueLabel;
    __weak IBOutlet UILabel *actualStartDateStaticLabel;
    __weak IBOutlet UILabel *actualStartDateValueLabel;
    __weak IBOutlet UILabel *completionDateStaticLabel;
    __weak IBOutlet UILabel *completionDateValueLabel;
    __weak IBOutlet UIView *tableViewBackgroundView;
    
    
}

@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation NewTaskDetailScreen
@synthesize isShowDetail, count, taskID, taskType, dataDict, isMe, isNewReq,EmployeeID;

- (void)viewDidLoad
{
    [super viewDidLoad];
    createdOnStaticLabel.hidden = YES;
    createdOnValueLabel.hidden = YES;
    actualStartDateStaticLabel.hidden = YES;
    actualStartDateValueLabel.hidden = YES;
    completionDateStaticLabel.hidden = YES;
    completionDateValueLabel.hidden = YES;

    
    SelectBtnString = @"";
    RateValue = 0.0;
    Api = [[WebApiService alloc] init];
    tab.estimatedRowHeight = 50;
    tab.rowHeight = UITableViewAutomaticDimension;
    [self registerTableViewCell];
    self.view.backgroundColor = [UIColor whiteColor];
    HeaderLabel.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),taskID];
    HeaderLabel.textColor = [UIColor whiteColor];
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    isServerResponded = FALSE;
    isShowDetail= TRUE;
    isMe = TRUE;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TaskDetailsWebApi) withObject:nil afterDelay:0.5];
    //[self.view addSubview:OuterView];
    //OuterView.hidden = YES;
    RatingView.tintColor =[UIColor NeedRevisionBtnColor];
    ConfirmationTitle.textColor = [UIColor TextBlueColor];
    NeedRevisionStaticLabel.textColor = [UIColor TextBlueColor];
    ApproveTaskStaticLabel.textColor = [UIColor TextBlueColor];
    FeedTextView.layer.cornerRadius = 6;
    FeedTextView.layer.borderWidth = 1;
    FeedTextView.layer.borderColor = delegate.borderColor.CGColor;
    CommentTextView.layer.cornerRadius = 6;
    CommentTextView.layer.borderWidth = 1;
    CommentTextView.layer.borderColor = delegate.borderColor.CGColor;
    CommentTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    CommentTextView.textColor = [UIColor lightGrayColor];
    FeedTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    FeedTextView.textColor = [UIColor lightGrayColor];
    
    [self updateTableViewUI];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
   // [self FeedbackLayout];
   // [self GiveRecognitionsLayout];


}
-(void)TaskDetailsWebApi{
    if(delegate.isInternetConnected){
//        if([SelectBtn isEqualToString:@"MARK"]){
//            params = @ {
//                @"comment": CommentTextView.text,
//                @"taskId": taskID,
//                @"employeeId"  : [ApplicationState userId],
//            };
//            ResponseDic = [Api WebApi:params Url:@"markCompleted"];
//        }
//        else if([SelectBtn isEqualToString:@"APPROVE"]){
//            params = @ {
//                @"comment": CommentTextView.text,
//                @"subTask":SubTask,
//                @"taskId": taskID,
//                @"user_id"  : [ApplicationState userId],
//            };
//            ResponseDic = [Api WebApi:params Url:@"addComment"];
//        }
//        else{
        ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getTaskDetail?taskId=%@&userId=%@",taskID,[ApplicationState userId]]];
       // }
        isServerResponded = YES;
        [refreshControl endRefreshing];
        if(ResponseDic==nil){
            [LoadingManager hideLoadingView:self.view];
            [self TaskDetailsWebApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"])))
            {
                
                
                
//                if([SelectBtn isEqualToString:@"MARK"]){
//                    [[self navigationController] popViewControllerAnimated:YES];
//                }
//                else if([SelectBtn isEqualToString:@"COMMENT"]){
//                    [[self navigationController] popViewControllerAnimated:YES];
//                }
//                else{
                
                    dataArray = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                [self updateUIWithData];
                //}
            }
            else{
                [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
            [tab reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}

-(void)updateTableViewUI
{
    tableViewBackgroundView.layer.cornerRadius = 2.0;
    tableViewBackgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    tableViewBackgroundView.layer.borderWidth = 0.5;
    tableViewBackgroundView.layer.masksToBounds =YES;
    tableViewBackgroundView.backgroundColor=[UIColor whiteColor];
    tableViewBackgroundView.layer.shadowRadius = 3.0f;
    tableViewBackgroundView.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
    tableViewBackgroundView.layer.shadowOpacity = 0.7f;
    tableViewBackgroundView.layer.masksToBounds = NO;
    tab.clipsToBounds = YES;
}

-(void)updateUIWithData
{
    if (dataArray != nil)
    {
        //Created On
        if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"creationDate"]))){
            NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"creationDate"];
            createdOnValueLabel.text = [self DateFormateChange:datenumber];
        }
        else{
            createdOnValueLabel.text = @"";
        }
        createdOnStaticLabel.hidden = NO;
        createdOnValueLabel.hidden = NO;
        completionDateStaticLabel.hidden = NO;
        completionDateValueLabel.hidden = NO;
        
        //Actual Start Date
        if (FALSE == isNewReq)
        {
            actualStartDateStaticLabel.hidden = NO;
            actualStartDateValueLabel.hidden = NO;
            if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"actualStartDate"]))){
                NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"actualStartDate"];
                actualStartDateValueLabel.text = [self DateFormateChange:datenumber];
            }
            else{
                actualStartDateValueLabel.text = @"";
            }
        }
        else{
            actualStartDateStaticLabel.hidden = YES;
            actualStartDateValueLabel.hidden = YES;
        }
        
        //Completion Date
        if (FALSE == isNewReq){
            completionDateStaticLabel.text = NSLocalizedString(@"COMPLETION_DATE", nil);
        }
        else{
            completionDateStaticLabel.text = NSLocalizedString(@"DURATION", nil);
        }
        
        if (FALSE == isNewReq)
        {
            if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"completionDate"]))){
                NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"completionDate"];
                completionDateValueLabel.text = [self DateFormateChange:datenumber];
            }
            else
            {
                completionDateValueLabel.text = @"";
            }
        }
        else
        {
            completionDateValueLabel.text  = [[dataArray objectAtIndex:0] objectForKey:@"duration"];
        }
    }
}

-(IBAction)btnfun:(id)sender{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==20){
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else if(btn.tag==1000){
        taskID = [[dataArray objectAtIndex:0] objectForKey:@"taskId"];
        if (TRUE == isNewReq){
            taskType = @"1";
            //[self showApproveAlertForNew];
        }
        else{
            taskType = @"1";
            OuterView.hidden = NO;
        }
    }
    else if(btn.tag==2000){
        if (TRUE == isNewReq){
            taskType = @"2";
            //[self showRejectAlertForNew];
        }
        else{
            taskType = @"3";
           // [self CreateCommentView];
        }
    }
}

- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    RateValue = sender.value;
    if(RateValue == 0.0){
        RatingLabel.hidden = YES;
    }
    else if(RateValue == 1.0){
        RatingLabel.text = [@"Below Expectation" uppercaseString];
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 2.0){
        RatingLabel.text = [@"Meet Expectation" uppercaseString];
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 3.0){
        RatingLabel.text = [@"Above Expectation" uppercaseString];
        RatingLabel.hidden = NO;
    }
}

-(void)RegcognitionsYes{
    ConfirmationConfirmView.hidden = YES;
    if([[ApplicationState GetRecognitionEnable] intValue] == 0){ // Not Able to give recognition
        CommingSoon = @"COMMINGSOON";
        NSString *normalText = @"Comming soon...\n Please give recognition using iConnect.";
        attributedString = [[NSMutableAttributedString alloc] initWithString:normalText];
        [self ShowAlert:@"" ButtonTitle:@"Close"];
    }
    else{
        NSString *RegcognitionsUrl = [NSString stringWithFormat:@"iconnect://?token=%@&user_key=%@&givenTo=%@&taskId=%@",[ApplicationState GetToken],[ApplicationState userId],EmployeeID,taskID];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:RegcognitionsUrl] options:@{} completionHandler:nil];
    }
}

-(void)registerTableViewCell
{
    UINib *NewTaskStatusTableViewCellNib = [UINib nibWithNibName:@"NewTaskStatusTableViewCell" bundle:nil];
    [tab registerNib:NewTaskStatusTableViewCellNib forCellReuseIdentifier:@"NewTaskStatusTableViewCell"];
    
    UINib *NewTaskDetailTableViewCellNib = [UINib nibWithNibName:@"NewTaskDetailTableViewCell" bundle:nil];
    [tab registerNib:NewTaskDetailTableViewCellNib forCellReuseIdentifier:@"NewTaskDetailTableViewCell"];
    
    UINib *NewTaskRejectOrApproveTableViewCellNib = [UINib nibWithNibName:@"NewTaskRejectOrApproveTableViewCell" bundle:nil];
    [tab registerNib:NewTaskRejectOrApproveTableViewCellNib forCellReuseIdentifier:@"NewTaskRejectOrApproveTableViewCell"];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (dataArray != nil)
    {
        if (section == 0)
        {
            return 1;
        }
        else if (section == 1)
        {
            if(isNewReq == YES)
            {
                return 5;
            }
            else
            {
                return 6;
            }
        }
        else if (section == 2)
        {
            subTaskData = [[dataArray objectAtIndex:0] objectForKey:@"subTask"];
            NSInteger subTaskCount = 0;
            if (((NSNull*)subTaskData != [NSNull null]) && subTaskData.count > 0 && [subTaskData[0] length] > 0)
            {
                subTaskCount = subTaskData.count;
            }
            else{
                subTaskCount = 0;
            }
            return subTaskCount;
        }
        else
        {
            return 2;
        }
    }
    else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0)
    {
        NewTaskStatusTableViewCell *statusCell = (NewTaskStatusTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskStatusTableViewCell" forIndexPath:indexPath];
        if (FALSE == isNewReq)
        {
            statusCell.statusLabel.text = @"Submitted";
            statusCell.statusLabel.textColor = delegate.submittedColor;
            statusCell.statusImageView.image=[UIImage imageNamed:@"submitted_new"];
        }
        else{
            statusCell.statusLabel.text = @"New";
            statusCell.statusLabel.textColor = delegate.pendingColor;
            statusCell.statusImageView.image=[UIImage imageNamed:@"pending_new"];
        }
        return statusCell;
    }
    else if (indexPath.section == 1)
    {
        if (isNewReq == YES)
        {
            if (indexPath.row == 0)
            {
                NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
                detailCell.detailStaticLabel.text = NSLocalizedString(@"TASK_NAME_WITH_COLON", nil);
                detailCell.deatilValueLabelLeadingConstraints.constant = 85;
                NSString *taskInfo = [[dataArray objectAtIndex:0] objectForKey:@"taskName"];
                if (taskInfo.length > 0)
                {
                    detailCell.detailValueLabel.text = [NSString stringWithFormat:@"%@%@",[[taskInfo substringToIndex:1] uppercaseString],[taskInfo substringFromIndex:1] ];
                }
                return detailCell;
            }
            else if (indexPath.row == 1)
            {
                NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
                detailCell.detailStaticLabel.text = NSLocalizedString(@"START_DATE_WITH_COLON", nil);
                detailCell.deatilValueLabelLeadingConstraints.constant = 80.0;
                if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"startDate"])))
                {
                    NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"startDate"];
                    detailCell.detailValueLabel.text = [self DateFormateChange:datenumber];
                }
                else
                {
                    detailCell.detailValueLabel.text = @"";
                }
                return detailCell;
            }
            else if (indexPath.row == 2)
            {
                NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
                detailCell.detailStaticLabel.text = NSLocalizedString(@"END_DATE_WITH_COLON", nil);
                detailCell.deatilValueLabelLeadingConstraints.constant = 70.0;
                if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"endDate"])))
                {
                    NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"endDate"];
                    detailCell.detailValueLabel.text = [self DateFormateChange:datenumber];
                }
                else
                {
                    detailCell.detailValueLabel.text = @"";
                }
                return detailCell;
            }
            else if (indexPath.row == 3)
            {
                NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
                detailCell.detailStaticLabel.text = NSLocalizedString(@"TYPE_WITH_COLON", nil);
                detailCell.detailValueLabel.text = [[dataArray objectAtIndex:0] objectForKey:@"type"];
                detailCell.deatilValueLabelLeadingConstraints.constant = 45.0;
                return detailCell;
            }
            else
            {
                NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
                detailCell.detailStaticLabel.text = NSLocalizedString(@"KPI_WITH_COLON", nil);
                detailCell.detailValueLabel.text = [NSString stringWithFormat:@"%@", [[dataArray objectAtIndex:0]  objectForKey:@"kpi"]];
                detailCell.deatilValueLabelLeadingConstraints.constant = 35.0;
                return detailCell;
            }
        }
        else
        {
            if (indexPath.row == 0)
            {
                NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
                detailCell.detailStaticLabel.text = NSLocalizedString(@"TASK_NAME_WITH_COLON", nil);
                detailCell.deatilValueLabelLeadingConstraints.constant = 85;
                NSString *taskInfo = [[dataArray objectAtIndex:0] objectForKey:@"taskName"];
                if (taskInfo.length > 0)
                {
                    detailCell.detailValueLabel.text = [NSString stringWithFormat:@"%@%@",[[taskInfo substringToIndex:1] uppercaseString],[taskInfo substringFromIndex:1] ];
                }
                return detailCell;
            }
            else if (indexPath.row == 1)
            {
                NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
                detailCell.detailStaticLabel.text = NSLocalizedString(@"START_DATE_WITH_COLON", nil);
                detailCell.deatilValueLabelLeadingConstraints.constant = 80.0;
                if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"startDate"])))
                {
                    NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"startDate"];
                    detailCell.detailValueLabel.text = [self DateFormateChange:datenumber];
                }
                else
                {
                    detailCell.detailValueLabel.text = @"";
                }
                return detailCell;
            }
            else if (indexPath.row == 2)
            {
                NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
                detailCell.detailStaticLabel.text = NSLocalizedString(@"END_DATE_WITH_COLON", nil);
                detailCell.deatilValueLabelLeadingConstraints.constant = 70.0;
                if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"endDate"])))
                {
                    NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"endDate"];
                    detailCell.detailValueLabel.text = [self DateFormateChange:datenumber];
                }
                else
                {
                    detailCell.detailValueLabel.text = @"";
                }
                return detailCell;
            }
            else if (indexPath.row == 3)
            {
                NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
                detailCell.detailStaticLabel.text = NSLocalizedString(@"DURATION_WITH_COLON", nil);
                detailCell.deatilValueLabelLeadingConstraints.constant = 70.0;
                detailCell.detailValueLabel.text = [[dataArray objectAtIndex:0] objectForKey:@"duration"];
                return detailCell;
            }
            else if (indexPath.row == 4)
            {
                NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
                detailCell.detailStaticLabel.text = NSLocalizedString(@"TYPE_WITH_COLON", nil);
                detailCell.detailValueLabel.text = [[dataArray objectAtIndex:0] objectForKey:@"type"];
                detailCell.deatilValueLabelLeadingConstraints.constant = 45.0;
                return detailCell;
            }
            else
            {
                NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
                detailCell.detailStaticLabel.text = NSLocalizedString(@"KPI_WITH_COLON", nil);
                detailCell.detailValueLabel.text = [NSString stringWithFormat:@"%@", [[dataArray objectAtIndex:0]  objectForKey:@"kpi"]];
                detailCell.deatilValueLabelLeadingConstraints.constant = 35.0;
                return detailCell;
            }
        }
    }
    else if (indexPath.section == 2)
    {
        NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
        detailCell.detailStaticLabel.text = NSLocalizedString(@"SUBTASK_WITH_COLON", nil);
        detailCell.deatilValueLabelLeadingConstraints.constant = 65.0;
        detailCell.detailValueLabel.text = subTaskData[indexPath.row];
        return detailCell;
    }
    else
    {
        if (indexPath.row == 0)
        {
            NewTaskDetailTableViewCell *detailCell = (NewTaskDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskDetailTableViewCell" forIndexPath:indexPath];
            detailCell.detailStaticLabel.text = NSLocalizedString(@"TASK_DETAILS_WITH_COLON", nil);
            detailCell.deatilValueLabelLeadingConstraints.constant = 90.0;
            detailCell.detailValueLabel.text = [[dataArray objectAtIndex:0] objectForKey:@"taskDetail"];
            detailCell.sepratorLabel.hidden = YES;
            return detailCell;
        }
        else
        {
            NewTaskRejectOrApproveTableViewCell *buttonCell = (NewTaskRejectOrApproveTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NewTaskRejectOrApproveTableViewCell" forIndexPath:indexPath];
            buttonCell.approveButton.titleLabel.font = delegate.contentBigFont;
            if(isNewReq == YES)
            {
                [buttonCell.approveButton addTarget:self action:@selector(ApproveBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                [buttonCell.approveButton setTitle:NSLocalizedString(@"APPROVE_BTN_CAPS", nil) forState:UIControlStateNormal];
            }
            else{
                [buttonCell.approveButton addTarget:self action:@selector(CompleteApproveBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                [buttonCell.approveButton setTitle:NSLocalizedString(@"APPROVE_BTN_CAPS", nil) forState:UIControlStateNormal];
            }
            [buttonCell.approveButton setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
            buttonCell.rejectButton.titleLabel.font = delegate.contentBigFont;
            if(isNewReq == YES)
            {
                [buttonCell.rejectButton setTitle:NSLocalizedString(@"REJECT_BTN_CAPS", nil) forState:UIControlStateNormal];
                [buttonCell.rejectButton addTarget:self action:@selector(RejectBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                [buttonCell.rejectButton setTitleColor:[UIColor RedColor] forState:UIControlStateNormal];
            }
            else
            {
                [buttonCell.rejectButton setTitle:NSLocalizedString(@"NEED_REVISION", nil) forState:UIControlStateNormal];
                [buttonCell.rejectButton addTarget:self action:@selector(NeedRevisionBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                [buttonCell.rejectButton setTitleColor:[UIColor NeedRevisionBtnColor] forState:UIControlStateNormal];
            }
            return buttonCell;
        }
    }
}
-(IBAction)FeedbackSubmitAction:(id)sender{
    if (RateValue == 0.0){
        [self ShowAlert:NSLocalizedString(@"PLEASE_FILL_THE_STAR", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if([FeedTextView.text isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_GIVE_THE_FEEDBACK", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        taskFeedback = FeedTextView.text;
        taskRating = RateValue;
        [FeedTextView resignFirstResponder];
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.3];
    }
}
-(IBAction)FeedbackCancelAction:(id)sender{
    OuterView.hidden = YES;
    [self FeedBackClear];
    [FeedTextView resignFirstResponder];
}
-(void)FeedBackClear{
    RatingView.value = 0.0;
    RatingLabel.text = @"";
    CommentTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    FeedTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    FeedTextView.delegate = self;
    CommentTextView.delegate = self;
    FeedTextView.textColor = [UIColor lightGrayColor];
    CommentTextView.textColor = [UIColor lightGrayColor];
    
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView isEqual: CommentTextView]){
        if ([CommentTextView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)])
        {
            CommentTextView.text = @"";
            CommentTextView.textColor = [UIColor blackColor];
        }
    }
    else
    {
        if ([FeedTextView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)])
        {
            FeedTextView.text = @"";
            FeedTextView.textColor = [UIColor blackColor];
        }
    }
    [textView becomeFirstResponder];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView isEqual: CommentTextView])
    {
        if ([CommentTextView.text isEqualToString:@""])
        {
            CommentTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
            CommentTextView.textColor = [UIColor blackColor];
        }
    }
    else
    {
        if ([FeedTextView.text isEqualToString:@""])
        {
            FeedTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
            FeedTextView.textColor = [UIColor blackColor];
        }
    }
    [textView resignFirstResponder];
}
-(void)RejectBtnAction:(UIButton*)sender{
    tagValue = sender.tag;
    taskType = @"2";
    ConfirmationView.hidden = NO;
    [ApproveRejectBtn setTitle:@"REJECT" forState:UIControlStateNormal];
    [ApproveRejectBtn setTitleColor:[UIColor RedColor] forState:UIControlStateNormal];
    ConfirmationTitle.text = @"Reject Task";
    SelectBtnString = @"REJECT";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    ConfirmationDescription.text = NSLocalizedString(@"ARE_YOU_SURE_REJECT", nil);
    OuterView.hidden = YES;
}
-(void)ApproveBtnAction:(UIButton*)sender{
    tagValue = sender.tag;
    taskType = @"1";
    ConfirmationView.hidden = NO;
    [ApproveRejectBtn setTitle:@"APPROVE" forState:UIControlStateNormal];
    [ApproveRejectBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    ConfirmationTitle.text = @"Approve Task";
    SelectBtnString = @"APPROVE";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    ConfirmationDescription.text = NSLocalizedString(@"ARE_YOU_SURE_APPROVE", nil);
    OuterView.hidden = YES;
}
-(void)NeedRevisionBtnAction:(UIButton*)sender{
    [self FeedBackClear];
    SelectBtnString = @"NEED REVISION";
    tagValue = sender.tag;
    taskType = @"3";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    NeedRevisionView.hidden = NO;
    OuterView.hidden = YES;
}
-(void)CompleteApproveBtnAction:(UIButton*)sender{
    [self FeedBackClear];
    SelectBtnString = @"";
    tagValue = sender.tag;
    taskType = @"1";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    EmployeeID = [[dataArray objectAtIndex:tagValue] objectForKey:@"empId"];
    OuterView.hidden = NO;
}
-(IBAction)ConfirmViewCancelAction:(id)sender{
    ConfirmationView.hidden = YES;
    NeedRevisionView.hidden = YES;
}
-(IBAction)ConfirmedViewOKCrossAction:(id)sender{
    ConfirmationConfirmView.hidden = YES;
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)ApproveRejectNeedRevisionCompleteApproveAction:(id)sender{
    ConfirmationView.hidden = YES;
    
    if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.5];
    }
    else if([SelectBtnString isEqualToString:@"NEED REVISION"]){
        if([CommentTextView.text isEqualToString:@""]){
            [self ShowAlert:NSLocalizedString(@"PLEASE_GIVE_THE_FEEDBACK", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else{
            NeedRevisionView.hidden = YES;
            [LoadingManager showLoadingView:self.view];
            [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.5];
        }
    }
    else{
        [self RegcognitionsYes];
    }
}
-(void) startTheBgForStatus{
    if(delegate.isInternetConnected){
        
        if(OuterView.hidden == NO){
            int ConvertRating = (int)RateValue;
            params = @ {
                @"status": taskType,
                @"taskId": taskID,
                @"employeeId": [ApplicationState userId],
                @"rating" : [NSNumber numberWithInt:ConvertRating],
                @"feedback" : FeedTextView.text
            };
        }
        else{
            NSString *Comment = @"";
            Comment = CommentTextView.text;
            if(Comment == nil){
                Comment = @"";
            }
            if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
                params = @ {
                    @"status": taskType,
                    @"taskId": taskID,
                    @"user_id": [ApplicationState userId],
                    @"comment" : Comment
                };
            }
            else{
                params = @ {
                    @"status": taskType,
                    @"taskId": taskID,
                    @"userId": [ApplicationState userId],
                    @"comment" : Comment
                };
            }
        }
        NSLog(@"params==%@",params);
        if(OuterView.hidden == NO){
            ResponseDic = [Api WebApi:params Url:@"taskApproval"];
        }
        else{
            if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
                ResponseDic = [Api WebApi:params Url:@"updateTaskStatus"];
            }
            else{
                ResponseDic = [Api WebApi:params Url:@"needRevision"];
            }
        }
        if(ResponseDic==nil){
            [LoadingManager hideLoadingView:self.view];
            [self startTheBgForStatus];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            if(OuterView.hidden == NO){
                if([[[ResponseDic valueForKey:@"object"] valueForKey:@"recognitationStatus"] intValue]==1){
                    ConfirmationView.hidden = NO;
                    ConfirmationTitle.text = @"Give Recognition";
                    ConfirmationDescription.text = @"Would you link to give recognitions?";
                    [ApproveRejectBtn setTitle:@"YES" forState:UIControlStateNormal];
                    
                }
                else{
                    ConfirmationConfirmTitle.text = @"RATING SENT";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"KPISubmit"];
                    ConfirmationConfirmView.hidden = NO;
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                }
            }
            else{
                if([SelectBtnString isEqualToString:@"APPROVE"]){
                    ConfirmationConfirmTitle.text = @"APPROVED";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"ApprovedKpi"];
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                    ConfirmationConfirmView.hidden = NO;
                }
                else if([SelectBtnString isEqualToString:@"REJECT"]){
                    ConfirmationConfirmTitle.text = @"REJECTED";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"RejectedKpi"];
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                    ConfirmationConfirmView.hidden = NO;
                }
                else{
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                    ConfirmationConfirmView.hidden = NO;
                    ConfirmationConfirmTitle.text = @"NEED REVISION";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"ApprovedKpi"];
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                }
            }
            OuterView.hidden = YES;
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert: [ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    if([CommingSoon isEqualToString:@"COMMINGSOON"]){
        ObjAlertController.isCome = @"KPI";
        ObjAlertController.AttributedString = attributedString;
    }
    else{
        ObjAlertController.MessageTitleStr = MsgTitle;
    }
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}


@end
