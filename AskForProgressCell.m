//
//  AskForProgressCell.m
//  iWork
//
//  Created by Shailendra on 08/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "AskForProgressCell.h"
#import "Header.h"
#import "iWork-Swift.h"
#import <QuartzCore/QuartzCore.h>

@implementation AskForProgressCell

- (void)awakeFromNib {
    [super awakeFromNib];

    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: self.TaskView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    self.TaskView.layer.mask = maskLayer2;
    [self.DetailsBtn setTitleColor:[UIColor ButtonSkyColor] forState:UIControlStateNormal];
}

- (void)configureCell:(NSDictionary *)info{
    
    
    if(IsSafeStringPlus(TrToString(info[@"creationDate"]))){
        NSNumber *datenumber = info[@"creationDate"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else if(IsSafeStringPlus(TrToString(info[@"creartedOn"]))){
        NSNumber *datenumber = info[@"creartedOn"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else if(IsSafeStringPlus(TrToString(info[@"created_on"]))){
        NSNumber *datenumber = info[@"created_on"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else if(IsSafeStringPlus(TrToString(info[@"creationOn"]))){
        NSNumber *datenumber = info[@"creationOn"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _DateLabel.text  = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"taskId"]))){
        _TaskIdLabel.text  = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),info[@"taskId"]];
    }
    else{
        _TaskIdLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskName"]))){
        _TaskNameLabel.text  = info[@"taskName"];
    }
    else if(IsSafeStringPlus(TrToString(info[@"taskNAme"]))){
        _TaskNameLabel.text  = info[@"taskNAme"];
    }
    else{
        _TaskNameLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"duration"]))){
        _DurationLabel.text  = info[@"duration"];
    }
    else{
        _DurationLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"createdBy"]))){
        _CreatedByLabel.text  = info[@"createdBy"];
    }
    else{
        _CreatedByLabel.text  = @" ";
    }
    
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm aa"];
    return [dateformate stringFromDate:Newdate];
}
@end
