//
//  MyTaskFilterViewController.m
//  iWork
//
//  Created by Shailendra on 20/07/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyTaskFilterViewController.h"
#import "Header.h"

@interface MyTaskFilterViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    
    IBOutlet UITableView *FilterTableView;
    IBOutlet UIView *DatePickerViewBg;
    IBOutlet UIDatePicker *DatePicker;
    NSDateFormatter *dateFormat;
    int offset;
    int limit;
    AppDelegate *delegate;
    NSString *DatePickerSelectionStr;
    NSString *StartDateStr;
    NSString *EndDateStr;
    NSString *TrackID;
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *CancelFilterBtn;
    IBOutlet UIButton *ApplyFilterBtn;
    IBOutlet UIView *BottomView;
    NSString *TaskID;
    NSString *TaskTypeSelect;
    NSString *TaskRequestStatusSelect;
    NSString *RatingSelect;
    NSString *HistoryTaskRequestStatusSelect;
    NSDictionary *UserInfo;
    NSIndexPath *indexPath1;
    TaskIDCell *Cell1;
    NSUserDefaults *userDefault;
    
}
@end

@implementation MyTaskFilterViewController
@synthesize isComeFrom;

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    offset = 0;
    limit = 50;
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    TrackID = @"";
    TaskTypeSelect = @"";
    DatePickerSelectionStr = @"";
    TaskRequestStatusSelect = @"";
    RatingSelect = @"";
    HistoryTaskRequestStatusSelect = @"";
    
    CancelBtn.layer.borderWidth = 1.0;
    CancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    CancelBtn.layer.cornerRadius = 3.0;
    CancelBtn.clipsToBounds = YES;
    
    CancelFilterBtn.layer.borderWidth = 1.0;
    CancelFilterBtn.layer.borderColor = delegate.redColor.CGColor;
    CancelFilterBtn.layer.cornerRadius = 3.0;
    CancelFilterBtn.clipsToBounds = YES;
    CancelFilterBtn.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    
    ApplyFilterBtn.layer.cornerRadius = 3.0;
    BottomView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
     return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    if([isComeFrom isEqualToString:@"NEW"]){
        return 4;
    }
    else if([isComeFrom isEqualToString:@"ONGOING"]){
        return 3;
    }
    else if([isComeFrom isEqualToString:@"HISTORY"]){
        return 5;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *Cell;
    if([isComeFrom isEqualToString:@"NEW"])
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict = [userDefault objectForKey:isComeFrom];
        NSLog(@"%@",dict);
        if(indexPath.row == 0){
            DateCell * Cell = (DateCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            [Cell.StartDateButton addTarget:self action:@selector(StartDateAction) forControlEvents:UIControlEventTouchUpInside];
            [Cell.EndDateButton addTarget:self action:@selector(EndDateAction) forControlEvents:UIControlEventTouchUpInside];
            if (dict != nil)
            {
                if ([[dict valueForKey:@"eStartDate"] isEqualToString:@""] || [dict valueForKey:@"eStartDate"] == nil)
                {
                    if ([StartDateStr isEqualToString:@""])
                    {
                        Cell.StartDateLabel.text = StartDateStr;
//                        Cell.StartDateLabel.text = @"Start Date";
                    }
                    else
                    {
                        Cell.StartDateLabel.text = StartDateStr;
                    }
                    
                    if ([EndDateStr isEqualToString:@""])
                    {
                        Cell.EndDateLabel.text = EndDateStr;
//                        Cell.EndDateLabel.text = @"End Date";
                    }
                    else
                    {
                        Cell.EndDateLabel.text = EndDateStr;
                    }
                }
                else
                {
                    if ([StartDateStr isEqualToString:@""] || StartDateStr == nil)
                    {
                        Cell.StartDateLabel.text = [dict valueForKey:@"eStartDate"];
                        StartDateStr = Cell.StartDateLabel.text;
                    }
                    else
                    {
                        Cell.StartDateLabel.text = StartDateStr;
                    }
                    
                    if ([EndDateStr isEqualToString:@""] || EndDateStr == nil)
                    {
                        Cell.EndDateLabel.text = [dict valueForKey:@"eEndDate"];
                        EndDateStr = Cell.EndDateLabel.text;
                    }
                    else
                    {
                        Cell.EndDateLabel.text = EndDateStr;
                    }
                }
            }
            else
            {
                if ([StartDateStr isEqualToString:@""])
                {
                    Cell.StartDateLabel.text = StartDateStr;
//                    Cell.StartDateLabel.text = @"Start Date";
                }
                else
                {
                    Cell.StartDateLabel.text = StartDateStr;
                }
                
                if ([EndDateStr isEqualToString:@""])
                {
                    Cell.EndDateLabel.text = EndDateStr;
                }
                else
                {
                    Cell.EndDateLabel.text = EndDateStr;
                }
            }

            return Cell;
        }
        else if(indexPath.row == 1)
        {
            TaskIDCell * Cell = (TaskIDCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            Cell.TaskIDTextField.delegate = self;
//            Cell.TaskIDTextField.keyboardType = UIKeyboardTypeNumberPad;
            if (dict != nil)
            {
                if ([[dict valueForKey:@"taskId"] integerValue] == 0 || [dict valueForKey:@"taskId"] == nil)
                {
                    if ([TrackID isEqualToString:@""])
                    {
                        Cell.TaskIDTextField.text = TrackID;
                        Cell.TaskIDTextField.placeholder = @"Task ID";
                    }
                    else
                    {
                        Cell.TaskIDTextField.text = TrackID;
                    }
                }
                else
                {
                    if ([TrackID isEqualToString:@""])
                    {
                        if ([[dict valueForKey:@"taskId"] integerValue] == -1)
                        {
                            Cell.TaskIDTextField.text = [userDefault valueForKey:@"taskId"];
                        }
                        else
                        {
                            Cell.TaskIDTextField.text = [NSString stringWithFormat:@"%@", [dict valueForKey:@"taskId"]];
                        }
                       TrackID = Cell.TaskIDTextField.text;
                        
                    }
                    else
                    {
                        Cell.TaskIDTextField.text = TrackID;
                    }
                }
            }
            else
            {
                if ([TrackID isEqualToString:@""])
                {
                    Cell.TaskIDTextField.text = TrackID;
                    Cell.TaskIDTextField.placeholder = @"Task ID";
                }
                else
                {
                    Cell.TaskIDTextField.text = TrackID;
                }
            }
            return Cell;
        }
        else if(indexPath.row == 2)
        {
            TaskTypeCell * Cell = (TaskTypeCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
            if (dict != nil)
            {
                if ([[dict valueForKey:@"task_type"] isEqualToString:@"Both"] || [[dict valueForKey:@"task_type"] isEqualToString:@"both"] || [dict valueForKey:@"task_type"] == nil)
                {
                    if ([TaskTypeSelect isEqualToString:@""])
                    {
                        TaskTypeSelect = [dict valueForKey:@"task_type"];
                    }
                    else
                    {
                        NSLog(@"%@",TaskTypeSelect);
                    }
                }
                else
                {
                    if ([TaskTypeSelect isEqualToString:@""])
                    {
                        TaskTypeSelect = [dict valueForKey:@"task_type"];
                    }
                    else
                    {
                        NSLog(@"%@",TaskTypeSelect);
                    }
                }
            }
            else
            {
                if ([TaskTypeSelect isEqualToString:@""])
                {
                    TaskTypeSelect = @"both";
                }
                else
                {
                    
                }
            }
            if([TaskTypeSelect  isEqualToString:@"task"]){
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"assignments"]){
                
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"both"]){
                
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            [Cell.TaskButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.AssignmentButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.BothButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
        else
        {
            TaskRequestStatusCell * Cell = (TaskRequestStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
            if (dict != nil)
            {
                if ([[dict valueForKey:@"task_request_status"] isEqualToString:@"Both"] || [[dict valueForKey:@"task_request_status"] isEqualToString:@"both"] || [dict valueForKey:@"task_request_status"] == nil)
                {
                    if ([TaskRequestStatusSelect isEqualToString:@""])
                    {
                        TaskRequestStatusSelect = [dict valueForKey:@"task_request_status"];
                    }
                    else
                    {
                        NSLog(@"%@",TaskRequestStatusSelect);
                    }
                }
                else
                {
                    if ([TaskRequestStatusSelect isEqualToString:@""])
                    {
                        TaskRequestStatusSelect = [dict valueForKey:@"task_request_status"];
                    }
                    else
                    {
                        NSLog(@"%@",TaskRequestStatusSelect);
                    }
                }
            }
            else
            {
                if ([TaskRequestStatusSelect isEqualToString:@""])
                {
                    TaskRequestStatusSelect = @"Both";
                }
                else
                {
                    
                }
            }
            if([TaskRequestStatusSelect isEqualToString:@"Assigned"]){
                Cell.AssignedImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AssignedImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskRequestStatusSelect isEqualToString:@"New"]){
                
                Cell.NewImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.NewImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskRequestStatusSelect isEqualToString:@"Both"]){
                
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            
            [Cell.AssignedButton addTarget:self action:@selector(TaskRequestAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.NewButton addTarget:self action:@selector(TaskRequestAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.BothButton addTarget:self action:@selector(TaskRequestAction:) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
    }
    else if([isComeFrom isEqualToString:@"ONGOING"])
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict = [userDefault objectForKey:isComeFrom];
        NSLog(@"%@",dict);
        if(indexPath.row == 0){
            DateCell * Cell = (DateCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            
            [Cell.StartDateButton addTarget:self action:@selector(StartDateAction) forControlEvents:UIControlEventTouchUpInside];
            [Cell.EndDateButton addTarget:self action:@selector(EndDateAction) forControlEvents:UIControlEventTouchUpInside];
            if (dict != nil)
            {
                if ([[dict valueForKey:@"start_date"] isEqualToString:@""] || [dict valueForKey:@"start_date"] == nil)
                {
                    if ([StartDateStr isEqualToString:@""])
                    {
                        Cell.StartDateLabel.text = StartDateStr;
                    }
                    else
                    {
                        Cell.StartDateLabel.text = StartDateStr;
                    }
                    
                    if ([EndDateStr isEqualToString:@""])
                    {
                        Cell.EndDateLabel.text = EndDateStr;
                    }
                    else
                    {
                        Cell.EndDateLabel.text = EndDateStr;
                    }
                }
                else
                {
                    if ([StartDateStr isEqualToString:@""] || StartDateStr == nil)
                    {
                        Cell.StartDateLabel.text = [dict valueForKey:@"start_date"];
                        StartDateStr = Cell.StartDateLabel.text;
                    }
                    else
                    {
                        Cell.StartDateLabel.text = StartDateStr;
                    }
                    
                    if ([EndDateStr isEqualToString:@""] || EndDateStr == nil)
                    {
                        Cell.EndDateLabel.text = [dict valueForKey:@"end_date"];
                        EndDateStr = Cell.EndDateLabel.text;
                    }
                    else
                    {
                        Cell.EndDateLabel.text = EndDateStr;
                    }
                }
            }
            else
            {
                if ([StartDateStr isEqualToString:@""])
                {
                    Cell.StartDateLabel.text = StartDateStr;
                }
                else
                {
                    Cell.StartDateLabel.text = StartDateStr;
                }
                
                if ([EndDateStr isEqualToString:@""])
                {
                    Cell.EndDateLabel.text = EndDateStr;
                }
                else
                {
                    Cell.EndDateLabel.text = EndDateStr;
                }
            }
            return Cell;
        }
        else if(indexPath.row == 1){
            TaskIDCell * Cell = (TaskIDCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            Cell.TaskIDTextField.delegate = self;
            if (dict != nil)
            {
                if ([[dict valueForKey:@"taskId"] integerValue] == 0 || [dict valueForKey:@"taskId"] == nil)
                {
                    if ([TrackID isEqualToString:@""])
                    {
                        Cell.TaskIDTextField.text = TrackID;
                        Cell.TaskIDTextField.placeholder = @"Task ID";
                    }
                    else
                    {
                        Cell.TaskIDTextField.text = TrackID;
                    }
                }
                else
                {
                    if ([TrackID isEqualToString:@""])
                    {
                        if ([[dict valueForKey:@"taskId"] integerValue] == -1)
                        {
                            Cell.TaskIDTextField.text = [userDefault valueForKey:@"taskId"];
                        }
                        else
                        {
                            Cell.TaskIDTextField.text = [NSString stringWithFormat:@"%@", [dict valueForKey:@"taskId"]];
                        }
                        TrackID = Cell.TaskIDTextField.text;
                        
                    }
                    else
                    {
                        Cell.TaskIDTextField.text = TrackID;
                    }
                }
            }
            else
            {
                if ([TrackID isEqualToString:@""])
                {
                    Cell.TaskIDTextField.text = TrackID;
                    Cell.TaskIDTextField.placeholder = @"Task ID";
                }
                else
                {
                    Cell.TaskIDTextField.text = TrackID;
                }
            }
            return Cell;
        }
        else {
            TaskTypeCell * Cell = (TaskTypeCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
            if (dict != nil)
            {
                if ([[dict valueForKey:@"task_type"] isEqualToString:@"Both"] || [[dict valueForKey:@"task_type"] isEqualToString:@"both"] || [dict valueForKey:@"task_type"] == nil)
                {
                    if ([TaskTypeSelect isEqualToString:@""])
                    {
                        TaskTypeSelect = [dict valueForKey:@"task_type"];
                    }
                    else
                    {
                        NSLog(@"%@",TaskTypeSelect);
                    }
                }
                else
                {
                    if ([TaskTypeSelect isEqualToString:@""])
                    {
                        TaskTypeSelect = [dict valueForKey:@"task_type"];
                    }
                    else
                    {
                        NSLog(@"%@",TaskTypeSelect);
                    }
                }
            }
            else
            {
                if ([TaskTypeSelect isEqualToString:@""])
                {
                    TaskTypeSelect = @"both";
                }
                else
                {
                    
                }
            }
            if([TaskTypeSelect  isEqualToString:@"task"]){
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"assignments"]){
                
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"both"]){
                
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            [Cell.TaskButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.AssignmentButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.BothButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
        
    }
    else{
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict = [userDefault objectForKey:isComeFrom];
        NSLog(@"%@",dict);
        if(indexPath.row == 0)
        {
            DateCell * Cell = (DateCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            
            [Cell.StartDateButton addTarget:self action:@selector(StartDateAction) forControlEvents:UIControlEventTouchUpInside];
            [Cell.EndDateButton addTarget:self action:@selector(EndDateAction) forControlEvents:UIControlEventTouchUpInside];
            if (dict != nil)
            {
                if ([[dict valueForKey:@"start_date"] isEqualToString:@""] || [dict valueForKey:@"start_date"] == nil)
                {
                    if ([StartDateStr isEqualToString:@""])
                    {
                        Cell.StartDateLabel.text = StartDateStr;
                    }
                    else
                    {
                        Cell.StartDateLabel.text = StartDateStr;
                    }
                    
                    if ([EndDateStr isEqualToString:@""])
                    {
                        Cell.EndDateLabel.text = EndDateStr;
                    }
                    else
                    {
                        Cell.EndDateLabel.text = EndDateStr;
                    }
                }
                else
                {
                    if ([StartDateStr isEqualToString:@""] || StartDateStr == nil)
                    {
                        Cell.StartDateLabel.text = [dict valueForKey:@"start_date"];
                        StartDateStr = Cell.StartDateLabel.text;
                    }
                    else
                    {
                        Cell.StartDateLabel.text = StartDateStr;
                    }
                    
                    if ([EndDateStr isEqualToString:@""] || EndDateStr == nil)
                    {
                        Cell.EndDateLabel.text = [dict valueForKey:@"end_date"];
                        EndDateStr = Cell.EndDateLabel.text;
                    }
                    else
                    {
                        Cell.EndDateLabel.text = EndDateStr;
                    }
                }
            }
            else
            {
                if ([StartDateStr isEqualToString:@""])
                {
                    Cell.StartDateLabel.text = StartDateStr;
                }
                else
                {
                    Cell.StartDateLabel.text = StartDateStr;
                }
                
                if ([EndDateStr isEqualToString:@""])
                {
                    Cell.EndDateLabel.text = EndDateStr;
                }
                else
                {
                    Cell.EndDateLabel.text = EndDateStr;
                }
            }
            return Cell;
        }
        else if(indexPath.row == 1)
        {
            TaskIDCell * Cell = (TaskIDCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            Cell.TaskIDTextField.delegate = self;
            if (dict != nil)
            {
                if ([[dict valueForKey:@"taskId"] integerValue] == 0 || [dict valueForKey:@"taskId"] == nil)
                {
                    if ([TrackID isEqualToString:@""])
                    {
                        Cell.TaskIDTextField.text = TrackID;
                        Cell.TaskIDTextField.placeholder = @"Task ID";
                    }
                    else
                    {
                        Cell.TaskIDTextField.text = TrackID;
                    }
                }
                else
                {
                    if ([TrackID isEqualToString:@""])
                    {
                        if ([[dict valueForKey:@"taskId"] integerValue] == -1)
                        {
                            Cell.TaskIDTextField.text = [userDefault valueForKey:@"taskId"];
                        }
                        else
                        {
                            Cell.TaskIDTextField.text = [NSString stringWithFormat:@"%@", [dict valueForKey:@"taskId"]];
                        }
                        TrackID = Cell.TaskIDTextField.text;
                        
                    }
                    else
                    {
                        Cell.TaskIDTextField.text = TrackID;
                    }
                }
            }
            else
            {
                if ([TrackID isEqualToString:@""])
                {
                    Cell.TaskIDTextField.text = TrackID;
                    Cell.TaskIDTextField.placeholder = @"Task ID";
                }
                else
                {
                    Cell.TaskIDTextField.text = TrackID;
                }
            }
            return Cell;
        }
        else if(indexPath.row == 2)
        {
            TaskTypeCell * Cell = (TaskTypeCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
            if (dict != nil)
            {
                if ([[dict valueForKey:@"task_type"] isEqualToString:@"Both"] || [[dict valueForKey:@"task_type"] isEqualToString:@"both"] || [dict valueForKey:@"task_type"] == nil)
                {
                    if ([TaskTypeSelect isEqualToString:@""])
                    {
                        TaskTypeSelect = [dict valueForKey:@"task_type"];
                    }
                    else
                    {
                        NSLog(@"%@",TaskTypeSelect);
                    }
                }
                else
                {
                    if ([TaskTypeSelect isEqualToString:@""])
                    {
                        TaskTypeSelect = [dict valueForKey:@"task_type"];
                    }
                    else
                    {
                        NSLog(@"%@",TaskTypeSelect);
                    }
                }
            }
            else
            {
                if ([TaskTypeSelect isEqualToString:@""])
                {
                    TaskTypeSelect = @"both";
                }
                else
                {
                    
                }
            }
            if([TaskTypeSelect  isEqualToString:@"task"]){
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"assignments"]){
                
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"both"]){
                
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            [Cell.TaskButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.AssignmentButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.BothButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
        else if(indexPath.row == 3){
            HistoryRatingCell * Cell = (HistoryRatingCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL4" forIndexPath:indexPath];
            if (dict != nil)
            {
                if ([[dict valueForKey:@"rating"] isEqualToString:@"All"] || [[dict valueForKey:@"rating"] isEqualToString:@"all"] || [dict valueForKey:@"rating"] == nil)
                {
                    if ([RatingSelect isEqualToString:@""])
                    {
                        RatingSelect = [dict valueForKey:@"rating"];
                    }
                    else
                    {
                        NSLog(@"%@",RatingSelect);
                    }
                }
                else
                {
                    if ([RatingSelect isEqualToString:@""])
                    {
                        RatingSelect = [dict valueForKey:@"rating"];
                    }
                    else
                    {
                        NSLog(@"%@",RatingSelect);
                    }
                }
            }
            else
            {
                if ([RatingSelect isEqualToString:@""])
                {
                    RatingSelect = @"all";
                }
                else
                {
                    
                }
            }

            if([RatingSelect  isEqualToString:@"lowest"]){
                Cell.LowestImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.LowestImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([RatingSelect isEqualToString:@"highest"]){
                
                Cell.HighestImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.HighestImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([RatingSelect isEqualToString:@"all"]){
                
                Cell.AllImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AllImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            [Cell.LowestButton addTarget:self action:@selector(RatingAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.HighestButton addTarget:self action:@selector(RatingAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.AllButton addTarget:self action:@selector(RatingAction:) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
        else{
            HistoryTaskRequestStatusCell * Cell = (HistoryTaskRequestStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL5" forIndexPath:indexPath];
            if (dict != nil)
            {
                if ([[dict valueForKey:@"task_request_status"] isEqualToString:@"All"] || [[dict valueForKey:@"task_request_status"] isEqualToString:@"all"] || [dict valueForKey:@"task_request_status"] == nil)
                {
                    if ([HistoryTaskRequestStatusSelect isEqualToString:@""])
                    {
                        HistoryTaskRequestStatusSelect = [dict valueForKey:@"task_request_status"];
                    }
                    else
                    {
                        NSLog(@"%@",HistoryTaskRequestStatusSelect);
                    }
                }
                else
                {
                    if ([HistoryTaskRequestStatusSelect isEqualToString:@""])
                    {
                        HistoryTaskRequestStatusSelect = [dict valueForKey:@"task_request_status"];
                    }
                    else
                    {
                        NSLog(@"%@",HistoryTaskRequestStatusSelect);
                    }
                }
            }
            else
            {
                if ([HistoryTaskRequestStatusSelect isEqualToString:@""])
                {
                    HistoryTaskRequestStatusSelect = @"all";
                }
                else
                {
                    
                }
            }

            if([HistoryTaskRequestStatusSelect  isEqualToString:@"Approved"]){
                Cell.ApprovedImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.ApprovedImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([HistoryTaskRequestStatusSelect isEqualToString:@"Rejected"]){
                
                Cell.RejectedImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.RejectedImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([HistoryTaskRequestStatusSelect isEqualToString:@"declined"]){
                
                Cell.DecliendImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.DecliendImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([HistoryTaskRequestStatusSelect isEqualToString:@"all"]){
                
                Cell.AllImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AllImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            [Cell.ApprovedButton addTarget:self action:@selector(HistoryTaskRequestStatusAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.RejectedButton addTarget:self action:@selector(HistoryTaskRequestStatusAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.DecliendButton addTarget:self action:@selector(HistoryTaskRequestStatusAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.AllButton addTarget:self action:@selector(HistoryTaskRequestStatusAction:) forControlEvents:UIControlEventTouchUpInside];
            
            return Cell;
        }
    }
    return Cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([isComeFrom isEqualToString:@"HISTORY"]){
        if(indexPath.row == 4){
            return 103;
        }
    }
    return 69;
}

-(void)AskTypeAction:(UIButton*)sender
{
    switch (sender.tag) {
        case 0:
            TaskTypeSelect = @"task";
            [self ReloadAskTypeSection];
            break;
        case 1:
            TaskTypeSelect = @"assignments";
            [self ReloadAskTypeSection];
            break;
        case 2:
            TaskTypeSelect = @"both";
            [self ReloadAskTypeSection];
            break;
        default:
            break;
    }
}
-(void)ReloadAskTypeSection
{
    if([isComeFrom isEqualToString:@"NEW"]){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        [FilterTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else if([isComeFrom isEqualToString:@"ONGOING"]){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        [FilterTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else{
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        [FilterTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}



-(void)TaskRequestAction:(UIButton*)sender{
    switch (sender.tag) {
        case 0:
            TaskRequestStatusSelect = @"Assigned";
            [self ReloadTaskRequestSection];
            break;
        case 1:
            TaskRequestStatusSelect = @"New";
            [self ReloadTaskRequestSection];
            break;
        case 2:
            TaskRequestStatusSelect = @"Both";
            [self ReloadTaskRequestSection];
            break;
        default:
            break;
    }
}
-(void)ReloadTaskRequestSection
{
    if([isComeFrom isEqualToString:@"NEW"]){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        [FilterTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else if([isComeFrom isEqualToString:@"HISTORY"]){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        [FilterTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else{
    }
}



-(void)RatingAction:(UIButton*)sender{
    switch (sender.tag) {
        case 0:
            RatingSelect = @"lowest";
            [self ReloadRatingSection];
            break;
        case 1:
            RatingSelect = @"highest";
            [self ReloadRatingSection];
            break;
        case 2:
            RatingSelect = @"all";
            [self ReloadRatingSection];
            break;
        default:
            break;
    }
}
-(void)ReloadRatingSection
{
    if([isComeFrom isEqualToString:@"HISTORY"]){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        [FilterTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
//        [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else{
    }
}


-(void)HistoryTaskRequestStatusAction:(UIButton*)sender{
    switch (sender.tag) {
        case 0:
            HistoryTaskRequestStatusSelect = @"Approved";
            [self ReloadTaskRequestSection];
            break;
        case 1:
            HistoryTaskRequestStatusSelect = @"Rejected";
            [self ReloadTaskRequestSection];
            break;
        case 2:
            HistoryTaskRequestStatusSelect = @"declined";
            [self ReloadTaskRequestSection];
            break;
        case 3:
            HistoryTaskRequestStatusSelect = @"all";
            [self ReloadTaskRequestSection];
            break;

        default:
            break;
    }
}

-(void)StartDateAction{
    DatePickerSelectionStr = @"Start";
    DatePickerViewBg.hidden = NO;
    [self.view endEditing:YES];
}
-(void)EndDateAction{
    DatePickerSelectionStr = @"End";
    DatePickerViewBg.hidden = NO;
    [self.view endEditing:YES];
}
-(IBAction)CancelBtn:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)CancelFilterBtn:(id)sender
{
    [FilterTableView reloadData];
    if([isComeFrom isEqualToString:@"NEW"])
    {
        [userDefault removeObjectForKey:isComeFrom];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearFilteriPM" object:nil userInfo:nil];
    }
    else if([isComeFrom isEqualToString:@"ONGOING"])
    {
        [userDefault removeObjectForKey:isComeFrom];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearFilteriPM" object:nil userInfo:nil];
    }
    else if([isComeFrom isEqualToString:@"HISTORY"])
    {
        [userDefault removeObjectForKey:isComeFrom];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearFilteriPM" object:nil userInfo:nil];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)SubmitBtn:(id)sender{
    
    indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
    Cell1 = [FilterTableView cellForRowAtIndexPath:indexPath1];
    TrackID = Cell1.TaskIDTextField.text;
    if([StartDateStr isEqualToString:@"Start Date"]|| StartDateStr == nil){
        StartDateStr = @"";
    }
    if([EndDateStr isEqualToString:@"End Date"]|| EndDateStr == nil){
        EndDateStr = @"";
    }
    
    NSNumber *taskID;
    if ([self validateString:TrackID]){
        taskID = [NSNumber numberWithInteger:[TrackID integerValue]];
    }
    else
    {
        [userDefault setValue:TrackID forKey:@"taskId"];
        taskID = [NSNumber numberWithInt:-1];
    }
    
    
    if([isComeFrom isEqualToString:@"NEW"]){
        UserInfo = @ {
            @"eEndDate" : EndDateStr,
            @"eStartDate" :StartDateStr,
            @"taskId" :taskID,
            @"task_request_status": TaskRequestStatusSelect,
            @"task_type": TaskTypeSelect,
            @"employeeId" : [ApplicationState userId],
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        
        NSLog(@"UserInfo==%@",UserInfo);
        [userDefault setObject:UserInfo forKey:isComeFrom];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NEWTASK" object:nil userInfo:UserInfo];
    }
    else if([isComeFrom isEqualToString:@"ONGOING"]){
        UserInfo = @ {
            @"end_date" : EndDateStr,
            @"start_date" :StartDateStr,
            @"taskId" :taskID,
            @"task_type": TaskTypeSelect,
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        NSLog(@"UserInfo==%@",UserInfo);
        [userDefault setObject:UserInfo forKey:isComeFrom];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ONGOINGTASK" object:nil userInfo:UserInfo];
    }
    else if([isComeFrom isEqualToString:@"HISTORY"]){
        UserInfo = @ {
            @"end_date" : EndDateStr,
            @"start_date" :StartDateStr,
            @"taskId" :taskID,
            @"task_type": TaskTypeSelect,
            @"rating" : RatingSelect,
            @"task_request_status" : HistoryTaskRequestStatusSelect,
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        NSLog(@"UserInfo==%@",UserInfo);
        [userDefault setObject:UserInfo forKey:isComeFrom];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HISTORYTASK" object:nil userInfo:UserInfo];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)DatePickerDoneAction{
    DatePickerViewBg.hidden = YES;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    if([DatePickerSelectionStr isEqualToString:@"Start"]){
        StartDateStr = [dateFormat stringFromDate: DatePicker.date];
        EndDateStr = @"End Date";
    }
    else if([DatePickerSelectionStr isEqualToString:@"End"]){
        EndDateStr = [dateFormat stringFromDate: DatePicker.date];
    }
    [FilterTableView reloadData];
    if(![EndDateStr isEqualToString:@""]){
        NSComparisonResult result;
        NSDate *startDate = [dateFormat dateFromString:StartDateStr];
        NSDate *endDate = [dateFormat dateFromString:EndDateStr];
        result = [startDate compare:endDate];
        if(result==NSOrderedAscending){
            [self ReloadSection];
        }
        else if(result==NSOrderedDescending){
            EndDateStr = @"End Date";
            StartDateStr = @"Start Date";
            [self ShowAlert:@"You cannot select date earlier to start date" ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
    }
    [FilterTableView reloadData];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ReloadSection{
    [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
- (BOOL)validateString:(NSString *)string
{
    NSString *pattern = @"^[0-9]+$";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = NO;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = YES;
    
    return didValidate;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    DatePickerViewBg.hidden = YES;
}
@end
