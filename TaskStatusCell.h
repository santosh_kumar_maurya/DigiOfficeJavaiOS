//
//  TaskStatusCell.h
//  iWork
//
//  Created by Shailendra on 05/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
@interface TaskStatusCell : UITableViewCell{
    AppDelegate *delegate;
}

@property(nonatomic,strong)IBOutlet UIView *MyTaskStatusView;
@property(nonatomic,strong)IBOutlet UIView *MyTaskView;
@property(nonatomic,strong)IBOutlet UILabel *MyTaskStatusStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *CompleteStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *CompleteLabel;
@property(nonatomic,strong)IBOutlet UILabel *OnScheduleStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *OnScheduleLabel;
@property(nonatomic,strong)IBOutlet UILabel *BehindStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *BehindLabel;
@property(nonatomic,strong)IBOutlet UIImageView *ProfileImageView;
@property(nonatomic,strong)IBOutlet UILabel *UserNameLabel;
@property(nonatomic,strong)IBOutlet UILabel *DesignationLabel;
    


@end
