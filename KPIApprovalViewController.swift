//
//  KPIApprovalViewController.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class KPIApprovalViewController: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextViewDelegate,RejectionDelegate {
    
    @IBOutlet weak var KPIApprovalTableView: UITableView!
    @IBOutlet var RejectionOuterView: UIView!
    @IBOutlet var RejectTitleLabel: UILabel!
    @IBOutlet var RejectionTextView : UITextView!
    @IBOutlet var PopupView : UIView!
    @IBOutlet var PopupImage : UIImageView!
    @IBOutlet var PopupTitleLabel : UILabel!
    @IBOutlet var PopupDescriptionLabel : UILabel!
    @IBOutlet var NoDataView : UIView!
    @IBOutlet var NoDataLabel : UILabel!
    var delegate = AppDelegate()
    var ResponseDic : NSDictionary!
    var ResponseArray = NSArray()
    var refreshControl = UIRefreshControl()
    var Api: WebApiService =  WebApiService()
    var Dic : NSDictionary!
    var TagValue : Int = 0
    var CheckValue : String! = ""
    var KPIID : String = ""
    var TaskID : String = ""
    var SelectedArray = NSMutableArray()
    var SelectedIndex = NSMutableArray()
    var GateChildArray = NSMutableArray()
    var GateChildArray1 = NSMutableArray()
    var GateParentArray = [NSMutableArray]()
    var GateParentArray1 = [NSMutableArray]()
    
    var RejectionTextStr : String! = ""
    
    var GateStaticArrays = ["Gate 4","Gate 5","Gate 3","Gate 2","Gate 1"]
    var GateStaticArrays1 = ["Gate 1","Gate 2","Gate 3","Gate 4","Gate 5"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        IQKeyboardManager.shared().isEnabled = true
        RejectionTextView.layer.cornerRadius = 3
        RejectionTextView.layer.borderColor = UIColor.lightGray.cgColor
        RejectionTextView.layer.borderWidth = 1.0
        RejectTitleLabel.textColor = UIColor.TextBlueColor()
        delegate = UIApplication.shared.delegate as! AppDelegate
        KPIApprovalTableView.estimatedRowHeight = 50;
        KPIApprovalTableView.rowHeight = UITableViewAutomaticDimension;
        
        refreshControl.addTarget(self, action: #selector(KPIApprovalViewController.reloadData), for: UIControlEvents.valueChanged)
        KPIApprovalTableView.addSubview(refreshControl)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //LoadingManager.showLoading(self.view)
        self.perform(#selector(KPIApprovalViewController.KPIApprovalWebApi), with: nil, afterDelay: 0.2)
    }
    func reloadData(){
         self.perform(#selector(KPIApprovalViewController.KPIApprovalWebApi), with: nil, afterDelay: 0.2)
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return ResponseArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let Cell : KPIApprovalTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "CELL", for: indexPath) as! KPIApprovalTableViewCell
        Cell.ApprovedBtn.tag = indexPath.row
        Cell.RejectedBtn.tag = indexPath.row
        Cell.btnViewMoreOrLess.tag = indexPath.row
        if(ResponseArray.count > 0){
            Dic = ResponseArray[indexPath.row] as! NSDictionary
            Cell.CellConfigure(info: Dic)
    
            if(SelectedIndex.contains(SelectedArray[indexPath.row])){
                Cell.GateStaticArray = GateStaticArrays1 as! NSMutableArray
                Cell.GateValueArray = GateParentArray1[indexPath.row]
                Cell.btnViewMoreOrLess.setImage(UIImage(named: "TargetUp"), for: UIControlState.normal)
                Cell.Hight.constant = CGFloat(35 * Cell.GateValueArray.count)
                Cell.TargetTableView.backgroundColor = UIColor.RedColor()
                
            }
            else{
                Cell.GateStaticArray = GateStaticArrays as! NSMutableArray
                Cell.GateValueArray = GateParentArray[indexPath.row]
                Cell.btnViewMoreOrLess.setImage(UIImage(named: "TargetDown"), for: UIControlState.normal)
                Cell.Hight.constant = 35
            }
            Cell.TargetTableView.reloadData()
        }
        return Cell
    }
    func KPIApprovalWebApi(){
        if (delegate.isInternetConnected()){
            
            let userId = UserDefaults.standard.string(forKey: "kUserId")
            let param : NSDictionary! = ["status": "1", "userId": userId!]
            ResponseDic = Api.webApi(param as! [AnyHashable : Any]!, url: "getKpiApprovalList") as NSDictionary! // status 1 for approval
            refreshControl.endRefreshing()
           NoDataView.isHidden = true
            if ResponseDic != nil{
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    LoadingManager.hideLoading(view)
                    ResponseArray = ResponseDic.value(forKey: "object") as! NSArray
                    if(ResponseArray.count==0){
                        NodataFound()
                    }
                    for i in 0 ..< ResponseArray.count {
                        SelectedArray.add("\(i)")
                        GateChildArray = NSMutableArray()
                        GateChildArray1 = NSMutableArray()
                        let Dic = ResponseArray[i] as! NSDictionary;
                        let Gete1 = Dic.value(forKey: "gate1") as! String
                        let Gete2 = Dic.value(forKey: "gate2") as! String
                        let Gete3 = Dic.value(forKey: "gate3") as! String
                        let Gete4 = Dic.value(forKey: "gate4") as! String
                        let Gete5 = Dic.value(forKey: "gate5") as! String
                        GateChildArray.add("\(Gete4)")
                        GateChildArray.add("\(Gete5)")
                        GateChildArray.add("\(Gete3)")
                        GateChildArray.add("\(Gete2)")
                        GateChildArray.add("\(Gete1)")
                        GateParentArray.append(GateChildArray)
                        
                        GateChildArray1.add("\(Gete1)")
                        GateChildArray1.add("\(Gete2)")
                        GateChildArray1.add("\(Gete3)")
                        GateChildArray1.add("\(Gete4)")
                        GateChildArray1.add("\(Gete5)")
                        GateParentArray1.append(GateChildArray1)
                    }
                    self.KPIApprovalTableView.reloadData()
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: ResponseDic.value(forKey: "message")))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(KPIApprovalViewController.KPIApprovalWebApi), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func NodataFound(){
        if(ResponseArray.count == 0){
            NoDataView.isHidden = false
            NoDataLabel.isHidden = false
            NoDataLabel.text = "No record found"
        }
    }
    func ApprovedApiWebApi(){
        if (delegate.isInternetConnected()){
            
            let userId = UserDefaults.standard.string(forKey: "kUserId")
            var param : NSDictionary!
            if(CheckValue == "APPROVE"){
                 param = ["kpiId": "\(KPIID)", "userId": userId!, "rejectionRemarks": "", "status": "1"]
            }
            else if (CheckValue == "REJECT") {
               param = ["kpiId": "\(KPIID)", "userId": userId!, "rejectionRemarks": "\(RejectionTextStr!)", "status": "2"]
            }
            ResponseDic = Api.webApi(param as! [AnyHashable : Any]!, url: "actionByManagerOnKpi") as NSDictionary!
           
            if ResponseDic != nil{
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    RejectionOuterView.isHidden = true
                    LoadingManager.hideLoading(view)
                    if(CheckValue == "APPROVE"){
                        self.ShowAlertApprove(MsgTitle: "APPROVED", MsgDiscription: "\(ResponseDic.value(forKey: "message")!)" as NSString, ImageName: "ApprovedKpi", OpenViewName: "CONFORMVIEW");
                        
                        
//                        PopupView.isHidden = false
//                        CustomPopView().AddCustomPopup(CustomView: PopupView)
//                        PopupImage.image = UIImage(named: "ApprovedKpi")
//                        PopupTitleLabel.text = "APPROVED"
//                        PopupDescriptionLabel.text = "\(ResponseDic.value(forKey: "message")!)"
                    }
                    else if (CheckValue == "REJECT"){
 //                       PopupImage.image = UIImage(named: "RejectedKpi")
//                        PopupView.isHidden = false
//                        CustomPopView().AddCustomPopup(CustomView: PopupView)
//                        PopupTitleLabel.text = "REJECTED"
//                        PopupDescriptionLabel.text = "\(ResponseDic.value(forKey: "message")!)"
                    self.ShowAlertApprove(MsgTitle: "REJECTED", MsgDiscription: "\(ResponseDic.value(forKey: "message")!)" as NSString, ImageName: "ApprovedKpi", OpenViewName: "CONFORMVIEW");
                        
                    }
                    self.perform(#selector(KPIApprovalViewController.KPIApprovalWebApi), with: nil, afterDelay: 0.5)
                    self.KPIApprovalTableView.reloadData()
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: ResponseDic.value(forKey: "message")))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(KPIApprovalViewController.ApprovedApiWebApi), with: nil, afterDelay: 0.1)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    @IBAction func CrossCancelAction(){
        RejectionOuterView.isHidden = true
        RejectionTextView.text = ""
    }
    
    @IBAction func ApprovedAction(sender:UIButton){
        TagValue = sender.tag
        Dic = ResponseArray[TagValue] as! NSDictionary
        KPIID = "\(String(describing: Dic["kpiId"]!))"
        CheckValue = "APPROVE"
        LoadingManager.hideLoading(view)
        self.perform(#selector(KPIApprovalViewController.ApprovedApiWebApi), with: nil, afterDelay: 0.1)
    }
    @IBAction func RejectAction(sender:UIButton){
        
        
       // RejectionOuterView.isHidden = false

        TagValue = sender.tag
        Dic = ResponseArray[TagValue] as! NSDictionary
        KPIID = "\(String(describing: Dic["kpiId"]!))"
        self.ShowAlertApprove(MsgTitle: "", MsgDiscription: "", ImageName: "", OpenViewName: "REJECTVIEW");
        //CustomPopView().AddCustomPopup(CustomView: RejectionOuterView)
    }
    func RejectionWebApiCall(RejectionText: String) {
        CheckValue = "REJECT"
        RejectionTextStr = RejectionText
        print("RejectionTextStr=====\(RejectionTextStr!)")
        LoadingManager.hideLoading(view)
        self.perform(#selector(KPIApprovalViewController.ApprovedApiWebApi), with: nil, afterDelay: 0.1)
    }
    @IBAction func SubmitAction(sender:UIButton){
        if(RejectionTextView.text == ""){
            self.ShowAlert(MsgTitle: "Please enter rejection", BtnTitle: "OK")
        }
        else{
            LoadingManager.hideLoading(view)
            self.perform(#selector(KPIApprovalViewController.ApprovedApiWebApi), with: nil, afterDelay: 0.1)
        }
    }
    @IBAction func PopupOkayAction(){
        PopupView.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnViewMoreOrLessTapped(sender: UIButton){
        let Tagvalue = sender.tag;
        if(SelectedIndex.contains("\(Tagvalue)")){
            SelectedIndex.remove("\(Tagvalue)")
        }
        else{
            SelectedIndex.add("\(Tagvalue)")
        }
        KPIApprovalTableView.reloadData()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: false, completion: nil)
    }
    func ShowAlertApprove(MsgTitle:NSString,MsgDiscription:NSString,ImageName:NSString,OpenViewName:NSString){
        
        let ObjApproveController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "ApprovePopUpViewController") as! ApprovePopUpViewController
        ObjApproveController.TitleStr = MsgTitle as String!;
        ObjApproveController.DiscriptionStr = MsgDiscription as String!
        ObjApproveController.ImageNameStr = ImageName as String!
        ObjApproveController.NameOpenView = OpenViewName as String!
        ObjApproveController.delegate = self
        ObjApproveController.view.backgroundColor = UIColor.clear
        ObjApproveController.providesPresentationContextTransitionStyle = true
        ObjApproveController.definesPresentationContext = true
        ObjApproveController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjApproveController, animated: false, completion: nil)
    }
}
