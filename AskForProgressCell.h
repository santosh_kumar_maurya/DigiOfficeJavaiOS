//
//  AskForProgressCell.h
//  iWork
//
//  Created by Shailendra on 08/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AskForProgressCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *TaskView;
@property (weak, nonatomic) IBOutlet UILabel *TaskIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *DateLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *DurationStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *DurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *CreatedByLabel;
@property (weak, nonatomic) IBOutlet UIButton *StartCancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *DetailsBtn;
@property (weak, nonatomic) IBOutlet UIImageView *StatusImageView;
@property (weak, nonatomic) IBOutlet UILabel *StatusLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *StatusLabelHeightConstraints;

- (void)configureCell:(NSDictionary *)info;

@end
