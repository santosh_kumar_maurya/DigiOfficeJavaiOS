//
//  TeamMemberViewController.swift
//  iWork
//
//  Created by Shailendra on 26/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class TeamMemberViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,RecognitionTableViewCellProtocol {

  
    @IBOutlet var TeamMemberTableView: UITableView!
    @IBOutlet var HeaderLabel : UILabel!
    var recognitionDic : NSDictionary!
    var popUpResponseDict : NSDictionary!
    var ResponseDic : NSDictionary!
    var strRewardPoints : NSString! = ""
    var Api: WebApiService =  WebApiService()
    var refreshControl = UIRefreshControl()
    var delegate = AppDelegate()
    var averageFeedback : NSNumber!
    var KPIArray = NSArray()
    var EmployeeDic : NSDictionary!
    var ResponseSkillArrays = NSArray()
    var ResponseValueArrays = NSArray()
   
    override func viewDidLoad() {

        delegate = UIApplication.shared.delegate as! AppDelegate
        super.viewDidLoad()
        TeamMemberTableView.estimatedRowHeight = 150;
        TeamMemberTableView.backgroundColor = UIColor.white
        TeamMemberTableView.rowHeight = UITableViewAutomaticDimension;
       
        TeamMemberTableView.register(UINib(nibName: "RecognitionTableViewCell", bundle: nil), forCellReuseIdentifier: "RecognitionTableViewCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        LoadingManager.showLoading(self.view)
        self.perform(#selector(TeamMemberViewController.GetAverageFeedback), with: nil, afterDelay: 0.5)
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(ResponseSkillArrays.count == 0 && ResponseValueArrays.count == 0){
            return 4
        }
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let Cell : TaskStatusCell = tableView.dequeueReusableCell(
                withIdentifier: "CELL", for: indexPath) as! TaskStatusCell
             Cell.userNameLabel.text = "\(String(describing: EmployeeDic.value(forKey: "empName")!))"
             Cell.designationLabel.text = "\(String(describing: EmployeeDic.value(forKey: "designation")!))"
            Cell.completeLabel.text = "\(String(describing: EmployeeDic.value(forKey: "taskCompleted")!))"
            Cell.onScheduleLabel.text = "\(String(describing: EmployeeDic.value(forKey: "taskOnSchedule")!))"
             Cell.behindLabel.text = "\(String(describing: EmployeeDic.value(forKey: "taskBehind")!))"
            return Cell
        }
        else if(indexPath.row == 1){
            let Cell : EmployeeAverageFeedbacksCell = tableView.dequeueReusableCell(
                withIdentifier: "CELL1", for: indexPath) as! EmployeeAverageFeedbacksCell
            if(averageFeedback != nil){
                let FloatValue = averageFeedback
                Cell.RatingView.value = FloatValue as! CGFloat
                Cell.RatingView.tintColor = UIColor.NeedRevisionBtnColor()
            }
            else{
                Cell.RatingView.value = 0.0
            }
            return Cell
        }
        else if(indexPath.row == 2){
            let Cell : KPIDetailsCell = tableView.dequeueReusableCell(
                withIdentifier: "CELL2", for: indexPath) as! KPIDetailsCell
            if(KPIArray.count>0){
                Cell.KPIArrays = KPIArray
                Cell.KPITableView.reloadData()
                Cell.hight.constant = CGFloat(36 * KPIArray.count)
            }
            return Cell
        }
        else if(indexPath.row == 3){
            if(ResponseSkillArrays.count == 0 && ResponseValueArrays.count == 0){
                let Cell : PointRewardsCell = tableView.dequeueReusableCell(
                    withIdentifier: "CELL4", for: indexPath) as! PointRewardsCell
                if(strRewardPoints != ""){
                    Cell.TotalRewardsPointsLabel.text = "\(strRewardPoints!)"
                }
                else{
                    Cell.TotalRewardsPointsLabel.text = " "
                }
                return Cell
            }
            else{
                let Cell : RecognitionTableViewCell = tableView.dequeueReusableCell(
                    withIdentifier: "RecognitionTableViewCell", for: indexPath) as! RecognitionTableViewCell
                Cell.delegate = self;
                Cell.cellType = "Line";
                if(recognitionDic.count>0 || recognitionDic != nil){
                    Cell.configureCellWithData(dict: recognitionDic!)
                }
                return Cell
            }
        }
        else {
            let Cell : PointRewardsCell = tableView.dequeueReusableCell(
                withIdentifier: "CELL4", for: indexPath) as! PointRewardsCell
            if("\(strRewardPoints!)" != ""){
              Cell.TotalRewardsPointsLabel.text = "\(strRewardPoints!)"
            }
            else{
               Cell.TotalRewardsPointsLabel.text = " "
            }
            return Cell
        }
    }
    @IBAction func CompleteTaskAction(){
        let viewController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "EmployeeDetailsParentViewController") as! EmployeeDetailsParentViewController
        viewController.dics = EmployeeDic as! [AnyHashable : Any]!
        viewController.taskType = "COMPLETETASK"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func OnScheduleTaskAction(){
        let viewController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "EmployeeDetailsParentViewController") as! EmployeeDetailsParentViewController
        viewController.dics = EmployeeDic as! [AnyHashable : Any]!;
        viewController.taskType = "ONSCHUDLETASK"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func BehindTaskAction(){
        let viewController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "EmployeeDetailsParentViewController") as! EmployeeDetailsParentViewController
        viewController.dics = EmployeeDic as! [AnyHashable : Any]!;
        viewController.taskType = "BEHINDTASK"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func AverageFeedbackAction(){
        let viewController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "AverageFeedbackViewController") as! AverageFeedbackViewController
        viewController.EmployeeID = "\(String(describing: EmployeeDic.value(forKey: "empId")!))"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func PointRewardAction(){
//        let viewController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "PointRewardsViewController") as! PointRewardsViewController
//        viewController.totalRewardsPoint = "\(strRewardPoints!)"
//        viewController.EmployeeID = "\(String(describing: EmployeeDic.value(forKey: "empId")!))"
//        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func GiveRecognitionAction(){
        let viewController = UIStoryboard(name: "PMS", bundle: nil).instantiateViewController(withIdentifier: "GiveRecognitionsViewController") as! GiveRecognitionsViewController
         viewController.EmployeeID = "\(String(describing: EmployeeDic.value(forKey: "empId")!))"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func BackAction(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func HomeAction(){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContainerParentViewController") as! ContainerParentViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    func cellTapped(selectedIndex: Int, selectedCell: Int) {
        if(selectedIndex == 0){
            let SkillArrays = (recognitionDic["skillsData"] as! NSArray).value(forKey: "id") as! NSArray
            let IdNumber = SkillArrays.object(at: selectedCell)
            callWebServiceForSkills(sender: "\(IdNumber)" as NSString) // skill
        }
        else if(selectedIndex == 1){
            let ValuesArrays = (recognitionDic["valuesData"] as! NSArray).value(forKey: "id") as! NSArray
             let IdNumber = ValuesArrays.object(at: selectedCell)
            callWebServiceForValues(sender: "\(IdNumber)" as NSString) // value
        }
        if(popUpResponseDict.count>0){
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecognitionPopUpViewController") as! RecognitionPopUpViewController
            
            viewController.responseDict = popUpResponseDict;
            if (selectedIndex == 0){
                viewController.title = "Skills";
            }
            else{
                viewController.title = "Values";
            }
            viewController.providesPresentationContextTransitionStyle = true
            viewController.definesPresentationContext = true
            viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(viewController, animated: false, completion: nil)
        }
    }
    func callWebServiceForSkills(sender:NSString){
        
        if (delegate.isInternetConnected()){
            popUpResponseDict = Api.webApi(nil, url: NSString(format: "getSkillUsersData?id=%@","\(sender)&showTeamData=true&employeeId=\(String(describing: EmployeeDic.value(forKey: "empId")!))") as String!) as NSDictionary!
            print("getSkillUsersData==\(String(describing: popUpResponseDict))")
            if(popUpResponseDict==nil){
                LoadingManager.hideLoading(view)
            }
            else if(popUpResponseDict.value(forKey: "statusCode") as! Int == 5 || popUpResponseDict.value(forKey: "statusCode") as! Int == 601){
                popUpResponseDict = popUpResponseDict.value(forKey: "object") as! NSDictionary
            }
            else{
               ShowAlert(MsgTitle: "\(String(describing: popUpResponseDict.value(forKey: "message")!))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
            }
        }
        else{
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func callWebServiceForValues(sender:NSString){
        
        if (delegate.isInternetConnected()){
            popUpResponseDict = Api.webApi(nil, url: NSString(format: "getValueUsersData?id=%@","\(sender)&showTeamData=true&employeeId=\(String(describing: EmployeeDic.value(forKey: "empId")!))") as String!) as NSDictionary!
            print("getSkillUsersData==\(String(describing: popUpResponseDict))")
            
            if(popUpResponseDict==nil){
                LoadingManager.hideLoading(view)
            }
            else if(popUpResponseDict.value(forKey: "statusCode") as! Int == 5 || popUpResponseDict.value(forKey: "statusCode") as! Int == 601){
                popUpResponseDict = popUpResponseDict.value(forKey: "object") as! NSDictionary
                
            }
            else{
               ShowAlert(MsgTitle: "\(String(describing: popUpResponseDict.value(forKey: "message")))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
            }
        }
        else{
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
        
    }
    func  WebApiCellForRecognition(){
        
        if (delegate.isInternetConnected()){
            //let userId = UserDefaults.standard.string(forKey: "kUserId")
            recognitionDic = Api.webApi(nil, url: "getRecongnitions?userId=\("\(String(describing: EmployeeDic.value(forKey: "empId")!))")") as NSDictionary!
            if recognitionDic != nil {
                print("recognitionDic==",recognitionDic);
                if(recognitionDic.value(forKey: "statusCode") as! Int == 5 || recognitionDic.value(forKey: "statusCode") as! Int == 602){
                    LoadingManager.hideLoading(view)
                    let responseObject = recognitionDic.value(forKey: "object") as! NSDictionary
                    let recognitionData = responseObject.value(forKey: "recognitionData") as! NSDictionary
                    recognitionDic = recognitionData;
                    
                    ResponseSkillArrays = recognitionDic.value(forKey: "skillsData") as! NSArray
                    ResponseValueArrays = recognitionDic.value(forKey: "valuesData") as! NSArray
                    
                    let Points  = responseObject.value(forKey: "rewardpoints") as! NSNumber
                    strRewardPoints = NSString(format: "%@", Points)
                    self.TeamMemberTableView.reloadData()
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: recognitionDic.value(forKey: "message")))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }
            else{
               self.perform(#selector(TeamMemberViewController.WebApiCellForRecognition), with: nil, afterDelay: 0.2)
            }
            
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func  GetAverageFeedback(){
        
        if (delegate.isInternetConnected()){
            //let userId = UserDefaults.standard.string(forKey: "kUserId")
            let param : NSDictionary! = ["employeeId": "\(String(describing: EmployeeDic.value(forKey: "empId")!))","status": "0"]// status 1 for avarage on team member
            ResponseDic = Api.webApi(param as! [AnyHashable : Any]!, url: "getTeamAverageFeedback") as NSDictionary!
            refreshControl.endRefreshing()
            if ResponseDic != nil{
                LoadingManager.hideLoading(view)
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    if (ResponseDic["object"] as? NSArray) != nil{
                        let ObjectArray = ResponseDic.value(forKey: "object") as! NSArray
                        let Dic = ObjectArray[0] as! NSDictionary
                        averageFeedback = Dic.value(forKey: "averageFeedback") as! NSNumber
                    }
                    self.TeamMemberTableView.reloadData()
                    self.perform(#selector(TeamMemberViewController.GetKPIWebApi), with: nil, afterDelay: 0.2)
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: ResponseDic.value(forKey: "message")))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }
            else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(TeamMemberViewController.GetAverageFeedback), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func GetKPIWebApi()
    {
        if (delegate.isInternetConnected()){
           // let userId = UserDefaults.standard.string(forKey: "kUserId")
            ResponseDic = Api.webApi(nil, url:"getKPI?user_id=\("\(String(describing: EmployeeDic.value(forKey: "empId")!))")") as NSDictionary!
            
            if ResponseDic != nil{
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    LoadingManager.hideLoading(view)
                    let Dic = ResponseDic.value(forKey: "object") as! NSDictionary
                    KPIArray = Dic.value(forKey: "kpi") as! NSArray
                    self.TeamMemberTableView.reloadData()
                 self.perform(#selector(TeamMemberViewController.WebApiCellForRecognition), with: nil, afterDelay: 0.2)
                    
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: popUpResponseDict.value(forKey: "message")))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }
            else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(TeamMemberViewController.GetKPIWebApi), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: false, completion: nil)
    }
}
