//
//  PointRewardsViewController.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit


class PointRewardsViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var PointRewardsTableView: UITableView!
    var ObjAppDelegate = AppDelegate()
    var ResponseDic : NSDictionary!
    var PointsArray  = NSArray()
    var refreshControl = UIRefreshControl()
    var Api: WebApiService =  WebApiService()
    var totalRewardsPoint : String! = ""
    var Dic : NSDictionary!
    var EmployeeID : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ObjAppDelegate = UIApplication.shared.delegate as! AppDelegate
        PointRewardsTableView.estimatedRowHeight = 50;
        PointRewardsTableView.backgroundColor = UIColor.clear;
        PointRewardsTableView.rowHeight = UITableViewAutomaticDimension;
        let Nib = UINib.init(nibName: "RewardPointsCell", bundle: nil)
        PointRewardsTableView.register(Nib, forCellReuseIdentifier: "RewardPointsCell")
        refreshControl.addTarget(self, action: #selector(PointRewardsViewController.reloadData), for: UIControlEvents.valueChanged)
        PointRewardsTableView.addSubview(refreshControl)
        LoadingManager.showLoading(self.view)
        self.perform(#selector(PointRewardsViewController.PointRewardsWebApi), with: nil, afterDelay: 0.5)
    }
    func reloadData(){
        self.perform(#selector(PointRewardsViewController.PointRewardsWebApi), with: nil, afterDelay: 0.5)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(PointsArray.count > 0 ){
            return 2
        }
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let Cell : RewardPointsCell = tableView.dequeueReusableCell(
                withIdentifier: "RewardPointsCell", for: indexPath) as! RewardPointsCell
            Cell.headerTitleLabel.text = NSLocalizedString("TOTAL_REWARD_POINTS", comment: "") as String
            Cell.pointLabel.text = "\(totalRewardsPoint!)"
            return Cell
        }
        else{
            let Cell : TotalRewardDetailsCell = tableView.dequeueReusableCell(
                withIdentifier: "CELL1", for: indexPath) as! TotalRewardDetailsCell
            if(PointsArray.count>0){
                Cell.RewordPointsArray = PointsArray
                Cell.PointTableView.reloadData()
                Cell.hight.constant = CGFloat(50 * PointsArray.count)
            }
            return Cell
        }
    }
    func PointRewardsWebApi(){
        let userId = UserDefaults.standard.string(forKey: "kUserId")
        if (ObjAppDelegate.isInternetConnected()){
            ResponseDic = Api.webApi(nil, url: "getPointRewardsDetail?employeeID=\(userId!)") as NSDictionary!
            refreshControl.endRefreshing()
            if ResponseDic != nil{
                LoadingManager.hideLoading(view)
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    PointsArray = ResponseDic.value(forKey: "object") as! NSArray
                    self.PointRewardsTableView.reloadData()
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: ResponseDic.value(forKey: "message")))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }
            else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(PointRewardsViewController.PointRewardsWebApi), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    @IBAction func BackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func HomeAction(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContainerParentViewController") as! ContainerParentViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: false, completion: nil)
    }
}
