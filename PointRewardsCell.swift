//
//  PointRewardsCell.swift
//  iWork
//
//  Created by Shailendra on 26/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class PointRewardsCell: UITableViewCell {

    @IBOutlet var CustomView: UIView!
    @IBOutlet var TotalRewardsPointsLabel: UILabel!
    @IBOutlet var TotalRewardsPointsValueLabel: UILabel!
    @IBOutlet var RewardsPointsStaticLabel: UILabel!
    @IBOutlet var GiveRecoginitionStaticLabel: UILabel!
    @IBOutlet var GiveRecoginitionBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        CustomView.layer.cornerRadius = 2.0
        CustomView.mask?.layer.cornerRadius = 7.0
        CustomView.layer.shadowRadius = 3.0;
        CustomView.layer.shadowColor = UIColor.black.cgColor
        CustomView.layer.shadowOffset = CGSize(width: -1.0, height: 2.0)
        CustomView.layer.shadowOpacity = 0.7
        CustomView.layer.masksToBounds = false
        
        RewardsPointsStaticLabel.textColor = UIColor.TextBlueColor()
        TotalRewardsPointsLabel.textColor = UIColor.TextBlueColor()
        GiveRecoginitionStaticLabel.textColor = UIColor.TextBlueColor()
        TotalRewardsPointsValueLabel.textColor = UIColor.TextBlueColor()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
