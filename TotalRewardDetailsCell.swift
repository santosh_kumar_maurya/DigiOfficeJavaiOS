//
//  TotalRewardDetailsCell.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class TotalRewardDetailsCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var PointTableView : UITableView!
    @IBOutlet var CustomView : UIView!
    var RewordPointsArray  = NSArray()
    @IBOutlet var DetailsStaticLabel: UILabel!
    @IBOutlet var hight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CustomView.layer.cornerRadius = 2.0
        CustomView.layer.shadowRadius = 3.0
        CustomView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        CustomView.layer.shadowOpacity = 0.7
        CustomView.layer.masksToBounds = false
        DetailsStaticLabel.textColor = UIColor.TextBlueColor()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(RewordPointsArray.count>0){
            return RewordPointsArray.count;
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell : PointsCell = tableView.dequeueReusableCell(
            withIdentifier: "CELL", for: indexPath) as! PointsCell
        if(RewordPointsArray.count>0){
            let info = RewordPointsArray[indexPath.row] as! NSDictionary
            if (info["points"] as? NSNumber) != nil{
                Cell.PointLabel.text = "\(String(describing: info["points"]!)) Points"
            }
            else{
                Cell.PointLabel.text = " "
            }
            if (info["pointDate"] as? NSNumber) != nil{
                let Number = info["pointDate"] as! NSNumber
                Cell.DateLabel.text = GetDate(datenumber: Number)
            }
            else{
                Cell.DateLabel.text = " "
            }
        }
        return Cell
    }
    func GetDate(datenumber:NSNumber) -> String{
        let DateStr = NSString(format: "%@", datenumber)
        let miliSec = Double(DateStr as String)
        let TakeOffDate = NSDate(timeIntervalSince1970: miliSec!/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateStr = dateFormatter.string(from: TakeOffDate as Date)
        let Newdate = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter.string(from: Newdate!)
    }
}
