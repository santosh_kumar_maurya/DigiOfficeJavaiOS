//
//  HistoryViewController.m
//  iWork
//
//  Created by Shailendra on 08/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "HistoryViewController.h"
#import "Header.h"
@interface HistoryViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *HistoryTableView;
    NSMutableArray *HistoryArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
    IBOutlet UIView *NoDataView;
}

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [self EmptyArray];
    [self clearfilterUserdefaultData];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    HistoryTableView.estimatedRowHeight = 50;
    HistoryTableView.rowHeight = UITableViewAutomaticDimension;
    HistoryTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, HistoryTableView.bounds.size.width, 10.f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [HistoryTableView addSubview:refreshControl];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"HISTORYTASK" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ClearFilterData:) name:@"ClearFilteriPM" object:nil];
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [HistoryTableView registerNib:Nib forCellReuseIdentifier:@"CELL"];
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(HistoryWebApi) withObject:nil afterDelay:0.5];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   }
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    HistoryArray = [[NSMutableArray alloc] init];
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(HistoryWebApi) withObject:nil afterDelay:0.5];
}
-(void)ClearFilterData:(NSNotification*)notification{
    CheckValue = @"";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(HistoryWebApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData{
    if(![CheckValue isEqualToString:@"FILTER"]){
        CheckValue = @"Refresh";
        [self HistoryWebApi];
    }
    [self EmptyArray];
}
- (void)HistoryWebApi {
    if(delegate.isInternetConnected){
        if(![CheckValue isEqualToString:@"FILTER"]){
            params = @ {
                @"end_date" : @"",
                @"start_date" :@"",
                @"taskId" :@"",
                @"task_type": @"",
                @"task_request_status" : @"",
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"viewAllHistoryDashTask?employeeId=%@",[ApplicationState userId]]];
        [refreshControl endRefreshing];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self HistoryWebApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            NSLog(@"HistoryResponseDic==%@",ResponseDic);
            [LoadingManager hideLoadingView:self.view];
            NoDataView.hidden = YES;
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    [HistoryTableView reloadData];
                }
                else if(ResponseArrays.count == 0 && HistoryArray.count == 0){
                    [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(HistoryArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Record Found";
    }
    [HistoryTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self HistoryWebApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [HistoryArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [HistoryArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.tag = indexPath.row;
    Cell.ViewDetailsBtn.tag = indexPath.row;
    [Cell.ViewDetailsBtn addTarget:self action:@selector(GotoEmployeeDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if(HistoryArray.count>0){
        NSDictionary * responseData = [HistoryArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
        
        NSString *statusType;
        NSString *imageType;
        if ([[responseData objectForKey:@"status"] isEqualToString:@"Cancelled"]){
            statusType = @"Cancelled";
            imageType = @"StatusRejected";
            Cell.LineLabel.hidden = NO;
            Cell.ViewDetailsBtn.hidden =NO;
            Cell.hight.constant = 32.0;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"status"] isEqualToString:@"Discarded"]){
            statusType = @"Discarded";
            imageType = @"StatusRejected";
            Cell.ViewDetailsBtn.hidden =YES;
            Cell.LineLabel.hidden = YES;
            Cell.hight.constant = 0.0;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if([[responseData objectForKey:@"status"] isEqualToString:@"Declined"]||[[responseData objectForKey:@"status"] isEqualToString:@"FAILED"]){
            statusType = @"Declined";
            imageType = @"StatusRejected";
            Cell.ViewDetailsBtn.hidden =YES;
            Cell.hight.constant = 0.0;
            Cell.LineLabel.hidden = YES;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if([[responseData objectForKey:@"status"] isEqualToString:@"In-Progress"]){
            statusType = @"In-Progress";
            imageType = @"WatingForApprove";
            Cell.ViewDetailsBtn.hidden =NO;
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"status"] isEqualToString:@"Submitted"]){
            statusType = @"Submitted";
            imageType = @"Completes";
            Cell.ViewDetailsBtn.hidden =NO;
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"status"] isEqualToString:@"CLOSED"]){
            statusType = @"Assigned";
            imageType = @"Completes";
            Cell.ViewDetailsBtn.hidden =NO;
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"status"] isEqualToString:@"Assigned"]){
            statusType = @"Assigned";
            imageType = @"Completes";
            Cell.ViewDetailsBtn.hidden =YES;
            Cell.hight.constant = 0.0;
            Cell.LineLabel.hidden = YES;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"status"] isEqualToString:@"New"]||[[responseData valueForKey:@"status"] isEqualToString:@"new"]){
            statusType = @"New";
            imageType = @"New";
            Cell.ViewDetailsBtn.hidden =YES;
            Cell.hight.constant = 0.0;
            Cell.LineLabel.hidden = YES;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if([[responseData objectForKey:@"status"] isEqualToString:@"Rejected"]){
            statusType = @"Rejected";
            imageType = @"StatusRejected";
            Cell.ViewDetailsBtn.hidden = NO;
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"status"] isEqualToString:@"Approved"]){
            Cell.RatingView.hidden = NO;
            Cell.RateStaticLabel.hidden = NO;
            Cell.DurationStaticLabel.hidden = YES;
            Cell.DurationLabel.hidden = YES;
            statusType = @"Competed";
            imageType = @"Completes";
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.ViewDetailsBtn.hidden = NO;
        }
        Cell.StatusLabel.text = statusType;
        Cell.StatusLabel.textColor = [UIColor whiteColor];
        Cell.StausImageView.image = [UIImage imageNamed:imageType];
    }
    return Cell;

    
}

-(void)GotoEmployeeDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    
    NSString* str = [[HistoryArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
    viewController.taskID = str;
    viewController.isManager = TRUE;
    [[self navigationController] pushViewController:viewController animated: YES];
    
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
-(void)clearfilterUserdefaultData
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"HISTORY"];
    [userDefault synchronize];
}
@end
