#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DownPicker.h"
#import "DQAlertView.h"
#import "NYAlertViewController.h"
#import "VCFloatingActionButton.h"
#import "AFURLSessionManager.h"

@interface TaskDetailManagerScreen : UIViewController <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,floatMenuDelegate,UITextViewDelegate>
{
    AppDelegate *delegate;
    UITableView *tab;
    NSMutableArray *tabdataarray1,*tabdataarray2,*dataArray,*dataConv;
    NSMutableDictionary *dataDict;
    UILabel *flatButton_lab,*flatButton1_lab,*sel_lab,*lab1,*datelab,*flatButton2_lab;
    UITextField *searchtxt,*searchtxt1,*renttext,*phonetext,*desctext;
    BOOL isShowDetail, isMe, isManager, isServerResponded;
    int taskType;
    NSString *taskID;
    int count;
   
    
    UIView *mainScreenView, *msgvw;//, *screenView;
    UIAlertView *alertView;
    UITextField *alertTextField, *alertTextField1;
    VCFloatingActionButton *addButton;
   
}
@property (nonatomic) BOOL isShowDetail, isManager;
@property (nonatomic) int count, taskType;
@property (nonatomic, retain) NSMutableDictionary *dataDict;
@property (nonatomic, retain) NSString *taskID;


@property (strong, nonatomic) NSString *isComeFrom;

@end
