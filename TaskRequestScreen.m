
#import "TaskRequestScreen.h"
#import "Header.h"
#import "iWork-Swift.h"
@interface TaskRequestScreen ()<UITextViewDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    UINavigationBar *NavBar;
    UIRefreshControl *refreshControl;

    IBOutlet UIView *OuterView;
    IBOutlet HCSStarRatingView *RatingView;
    IBOutlet UILabel *ApproveTaskStaticLabel;
    IBOutlet UILabel *RatingLabel;
    
    IBOutlet UIButton *ApproveRejectBtn;
    IBOutlet UIButton *CancelBtn;
   
    IBOutlet UITextView *FeedTextView;
    float RateValue;
    NSDateFormatter *dateFormat;
    
    NSDictionary *params;
    NSDictionary *ResponseDic;
    NSString *Status;
    WebApiService *Api;
    int offset;
    int limit;
    NSString *CheckValue;
    IBOutlet UITableView *tab;
    IBOutlet UIButton *TaskCreationBtn;
    IBOutlet UIButton *TaskCompletionBtn;
    IBOutlet UILabel *TaskCreationLabel;
    IBOutlet UILabel *TaskCompletionLabel;
    IBOutlet UILabel *HeaderLabel;
    int tagValue;
    IBOutlet UILabel *Nodatalabel;
    IBOutlet UIView *NoDataView;
    IBOutlet UIButton *resetButton;
    NSString *SelectBtnString;
    NSString *EmployeeID;
    
    

    IBOutlet UIView *NeedRevisionView;
    IBOutlet UITextView *CommentTextView;
    IBOutlet UILabel *NeedRevisionStaticLabel;
    
    IBOutlet UIView *ConfirmationView;
    IBOutlet UILabel *ConfirmationTitle;
    IBOutlet UILabel *ConfirmationDescription;
    
    IBOutlet UIView *ConfirmationConfirmView;
    IBOutlet UILabel *ConfirmationConfirmTitle;
    IBOutlet UILabel *ConfirmationConfirmDescription;
    IBOutlet UIImageView *ConfirmationConfirmImageView;
    NSString *CommingSoon;
    NSMutableAttributedString *attributedString;
    
}
@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation TaskRequestScreen

@synthesize count,taskType,taskID,taskFeedback,taskRating,isNewReq,serverMsg,serverMsg1;
- (void)viewDidLoad{
    [super viewDidLoad];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
    RateValue = 0.0;
    taskRating = 0;
    taskFeedback = @"";
    CheckValue =@"";
    EmployeeID = @"";
    Api = [[WebApiService alloc] init];
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    filterData = [[NSMutableDictionary alloc] init];
    tab.estimatedRowHeight = 50;
    tab.backgroundColor = [UIColor whiteColor];
    tab.rowHeight = UITableViewAutomaticDimension;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
    serverMsg = NSLocalizedString(@"NO_PENDING_REQUEST", nil);
    serverMsg1 = NSLocalizedString(@"NO_COMPLETE_REQUEST", nil);
    
    if(isNewReq == YES){
        TaskCreationLabel.hidden = NO;
        TaskCompletionLabel.hidden = YES;
    }
    else{
        TaskCreationLabel.hidden = YES;
        TaskCompletionLabel.hidden = NO;
    }
    HeaderLabel.text = NSLocalizedString(@"TASK_REQUESTE_QUEUE", nil);
    self.view.backgroundColor = [UIColor whiteColor];
   
    ConfirmationTitle.textColor = [UIColor TextBlueColor];
    [CancelBtn setTitleColor:[UIColor RejectBtnColor] forState:UIControlStateNormal];
    [ApproveRejectBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    
    FeedTextView.layer.cornerRadius = 6;
    FeedTextView.layer.borderWidth = 1;
    FeedTextView.layer.borderColor = delegate.borderColor.CGColor;
    CommentTextView.layer.cornerRadius = 6;
    CommentTextView.layer.borderWidth = 1;
    CommentTextView.layer.borderColor = delegate.borderColor.CGColor;
    ApproveTaskStaticLabel.textColor = [UIColor TextBlueColor];
    NeedRevisionStaticLabel.textColor = [UIColor TextBlueColor];
    CommentTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    CommentTextView.textColor = [UIColor lightGrayColor];
    FeedTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    FeedTextView.textColor = [UIColor lightGrayColor];
    
    
}
-(void) viewWillAppear:(BOOL)animated{
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
    [filterData setObject:@"" forKey:@"fromDate"];
    [filterData setObject:@"" forKey:@"toDate"];
    [filterData setObject:@"" forKey:@"taskId"];
}
-(void) fetchDetail{
    if(delegate.isInternetConnected){
        if(isNewReq == YES){
           Status = @"1";
        }
        else{
           Status = @"3";
        }
        if([CheckValue isEqualToString:@"FILTER"]){
            params = @ {
                @"fromDate": fromDate.text,
                @"handset_id": @"",
                @"status": Status,
                @"taskId": taskId.text,
                @"toDate": toDate.text,
                @"user_id": [ApplicationState userId],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        else{
            params = @ {
                @"fromDate": @"",
                @"handset_id": @"",
                @"status": Status,
                @"taskId": @"",
                @"toDate": @"",
                @"user_id": [ApplicationState userId],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        NSLog(@"%@",params);
        ResponseDic = [Api WebApi:params Url:@"viewAllTeamTask"];
        isServerResponded = TRUE;
        [refreshControl endRefreshing];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self fetchDetail];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            NSLog(@"CompleteResponseDic==%@",ResponseDic);
            NoDataView.hidden = YES;
           if(isNewReq == YES){
                if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskCreation"]))){
                   NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"taskCreation"];
                   if(ResponseArrays.count>0){
                       [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"taskCreation"]];
                       [tab reloadData];
                   }
                   else if(dataArray.count == 0){
                      [self NoDataFound];
                   }
               }
               else{
                   [self NoDataFound];
               }
           }
           else{
               if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskCompletion"]))){
                   NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"taskCompletion"];
                   if(ResponseArrays.count>0){
                       [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"taskCompletion"]];
                       [tab reloadData];
                   }
                   else if(dataArray1.count == 0){
                       [self NoDataFound];
                   }
               }
               else{
                   [self NoDataFound];
               }
           }
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(isNewReq == YES){
        if(dataArray.count == 0){
            NoDataView.hidden = NO;
            Nodatalabel.hidden = NO;
            Nodatalabel.text = @"No Data Found";
        }
    }
    else{
        if(dataArray1.count == 0){
            NoDataView.hidden = NO;
            Nodatalabel.hidden = NO;
            Nodatalabel.text = @"No Data Found";
        }
    }
    [tab reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue;
    if(isNewReq == YES){
        if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskCreation"]))){
            ArraysValue = [[ResponseDic objectForKey:@"object"] valueForKey:@"taskCreation"];
        }
    }
    else{
        if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskCompletion"]))){
           ArraysValue = [[ResponseDic objectForKey:@"object"] valueForKey:@"taskCompletion"];
        }
    }
    if(ArraysValue.count>0){
        offset = offset + 50;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self fetchDetail];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        if(isNewReq == YES){
            [dataArray addObject:BindDataDic];
        }
        else{
           [dataArray1 addObject:BindDataDic];
        }
    }
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)FilterAction:(id)sender{
    [self filterView];
}
- (void)reloadData
{
    [refreshControl endRefreshing];
    [self ClearData];
    [self EmptyArray];
    if (![CheckValue isEqualToString:@"FILTER"]){
        [self fetchDetail];
    }
  
    
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    dataArray = [[NSMutableArray alloc] init];
    dataArray1 = [[NSMutableArray alloc] init];
}
-(IBAction)GotoDetails:(UIButton*)sender{
    tagValue = sender.tag;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NewTaskDetailScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"NewTaskDetailScreen"];
    if(isNewReq == TRUE){
        ObjViewController.isNewReq = TRUE;
        ObjViewController.taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    }
    else{
        ObjViewController.isNewReq = FALSE;
        ObjViewController.taskID = [[dataArray1 objectAtIndex:tagValue] objectForKey:@"taskId"];
        ObjViewController.EmployeeID = [[dataArray1 objectAtIndex:tagValue] objectForKey:@"empId"];
    }
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)btnfun:(id)sender
{
    UIButton *btn=(UIButton *)sender;
      if(1000 == btn.tag)
      {
          if (![fromDate.text isEqualToString:@""] || ![toDate.text isEqualToString:@""])
          {
              [filterData setObject:fromDate.text forKey:@"fromDate"];
              [filterData setObject:toDate.text forKey:@"toDate"];
          }
          if (![taskId.text isEqualToString:@""])
          {
              [filterData setObject:taskId.text forKey:@"taskId"];
          }
          [self EmptyArray];
          CheckValue = @"FILTER";
          
          [filterView removeFromSuperview];
          [LoadingManager showLoadingView:self.view];
          [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
    }
    else if (1001 == btn.tag)
    {
        [self.view endEditing:YES];
        dateTimePicker.hidden = TRUE;
        NavBar.hidden = TRUE;
        [filterView removeFromSuperview];
    }
}
-(IBAction)ResetBtnAction:(id)sender{
    CheckValue = @"";
    resetButton.hidden = YES;
    [self ClearData];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
}
-(void)ClearFilterAction
{
    [self.view endEditing:YES];
    dateTimePicker.hidden = TRUE;
    NavBar.hidden = TRUE;
    [filterView removeFromSuperview];
    [self ClearData];
    [self EmptyArray];
    CheckValue = @"FEEDBACK";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
}
-(IBAction)TaskCrationAction:(id)sender{
    isNewReq = YES;
    TaskCreationLabel.hidden = NO;
    TaskCompletionLabel.hidden = YES;
    [self ClearData];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
}
-(IBAction)TaskCompletionAction:(id)sender{
    isNewReq = NO;
    TaskCreationLabel.hidden = YES;
    TaskCompletionLabel.hidden = NO;
    [self ClearData];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
}
-(void)ClearData{
    [filterData setObject:@"" forKey:@"fromDate"];
    [filterData setObject:@"" forKey:@"toDate"];
    [filterData setObject:@"" forKey:@"taskId"];
    tab.backgroundColor = [UIColor whiteColor];
    offset = 0;
    limit = 50;
    dataArray = [[NSMutableArray alloc] init];
    dataArray1 = [[NSMutableArray alloc] init];
}
-(void)DonePicker{
    NavBar.hidden = YES;
    dateTimePicker.hidden = TRUE;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    if (TRUE ==  isFromDate){
        fromDate.text=[dateFormat stringFromDate:dateTimePicker.date];
    }
    else{
        toDate.text=[dateFormat stringFromDate:dateTimePicker.date];
    }
    
    if(![toDate.text isEqualToString:@""]){
        NSComparisonResult result;
        NSDate *startDate = [dateFormat dateFromString:fromDate.text];
        NSDate *endDate = [dateFormat dateFromString:toDate.text];
        result = [startDate compare:endDate];
        if(result==NSOrderedAscending){
            
        }
        else if(result==NSOrderedDescending){
            toDate.text = @"";
            [self ShowAlert:@"You cannot select date earlier to start date" ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (TRUE == isNewReq){
        return [dataArray count];
    }
    else{
        return [dataArray1 count];
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if(isNewReq == YES){
        TaskRequestCell *Cell = (TaskRequestCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        Cell.RejectBtn.tag = indexPath.row;
        Cell.ApproveBtn.tag = indexPath.row;
        Cell.ApproveRejectViewDetailsBtn.tag = indexPath.row;
        if(dataArray.count>0){
            NSDictionary * responseData = [dataArray objectAtIndex:indexPath.row];
            [Cell configureCell:responseData];
        }
        return Cell;
    }
    else{
        TaskRequestCell *Cell = (TaskRequestCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        Cell.NeedRevisionBtn.tag = indexPath.row;
        Cell.CompleteApproveBtn.tag = indexPath.row;
        Cell.ApproveRejectViewDetailsBtn.tag = indexPath.row;
        if(dataArray1.count>0){
            NSDictionary * responseData = [dataArray1 objectAtIndex:indexPath.row];
            [Cell configureCell:responseData];
        }
        return Cell;
    }
}
-(IBAction)RejectBtnAction:(UIButton*)sender{
    tagValue = sender.tag;
    taskType = @"2";
    ConfirmationView.hidden = NO;
    [ApproveRejectBtn setTitle:@"REJECT" forState:UIControlStateNormal];
    [ApproveRejectBtn setTitleColor:[UIColor RedColor] forState:UIControlStateNormal];
    ConfirmationTitle.text = @"Reject Task";
    SelectBtnString = @"REJECT";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    ConfirmationDescription.text = NSLocalizedString(@"ARE_YOU_SURE_REJECT", nil);
    OuterView.hidden = YES;
}
-(IBAction)ApproveBtnAction:(UIButton*)sender{
    [self FeedBackClear];
    tagValue = sender.tag;
    taskType = @"1";
    ConfirmationView.hidden = NO;
    [ApproveRejectBtn setTitle:@"APPROVE" forState:UIControlStateNormal];
    [ApproveRejectBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    ConfirmationTitle.text = @"Approve Task";
    SelectBtnString = @"APPROVE";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    ConfirmationDescription.text = NSLocalizedString(@"ARE_YOU_SURE_APPROVE", nil);
    OuterView.hidden = YES;
}
-(IBAction)NeedRevisionBtnAction:(UIButton*)sender{
    [self FeedBackClear];
    SelectBtnString = @"NEED REVISION";
    tagValue = sender.tag;
    taskType = @"3";
    taskID = [[dataArray1 objectAtIndex:tagValue] objectForKey:@"taskId"];
    NeedRevisionView.hidden = NO;
    OuterView.hidden = YES;
}
-(IBAction)CompleteApproveBtnAction:(UIButton*)sender{
    [self FeedBackClear];
    SelectBtnString = @"";
    tagValue = sender.tag;
    taskType = @"1";
    taskID = [[dataArray1 objectAtIndex:tagValue] objectForKey:@"taskId"];
    EmployeeID = [[dataArray1 objectAtIndex:tagValue] objectForKey:@"empId"];
    OuterView.hidden = NO;
}
-(IBAction)ConfirmViewCancelAction:(id)sender{
    ConfirmationView.hidden = YES;
    NeedRevisionView.hidden = YES;
    [CommentTextView resignFirstResponder];
}
-(IBAction)ConfirmedViewOKCrossAction:(id)sender{
    ConfirmationConfirmView.hidden = YES;
//    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)ApproveRejectNeedRevisionCompleteApproveAction:(id)sender{
    ConfirmationView.hidden = YES;
   
    if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.5];
    }
    else if([SelectBtnString isEqualToString:@"NEED REVISION"]){
        if([CommentTextView.text isEqualToString:@""]||[CommentTextView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)]){
            [self ShowAlert:NSLocalizedString(@"PLEASE_GIVE_THE_FEEDBACK", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else{
            NeedRevisionView.hidden = YES;
            [LoadingManager showLoadingView:self.view];
            [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.5];
        }
    }
    else{
        [self RegcognitionsYes];
    }
}
-(void) startTheBgForStatus
{
    if(delegate.isInternetConnected){
        
        if(OuterView.hidden == NO){
            int ConvertRating = (int)RateValue;
            params = @ {
                @"status": taskType,
                @"taskId": taskID,
                @"employeeId": [ApplicationState userId],
                @"rating" : [NSNumber numberWithInt:ConvertRating],
                @"feedback" : FeedTextView.text
            };
        }
        else{
            NSString *Comment = @"";
            Comment = CommentTextView.text;
            if(Comment == nil){
                Comment = @"";
            }
            if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
                params = @ {
                    @"status": taskType,
                    @"taskId": taskID,
                    @"user_id": [ApplicationState userId],
                    @"comment" : Comment
                };
            }
            else{
                params = @ {
                    @"status": taskType,
                    @"taskId": taskID,
                    @"userId": [ApplicationState userId],
                    @"comment" : Comment
                };
            }
        }
        NSLog(@"params==%@",params);
        if(OuterView.hidden == NO){
            ResponseDic = [Api WebApi:params Url:@"taskApproval"];
        }
        else{
            if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
                ResponseDic = [Api WebApi:params Url:@"updateTaskStatus"];
            }
            else{
                ResponseDic = [Api WebApi:params Url:@"needRevision"];
            }
        }
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self startTheBgForStatus];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            if(OuterView.hidden == NO){
                if([[[ResponseDic valueForKey:@"object"] valueForKey:@"recognitationStatus"] intValue]==1){
                    ConfirmationView.hidden = NO;
                    ConfirmationTitle.text = @"Give Recognition";
                    ConfirmationDescription.text = @"Would you link to give recognitions?";
                    [ApproveRejectBtn setTitle:@"YES" forState:UIControlStateNormal];
                    
                }
                else{
                    ConfirmationConfirmTitle.text = @"RATING SENT";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"KPISubmit"];
                    ConfirmationConfirmView.hidden = NO;
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                }
            }
            else{
                if([SelectBtnString isEqualToString:@"APPROVE"]){
                    ConfirmationConfirmTitle.text = @"APPROVED";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"ApprovedKpi"];
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                    ConfirmationConfirmView.hidden = NO;
                }
                else if([SelectBtnString isEqualToString:@"REJECT"]){
                    ConfirmationConfirmTitle.text = @"REJECTED";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"RejectedKpi"];
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                    ConfirmationConfirmView.hidden = NO;
                }
                else{
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                    ConfirmationConfirmView.hidden = NO;
                    ConfirmationConfirmTitle.text = @"NEED REVISION";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"ApprovedKpi"];
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                }
            }
            OuterView.hidden = YES;
            [self reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert: [ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
}
-(void)filterView
{
    filterView = [[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    filterView.backgroundColor = [UIColor whiteColor];
    filterView.layer.borderWidth = 0.5;
    filterView.layer.borderColor = delegate.borderColor.CGColor;
    
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor = delegate.headerbgColler;
    [filterView addSubview:headerview];
    
    UILabel *headerlab1=[[UILabel alloc] initWithFrame:CGRectMake(2*delegate.margin, 10,delegate.devicewidth, delegate.headerheight)];
    headerlab1.textColor=[UIColor whiteColor];
    headerlab1.font=delegate.headFont;
    headerlab1.text=NSLocalizedString(@"FILTERS", nil);
    headerlab1.textAlignment=ALIGN_LEFT;
    [headerview addSubview:headerlab1];

    UIButton *ButtonCancel = [[UIButton alloc] initWithFrame:CGRectMake(delegate.devicewidth -90, 25, 80, 30)];
    ButtonCancel.backgroundColor = [UIColor clearColor];
    ButtonCancel.clipsToBounds = YES;
    ButtonCancel.titleLabel.font = delegate.normalFont;
    [ButtonCancel setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    ButtonCancel.layer.cornerRadius = 5.0;
    ButtonCancel.layer.borderColor = [UIColor whiteColor].CGColor;
    ButtonCancel.layer.borderWidth =  1.0;
    [ButtonCancel setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    [ButtonCancel addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    ButtonCancel.tag = 1001;
    [headerview addSubview:ButtonCancel];
    
    
    dateTimePicker = [[UIDatePicker alloc] init];
    dateTimePicker.datePickerMode = UIDatePickerModeDate;
    dateTimePicker.backgroundColor=[UIColor whiteColor];
    //[dateTimePicker setMinimumDate:[NSDate date]];
   // [dateTimePicker addTarget:self  action:@selector(dateTimeChange:) forControlEvents:UIControlEventValueChanged];

    dateTimePicker.layer.zPosition = 1;
    [dateTimePicker setFrame: CGRectMake(0,(filterView.frame.size.height-162), filterView.frame.size.width, 162)];
    [filterView addSubview:dateTimePicker];
    dateTimePicker.hidden = TRUE;
    
    NavBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(dateTimePicker.frame.origin.x, dateTimePicker.frame.origin.y-44, dateTimePicker.frame.size.width, 44)];
    NavBar.barStyle = UIBarStyleDefault;
    NavBar.backgroundColor = [UIColor grayColor];
    [filterView addSubview:NavBar];
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:@"DONE" style:UIBarButtonItemStylePlain target:self action:@selector(DonePicker)];
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.rightBarButtonItem = backButton;
    NavBar.items = @[ navItem ];
    NavBar.hidden = TRUE;
    
    CGFloat ypos = delegate.headerheight + 20;
    UILabel *fromLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.font = delegate.contentFont;
    fromLabel.text = NSLocalizedString(@"FROM", nil);
    [filterView addSubview:fromLabel];
    
    fromDate=[[UITextField alloc] initWithFrame:CGRectMake(30, ypos+25, 120, 25)];
    fromDate.keyboardType=UIKeyboardTypeDefault;
    fromDate.font=delegate.contentFont;
    fromDate.borderStyle=UITextBorderStyleRoundedRect;
    fromDate.delegate = self;
    fromDate.tag = 100;
    fromDate.placeholder = NSLocalizedString(@"START_DATE", nil);
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    image.image = [UIImage imageNamed:@"calendar"] ;
    fromDate.leftViewMode = UITextFieldViewModeAlways;
    [fromDate setLeftView:image];
    fromDate.textColor = delegate.dimColor;
    fromDate.borderStyle = UITextBorderStyleNone;
    [fromDate setBackgroundColor:[UIColor clearColor]];
    fromDate.text = [filterData objectForKey:@"fromDate"];
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, fromDate.frame.size.height - 1, fromDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [fromDate.layer addSublayer:bottomBorder];
    
    fromDate.autocorrectionType = UITextAutocorrectionTypeNo;
    [filterView addSubview:fromDate];

    int xpos = delegate.devicewidth - 140;
    UILabel *toLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, 80, 15)];
    toLabel.textColor = [UIColor blackColor];
    toLabel.font = delegate.contentFont;
    toLabel.text =NSLocalizedString(@"TO", nil);
    [filterView addSubview:toLabel];
    
    UIButton *startDateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [startDateButton setFrame:CGRectMake(30, ypos+25, 120, 25)];
    [startDateButton addTarget:self action:@selector(openPicker:) forControlEvents:UIControlEventTouchUpInside];
    startDateButton.backgroundColor = [UIColor clearColor];
    [filterView addSubview:startDateButton];
    
    ypos = ypos + 25;
    
    toDate=[[UITextField alloc] initWithFrame:CGRectMake(xpos, ypos, 120, 25)];
    toDate.keyboardType=UIKeyboardTypeDefault;
    toDate.font=delegate.contentFont;
    toDate.borderStyle=UITextBorderStyleRoundedRect;
    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    image1.image = [UIImage imageNamed:@"calendar"] ;
    toDate.leftViewMode = UITextFieldViewModeAlways;
    [toDate setLeftView:image1];
    toDate.delegate=self;
    toDate.tag = 101;
    toDate.placeholder =  NSLocalizedString(@"END_DATE", nil);
    toDate.textColor = delegate.dimColor;
    toDate.autocorrectionType = UITextAutocorrectionTypeNo;
    toDate.text = [filterData objectForKey:@"toDate"];
    toDate.borderStyle = UITextBorderStyleNone;
    [toDate setBackgroundColor:[UIColor clearColor]];
    bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, toDate.frame.size.height - 1, toDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [toDate.layer addSublayer:bottomBorder];
    
    [filterView addSubview:toDate];
    
    UIButton *endDateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [endDateButton setFrame:CGRectMake(xpos, ypos, 120, 25)];
    [endDateButton addTarget:self action:@selector(openPickerForEndDate:) forControlEvents:UIControlEventTouchUpInside];
    endDateButton.backgroundColor = [UIColor clearColor];
    [filterView addSubview:endDateButton];
    
    ypos = ypos + toDate.frame.size.height + 20;{
        UILabel *taskLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 300, 15)];
        taskLabel.textColor = [UIColor blackColor];
        taskLabel.font = delegate.contentFont;
        taskLabel.text = NSLocalizedString(@"TASK_ID", nil);
        [filterView addSubview:taskLabel];
        
        ypos = ypos + taskLabel.frame.size.height + 20;
        
        taskId = [[UITextField alloc] initWithFrame:CGRectMake(30, ypos, 300, 25)];
        taskId.keyboardType=UIKeyboardTypeNumberPad;
        taskId.font=delegate.contentFont;
        taskId.borderStyle=UITextBorderStyleRoundedRect;
        taskId.delegate=self;
        taskId.placeholder =NSLocalizedString(@"TASK_ID", nil);
        taskId.textColor = delegate.dimColor;
        taskId.autocorrectionType = UITextAutocorrectionTypeNo;
        taskId.text = [filterData objectForKey:@"taskId"];
        taskId.borderStyle = UITextBorderStyleNone;
        [taskId setBackgroundColor:[UIColor clearColor]];
        CALayer* bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0, taskId.frame.size.height - 1, taskId.frame.size.width-50, 1.0);
        bottomBorder.backgroundColor = delegate.borderColor.CGColor;
        [taskId.layer addSublayer:bottomBorder];
        [filterView addSubview:taskId];
        ypos = ypos + taskId.frame.size.height + 20;
    }
    
    ypos = self.view.frame.size.height - 55;
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(delegate.margin, ypos, delegate.devicewidth-delegate.margin*2, 40)];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: footerView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    //footerView.layer.mask = maskLayer1;
    [filterView addSubview:footerView];
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2+10, 0, footerView.frame.size.width/2-10, 40)];
    submitButton.backgroundColor = delegate.redColor;
    submitButton.clipsToBounds = YES;
    submitButton.titleLabel.font = delegate.normalFont;
    [submitButton setTitle:NSLocalizedString(@"APPLY_FILTER", nil) forState:UIControlStateNormal];
    submitButton.layer.cornerRadius = 5.0;
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    submitButton.tag=1000;
    [footerView addSubview:submitButton];
    
    UIButton *ClearFilterButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width/2-10, 40)];
    ClearFilterButton.backgroundColor = [UIColor whiteColor];
    ClearFilterButton.clipsToBounds = YES;
    ClearFilterButton.titleLabel.font = delegate.normalFont;
    [ClearFilterButton setTitleColor: delegate.redColor forState:UIControlStateNormal];
    ClearFilterButton.layer.cornerRadius = 5.0;
    ClearFilterButton.layer.borderColor = delegate.redColor.CGColor;
    ClearFilterButton.layer.borderWidth =  1.0;
    [ClearFilterButton setTitle:@"CLEAR FILTER" forState:UIControlStateNormal];
    [ClearFilterButton setTitleColor:delegate.redColor forState:UIControlStateNormal];
    [ClearFilterButton addTarget:self action:@selector(ClearFilterAction) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:ClearFilterButton];


    [self.view addSubview:filterView];
    [self.view bringSubviewToFront:filterView];
}

-(void) startTheBgForFilter{
    if([delegate isInternetConnected]){
        CheckValue = @"FILTER";
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NavBar.hidden = YES;
    dateTimePicker.hidden = YES;
//    if (100 == textField.tag)
//    {
//        isFromDate = TRUE;
//        [fromDate resignFirstResponder];
//        [textField resignFirstResponder];
//        dateTimePicker.hidden = FALSE;
//        NavBar.hidden = FALSE;
//    }
//    else if (101 == textField.tag)
//    {
//        isFromDate = FALSE;
//        [toDate resignFirstResponder];
//        [textField resignFirstResponder];
//        dateTimePicker.hidden = FALSE;
//        NavBar.hidden = FALSE;
//    }
    
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)FeedbackSubmitAction:(id)sender{
    if (RateValue == 0.0){
        [self ShowAlert:NSLocalizedString(@"PLEASE_FILL_THE_STAR", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if([FeedTextView.text isEqualToString:@""]||[FeedTextView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_GIVE_THE_FEEDBACK", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        taskFeedback = FeedTextView.text;
        taskRating = RateValue;
        [FeedTextView resignFirstResponder];
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.3];
    }
}
-(IBAction)FeedbackCancelAction:(id)sender{
    OuterView.hidden = YES;
    [self FeedBackClear];
    [FeedTextView resignFirstResponder];
}
-(void)FeedBackClear{
    RatingView.value = 0.0;
    RatingLabel.text = @"";
    CommentTextView.textColor = [UIColor lightGrayColor];
    FeedTextView.textColor = [UIColor lightGrayColor];
    FeedTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    CommentTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    FeedTextView.delegate= self;
    CommentTextView.delegate= self;
}
- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    RateValue = sender.value;
    [RatingView setTintColor:[UIColor NeedRevisionBtnColor]];
    if(RateValue == 0.0){
        RatingLabel.hidden = YES;
    }
    else if(RateValue == 1.0){
        RatingLabel.text = [@"Below Expectation" uppercaseString];
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 2.0){
        RatingLabel.text = [@"Meet Expectation" uppercaseString];
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 3.0){
        RatingLabel.text = [@"Above Expectation" uppercaseString];
        RatingLabel.hidden = NO;
    }
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    if([textView isEqual:CommentTextView]){
        if([CommentTextView.text isEqualToString: NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)]){
            CommentTextView.text = @"";
            CommentTextView.textColor = [UIColor blackColor];
        }
    }
    else if([textView isEqual:FeedTextView]){
        if([FeedTextView.text isEqualToString: NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)]){
            FeedTextView.text = @"";
            FeedTextView.textColor = [UIColor blackColor];
        }
    }
    [textView becomeFirstResponder];
}
-(void)
:(UITextView *)textView{
    
    if([textView isEqual:CommentTextView]){
        if([CommentTextView.text isEqualToString:@""]){
            CommentTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
            CommentTextView.textColor = [UIColor lightGrayColor];
        }
    }
    else if([textView isEqual:FeedTextView]){
        if([FeedTextView.text isEqualToString:@""]){
            FeedTextView.text =  NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
            FeedTextView.textColor = [UIColor lightGrayColor];
        }
    }
    [textView resignFirstResponder];
}
-(void)RegcognitionsYes{
    ConfirmationConfirmView.hidden = YES;
    if([[ApplicationState GetRecognitionEnable] intValue] == 0){ // Not Able to give recognition
        CommingSoon = @"COMMINGSOON";
        NSString *normalText = @"Comming soon...\n Please give recognition using iConnect.";
        attributedString = [[NSMutableAttributedString alloc] initWithString:normalText];
        [self ShowAlert:@"" ButtonTitle:@"Close"];
    }
    else{
        NSString *RegcognitionsUrl = [NSString stringWithFormat:@"iconnect://?token=%@&user_key=%@&givenTo=%@&taskId=%@",[ApplicationState GetToken],[ApplicationState userId],EmployeeID,taskID];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:RegcognitionsUrl] options:@{} completionHandler:nil];
    }
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    
    if([CommingSoon isEqualToString:@"COMMINGSOON"]){
        ObjAlertController.isCome = @"KPI";
        ObjAlertController.AttributedString = attributedString;
    }
    else{
        ObjAlertController.MessageTitleStr = MsgTitle;
    }
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
-(IBAction)openPicker:(id)sender
{
    [self.view endEditing:YES];
    isFromDate = TRUE;
    dateTimePicker.hidden = FALSE;
    NavBar.hidden = FALSE;
}
-(IBAction)openPickerForEndDate:(id)sender
{
    [self.view endEditing:YES];
    isFromDate = FALSE;
    dateTimePicker.hidden = FALSE;
    NavBar.hidden = FALSE;
    
}
@end

