#import "FeedbackScreen.h"
#import "Header.h"
#import "iWork-Swift.h"

@interface FeedbackScreen () <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    
    UINavigationBar *NavBar;
    UIRefreshControl *refreshControl;
    NSDateFormatter *dateFormat;
    AppDelegate *delegate;
    NSString *handelId;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    NSMutableArray *feedbackArray;
   
    IBOutlet UITableView *tab;
    IBOutlet UILabel *headerlab;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UIView *NoDataView;
    IBOutlet UIButton *resetButton;
    WebApiService *Api;
    
    NSMutableDictionary *dataDict;
    UITextField *fromDate,*toDate, *taskID, *taskTYPE, *taskRating, *taskStatus;
    TNCircularRadioButtonData *type1, *type2, *type3;
    TNCircularRadioButtonData *rating1, *rating2, *rating3;
    BOOL isFromDate;
    int taskType;
    int count;

    IBOutlet UIView *DatePickerViewBg;
    IBOutlet UIDatePicker *dateTimePicker;
    UIView *msgvw,*screenView,*filterView;
    TNRadioButtonGroup* typeGroup, *ratingGroup, *statusGroup;
    NSString* filterTaskType, *filterRating, *filterTaskStatus, *filterTaskId, *toFilterDate, *fromFilterDate;
    NSMutableDictionary *filterData;
    int offset;
    int limit;
    NSString *CheckValue;
}
@end


@implementation FeedbackScreen

- (void)viewDidLoad{
    [super viewDidLoad];
    [self ClearData];
    filterRating = @"";
    filterTaskType = @"";
    CheckValue = @"FEEDBACK";
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    headerlab.text = NSLocalizedString(@"MY_FEEDBACK", nil);
    handelId = [NSString stringWithFormat:@"%@",[delegate.dataFull objectForKey:@"handsetId"]];
    [resetButton setTitle:NSLocalizedString(@"RESET_BTN", nil) forState:UIControlStateNormal];
    
    Api = [[WebApiService alloc] init];
    tab.estimatedRowHeight = 50;
    tab.backgroundColor = delegate.BackgroudColor;
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tab.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
    self.view.backgroundColor = delegate.BackgroudColor;
    filterData = [[NSMutableDictionary alloc] init];
    [LoadingManager showLoadingView:self.view];
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [tab registerNib:Nib forCellReuseIdentifier:@"CELL"];
    [self performSelector:@selector(FeedbackWebApi) withObject:nil afterDelay:0.5];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
}
- (void)reloadData
{
    if (![CheckValue isEqualToString:@"FILTER"]){
        CheckValue = @"Refresh";
        [self FeedbackWebApi];
    }
    [self ClearData];
}
-(void)ClearData{
    offset = 0;
    limit = 50;
    feedbackArray = [[NSMutableArray alloc] init];
}
-(void)FeedbackWebApi{
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"FEEDBACK"] || [CheckValue isEqualToString:@"Refresh"]){
            params = @ {
                @"rating": @"",
                @"startDate": @"",
                @"endDate": @"",
                @"taskId": @"",
                @"taskType": @"",
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        else if([CheckValue isEqualToString:@"FILTER"]){
            params = @ {
                @"rating": filterRating,
                @"startDate": fromDate.text,
                @"endDate": toDate.text,
                @"taskId": taskID.text,
                @"taskType": filterTaskType,
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        NSLog(@"params===%@",params);
        ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"myFeedbackCountList?employeeID=%@",[ApplicationState userId]]];
        [refreshControl endRefreshing];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self FeedbackWebApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            NSLog(@"FeedResponseDic==%@",ResponseDic);
            NoDataView.hidden = YES;
            if([CheckValue isEqualToString:@"FILTER"]){
                //resetButton.hidden = NO;
            }
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    NoDataLabel.hidden = YES;
                    [tab reloadData];
                }
                else if(ResponseArrays.count == 0 && feedbackArray.count == 0){
                    [self NoDataFound];
                }
            }
            else{
               [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(feedbackArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Data Found";
    }
    [tab reloadData];

}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if([CheckValue isEqualToString:@"Refresh"]){
        CheckValue = @"FEEDBACK";
    }
    else {
        if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
            NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
            if(ArraysValue.count>0){
                offset = offset + 50;
                [self FeedbackWebApi];
            }
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [feedbackArray addObject:BindDataDic];
    }
}
- (IBAction)DonePicker {
    NavBar.hidden = YES;
    dateTimePicker.hidden = TRUE;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    if (TRUE ==  isFromDate){
        fromDate.text=[dateFormat stringFromDate:dateTimePicker.date];
    }
    else{
        toDate.text=[dateFormat stringFromDate:dateTimePicker.date];
    }
    
    if(![toDate.text isEqualToString:@""]){
        NSComparisonResult result;
        NSDate *startDate = [dateFormat dateFromString:fromDate.text];
        NSDate *endDate = [dateFormat dateFromString:toDate.text];
        result = [startDate compare:endDate];
        if(result==NSOrderedAscending){
        }
        else if(result==NSOrderedDescending){
            toDate.text = @"";
            [self ShowAlert:@"You cannot select date earlier to start date" ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
    }
}
-(IBAction)btnfun:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(1000 == btn.tag)
    {
        
        DatePickerViewBg.hidden = TRUE;
        NavBar.hidden = TRUE;
        
        if (![fromDate.text isEqualToString:@""] || ![toDate.text isEqualToString:@""])
        {
            [filterData setObject:fromDate.text forKey:@"fromDate"];
            [filterData setObject:toDate.text forKey:@"toDate"];
        }
        
        if (![taskID.text isEqualToString:@""])
        {
            [filterData setObject:taskID.text forKey:@"taskId"];
        }
        
        if (type1.selected == YES)
        {
            [filterData setObject:@"MyTasks" forKey:@"taskType"];
        }
        
        if (type2.selected == YES)
        {
            [filterData setObject:@"Assignments" forKey:@"taskType"];
        }
        
        if (type3.selected == YES)
        {
            [filterData setObject:@"all" forKey:@"taskType"];
        }

        if (rating1.selected == YES)
        {
            [filterData setObject:@"Lowest" forKey:@"taskRating"];
        }
        
        if (rating2.selected == YES)
        {
            [filterData setObject:@"Highest" forKey:@"taskRating"];
        }
        
        if (rating3.selected == YES)
        {
            [filterData setObject:@"all" forKey:@"taskRating"];
        }
        
        [filterView removeFromSuperview];
        [self startTheBgForFilter];
    }
    else if(1001 == btn.tag)
    {
        dateTimePicker.hidden = TRUE;
        NavBar.hidden = TRUE;
        [filterView removeFromSuperview];
       // resetButton.hidden =YES;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [feedbackArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.tag = indexPath.row;
    Cell.ViewDetailsBtn.tag = indexPath.row;
    [Cell.ViewDetailsBtn addTarget:self action:@selector(GotoTaskDetails:) forControlEvents:UIControlEventTouchUpInside];
    Cell.RatingView.hidden = NO;
    Cell.DurationStaticLabel.hidden = YES;
    Cell.DurationLabel.hidden = YES;
    
    if(feedbackArray.count>0){
        NSDictionary * responseData = [feedbackArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
        Cell.StatusLabel.text = @"Completed";
        Cell.RatingView.tintColor = [UIColor NeedRevisionBtnColor];
    }
    return Cell;
}
-(void)GotoTaskDetails:(UIButton*)sender{
    int TagValue = sender.tag;
    NSString* str = [[feedbackArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
    viewController.taskID = str;
    viewController.isManager = NO;
    [[self navigationController] pushViewController:viewController animated: YES];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
   
        dateTimePicker.hidden = YES;
        NavBar.hidden = YES;
}
-(void)filterView{
    
    filterView = [[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, 120)];
    filterView.backgroundColor = [UIColor whiteColor];
    filterView.layer.borderWidth = 0.5;
    filterView.layer.borderColor = delegate.borderColor.CGColor;
    
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor = delegate.headerbgColler;
    [filterView addSubview:headerview];
    
    UILabel *headerlab1=[[UILabel alloc] initWithFrame:CGRectMake(2*delegate.margin, 0,delegate.devicewidth, delegate.headerheight)];
    headerlab1.textColor=[UIColor whiteColor];
    headerlab1.font=delegate.headFont;
    headerlab1.text=NSLocalizedString(@"FILTERS", nil);
    [headerview addSubview:headerlab1];
    

    UIButton *ButtonCancel = [[UIButton alloc] initWithFrame:CGRectMake(delegate.devicewidth -90, 25, 80, 30)];
    ButtonCancel.backgroundColor = [UIColor clearColor];
    ButtonCancel.clipsToBounds = YES;
    ButtonCancel.titleLabel.font = delegate.normalFont;
    [ButtonCancel setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    ButtonCancel.layer.cornerRadius = 5.0;
    ButtonCancel.layer.borderColor = [UIColor whiteColor].CGColor;
    ButtonCancel.layer.borderWidth =  1.0;
    [ButtonCancel setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    [ButtonCancel addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    ButtonCancel.tag=1001;
    [headerview addSubview:ButtonCancel];
    
    
    int ypos = delegate.headerheight + 20;
    UILabel *fromLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.font = delegate.ooredoo;
    fromLabel.text = NSLocalizedString(@"FROM", nil);
    [filterView addSubview:fromLabel];
    
    
    fromDate=[[UITextField alloc] initWithFrame:CGRectMake(30, ypos+20, 120, 25)];
    fromDate.keyboardType=UIKeyboardTypeDefault;
    fromDate.font=delegate.contentFont;
    fromDate.borderStyle=UITextBorderStyleRoundedRect;
    fromDate.delegate=self;
    fromDate.tag = 100;
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
    image.image = [UIImage imageNamed:@"calendar"] ;
    fromDate.leftViewMode = UITextFieldViewModeAlways;
    [fromDate setLeftView:image];
    fromDate.placeholder =NSLocalizedString(@"START_DATE", nil);
    fromDate.textColor = delegate.dimColor;
    fromDate.borderStyle = UITextBorderStyleNone;
    fromDate.text = [filterData objectForKey:@"fromDate"];
    [fromDate setBackgroundColor:[UIColor clearColor]];
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, fromDate.frame.size.height - 1, fromDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [fromDate.layer addSublayer:bottomBorder];
    
    fromDate.autocorrectionType = UITextAutocorrectionTypeNo;
    [filterView addSubview:fromDate];
    
    int xpos = delegate.devicewidth - 140;
    UILabel *toLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, 80, 15)];
    toLabel.textColor = [UIColor blackColor];
    toLabel.font = delegate.ooredoo;
    toLabel.text = NSLocalizedString(@"TO", nil);
    [filterView addSubview:toLabel];
    
    UIButton *startDateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [startDateButton setFrame:CGRectMake(30, ypos+20, 120, 25)];
    [startDateButton addTarget:self action:@selector(openPicker:) forControlEvents:UIControlEventTouchUpInside];
    startDateButton.backgroundColor = [UIColor clearColor];
    [filterView addSubview:startDateButton];
    
    
    toDate=[[UITextField alloc] initWithFrame:CGRectMake(xpos, ypos+20, 120, 25)];
    toDate.keyboardType=UIKeyboardTypeDefault;
    toDate.font=delegate.contentFont;
    toDate.borderStyle=UITextBorderStyleRoundedRect;
    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
    image1.image = [UIImage imageNamed:@"calendar"] ;
    toDate.leftViewMode = UITextFieldViewModeAlways;
    [toDate setLeftView:image1];
    toDate.delegate=self;
    toDate.tag = 101;
    toDate.placeholder = NSLocalizedString(@"END_DATE", nil);
    toDate.textColor = delegate.dimColor;
    toDate.autocorrectionType = UITextAutocorrectionTypeNo;
    
    toDate.borderStyle = UITextBorderStyleNone;
    toDate.text = [filterData objectForKey:@"toDate"];
    [toDate setBackgroundColor:[UIColor clearColor]];
    bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, fromDate.frame.size.height - 1, toDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [toDate.layer addSublayer:bottomBorder];
    [filterView addSubview:toDate];
    
    UIButton *endDateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [endDateButton setFrame:CGRectMake(xpos, ypos+20, 120, 25)];
    [endDateButton addTarget:self action:@selector(openPickerForEndDate:) forControlEvents:UIControlEventTouchUpInside];
    endDateButton.backgroundColor = [UIColor clearColor];
    [filterView addSubview:endDateButton];
    
    
    ypos = ypos + fromLabel.frame.size.height + fromDate.frame.size.height + 20;
    
    UILabel *taskLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 300, 15)];
    taskLabel.textColor = [UIColor blackColor];
    taskLabel.font = delegate.ooredoo;
    taskLabel.text = NSLocalizedString(@"TASK_ID", nil);
    [filterView addSubview:taskLabel];
    
    ypos = ypos + taskLabel.frame.size.height + 20;
    taskID = [[UITextField alloc] initWithFrame:CGRectMake(30, ypos, 300, 25)];
    taskID.keyboardType=UIKeyboardTypeNumberPad;
    taskID.font=delegate.contentFont;
    taskID.borderStyle=UITextBorderStyleRoundedRect;
    taskID.delegate=self;
    taskID.placeholder =NSLocalizedString(@"TASK_ID", nil);
    taskID.textColor = delegate.dimColor;
    taskID.autocorrectionType = UITextAutocorrectionTypeNo;
    taskID.text = [filterData objectForKey:@"taskId"];
    taskID.borderStyle = UITextBorderStyleNone;
    [taskID setBackgroundColor:[UIColor clearColor]];
    bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, taskID.frame.size.height - 1, taskID.frame.size.width-50, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [taskID.layer addSublayer:bottomBorder];
    [filterView addSubview:taskID];

    ypos = ypos + taskID.frame.size.height + 20;
   
    UILabel *typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    typeLabel.textColor = [UIColor blackColor];
    typeLabel.font = delegate.ooredoo;
    typeLabel.text = NSLocalizedString(@"TASK_TYPE", nil);
    [filterView addSubview:typeLabel];
    
    ypos = ypos + typeLabel.frame.size.height + 20;

    type1 = [TNCircularRadioButtonData new];
    type1.labelText = @"Task";
    type1.identifier = @"MyTasks";
    type1.circleActiveColor = type1.labelActiveColor = delegate.dimBlack;
    type1.circlePassiveColor = type1.labelPassiveColor = delegate.dimColor;
    type1.borderRadius = 12;
    type1.circleRadius = 5;
    
    type2 = [TNCircularRadioButtonData new];
    type2.labelText = NSLocalizedString(@"ASSIGNMENT", nil);
    type2.identifier = @"Assignments";
    type2.circleActiveColor = type2.labelActiveColor = delegate.dimBlack;
    type2.circlePassiveColor = type2.labelPassiveColor = delegate.dimColor;
    type2.borderRadius = 12;
    type2.circleRadius = 5;
    
    type3 = [TNCircularRadioButtonData new];
    type3.labelText = NSLocalizedString(@"BOTH", nil);
    type3.identifier = @"Both";

    type3.circleActiveColor = type3.labelActiveColor = delegate.dimBlack;
    type3.circlePassiveColor = type3.labelPassiveColor = delegate.dimColor;
    type3.borderRadius = 12;
    type3.circleRadius = 5;
    
    if ([[filterData objectForKey:@"taskType"] isEqualToString:@"MyTasks"]){
        type1.selected = YES;
    }
    else if ([[filterData objectForKey:@"taskType"] isEqualToString:@"Assignments"]){
        type2.selected = YES;
    }
    else{
        type3.selected = YES;
    }
    
   
    typeGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[type1, type2, type3] layout:TNRadioButtonGroupLayoutHorizontal];
    typeGroup.identifier = NSLocalizedString(@"MY_GROUP", nil);
    [typeGroup create];
    typeGroup.labelFont = delegate.contentFont;
    typeGroup.labelColor = delegate.dimBlack;
    typeGroup.frame = CGRectMake(30, ypos, 300, 20);
    [filterView addSubview:typeGroup];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(typeGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:typeGroup];
    
    ypos = ypos + typeGroup.frame.size.height + 20;
    UILabel *ratingLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    ratingLabel.textColor = [UIColor blackColor];
    ratingLabel.font = delegate.ooredoo;
    ratingLabel.text =  NSLocalizedString(@"RATING", nil);
    [filterView addSubview:ratingLabel];
    
    ypos = ypos + typeLabel.frame.size.height + 20;
    
    rating1 = [TNCircularRadioButtonData new];
    rating1.labelText = NSLocalizedString(@"LOWEST", nil);
    rating1.identifier = @"Lowest";
    rating1.circleActiveColor = rating1.labelActiveColor = delegate.dimBlack;
    rating1.circlePassiveColor = rating1.labelPassiveColor = delegate.dimColor;
    rating1.borderRadius = 12;
    rating1.circleRadius = 5;
    
    rating2 = [TNCircularRadioButtonData new];
    rating2.labelText = NSLocalizedString(@"HIGHEST", nil);
    rating2.identifier = @"Highest";
    rating2.circleActiveColor = rating2.labelActiveColor = delegate.dimBlack;
    rating2.circlePassiveColor = rating2.labelPassiveColor = delegate.dimColor;
    rating2.borderRadius = 12;
    rating2.circleRadius = 5;
    
    rating3 = [TNCircularRadioButtonData new];
    rating3.labelText = NSLocalizedString(@"ALL", nil);
    rating3.identifier = @"Both";
    rating3.circleActiveColor = rating3.labelActiveColor = delegate.dimBlack;
    rating3.circlePassiveColor = rating3.labelPassiveColor = delegate.dimColor;
   
    rating3.selected = YES;
    rating3.borderRadius = 12;
    rating3.circleRadius = 5;
    
    if ([[filterData objectForKey:@"taskRating"] isEqualToString:@"Lowest"]){
        rating1.selected = YES;
    }
    else if ([[filterData objectForKey:@"taskRating"] isEqualToString:@"Highest"]){
        rating2.selected = YES;
    }
    else
    {
        rating3.selected = YES;
    }
    ratingGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[rating1, rating2, rating3] layout:TNRadioButtonGroupLayoutHorizontal];
    ratingGroup.identifier = NSLocalizedString(@"RATING_GROUP", nil);
    [ratingGroup create];
    ratingGroup.position = CGPointMake(30, ypos);
    ratingGroup.labelFont = delegate.contentFont;
    ratingGroup.labelColor = delegate.dimBlack;
    [filterView addSubview:ratingGroup];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ratingGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:ratingGroup];
    
//    ypos = ypos + statusGroup.frame.size.height + 20;
    ypos = self.view.frame.size.height - 55;
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(delegate.margin, ypos, delegate.devicewidth-delegate.margin*2, 40)];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: footerView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    //footerView.layer.mask = maskLayer1;
    [filterView addSubview:footerView];
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2+10, 0, footerView.frame.size.width/2-10, 40)];
    submitButton.backgroundColor = delegate.redColor;
    submitButton.clipsToBounds = YES;
    submitButton.titleLabel.font = delegate.normalFont;
    [submitButton setTitle:NSLocalizedString(@"APPLY_FILTER", nil) forState:UIControlStateNormal];
    submitButton.layer.cornerRadius = 5.0;
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    submitButton.tag=1000;
    [footerView addSubview:submitButton];
    
    UIButton *ClearFilterButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width/2-10, 40)];
    ClearFilterButton.backgroundColor = [UIColor whiteColor];
    ClearFilterButton.clipsToBounds = YES;
    ClearFilterButton.titleLabel.font = delegate.normalFont;
    [ClearFilterButton setTitleColor: delegate.redColor forState:UIControlStateNormal];
    ClearFilterButton.layer.cornerRadius = 5.0;
    ClearFilterButton.layer.borderColor = delegate.redColor.CGColor;
    ClearFilterButton.layer.borderWidth =  1.0;
    [ClearFilterButton setTitle:@"CLEAR FILTER" forState:UIControlStateNormal];
    [ClearFilterButton setTitleColor:delegate.redColor forState:UIControlStateNormal];
    [ClearFilterButton addTarget:self action:@selector(ClearFilterAction) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:ClearFilterButton];
    
    ypos = ypos + submitButton.frame.size.height + 20;
    filterView.frame = CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight);
   // [DatePickerViewBg setFrame: CGRectMake(0,(delegate.deviceheight-260), delegate.deviceheight, DatePickerViewBg.frame.size.height)];
    
   // [filterView addSubview:DatePickerViewBg];
    dateTimePicker = [[UIDatePicker alloc] init];
    dateTimePicker.datePickerMode = UIDatePickerModeDate;
    dateTimePicker.date = [NSDate date];
//    [dateTimePicker addTarget:self  action:@selector(dateTimeChange:) forControlEvents:UIControlEventValueChanged];
    
    [dateTimePicker setFrame: CGRectMake(0,(filterView.frame.size.height-162), filterView.frame.size.width, 162)];
    dateTimePicker.backgroundColor = [UIColor whiteColor];
    dateTimePicker.layer.zPosition = 1;
    [filterView addSubview:dateTimePicker];
    dateTimePicker.hidden = TRUE;
    
    NavBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(dateTimePicker.frame.origin.x, dateTimePicker.frame.origin.y-44, dateTimePicker.frame.size.width, 44)];
    NavBar.barStyle = UIBarStyleDefault;
    NavBar.backgroundColor = [UIColor whiteColor];
    [filterView addSubview:NavBar];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:@"DONE" style:UIBarButtonItemStylePlain target:self action:@selector(DonePicker)];
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.rightBarButtonItem = backButton;
    NavBar.items = @[ navItem ];
    NavBar.hidden = TRUE;
    
    [self.view addSubview:filterView];
}
- (void)typeGroupUpdated:(NSNotification *)notification {
    filterTaskType = typeGroup.selectedRadioButton.data.identifier;
}
- (void)ratingGroupUpdated:(NSNotification *)notification {
    filterRating = ratingGroup.selectedRadioButton.data.identifier;
}
- (void)statusGroupUpdated:(NSNotification *)notification {
    filterTaskStatus = statusGroup.selectedRadioButton.data.identifier;
}
-(void) startTheBgForFilter{
    if(delegate.isInternetConnected){
        feedbackArray = [[NSMutableArray alloc] init];
        offset = 0;
        limit = 50;
        CheckValue = @"FILTER";
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(FeedbackWebApi) withObject:nil afterDelay:0.5];
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}

-(void)ClearFilterAction
{
    [LoadingManager showLoadingView:self.view];
    filterTaskStatus = filterRating = filterTaskId = filterTaskType = @"";
    [filterData setObject:@"" forKey:@"fromDate"];
    [filterData setObject:@"" forKey:@"toDate"];
    [filterData setObject:@"" forKey:@"taskId"];
    [filterData setObject:@"" forKey:@"taskStatus"];
    [filterData setObject:@"" forKey:@"taskType"];
    [filterData setObject:@"" forKey:@"taskRating"];

    
    dateTimePicker.hidden = TRUE;
    NavBar.hidden = TRUE;
    [filterView removeFromSuperview];
    [self ClearData];
    CheckValue = @"FEEDBACK";
    offset = 0;
    limit = 50;
    
    [self FeedbackWebApi];
}
-(IBAction)FilterAction:(id)sender{
    [self filterView];
}



-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
-(IBAction)openPicker:(id)sender
{
    [self.view endEditing:YES];
    isFromDate = TRUE;
    dateTimePicker.hidden = FALSE;
    NavBar.hidden = FALSE;
}
-(IBAction)openPickerForEndDate:(id)sender
{
    [self.view endEditing:YES];
    isFromDate = FALSE;
    dateTimePicker.hidden = FALSE;
    NavBar.hidden = FALSE;

}


@end
