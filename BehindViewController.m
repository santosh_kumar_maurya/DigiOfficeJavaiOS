//
//  BehindViewController.m
//  iWork
//
//  Created by Shailendra on 07/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "BehindViewController.h"
#import "Header.h"
@interface BehindViewController()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *BehindTableView;
    NSMutableArray *BehindArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UIView *NoDataView;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
}

@end

@implementation BehindViewController
@synthesize BehindDics;

- (void)viewDidLoad {
//    [self clearfilterUserdefaultData];
    offset = 0;
    limit = 50;
    Api = [[WebApiService alloc] init];
    BehindArray = [[NSMutableArray alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    BehindTableView.estimatedRowHeight = 50;
    BehindTableView.backgroundColor = delegate.BackgroudColor;
    BehindTableView.rowHeight = UITableViewAutomaticDimension;
    BehindTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, BehindTableView.bounds.size.width, 10.f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [BehindTableView addSubview:refreshControl];
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [BehindTableView registerNib:Nib forCellReuseIdentifier:@"CELL"];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"BEHIND" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ClearFilterData:) name:@"ClearFilteriPM" object:nil];
    [self BehindScheduleApi];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if(![CheckValue isEqualToString:@"FILTER"]){
        [self EmptyArray];
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(BehindScheduleApi) withObject:nil afterDelay:0.5];
    }
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    BehindArray = [[NSMutableArray alloc] init];
    [self BehindScheduleApi];
}
- (void)reloadData
{
    if(![CheckValue isEqualToString:@"FILTER"]){
        CheckValue = @"Refresh";
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(BehindScheduleApi) withObject:nil afterDelay:0.5];
    }
    [self EmptyArray];
}
-(void)ClearFilterData:(NSNotification*)notification{
    CheckValue = @"";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(BehindScheduleApi) withObject:nil afterDelay:0.5];
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    BehindArray = [[NSMutableArray alloc] init];
}
- (void)BehindScheduleApi {
    if(delegate.isInternetConnected){
        if(![CheckValue isEqualToString:@"FILTER"]){
            params = @ {
                @"fromDate": @"",
                @"handsetId": @"",
                @"status": @"5",
                @"taskId": @"",
                @"taskType": @"",
                @"toDate": @"",
                @"user_id"  : [BehindDics valueForKey:@"empId"],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        ResponseDic =  [Api WebApi:params Url:@"getEmployeeTaskList"];
        [refreshControl endRefreshing];
        if(ResponseDic==nil){
            [LoadingManager hideLoadingView:self.view];
            [self BehindScheduleApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            NSLog(@"BehindScheduleResponseDic==%@",ResponseDic);
            NoDataView.hidden = YES;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]];
                }
                else {
                    [LoadingManager hideLoadingView:self.view];
                    [self NoRecordFound];
                }
                [BehindTableView reloadData];
            }
            else{
                [LoadingManager hideLoadingView:self.view];
                [self NoRecordFound];
            }
            [BehindTableView reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self NoRecordFound];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoRecordFound{
    if(BehindArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Record Found";
    }
    [BehindTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
     if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
        NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            if([CheckValue isEqualToString:@"FILTER"]){
                NSMutableDictionary *NewDic = [params mutableCopy];
                for (int i = 0 ; i< [NewDic count];i++){
                    if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                        [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                    }
                    else{
                        [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                    }
                }
                params = [NewDic mutableCopy];
            }
            [self BehindScheduleApi];
        }
    }
    
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [BehindArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return   1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return   [BehindArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.ViewDetailsBtn.tag = indexPath.row;
    Cell.RatingView.hidden = YES;
    Cell.RateStaticLabel.hidden = YES;
    Cell.DurationStaticLabel.hidden = NO;
    Cell.DurationLabel.hidden = NO;
    Cell.StatusLabel.text = @"In Progress";
    [Cell.ViewDetailsBtn addTarget:self action:@selector(GotoEmployeeDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    if(BehindArray.count>0){
        NSDictionary * responseData = [BehindArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
         Cell.StausImageView.image = [UIImage imageNamed:@"WatingForApprove"];
    }
    return Cell;
}
-(void)GotoEmployeeDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    NSString* str = [[BehindArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    if ([[[BehindArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
        TaskDetailManagerScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailManagerScreen"];
        viewController.taskID = str;
        viewController.isManager = YES;
        [[self navigationController] pushViewController:viewController animated: YES];

    }
    else{
        HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
-(void)clearfilterUserdefaultData
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"Behind"];
    [userDefault synchronize];
}
@end
