//
//  SubmitCell.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class SubmitBtnCell: UITableViewCell {

    @IBOutlet weak var btnSubmit: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.configureCell()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell()
    {
        self.btnSubmit.layer.cornerRadius = 5
        self.btnSubmit.layer.masksToBounds = true
        self.btnSubmit.layer.borderWidth = 0.5;
        self.btnSubmit.layer.borderColor = UIColor.clear.cgColor
    }
    


}
