//
//  OngoingViewController.m
//  iWork
//
//  Created by Shailendra on 08/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "OngoingViewController.h"
#import "Header.h"
@interface OngoingViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *OngoingTableView;
    NSMutableArray *OngoingArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
    IBOutlet UIView *NoDataView;
}

@end

@implementation OngoingViewController

- (void)viewDidLoad {
    [self EmptyArray];
    [self clearfilterUserdefaultData];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    OngoingTableView.estimatedRowHeight = 50;
    OngoingTableView.rowHeight = UITableViewAutomaticDimension;
    OngoingTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, OngoingTableView.bounds.size.width, 10.f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [OngoingTableView addSubview:refreshControl];
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [OngoingTableView registerNib:Nib forCellReuseIdentifier:@"CELL"];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"ONGOINGTASK" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ClearFilterData:) name:@"ClearFilteriPM" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if(![CheckValue isEqualToString:@"FILTER"]){
        [self EmptyArray];
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(GoingTaskWebApi) withObject:nil afterDelay:0.5];
    }
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    OngoingArray = [[NSMutableArray alloc] init];
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GoingTaskWebApi) withObject:nil afterDelay:0.5];
}
-(void)ClearFilterData:(NSNotification*)notification{
    CheckValue = @"";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GoingTaskWebApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData{
    if(![CheckValue isEqualToString:@"FILTER"]){
        CheckValue = @"Refresh";
        [self GoingTaskWebApi];
    }
    [self EmptyArray];
}
- (void)GoingTaskWebApi {
    
    if(delegate.isInternetConnected){

        if(![CheckValue isEqualToString:@"FILTER"]){
            params = @ {
                @"end_date" : @"",
                @"start_date" :@"",
                @"taskId" :@"",
                @"task_type": @"",
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"viewAllOngoingDashTask?employeeId=%@",[ApplicationState userId]]];
        
        [refreshControl endRefreshing];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self GoingTaskWebApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            NSLog(@"OngoingTaskResponseDic==%@",ResponseDic);
            [LoadingManager hideLoadingView:self.view];
            NoDataView.hidden = YES;
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    [OngoingTableView reloadData];
                }
                else if(ResponseArrays.count == 0 && OngoingArray.count == 0){
                    [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(OngoingArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Record Found";
    }
    [OngoingTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self GoingTaskWebApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [OngoingArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [OngoingArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.tag = indexPath.row;
    Cell.ViewDetailsBtn.tag = indexPath.row;
    Cell.RatingView.hidden = YES;
    Cell.RateStaticLabel.hidden = YES;
    Cell.DurationStaticLabel.hidden = NO;
    Cell.DurationLabel.hidden = NO;
    [Cell.ViewDetailsBtn addTarget:self action:@selector(GotoEmployeeDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    if(OngoingArray.count>0){
        NSDictionary * responseData = [OngoingArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
        Cell.StatusLabel.text = @"In-Progress";
        Cell.StausImageView.image = [UIImage imageNamed:@"WatingForApprove"];
        Cell.DurationLabel.text = [responseData valueForKey:@"timeLeft"];
        if([Cell.DurationLabel.text containsString:@"-"]){
            Cell.DurationStaticLabel.text = @"Overdue";
            Cell.DurationLabel.text = [Cell.DurationLabel.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        }
        else{
            Cell.DurationStaticLabel.text = @"Time Left";
        }
        
    }
    return Cell;
}

-(void)GotoEmployeeDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    NSString* str = [[OngoingArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskDetailManagerScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailManagerScreen"];
    viewController.taskID = str;
    viewController.isManager = FALSE;
    [[self navigationController] pushViewController:viewController animated: YES];

}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
-(void)clearfilterUserdefaultData
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"ONGOING"];
    [userDefault synchronize];
}
@end
