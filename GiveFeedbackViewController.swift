//
//  GiveFeedbackViewController.swift
//  iWork
//
//  Created by Shailendra on 17/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class GiveFeedbackViewController: UIViewController, UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    
    @IBOutlet var SelectEffevtiveCompetencyLabel : UILabel!
    @IBOutlet var SelectLackingCompetencyLabel : UILabel!
    @IBOutlet var FeedbackTextView : UITextView!
    
    @IBOutlet var PickerViewBgView : UIView!
    @IBOutlet var PickerView : UIPickerView!
    var TaskID : String! = ""
   
    var ResponseDic = NSDictionary()
    var Api: WebApiService =  WebApiService()
    var delegate = AppDelegate()
    var param : NSDictionary!
    var CheckValue : String! = ""
    
    var CompentencyNameArray = NSArray()
    var CompentencyIDArray = NSArray()
    var SelectOption : String! = ""
    
    var SelectEffevtiveID : String! = ""
    var SelectLackingID : String! = ""
    @IBOutlet var ConfirmationView : UIView!
    @IBOutlet var ConfirmationLabel : UILabel!
    @IBOutlet var HeaderLabel : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SelectEffevtiveCompetencyLabel.text = "Select Effective Competency";
        SelectLackingCompetencyLabel.text = "Select Lacking Competency";
        HeaderLabel.text = "Feedback : Task ID \(TaskID!)"
        delegate = UIApplication.shared.delegate as! AppDelegate
        CheckValue = "GETLIST"
        FeedbackTextView.textColor = UIColor.lightGray
        FeedbackTextView.text = "Write Your Feedback"
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        IQKeyboardManager.shared().isEnabled = true

    }
    override func viewWillAppear(_ animated: Bool) {
        LoadingManager.showLoading(self.view)
        self.perform(#selector(GiveFeedbackViewController.GetLackingList), with: nil, afterDelay: 0.5)
    }
    @IBAction func CancelBackAction(){
       FeedbackTextView.text = "Write Your Feedback"
       FeedbackTextView.textColor = UIColor.lightGray
      self.navigationController?.popViewController(animated: true)
    }
    @IBAction func CreateAction(){
        CheckValue  = "POSTFEEDBACK"
        if(SelectEffevtiveCompetencyLabel.text == "Select Effective Competency"){
            self.ShowAlert(MsgTitle:"Please fill required details", BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
        else if(SelectLackingCompetencyLabel.text == "Select Lacking Competency"){
             self.ShowAlert(MsgTitle:"Please fill required details", BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
        else if(FeedbackTextView.text == "Write Your Feedback" || FeedbackTextView.text  == ""){
             self.ShowAlert(MsgTitle:"Please fill required details", BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
        else{
            LoadingManager.showLoading(self.view)
            self.perform(#selector(GiveFeedbackViewController.GetLackingList), with: nil, afterDelay: 0.5)
        }
    }
    func  GetLackingList(){
        
        if (delegate.isInternetConnected()){
            
            let userId = UserDefaults.standard.string(forKey: "kUserId")
            if(CheckValue  == "GETLIST"){
               ResponseDic = Api.webApi(nil, url:"giveFeedback/List") as NSDictionary!
            }
            else if(CheckValue  == "POSTFEEDBACK"){
                param = ["effective_competency": SelectEffevtiveID,"employeeId": "\(userId!)","feedback": "\(FeedbackTextView.text!)","lacking_competency": "\(SelectLackingID!)","taskId": TaskID!]
                ResponseDic = Api.webApi(param as! [AnyHashable : Any]!, url:"giveFeedback") as NSDictionary!
            }
        
            if ResponseDic != nil {
                LoadingManager.hideLoading(view)
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    if(CheckValue  == "GETLIST"){
                        if (ResponseDic["object"] as? NSArray) != nil{
                            let Arrays = ResponseDic.value(forKey: "object") as! NSArray
                            CompentencyNameArray = Arrays.value(forKey: "name") as! NSArray
                            CompentencyIDArray = Arrays.value(forKey: "id") as! NSArray
                            PickerView.reloadAllComponents()
                            
                        }
                    }
                    else if(CheckValue  == "POSTFEEDBACK"){
                        ConfirmationLabel.text = "\(String(describing: ResponseDic.value(forKey: "message")!))"
                        ConfirmationView.isHidden = false
                    }
                    
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: ResponseDic.value(forKey: "message")))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }
            else{
                LoadingManager.showLoading(self.view)
                  self.perform(#selector(GiveFeedbackViewController.GetLackingList), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write Your Feedback" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        textView.becomeFirstResponder()
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == ""){
          textView.text = "Write Your Feedback"
            textView.textColor = UIColor.lightGray
        }
        textView.resignFirstResponder()
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return CompentencyIDArray.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return CompentencyNameArray[row] as? String
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(SelectOption == "EFFECTIVE"){
            SelectEffevtiveCompetencyLabel.text = "\(CompentencyNameArray[row])"
             SelectEffevtiveID =  "\(CompentencyIDArray[row])"
        }
        else if(SelectOption == "LACKING"){
            SelectLackingCompetencyLabel.text = "\(CompentencyNameArray[row])"
             SelectLackingID =  "\(CompentencyIDArray[row])"
        }
       

    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let TitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 320, height: 100))
        if(component == 0){
            let Views = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            Views.backgroundColor = UIColor.clear
            TitleLabel.backgroundColor = UIColor.clear
            TitleLabel.textAlignment = NSTextAlignment.center
            TitleLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
            TitleLabel.numberOfLines = 5
            TitleLabel.font = UIFont.systemFont(ofSize: 13)
            TitleLabel.text = CompentencyNameArray[row] as? String
            Views.addSubview(TitleLabel)
        }
        return TitleLabel
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    @IBAction func SelectEffectiveAction(){
        SelectOption = "EFFECTIVE"
        PickerViewBgView.isHidden = false
    }
    @IBAction func SelectLackingAction(){
        SelectOption = "LACKING"
        PickerViewBgView.isHidden = false
    }
    @IBAction func PickerViewDoneAction(){
        PickerViewBgView.isHidden = true
    }
    func KeyBoardHide(){
        FeedbackTextView.resignFirstResponder()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    @IBAction func HomeAction(sender:UIButton){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContainerParentViewController") as! ContainerParentViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        print("MsgTitle====\(MsgTitle)");
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: false, completion: nil)
    }
    
    
}
