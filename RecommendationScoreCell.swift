//
//  RecommendationScoreCell.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class RecommendationScoreCell: UITableViewCell {

    @IBOutlet var AverageNumberLabel : UILabel!
    @IBOutlet var RecommendationsLabel : UILabel!
    @IBOutlet var OnTheScaleOfLabel : UILabel!
    @IBOutlet var ScoreLabel : UILabel!
    @IBOutlet var CustomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        CustomView.layer.cornerRadius = 2.0
        CustomView.mask?.layer.cornerRadius = 7.0
        CustomView.layer.shadowRadius = 3.0;
        CustomView.layer.shadowColor = UIColor.black.cgColor
        CustomView.layer.shadowOffset = CGSize(width: -1.0, height: 2.0)
        CustomView.layer.shadowOpacity = 0.7
        CustomView.layer.masksToBounds = false
       
        self.RecommendationsLabel.textColor = UIColor.TextBlueColor()
        self.AverageNumberLabel.textColor = UIColor.TextBlueColor()
        self.OnTheScaleOfLabel.textColor = UIColor.TextBlueColor()
    }
}
