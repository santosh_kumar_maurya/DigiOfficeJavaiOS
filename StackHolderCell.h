//
//  StackHolderCell.h
//  iWork
//
//  Created by Shailendra on 25/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StackHolderCell : UITableViewCell

@property(nonnull,retain)IBOutlet UILabel *NameLabel;
@end
