//
//  SelectEmployeeCell.swift
//  iWork
//
//  Created by Shailendra on 15/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class SelectEmployeeCell: UITableViewCell {

    @IBOutlet var EmployeeNameTextField : UITextField!
    @IBOutlet weak var innerView : UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        innerView.layer.cornerRadius = 2.0
        innerView.layer.borderWidth = 1.0
        innerView.layer.borderColor = UIColor.darkGray.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
