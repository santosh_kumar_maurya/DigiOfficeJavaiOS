//
//  PointsCell.swift
//  iWork
//
//  Created by Shailendra on 20/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class PointsCell: UITableViewCell {

    @IBOutlet var PointLabel : UILabel!
    @IBOutlet var DateLabel : UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
