//
//  AskForProgressViewController.m
//  iWork
//
//  Created by Shailendra on 08/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "AskForProgressViewController.h"
#import  "Header.h"

@interface AskForProgressViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *AskTableView;
    NSMutableArray *AskArray;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UIView *NoDataView;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    WebApiService *Api;
    NSDictionary *ResponseDic;
    int offset;
    int limit;
    int TagValue;
}

@end
@implementation AskForProgressViewController

- (void)viewDidLoad {
    [self EmptyArray];
    [self ClearFilterData];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    AskTableView.estimatedRowHeight = 50;
    AskTableView.rowHeight = UITableViewAutomaticDimension;
    AskTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, AskTableView.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [AskTableView addSubview:refreshControl];
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [AskTableView registerNib:Nib forCellReuseIdentifier:@"CELL"];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"ASKPROGRESS" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ClearFilterData:) name:@"ClearFilteriPM" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [LoadingManager showLoadingView:self.view];
    [self EmptyArray];
    [self performSelector:@selector(AskForProgressApi) withObject:nil afterDelay:0.5];
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(AskForProgressApi) withObject:nil afterDelay:0.5];
}
-(void)ClearFilterData:(NSNotification*)notification{
    CheckValue = @"";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(AskForProgressApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData
{
    if(![CheckValue isEqualToString:@"FILTER"]){
        CheckValue = @"Refresh";
        [self AskForProgressApi];
    }
    [self EmptyArray];
    
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    AskArray = [[NSMutableArray alloc] init];
}
- (void)AskForProgressApi {
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"FILTER"]){
            AskArray = [[NSMutableArray alloc] init];
            ResponseDic =  [Api WebApi:params Url:@"askForProgress"];
        }
        else{
            params = @ {
                @"employeeId": @"",
                @"employeeName":@"",
                @"fromDate": @"",
                @"taskId": @"",
                @"taskType": @"",
                @"toDate": @"",
                @"user_Id"  : [ApplicationState userId],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
            ResponseDic =  [Api WebApi:params Url:@"askForProgress"];
        }
        [refreshControl endRefreshing];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self AskForProgressApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            NSLog(@"AskForProgress==%@",ResponseDic);
            NoDataView.hidden = YES;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]];
                     [AskTableView reloadData];
                }
                else if(AskArray.count == 0){
                     [self NoDataFound];
                }
            }
            else{
                 [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(AskArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Data Found";
    }
    [AskTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if([CheckValue isEqualToString:@"Refresh"]){
        CheckValue = @"";
    }
    else{
        if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
            NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
            if(ArraysValue.count>0){
                offset = offset + 50;
                if([CheckValue isEqualToString:@"FILTER"]){
                    NSMutableDictionary *NewDic = [params mutableCopy];
                    for (int i = 0 ; i< [NewDic count];i++){
                        if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                            [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                        }
                        else{
                            [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                        }
                    }
                    params = [NewDic mutableCopy];
                }
                [self AskForProgressApi];
            }
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [AskArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [AskArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.tag = indexPath.row;
    Cell.ViewDetailsBtn.tag = indexPath.row;
    Cell.RatingView.hidden = YES;
    Cell.RateStaticLabel.hidden = YES;
    Cell.DurationStaticLabel.hidden = NO;
    Cell.DurationLabel.hidden = NO;
    [Cell.ViewDetailsBtn addTarget:self action:@selector(GotoDetailsPage:) forControlEvents:UIControlEventTouchUpInside];
    if(AskArray.count>0){
        NSDictionary * responseData = [AskArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
        Cell.StatusLabel.text = @"In-Progress";
        Cell.StausImageView.image = [UIImage imageNamed:@"WatingForApprove"];
        Cell.CreatedByStaticLabel.text = @"Assigned to";
        Cell.CreatedByLabel.text = [responseData valueForKey:@"assignedTo"];
    }
    return Cell;
}
-(IBAction)GotoDetailsPage:(UIButton*)sender{
    
    TagValue = sender.tag;
    NSString* str = [[AskArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskDetailManagerScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailManagerScreen"];
    viewController.taskID = str;
    viewController.isManager = YES;
    viewController.isComeFrom = @"ASKFORPROGRESS";
    [[self navigationController] pushViewController:viewController animated: YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
-(IBAction)FilterAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    AskProcessFilterViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"AskProcessFilterViewController"];
    [[self navigationController] presentViewController:ObjController animated:YES completion:nil];
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ClearFilterData
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"ASKPROGRESS"];
    [userDefault synchronize];
}
@end
