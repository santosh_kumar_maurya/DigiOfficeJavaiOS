//
//  LineManagerTableViewCell.swift
//  iWork
//
//  Created by mac book pro on 22/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class LineManagerTableViewCell: UITableViewCell {

    @IBOutlet weak var txtData: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
