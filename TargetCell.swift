//
//  TargetCell.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class TargetCell: UITableViewCell {

    @IBOutlet weak var txtData: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.updateUI()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI()
    {
        let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
        txtData.leftView = paddingView
        txtData.leftViewMode = UITextFieldViewMode.always
    }

}
