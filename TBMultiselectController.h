//
//  MultiSelectViewController.h
//  MultiSelectUIXibVersion
//
//  Created by Terry Bu on 10/28/14.
//  Copyright (c) 2014 TurnToTech. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <AddressBook/AddressBook.h>
#import "Contact.h"
#import "AppDelegate.h"


@class TBMultiselectController;

@protocol TBMultiSelectControlDelegate<NSObject>
@required
- (void)multiSelectControllerDidCancel:(TBMultiselectController *)controller;
- (void)multiSelectController:(TBMultiselectController *)controller didFinishPickingSelections1:(NSMutableString *)selections;
@end



@interface TBMultiselectController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate,UISearchResultsUpdating>
{
    AppDelegate *delegate;
    UILabel *headerlab;
    UITableView *tab;
    NSMutableArray *savedArrayOfEmployees;
    int selectedCounter;
    UIBarButtonItem *selectedContactsCounterButton;
    //UISearchBar *searchBar;
    UISearchController *searchController;
    NSMutableArray *StatckHolderID;
}

//@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *searchResults1;
@property (nonatomic, strong) NSMutableArray *selectedContacts;

@property (nonatomic,strong) id<TBMultiSelectControlDelegate> delegate1;

//@property (nonatomic, strong) NSMutableArray *savedArrayOfEmployees;

- (IBAction)sendButton: (id) sender;
//- (IBAction)customBackXButton:(id)sender;


@end
