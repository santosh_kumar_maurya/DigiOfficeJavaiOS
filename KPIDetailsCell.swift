//
//  KPIDetailsCell.swift
//  iWork
//
//  Created by Shailendra on 26/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class KPIDetailsCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var CustomView: UIView!
    @IBOutlet var KPITableView: UITableView!
    @IBOutlet var KPIDetailsStaticLabel: UILabel!
    @IBOutlet var hight: NSLayoutConstraint!
    var KPIArrays = NSArray()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        CustomView.layer.cornerRadius = 2.0
        CustomView.mask?.layer.cornerRadius = 7.0
        CustomView.layer.shadowRadius = 3.0;
        CustomView.layer.shadowColor = UIColor.black.cgColor
        CustomView.layer.shadowOffset = CGSize(width: -1.0, height: 2.0)
        CustomView.layer.shadowOpacity = 0.7
        CustomView.layer.masksToBounds = false
        
        KPIDetailsStaticLabel.textColor = UIColor.TextBlueColor()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(KPIArrays.count>0){
            return KPIArrays.count;
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell : TeamMemberKPICell = tableView.dequeueReusableCell(
            withIdentifier: "CELL", for: indexPath) as! TeamMemberKPICell
        if(KPIArrays.count>0){
            let Dic = KPIArrays[indexPath.row] as! NSDictionary
            if (Dic["kpiDesc"] as? String) != nil{
                Cell.KPILabel.text = "\(String(describing: Dic["kpiDesc"]!))"
            }
            else{
                Cell.KPILabel.text = " "
            }
        }
        return Cell
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
