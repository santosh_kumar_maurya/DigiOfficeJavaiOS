//
//  TeamKPICell.swift
//  iWork
//
//  Created by Shailendra on 27/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class TeamKPICell: UITableViewCell {
    
    @IBOutlet weak var TeamKpiView : UIView!
    @IBOutlet weak var TeamKpiStaticLabel : UILabel!
    @IBOutlet weak var ForApprovalLabel : UILabel!
    @IBOutlet weak var ForApprovalStaticLabel : UILabel!
    @IBOutlet weak var HistoryLabel : UILabel!
    @IBOutlet weak var HistoryStaticLabel : UILabel!
    @IBOutlet weak var ApproveTaskButton : UIButton!
    @IBOutlet weak var HistoryButton : UIButton!
    @IBOutlet weak var TeamViewAllTaskButton : UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        TeamKpiView.layer.cornerRadius = 2.0
        TeamKpiView.mask?.layer.cornerRadius = 7.0
        TeamKpiView.layer.shadowRadius = 3.0;
        TeamKpiView.layer.shadowColor = UIColor.black.cgColor
        TeamKpiView.layer.shadowOffset = CGSize(width: -1.0, height: 2.0)
        TeamKpiView.layer.shadowOpacity = 0.7
        TeamKpiView.layer.masksToBounds = false
        TeamKpiStaticLabel.textColor = UIColor.TextBlueColor()
        ForApprovalStaticLabel.textColor = UIColor.TextBlueColor()
        HistoryStaticLabel.textColor = UIColor.TextBlueColor()
        
        TeamViewAllTaskButton.layer.cornerRadius = 17
        TeamViewAllTaskButton.clipsToBounds = true
        TeamViewAllTaskButton.backgroundColor = UIColor.ButtonSkyColor()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
