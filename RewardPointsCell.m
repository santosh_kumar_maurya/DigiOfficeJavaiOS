//
//  RewardPointsCell.m
//  iWork
//
//  Created by Shailendra on 19/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "RewardPointsCell.h"
#import "iWork-Swift.h"

@implementation RewardPointsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _PointView.layer.cornerRadius = 2.0;
    _PointView.maskView.layer.cornerRadius = 7.0f;
    _PointView.layer.shadowRadius = 3.0f;
    _PointView.layer.shadowColor = [UIColor blackColor].CGColor;
    _PointView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _PointView.layer.shadowOpacity = 0.7f;
    _PointView.layer.masksToBounds = NO;
    
    _HeaderTitleLabel.textColor = [UIColor TextBlueColor];
    self.TopView.backgroundColor = [UIColor TopBarYellowColor];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: self.TopView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){2.0}].CGPath;
    self.TopView.layer.mask = maskLayer1;
    self.HeaderTitleLabel.textColor = [UIColor TextBlueColor];
    self.PointTitleLabel.textColor = [UIColor TextBlueColor];
    self.PointLabel.textColor = [UIColor TextBlueColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
