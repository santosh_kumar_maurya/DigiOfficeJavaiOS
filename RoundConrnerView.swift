//
//  RoundConrnerView.swift
//  iWork
//
//  Created by Shailendra on 17/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

public class RoundConrnerView: UIView {

    override open func layoutSubviews() {
        
        layer.cornerRadius = 3.0
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.darkGray.cgColor
        self.backgroundColor = UIColor.white;
    }
}
