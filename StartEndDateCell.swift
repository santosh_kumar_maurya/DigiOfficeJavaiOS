//
//  StartEndDateCell.swift
//  iWork
//
//  Created by Shailendra on 12/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit


class StartEndDateCell: UITableViewCell {

    
    @IBOutlet var StartDateTextField : UITextField!
    @IBOutlet var EndDateTextField : UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
