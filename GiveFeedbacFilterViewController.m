//
//  GiveFeedbacFilterViewController.m
//  iWork
//
//  Created by Shailendra on 02/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "GiveFeedbacFilterViewController.h"
#import "Header.h"

@interface GiveFeedbacFilterViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
   
    NSMutableArray *arrayForBool;
    NSMutableArray *FilterArray;
    IBOutlet UITableView *FilterTableView;
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *CancelFilterBtn;
    IBOutlet UIButton *ApplyFilterBtn;
    AppDelegate *delegate;
    IBOutlet UIView *BottomView;
    NSMutableArray *PlusImageArray;
    NSMutableArray *MinusImageArray;
    
    NSIndexPath *indexPath3;
    MyWorkEmployeeCell *Cell3;
    NSIndexPath *indexPath4;
    MyWorkLocationCell *Cell4;
    NSIndexPath *indexPath5;
    DurationCell *Cell5;
    
    NSString *SelectDuration;
    
    IBOutlet UIView *DatePickerViewBg;
    IBOutlet UIDatePicker *DatePicker;
    
    NSString *DatePickerSelectionStr;
    NSString *StartDateStr;
    NSString *EndDateStr;
    BOOL RadioSelect;
    BOOL isTrackID;
    NSString *LocationSting;
    NSString *iWorkemp_idStr;
    NSString *iWorkemp_nameStr;
    NSString *EmployeeRadioSelect;
    
    NSString *EmployeeID;
    NSString *TrackID;
    NSString *EmployeeName;
    
    NSString *StartDate;
    NSString *EndDate;
    
    NSDictionary *UserDic;
    NSDateFormatter *dateFormat;
    int StartTimeStamp;
    int EndTimeStamp;
    int offset;
    int limit;
    
    NSUserDefaults *userDefault;
    
    
}@end

@implementation GiveFeedbacFilterViewController
@synthesize isComeFrom;
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
    userDefault = [NSUserDefaults standardUserDefaults];
    offset = 0;
    limit = 50;
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    RadioSelect = YES;
    isTrackID = YES;
    EmployeeID = @"";
    EmployeeName = @"";
    TrackID = @"";
    StartDate = @"";
    EndDate = @"";
    DatePickerSelectionStr = @"";
    arrayForBool = [[NSMutableArray alloc] init];
    if([isComeFrom isEqualToString:@"GIVEFEEDBACK"]){
        PlusImageArray = [[NSMutableArray alloc] initWithObjects:@"Plus",@"Plus", nil];
        MinusImageArray = [[NSMutableArray alloc] initWithObjects:@"Minus",@"Minus", nil];
        FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"EMPLOYEE_FILTER", nil),NSLocalizedString(@"DURATION_FILTER", nil), nil];
      
        UINib *LocationNib = [UINib nibWithNibName:@"MyWorkLocationCell" bundle:nil];
        [FilterTableView registerNib:LocationNib forCellReuseIdentifier:@"CELL1"];

        UINib *SelectionNib = [UINib nibWithNibName:@"DurationCell" bundle:nil];
        [FilterTableView registerNib:SelectionNib forCellReuseIdentifier:@"CELL2"];
 
    }
   else if([isComeFrom isEqualToString:@"STACKHOLDER"]){
        PlusImageArray = [[NSMutableArray alloc] initWithObjects:@"Plus",@"Plus",@"Plus", nil];
        MinusImageArray = [[NSMutableArray alloc] initWithObjects:@"Minus",@"Minus",@"Minus", nil];
        FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"TASK_ID", nil),NSLocalizedString(@"EMPLOYEE_NAME", nil),NSLocalizedString(@"DURATION_FILTER", nil), nil];
       
       UINib *SearchNib = [UINib nibWithNibName:@"MyWorkEmployeeCell" bundle:nil];
       [FilterTableView registerNib:SearchNib forCellReuseIdentifier:@"CELL"];
       
       UINib *LocationNib = [UINib nibWithNibName:@"MyWorkLocationCell" bundle:nil];
       [FilterTableView registerNib:LocationNib forCellReuseIdentifier:@"CELL1"];
       
       UINib *SelectionNib = [UINib nibWithNibName:@"DurationCell" bundle:nil];
       [FilterTableView registerNib:SelectionNib forCellReuseIdentifier:@"CELL2"];
       
    }
   else if([isComeFrom isEqualToString:@"MYTEAM"] ){
       PlusImageArray = [[NSMutableArray alloc] initWithObjects:@"Plus", nil];
       MinusImageArray = [[NSMutableArray alloc] initWithObjects:@"Minus", nil];
       FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"EMPLOYEE_FILTER", nil), nil];
       
       UINib *SearchNib = [UINib nibWithNibName:@"MyWorkEmployeeCell" bundle:nil];
       [FilterTableView registerNib:SearchNib forCellReuseIdentifier:@"CELL"];
   }
   else if([isComeFrom isEqualToString:@"MYTASK"] ){
       PlusImageArray = [[NSMutableArray alloc] initWithObjects:@"Plus", nil];
       MinusImageArray = [[NSMutableArray alloc] initWithObjects:@"Minus", nil];
       FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"DURATION_FILTER", nil), nil];
       UINib *SelectionNib = [UINib nibWithNibName:@"DurationCell" bundle:nil];
       [FilterTableView registerNib:SelectionNib forCellReuseIdentifier:@"CELL2"];
   }

    arrayForBool=[[NSMutableArray alloc]init];
    for (int i=0; i<[FilterArray count]; i++) {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    FilterTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, FilterTableView.bounds.size.width, 0.01f)];

    CancelBtn.layer.borderWidth = 1.0;
    CancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    CancelBtn.layer.cornerRadius = 3.0;
    CancelBtn.clipsToBounds = YES;
    
    CancelFilterBtn.layer.borderWidth = 1.0;
    CancelFilterBtn.layer.borderColor = delegate.redColor.CGColor;
    CancelFilterBtn.layer.cornerRadius = 3.0;
    CancelFilterBtn.clipsToBounds = YES;
    CancelFilterBtn.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    
    ApplyFilterBtn.layer.cornerRadius = 3.0;
    BottomView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    self.view.backgroundColor = [UIColor whiteColor];
    FilterTableView.backgroundColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [FilterArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([isComeFrom isEqualToString:@"GIVEFEEDBACK"])
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict = [userDefault objectForKey:isComeFrom];
        NSLog(@"%@",dict);
        if(indexPath.section == 0)
        {
           MyWorkLocationCell * Cell = (MyWorkLocationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            Cell.LocationTextField.delegate = self;
            if (dict != nil)
            {
                if ([[dict valueForKey:@"employeeName"] isEqualToString:@""] || [dict valueForKey:@"employeeName"] == nil)
                {
                    
                    if ([EmployeeName isEqualToString:@""])
                    {
                        Cell.LocationTextField.text = EmployeeName;
                        Cell.LocationTextField.placeholder = NSLocalizedString(@"SEARCH_EMPLOYEE_NAME_FILTER", nil);
                    }
                    else
                    {
                        Cell.LocationTextField.text = EmployeeName;
                    }
                }
                else
                {
                    if ([EmployeeName isEqualToString:@""])
                    {
                        Cell.LocationTextField.text = [dict valueForKey:@"employeeName"];
                        EmployeeName = Cell.LocationTextField.text;
                    }
                    else
                    {
                        Cell.LocationTextField.text = EmployeeName;
                    }
                }
            }
            else
            {
                if ([EmployeeName isEqualToString:@""])
                {
                    Cell.LocationTextField.text = EmployeeName;
                    Cell.LocationTextField.placeholder = NSLocalizedString(@"SEARCH_EMPLOYEE_NAME_FILTER", nil);
                }
                else
                {
                    Cell.LocationTextField.text = EmployeeName;
                }
            }
            return Cell;
        }
        else{
            DurationCell * Cell = (DurationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
            [Cell.StartButton addTarget:self action:@selector(StartButtonAction) forControlEvents:UIControlEventTouchUpInside];
            [Cell.EndButton addTarget:self action:@selector(EndButtonAction) forControlEvents:UIControlEventTouchUpInside];
            Cell.DateView.hidden = NO;
    
            if (dict != nil)
            {
                if ([[dict valueForKey:@"startDate"] isEqualToString:@""] || [dict valueForKey:@"startDate"] == nil)
                {
                    if ([StartDateStr isEqualToString:@""])
                    {
                        Cell.StartDateTextField.text = StartDateStr;
                    }
                    else
                    {
                        Cell.StartDateTextField.text = StartDateStr;
                    }
                    
                    if ([EndDateStr isEqualToString:@""])
                    {
                        Cell.EndDateTextField.text = EndDateStr;
                    }
                    else
                    {
                        Cell.EndDateTextField.text = EndDateStr;
                    }
                }
                else
                {
                    if ([StartDateStr isEqualToString:@""] || StartDateStr == nil)
                    {
                        Cell.StartDateTextField.text = [dict valueForKey:@"startDate"];
                        StartDateStr = Cell.StartDateTextField.text;
                        StartDate = Cell.StartDateTextField.text;
                    }
                    else
                    {
                        Cell.StartDateTextField.text = StartDateStr;
                    }
                    
                    if ([EndDateStr isEqualToString:@""] || EndDateStr == nil)
                    {
                        Cell.EndDateTextField.text = [dict valueForKey:@"ennDate"];
                        EndDateStr = Cell.EndDateTextField.text;
                        EndDate = Cell.EndDateTextField.text;
                    }
                    else
                    {
                        Cell.EndDateTextField.text = EndDateStr;
                    }
                }
            }
            else
            {
                if ([StartDateStr isEqualToString:@""])
                {
                    Cell.StartDateTextField.text = StartDateStr;
                }
                else
                {
                    Cell.StartDateTextField.text = StartDateStr;
                }
                
                if ([EndDateStr isEqualToString:@""])
                {
                    Cell.EndDateTextField.text = EndDateStr;
                }
                else
                {
                    Cell.EndDateTextField.text = EndDateStr;
                }
            }
            return Cell;
        }
    }
    else if([isComeFrom isEqualToString:@"STACKHOLDER"])
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict = [userDefault objectForKey:isComeFrom];
        NSLog(@"%@",dict);
        if(indexPath.section == 0){
            MyWorkLocationCell * Cell = (MyWorkLocationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            Cell.LocationTextField.delegate = self;
            Cell.LocationTextField.keyboardType = UIKeyboardTypeNumberPad;
            if (dict != nil)
            {
                if ([[dict valueForKey:@"taskId"] integerValue] == 0 || [dict valueForKey:@"taskId"] == nil)
                {
                    if ([TrackID isEqualToString:@""])
                    {
                        Cell.LocationTextField.text = TrackID;
                        Cell.LocationTextField.placeholder = NSLocalizedString(@"SEARCH_TASK_ID", nil);
                    }
                    else
                    {
                        Cell.LocationTextField.text = TrackID;
                    }
                }
                else
                {
                    if ([TrackID isEqualToString:@""])
                    {
                        Cell.LocationTextField.text = [NSString stringWithFormat:@"%@", [dict valueForKey:@"taskId"]];
                        TrackID = Cell.LocationTextField.text;
                    }
                    else
                    {
                        Cell.LocationTextField.text = TrackID;
                    }
                }
            }
            else
            {
                if ([TrackID isEqualToString:@""])
                {
                    Cell.LocationTextField.text = TrackID;
                    Cell.LocationTextField.placeholder = NSLocalizedString(@"SEARCH_TASK_ID", nil);
                }
                else
                {
                    Cell.LocationTextField.text = TrackID;
                }
            }
                return Cell;
        }
        else if (indexPath.section == 1){
            MyWorkLocationCell * Cell = (MyWorkLocationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            Cell.LocationTextField.delegate = self;
//            Cell.LocationTextField.placeholder = NSLocalizedString(@"SEARCH_EMPLOYEE_NAME_FILTER", nil);
            Cell.LocationTextField.keyboardType = UIKeyboardTypeDefault;
//            Cell.LocationTextField.text = EmployeeName;
            if (dict != nil)
            {
                if ([[dict valueForKey:@"employeeName"] isEqualToString:@""] || [dict valueForKey:@"employeeName"] == nil)
                {
                    if ([EmployeeName isEqualToString:@""])
                    {
                        Cell.LocationTextField.text = EmployeeName;
                        Cell.LocationTextField.placeholder = NSLocalizedString(@"SEARCH_EMPLOYEE_NAME_FILTER", nil);
                    }
                    else
                    {
                        Cell.LocationTextField.text = EmployeeName;
                    }
                }
                else
                {
                    if ([EmployeeName isEqualToString:@""])
                    {
                        Cell.LocationTextField.text = [dict valueForKey:@"employeeName"];
                        EmployeeName = Cell.LocationTextField.text;
                    }
                    else
                    {
                        Cell.LocationTextField.text = EmployeeName;
                    }
                }
            }
            else
            {
                if ([EmployeeName isEqualToString:@""])
                {
                    Cell.LocationTextField.text = EmployeeName;
                    Cell.LocationTextField.placeholder = NSLocalizedString(@"SEARCH_EMPLOYEE_NAME_FILTER", nil);
                }
                else
                {
                    Cell.LocationTextField.text = EmployeeName;
                }
            }

            return Cell;
        }
        else{
            DurationCell * Cell = (DurationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
//            Cell.StartDateTextField.delegate = self;
//            Cell.EndDateTextField.delegate = self;
//            Cell.StartDateTextField.inputView = DatePickerViewBg;
//            Cell.EndDateTextField.inputView = DatePickerViewBg;
            [Cell.StartButton addTarget:self action:@selector(StartButtonAction) forControlEvents:UIControlEventTouchUpInside];
            [Cell.EndButton addTarget:self action:@selector(EndButtonAction) forControlEvents:UIControlEventTouchUpInside];
            Cell.DateView.hidden = NO;
            
            if (dict != nil)
            {
                if ([[dict valueForKey:@"startDate"] isEqualToString:@""] || [dict valueForKey:@"startDate"] == nil)
                {
                    if ([StartDateStr isEqualToString:@""])
                    {
                        Cell.StartDateTextField.text = StartDateStr;
                    }
                    else
                    {
                        Cell.StartDateTextField.text = StartDateStr;
                    }
                    
                    if ([EndDateStr isEqualToString:@""])
                    {
                        Cell.EndDateTextField.text = EndDateStr;
                    }
                    else
                    {
                        Cell.EndDateTextField.text = EndDateStr;
                    }
                }
                else
                {
                    if ([StartDateStr isEqualToString:@""] || StartDateStr == nil)
                    {
                        Cell.StartDateTextField.text = [dict valueForKey:@"startDate"];
                        StartDateStr = Cell.StartDateTextField.text;
                        StartDate = Cell.StartDateTextField.text;
                    }
                    else
                    {
                        Cell.StartDateTextField.text = StartDateStr;
                    }
                    
                    if ([EndDateStr isEqualToString:@""] || EndDateStr == nil)
                    {
                        Cell.EndDateTextField.text = [dict valueForKey:@"endDate"];
                        EndDateStr = Cell.EndDateTextField.text;
                        EndDate = Cell.EndDateTextField.text;
                    }
                    else
                    {
                        Cell.EndDateTextField.text = EndDateStr;
                    }
                }
            }
            else
            {
                if ([StartDateStr isEqualToString:@""])
                {
                    Cell.StartDateTextField.text = StartDateStr;
                }
                else
                {
                    Cell.StartDateTextField.text = StartDateStr;
                }
                
                if ([EndDateStr isEqualToString:@""])
                {
                    Cell.EndDateTextField.text = EndDateStr;
                }
                else
                {
                    Cell.EndDateTextField.text = EndDateStr;
                }
            }

            return Cell;
        }
    }
    else if([isComeFrom isEqualToString:@"MYTEAM"])
    {
        MyWorkEmployeeCell * Cell = (MyWorkEmployeeCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        if(RadioSelect==YES)
        {
            Cell.EmployeeNameRadioImage.image = [UIImage imageNamed:@"RadioOn"];
            Cell.EmployeeIDRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            Cell.EmployeeIDTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME_FILTER", nil);
            Cell.EmployeeIDTextField.keyboardType = UIKeyboardTypeDefault;
            [Cell.EmployeeIDTextField becomeFirstResponder];
            Cell.EmployeeIDTextField.text = iWorkemp_idStr;
            
        }
        else{
            Cell.EmployeeNameRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            Cell.EmployeeIDRadioImage.image = [UIImage imageNamed:@"RadioOn"];
            Cell.EmployeeIDTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID_FILTER", nil);
            Cell.EmployeeIDTextField.keyboardType = UIKeyboardTypeNumberPad;
            [Cell.EmployeeIDTextField becomeFirstResponder];
            Cell.EmployeeIDTextField.text = iWorkemp_nameStr;
        }
        [Cell.EmployeeNameBtn addTarget:self action:@selector(EmployeeRadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.EmployeeIDBtn addTarget:self action:@selector(EmployeeRadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
        return Cell;
    }
    
    else
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict = [userDefault objectForKey:isComeFrom];
        NSLog(@"%@",dict);
        DurationCell * Cell = (DurationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
//        Cell.StartDateTextField.delegate = self;
//        Cell.EndDateTextField.delegate = self;
//        Cell.StartDateTextField.inputView = DatePickerViewBg;
//        Cell.EndDateTextField.inputView = DatePickerViewBg;
        [Cell.StartButton addTarget:self action:@selector(StartButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [Cell.EndButton addTarget:self action:@selector(EndButtonAction) forControlEvents:UIControlEventTouchUpInside];
        Cell.DateView.hidden = NO;
        
        if (dict != nil)
        {
            if ([[dict valueForKey:@"s_date"] isEqualToString:@""] || [dict valueForKey:@"s_date"] == nil)
            {
                if ([StartDateStr isEqualToString:@""])
                {
                    Cell.StartDateTextField.text = StartDateStr;
                }
                else
                {
                    Cell.StartDateTextField.text = StartDateStr;
                }
                
                if ([EndDateStr isEqualToString:@""])
                {
                    Cell.EndDateTextField.text = EndDateStr;
                }
                else
                {
                    Cell.EndDateTextField.text = EndDateStr;
                }
            }
            else
            {
                if ([StartDateStr isEqualToString:@""] || StartDateStr == nil)
                {
                    Cell.StartDateTextField.text = [dict valueForKey:@"s_date"];
                    StartDateStr = Cell.StartDateTextField.text;
                    StartDate = Cell.StartDateTextField.text;
                }
                else
                {
                    Cell.StartDateTextField.text = StartDateStr;
                }
                
                if ([EndDateStr isEqualToString:@""] || EndDateStr == nil)
                {
                    Cell.EndDateTextField.text = [dict valueForKey:@"e_date"];
                    EndDateStr = Cell.EndDateTextField.text;
                    EndDate = Cell.EndDateTextField.text;
                }
                else
                {
                    Cell.EndDateTextField.text = EndDateStr;
                }
            }
        }
        else
        {
            if ([StartDateStr isEqualToString:@""])
            {
                Cell.StartDateTextField.text = StartDateStr;
            }
            else
            {
                Cell.StartDateTextField.text = StartDateStr;
            }
            
            if ([EndDateStr isEqualToString:@""])
            {
                Cell.EndDateTextField.text = EndDateStr;
            }
            else
            {
                Cell.EndDateTextField.text = EndDateStr;
            }
        }
        

        return Cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([isComeFrom isEqualToString:@"GIVEFEEDBACK"]||[isComeFrom isEqualToString:@"STACKHOLDER"]||[isComeFrom isEqualToString:@"MYTASK"]){
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
           return 60;
        }
        else{
          return 0;
        }
    }
    else if([isComeFrom isEqualToString:@"MYTEAM"]){
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return 150;
        }
        else{
            return 0;
        }
    }
    return 0;

}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
#pragma mark - Creating View for TableView Section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *Views=[[UIView alloc]initWithFrame:CGRectMake(0, 0, FilterTableView.frame.size.width,0)];
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, FilterTableView.frame.size.width,44)];
    sectionView.tag=section;
    sectionView.backgroundColor  = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
    UILabel *viewLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, FilterTableView.frame.size.width, 44)];
    viewLabel.backgroundColor=[UIColor clearColor];
    viewLabel.textAlignment = NSTextAlignmentLeft;
    viewLabel.textColor=[UIColor blackColor];
    viewLabel.font=[UIFont systemFontOfSize:14];
    viewLabel.text=[NSString stringWithFormat:@"%@",[FilterArray objectAtIndex:section]];
    [sectionView addSubview:viewLabel];
   
    UIImageView *ImageViews = [[UIImageView alloc] initWithFrame:CGRectMake(FilterTableView.frame.size.width-40, 17, 10, 10)];
    if ([[arrayForBool objectAtIndex:section] boolValue]){
        ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[MinusImageArray objectAtIndex:section]]];
    }
    else{
        ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[PlusImageArray objectAtIndex:section]]];
    }
    [sectionView addSubview:ImageViews];
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    return  sectionView;
    return Views;
}
#pragma mark - Table header gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[FilterArray count]; i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)EmployeeRadioBtnOnOff:(UIButton*)Sender{
    switch (Sender.tag) {
        case 0:
            RadioSelect = YES;
            [FilterTableView reloadData];
            break;
        case 1:
            RadioSelect = NO;
            [FilterTableView reloadData];
            break;
        default:
            break;
    }
}
-(void)clearData
{
    iWorkemp_idStr = @"";
    iWorkemp_nameStr = @"";
    EmployeeID = @"";
    EmployeeName = @"";
    TrackID = @"";
    StartDateStr = @"";
    EndDateStr = @"";
    RadioSelect=YES;
}
-(IBAction)CancelAction:(id)sender{
    [self GetCellValue];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)ClearFilterAction:(id)sender
{
    [FilterTableView reloadData];
    if([isComeFrom isEqualToString:@"GIVEFEEDBACK"])
    {
        [userDefault removeObjectForKey:isComeFrom];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearFilteriPM" object:nil userInfo:UserDic];
    }
    else if([isComeFrom isEqualToString:@"STACKHOLDER"])
    {
        [userDefault removeObjectForKey:isComeFrom];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearFilteriPM" object:nil userInfo:UserDic];
    }
    else if([isComeFrom isEqualToString:@"MYTEAM"])
    {
        [userDefault removeObjectForKey:isComeFrom];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearFilteriPM" object:nil userInfo:UserDic];
    }
    else if([isComeFrom isEqualToString:@"MYTASK"])
    {
        [userDefault removeObjectForKey:isComeFrom];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)MyTeamApplyFilter:(id)sender{
    [self GetCellValue];
    if([isComeFrom isEqualToString:@"GIVEFEEDBACK"]){
        EmployeeName = Cell4.LocationTextField.text;
        StartDate  = Cell5.StartDateTextField.text;
        EndDate  = Cell5.EndDateTextField.text;
        
        UserDic = @ {
            @"employeeName": EmployeeName,
            @"ennDate": EndDate,
            @"startDate": StartDate
        };
        [userDefault setObject:UserDic forKey:isComeFrom];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GIVEFEEDBACK" object:nil userInfo:UserDic];
    }
    else if([isComeFrom isEqualToString:@"STACKHOLDER"]){
        StartDate  = Cell5.StartDateTextField.text;
        EndDate  = Cell5.EndDateTextField.text;
        UserDic = @ {
            @"employeeName": EmployeeName,
            @"endDate": EndDate,
            @"startDate": StartDate,
            @"taskId":  [NSNumber numberWithInt:[TrackID intValue]],
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        [userDefault setObject:UserDic forKey:isComeFrom];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STACKHOLDER" object:nil userInfo:UserDic];
    }
    else if([isComeFrom isEqualToString:@"MYTEAM"]){
        if(RadioSelect == YES){
            EmployeeName = Cell3.EmployeeIDTextField.text;
            UserDic = @ {
                @"user_id" : [ApplicationState userId],
                @"empName":EmployeeName,
                @"handsetId": @"",
                @"empId":@""
            };
            
        }
        else if(RadioSelect == NO) {
            EmployeeID = Cell3.EmployeeIDTextField.text;
            UserDic = @ {
                @"user_id" : [ApplicationState userId],
                @"empName":@"",
                @"handsetId": @"",
                @"empId":EmployeeID
            };
        }
        [userDefault setObject:UserDic forKey:isComeFrom];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MYTEAM" object:nil userInfo:UserDic];
    }
    else if([isComeFrom isEqualToString:@"MYTASK"]){
        StartDate  = Cell5.StartDateTextField.text;
        EndDate  = Cell5.EndDateTextField.text;
        UserDic = @ {
            @"e_date": EndDate,
            @"employee_Id": [ApplicationState userId],
            @"s_date": StartDate
        };
        [userDefault setObject:UserDic forKey:isComeFrom];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MYTASK" object:nil userInfo:UserDic];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)StartButtonAction{
    DatePickerViewBg.hidden = NO;
    DatePickerSelectionStr = @"Start";
    [self.view endEditing:YES];
}
-(void)EndButtonAction{
    DatePickerViewBg.hidden = NO;
    DatePickerSelectionStr = @"End";
    [self.view endEditing:YES];
}

-(IBAction)DatePickerDoneAction{
    DatePickerViewBg.hidden = YES;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    if([DatePickerSelectionStr isEqualToString:@"Start"]){
        StartDateStr = [dateFormat stringFromDate: DatePicker.date];
        EndDateStr = @"";
    }
    else if([DatePickerSelectionStr isEqualToString:@"End"]){
        EndDateStr = [dateFormat stringFromDate: DatePicker.date];
    }
    [self ReloadSection];
//    [FilterTableView reloadData];
    if(![EndDateStr isEqualToString:@""]){
        NSComparisonResult result;
        NSDate *startDate = [dateFormat dateFromString:StartDateStr];
        NSDate *endDate = [dateFormat dateFromString:EndDateStr];
        result = [startDate compare:endDate];
        if(result==NSOrderedAscending){
            [self ReloadSection];
        }
        else if(result==NSOrderedDescending){
            EndDateStr = @"";
            [self ShowAlert:@"You cannot select date earlier to start date" ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
    }
}
-(void)ReloadSection{
    if([isComeFrom isEqualToString:@"MYTASK"]){
        [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else if([isComeFrom isEqualToString:@"STACKHOLDER"]){
        [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else{
        [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
-(void)GetCellValue{
    if([isComeFrom isEqualToString:@"GIVEFEEDBACK"]){
        indexPath4 = [NSIndexPath indexPathForRow:0 inSection:0];
        Cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
        [Cell4.LocationTextField resignFirstResponder];

        indexPath5 = [NSIndexPath indexPathForRow:0 inSection:1];
        Cell5 = [FilterTableView cellForRowAtIndexPath:indexPath5];
        [Cell5.StartDateTextField resignFirstResponder];
        [Cell5.EndDateTextField resignFirstResponder];
    }
    else if([isComeFrom isEqualToString:@"STACKHOLDER"]){
        indexPath4 = [NSIndexPath indexPathForRow:0 inSection:0];
        Cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
        TrackID = Cell4.LocationTextField.text;
        [Cell4.LocationTextField resignFirstResponder];
        
        indexPath4 = [NSIndexPath indexPathForRow:0 inSection:1];
        Cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
        EmployeeName = Cell4.LocationTextField.text;
        [Cell4.LocationTextField resignFirstResponder];
        
        indexPath5 = [NSIndexPath indexPathForRow:0 inSection:2];
        Cell5 = [FilterTableView cellForRowAtIndexPath:indexPath5];
        [Cell5.StartDateTextField resignFirstResponder];
        [Cell5.EndDateTextField resignFirstResponder];
    }
    else if([isComeFrom isEqualToString:@"MYTASK"]){
        indexPath5 = [NSIndexPath indexPathForRow:0 inSection:0];
        Cell5 = [FilterTableView cellForRowAtIndexPath:indexPath5];
        [Cell5.StartDateTextField resignFirstResponder];
        [Cell5.EndDateTextField resignFirstResponder];
    }
    else{
        indexPath3 = [NSIndexPath indexPathForRow:0 inSection:0];
        Cell3 = [FilterTableView cellForRowAtIndexPath:indexPath3];
        [Cell3.EmployeeIDTextField resignFirstResponder];
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    DatePickerViewBg.hidden = YES;
}
@end
