//
//  AllViewController.m
//  iWork
//
//  Created by Shailendra on 07/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "AllViewController.h"
#import "Header.h"

@interface AllViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *AllTableView;
    NSMutableArray *AllArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UIView *NoDataView;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
}

@end

@implementation AllViewController
@synthesize AllDics;
- (void)viewDidLoad {
//    [self clearfilterUserdefaultData];
    offset = 0;
    limit = 50;
    AllArray = [[NSMutableArray alloc] init];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    AllTableView.estimatedRowHeight = 50;
    AllTableView.rowHeight = UITableViewAutomaticDimension;
    AllTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, AllTableView.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [AllTableView addSubview:refreshControl];
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [AllTableView registerNib:Nib forCellReuseIdentifier:@"CELL"];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"ALL" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ClearFilterData:) name:@"ClearFilteriPM" object:nil];
     [self AllWebApi];
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated
{
    if(![CheckValue isEqualToString:@"FILTER"]){
        [self EmptyArray];
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(AllWebApi) withObject:nil afterDelay:0.5];
    }
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    AllArray = [[NSMutableArray alloc] init];
    [self AllWebApi];
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    AllArray = [[NSMutableArray alloc] init];
}
- (void)reloadData
{
    if(![CheckValue isEqualToString:@"FILTER"]){
        CheckValue = @"Refresh";
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(AllWebApi) withObject:nil afterDelay:0.5];
    }
    [self EmptyArray];
}
-(void)ClearFilterData:(NSNotification*)notification{
    CheckValue = @"";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(AllWebApi) withObject:nil afterDelay:0.5];
}
- (void)AllWebApi {
    if(delegate.isInternetConnected){
        if(![CheckValue isEqualToString:@"FILTER"]){
            params = @ {
                @"fromDate": @"",
                @"handsetId": @"",
                @"status": @"0",
                @"taskId": @"",
                @"taskType": @"",
                @"toDate": @"",
                @"user_id"  : [AllDics valueForKey:@"empId"],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        ResponseDic =  [Api WebApi:params Url:@"getEmployeeTaskList"];
        [refreshControl endRefreshing];
        if(ResponseDic==nil){
            [LoadingManager hideLoadingView:self.view];
            [self AllWebApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            NSLog(@"AllResponseDic==%@",ResponseDic);
            [LoadingManager hideLoadingView:self.view];
            NoDataView.hidden = YES;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]];
                }
                else {
                    [LoadingManager hideLoadingView:self.view];
                    [self NoRecordFound];
                }
                [AllTableView reloadData];
            }
            else{
                [LoadingManager hideLoadingView:self.view];
                [self NoRecordFound];
            }
            [AllTableView reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self NoRecordFound];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoRecordFound{
    if(AllArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Record Found";
    }
    [AllTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
        NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            if([CheckValue isEqualToString:@"FILTER"]){
                NSMutableDictionary *NewDic = [params mutableCopy];
                for (int i = 0 ; i< [NewDic count];i++){
                    if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                        [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                    }
                    else{
                        [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                    }
                }
                params = [NewDic mutableCopy];
            }
            [self AllWebApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [AllArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [AllArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
     Cell.tag = indexPath.row;
    Cell.ViewDetailsBtn.tag = indexPath.row;
    [Cell.ViewDetailsBtn addTarget:self action:@selector(GotoEmployeeDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(AllArray.count>0){
        NSDictionary * responseData = [AllArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
        
        NSString *statusType;
        NSString *imageType;
        if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Cancelled"]){
            statusType = @"Cancelled";
            imageType = @"StatusRejected";
            Cell.LineLabel.hidden = NO;
            Cell.ViewDetailsBtn.hidden =NO;
            Cell.hight.constant = 32.0;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Discarded"]){
            statusType = @"Discarded";
            imageType = @"StatusRejected";
            Cell.ViewDetailsBtn.hidden =YES;
            Cell.LineLabel.hidden = YES;
            Cell.hight.constant = 0.0;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Declined"]||[[responseData objectForKey:@"taskStatus"] isEqualToString:@"FAILED"]){
            statusType = @"Declined";
            imageType = @"StatusRejected";
            Cell.ViewDetailsBtn.hidden =YES;
            Cell.hight.constant = 0.0;
            Cell.LineLabel.hidden = YES;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if([[responseData objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
            statusType = @"In-Progress";
            imageType = @"WatingForApprove";
            Cell.ViewDetailsBtn.hidden =NO;
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Submitted"]){
            statusType = @"Submitted";
            imageType = @"Completes";
            Cell.ViewDetailsBtn.hidden =NO;
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"CLOSED"]){
            statusType = @"Assigned";
            imageType = @"Completes";
            Cell.ViewDetailsBtn.hidden =NO;
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
            statusType = @"Assigned";
            imageType = @"Completes";
            Cell.ViewDetailsBtn.hidden =YES;
            Cell.hight.constant = 0.0;
            Cell.LineLabel.hidden = YES;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"New"]||[[responseData valueForKey:@"taskStatus"] isEqualToString:@"new"]){
            statusType = @"New";
            imageType = @"New";
            Cell.ViewDetailsBtn.hidden =YES;
            Cell.hight.constant = 0.0;
            Cell.LineLabel.hidden = YES;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Rejected"]){
            statusType = @"Rejected";
            imageType = @"StatusRejected";
            Cell.ViewDetailsBtn.hidden = NO;
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Approved"]){
            Cell.RatingView.hidden = NO;
            Cell.RateStaticLabel.hidden = NO;
            Cell.DurationStaticLabel.hidden = YES;
            Cell.DurationLabel.hidden = YES;
            statusType = @"Competed";
            imageType = @"Completes";
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.ViewDetailsBtn.hidden = NO;
        }
        Cell.StatusLabel.text = statusType;
        Cell.StatusLabel.textColor = [UIColor whiteColor];
        Cell.StausImageView.image = [UIImage imageNamed:imageType];
    }
    return Cell;
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm aa"];
    return [dateformate stringFromDate:Newdate];
}
-(void)GotoEmployeeDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    NSString* str = [[AllArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    if ([[[AllArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
        TaskDetailManagerScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailManagerScreen"];
        viewController.taskID = str;
        viewController.isManager = YES;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
    else{
        HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
-(void)clearfilterUserdefaultData
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"All"];
    [userDefault synchronize];
}
@end
