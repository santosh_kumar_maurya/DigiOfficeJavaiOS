//
//  MyKIPCell.swift
//  iWork
//
//  Created by Shailendra on 25/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class EditKPICell: UITableViewCell {
    
    @IBOutlet var EditKPILabel : UILabel!
    @IBOutlet var EditKPIBtn : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
