#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DownPicker.h"
#import "DQAlertView.h"
#import "NYAlertViewController.h"
#import "VCFloatingActionButton.h"
#import "AFURLSessionManager.h"
#import "FCAlertView.h"

@interface NewTaskDetailScreen : UIViewController <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,floatMenuDelegate, FCAlertViewDelegate>
{
    AppDelegate *delegate;
    IBOutlet UITableView *tab;
    NSMutableArray *tabdataarray1,*tabdataarray2,*dataArray,*dataConv;
    NSMutableDictionary *dataDict;
    UILabel *flatButton_lab,*flatButton1_lab,*sel_lab,*lab1,*datelab,*flatButton2_lab;
    UITextField *searchtxt,*searchtxt1,*renttext,*phonetext,*desctext;
    BOOL isShowDetail, isMe, isNewReq, isServerResponded;
    NSString *taskID,*taskType,*taskFeedback;
    int count;
    long taskRating;
    UIPickerView *areaSelect;
    UIDatePicker *dateTimePicker;
    UIView *mainScreenView, *msgvw;//, *screenView;
    UIAlertView *alertView;
    UITextField *alertTextField, *alertTextField1;
    VCFloatingActionButton *addButton;
    FCAlertView *alert;
}
@property (nonatomic) BOOL isShowDetail,isMe, isNewReq;
@property (nonatomic) int count;
@property (nonatomic, retain) NSMutableDictionary *dataDict;
@property (nonatomic, retain) NSString *taskID, *taskType,*EmployeeID;
@property (strong, nonatomic) DownPicker *downPicker;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
- (void) ScreenDesign;
- (void)showCommentAlert;
@end
