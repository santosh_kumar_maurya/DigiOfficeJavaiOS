//
//  NeedRevisionCell.h
//  iWork
//
//  Created by Shailendra on 21/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NeedRevisionCell : UITableViewCell

@property(nonnull,retain)IBOutlet UILabel *NotificationLabel;
@property(nonnull,retain)IBOutlet UILabel *DateLabel;
@property(nonnull,retain)IBOutlet UIImageView *StatusImageView;
@property(nonnull,retain)IBOutlet UIButton *RjectBtn;
@property(nonnull,retain)IBOutlet UIButton *ApproveBtn;
@property(nonnull,retain)IBOutlet UIButton *NeedRevisionBtn;
@property(nonnull,retain)IBOutlet UIButton *CompleteApproveBtn;
@property(nonnull,retain)IBOutlet UIButton *StartTaskBtn;
@property(nonnull,retain)IBOutlet UILabel *LineLabel;

- (void)configureCell:(NSDictionary*_Nullable)info;
@end
