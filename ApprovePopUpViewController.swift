//
//  ApproveViewController.swift
//  iWork
//
//  Created by Shailendra on 05/11/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

@objc protocol RejectionDelegate {
    @objc optional func RejectionWebApiCall(RejectionText:String)
    @objc optional func StartCancelTask(TaskText:String)
}

class ApprovePopUpViewController: UIViewController {
    
    var delegate : RejectionDelegate?
    
    @IBOutlet var TitleLabel : UILabel!
    @IBOutlet var DiscriptionLabel : UILabel!
    @IBOutlet var iconImageView : UIImageView!
    var TitleStr : String!
    var DiscriptionStr : String!
    var ImageNameStr : String!
    
    @IBOutlet var ConfirmedView : UIView!
    @IBOutlet var RejectionView : UIView!
    @IBOutlet var RejectionTextView : UITextView!
    @IBOutlet var PopupTitleLabel : UILabel!
    
    @IBOutlet var ConfirmPopupView : UIView!
    @IBOutlet var ConfirmTitleLabel : UILabel!
    @IBOutlet var ConfirmDescriptionLabel : UILabel!
    @IBOutlet var OKayButton : UIButton!
    
    var NameOpenView : String! = ""
    var Objdelegate = AppDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ConfirmTitleLabel.textColor = UIColor.TextBlueColor()
        Objdelegate = UIApplication.shared.delegate as! AppDelegate
        RejectionTextView.layer.cornerRadius = 3
        RejectionTextView.layer.borderColor = UIColor.lightGray.cgColor
        RejectionTextView.layer.borderWidth = 1.0
        PopupTitleLabel.textColor = UIColor.TextBlueColor()
        if(NameOpenView == "CONFORMVIEW"){
            ConfirmedView.isHidden = false
            TitleLabel.text = "\(TitleStr!)"
            DiscriptionLabel.text = "\(DiscriptionStr!)"
            iconImageView.image = UIImage(named: "\(ImageNameStr!)")
        }
        else if(NameOpenView == "REJECTVIEW"){
             RejectionView.isHidden = false
        }
        else if(NameOpenView == "STARTTASK"){
            ConfirmPopupView.isHidden = false
            ConfirmTitleLabel.text = "\(TitleStr!)"
            ConfirmDescriptionLabel.text = "\(DiscriptionStr!)"
            OKayButton.setTitleColor(UIColor.ApproveGreenColor(), for: UIControlState.normal)
        }
        else if(NameOpenView == "CANCELTASK"){
             ConfirmPopupView.isHidden = false
            ConfirmTitleLabel.text = "\(TitleStr!)"
            ConfirmDescriptionLabel.text = "\(DiscriptionStr!)"
            OKayButton.setTitleColor(UIColor.RedColor(), for: UIControlState.normal)
        }
    }
    
    @IBAction func DisminssAction(){
        self.dismiss(animated: false, completion: nil);
    }
    @IBAction func SubmitAction(){
        if (Objdelegate.isInternetConnected()){
            if(RejectionTextView.text == ""){
                self.ShowAlert(MsgTitle: "Please enter rejection", BtnTitle: "OK")
            }
            else{
                delegate?.RejectionWebApiCall!(RejectionText: RejectionTextView.text!)
                self.dismiss(animated: false, completion: nil)
            }
        }
        else{
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
        
    }
    @IBAction func StartCancelAction(){
        if (Objdelegate.isInternetConnected()){
            if(NameOpenView == "STARTTASK"){
                delegate?.StartCancelTask!(TaskText: "STARTTASK")
                self.dismiss(animated: false, completion: nil);
            }
            else if(NameOpenView == "CANCELTASK"){
                delegate?.StartCancelTask!(TaskText: "CANCELTASK")
                self.dismiss(animated: false, completion: nil);
            }
        }
        else{
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: false, completion: nil)
    }
}
