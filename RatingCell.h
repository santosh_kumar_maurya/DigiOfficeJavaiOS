//
//  RatingCell.h
//  iWork
//
//  Created by Shailendra on 08/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *LowestBtn;
@property (weak, nonatomic) IBOutlet UIButton *HigestBtn;
@property (weak, nonatomic) IBOutlet UIButton *AllBtn;
@property (weak, nonatomic) IBOutlet UIImageView *LowestImageView;
@property (weak, nonatomic) IBOutlet UIImageView *HigestImageView;
@property (weak, nonatomic) IBOutlet UIImageView *AllImageView;
@property (weak, nonatomic) IBOutlet UILabel *LowestImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *HigestImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *AllImageLabel;


@end
