//
//  GiveRecognitionsCell.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class GiveRecognitionsCell: UITableViewCell {

    @IBOutlet var TaskIDLabel : UILabel!
    @IBOutlet var CreateDateLabel : UILabel!
    @IBOutlet var TaskNameStaticLabel : UILabel!
    @IBOutlet var TaskNameLabel : UILabel!
    @IBOutlet var AssignToStaticLabel : UILabel!
    @IBOutlet var AssignToLabel : UILabel!
    @IBOutlet var DetailsBtn : UIButton!
    @IBOutlet var GiveRecognitionsBtn : UIButton!
    @IBOutlet var RatingView: HCSStarRatingView!
    @IBOutlet var TaskView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        GiveRecognitionsBtn.setTitleColor(UIColor.ApproveGreenColor(), for: UIControlState.normal)
        DetailsBtn.setTitleColor(UIColor.ButtonSkyColor(), for: UIControlState.normal)
    }
    func ConfigurationCell(info:NSDictionary){
        if (info["taskId"] as? String) != nil{
            TaskIDLabel.text = "Task ID : \(String(describing: info["taskId"]!))"
        }
        else{
            TaskIDLabel.text = " "
        }
        if (info["taskCreationDate"] as? NSNumber) != nil{
            let Number = info["taskCreationDate"] as! NSNumber
            CreateDateLabel.text = GetDate(datenumber: Number)
        }
        else{
            CreateDateLabel.text = " "
        }
        if (info["taskName"] as? String) != nil{
            TaskNameLabel.text = "\(String(describing: info["taskName"]!))"
        }
        else{
            TaskNameLabel.text = " "
        }
        if (info["assignedTo"] as? String) != nil{
            AssignToLabel.text = "\(String(describing: info["assignedTo"]!))"
        }
        else{
            AssignToLabel.text = " "
        }
    }
    func GetDate(datenumber:NSNumber) -> String{
        let DateStr = NSString(format: "%@", datenumber)
        let miliSec = Double(DateStr as String)
        let TakeOffDate = NSDate(timeIntervalSince1970: miliSec!/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm aa"
        let dateStr = dateFormatter.string(from: TakeOffDate as Date)
        let Newdate = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = "dd MMM YYYY hh:mm aa"
        return dateFormatter.string(from: Newdate!)
    }
}
