//
//  SelectStakeHolderCell.swift
//  iWork
//
//  Created by Shailendra on 12/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit


class SelectStakeHolderCell: UITableViewCell {

    @IBOutlet var tagsView : ASJTagsView!
    @IBOutlet weak var tagViewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var innerView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //tagsView.delegate = self as? UIScrollViewDelegate
        //tagsView.isDeleteButtonRemove = true
        innerView.layer.cornerRadius = 2.0
        innerView.layer.borderWidth = 1.0
        innerView.layer.borderColor = UIColor.darkGray.cgColor
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
