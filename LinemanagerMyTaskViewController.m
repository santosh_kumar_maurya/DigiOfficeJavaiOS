//
//  LinemanagerMyTaskViewController.m
//  iWork
//
//  Created by Shailendra on 31/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LinemanagerMyTaskViewController.h"
#import "iWork-Swift.h"
#import "Header.h"



@interface LinemanagerMyTaskViewController ()<UITableViewDelegate,UITextViewDelegate,RecognitionTableViewCellProtocol,RainbowColorSource>{
    IBOutlet UITableView * MyTaskTableView;
    
    NSMutableArray *TaskArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    WebApiService *Api;
    NSDictionary *MyTaskDic;
    NSDictionary *ResponseDic;
    NSDictionary *recognitionDic;
    NSDictionary *recognitionDataDic;
    NSDictionary *popUpResponseDict;
    NSString *strRewardPoints;
    NSArray *SkillArray;
    NSArray *ValueArray;
    RainbowNavigation *rainbowNavigation;
    UINavigationBar* navbar;
    UILabel *notificationLabel;
}

@end

@implementation LinemanagerMyTaskViewController

- (void)viewDidLoad {
    [self clearfilterUserdefaultData];
    CheckValue = @"MYTASK";
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor clearColor];
    MyTaskTableView.backgroundColor = [UIColor clearColor];
    MyTaskTableView.estimatedRowHeight = 50;
    MyTaskTableView.rowHeight = UITableViewAutomaticDimension;
    MyTaskTableView.bounces = NO;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
   // [MyTaskTableView addSubview:refreshControl];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"MYTASK" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ClearFilterData:) name:@"ClearFilteriPM" object:nil];
    [self registerCell];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)configureNavigationBar{
    navbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 44)];
    [navbar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    navbar.shadowImage = [UIImage new];
    navbar.translucent = YES;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 20.0f, self.view.frame.size.width, navbar.frame.size.height)];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(-5.0f, 17.0f, 15.0f, 15.0f)];
    [backButton addTarget:self action:@selector(BackAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"BackBtn"] forState:UIControlStateNormal];
    backButton.imageView.contentMode = UIViewContentModeCenter;
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 100.0f, 30.0f)];
    [title setFont:[UIFont boldSystemFontOfSize:17]];
    
    title.text = NSLocalizedString(@"IPM", nil);
    title.textColor = [UIColor whiteColor];
    
    UITapGestureRecognizer* titleGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(BackAction:)];
    [title  setUserInteractionEnabled:YES];
    [title addGestureRecognizer:titleGesture];
    
    [customView addSubview:backButton];
    [customView addSubview:title];
    
    UIButton *notificationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [notificationButton setFrame:CGRectMake(self.view.frame.size.width - 52, 10.0f, 20.0f, 20.0f)];
    [notificationButton addTarget:self action:@selector(notificationTapped:) forControlEvents:UIControlEventTouchUpInside];
    [notificationButton setImage:[UIImage imageNamed:@"new_ic_notification"] forState:UIControlStateNormal];
    notificationButton.imageView.contentMode = UIViewContentModeCenter;
    
    notificationLabel = [[UILabel alloc] initWithFrame:CGRectMake(notificationButton.frame.origin.x + 8, 5.0f, 20.0f, 20.0f)];
    notificationLabel.text = @"0";
    notificationLabel.layer.cornerRadius = 10;
    notificationLabel.clipsToBounds = YES;
    notificationLabel.backgroundColor = delegate.yellowColor;
    notificationLabel.font = [UIFont systemFontOfSize:10];
    notificationLabel.textAlignment = NSTextAlignmentCenter;
    notificationLabel.textColor = [UIColor whiteColor];
    
    UIButton *HeaderProfileButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [HeaderProfileButton setFrame:CGRectMake(self.view.frame.size.width - 87, 10.0f, 20.0f, 20.0f)];
    [HeaderProfileButton addTarget:self action:@selector(openProfile) forControlEvents:UIControlEventTouchUpInside];
    [HeaderProfileButton setImage:[UIImage imageNamed:@"New_Profile"] forState:UIControlStateNormal];
    HeaderProfileButton.imageView.contentMode = UIViewContentModeCenter;
    
    UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [homeButton setFrame:CGRectMake(self.view.frame.size.width - 120, 10.0f, 20.0f, 20.0f)];
    [homeButton addTarget:self action:@selector(BackAction:) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setImage:[UIImage imageNamed:@"iPMhome"] forState:UIControlStateNormal];
    homeButton.imageView.contentMode = UIViewContentModeCenter;
    
    [customView addSubview:homeButton];
    [customView addSubview:HeaderProfileButton];
    [customView addSubview:notificationButton];
    [customView addSubview:notificationLabel];
    
    UINavigationItem* navItem = [[UINavigationItem alloc] init];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:customView];
    
    navItem.leftBarButtonItem = leftButton;
    [navbar setItems:@[navItem]];

    [self.view addSubview:navbar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateNotifications) name:@"Notification" object:nil];
    
    rainbowNavigation = [[RainbowNavigation alloc] init];
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIImageView *imageView =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.width * 0.65)];
    imageView.image=[UIImage imageNamed:@"iPMDashboardBanner"];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    MyTaskTableView.tableHeaderView = imageView;
    
}
- (IBAction)notificationTapped:(id)sender {
    [delegate DeleteNotificationCounter:@"IPM"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NotificationScreen *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"NotificationScreen"];
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(void)openProfile{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    ObjViewController.isComeFrom = @"IPM";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)UpdateNotifications{
    notificationLabel.alpha=0.0;
    notificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IPM"]];
    if ([notificationLabel.text intValue]>0) {
        notificationLabel.alpha=1.0;
        notificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IPM"]];
    }
}
-(void)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(void)registerCell{
    
    UINib *recognitionNib = [UINib nibWithNibName:@"RecognitionTableViewCell" bundle:nil];
    [MyTaskTableView registerNib:recognitionNib forCellReuseIdentifier:@"RecognitionTableViewCell"];
    
    UINib *Nib = [UINib nibWithNibName:@"RewardPointsCell" bundle:nil];
    [MyTaskTableView registerNib:Nib forCellReuseIdentifier:@"CELL5"];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TaskWebApi) withObject:nil afterDelay:0.1];
    [self UpdateNotifications];
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TaskWebApi) withObject:nil afterDelay:0.5];
}
-(void)ClearFilterData:(NSNotification*)notification{
    CheckValue = @"MYTASK";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TaskWebApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self TaskWebApi];
}
- (void)TaskWebApi {
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"MYTASK"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"])
            params = @ {
                @"e_date": @"",
                @"employee_Id": [ApplicationState userId],
                @"s_date": @""
            };
            ResponseDic =  [Api WebApi:params Url:@"myDashTaskCount"];
        }
        [refreshControl endRefreshing];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self TaskWebApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            [self WebApiCellForRecognition];
            [LoadingManager hideLoadingView:self.view];
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                MyTaskDic = [ResponseDic valueForKey:@"object"];
                NSLog(@"MyTaskDic==%@",MyTaskDic);
            }
            [MyTaskTableView reloadData];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(SkillArray.count == 0 && ValueArray.count == 0){
        return 6;
    }
    return  7;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        BannerCell *Cell = (BannerCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL4" forIndexPath:indexPath];
        return Cell;
    }
    else if(indexPath.row == 1)
    {
        TaskStatusCell *Cell = (TaskStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"approved_rejected"]))){
            Cell.CompleteLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"approved_rejected"]];
        }
        else{
            Cell.CompleteLabel.text  = @"";
        }
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"ongoing_on_schedule"]))){
            Cell.OnScheduleLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"ongoing_on_schedule"]];
        }
        else{
            Cell.OnScheduleLabel.text  = @"";
        }
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"ongoing_behind_schedule"]))){
            Cell.BehindLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"ongoing_behind_schedule"]];
        }
        else{
            Cell.BehindLabel.text  = @"";
        }
        UITapGestureRecognizer* Gestures = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoProfilePage)];
        [Cell.ProfileImageView  setUserInteractionEnabled:YES];
        [Cell.ProfileImageView addGestureRecognizer:Gestures];
        
        return Cell;
    }
    else if(indexPath.row == 2)
    {
        SubmitCell *Cell = (SubmitCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        return Cell;
        
    }
    else if(indexPath.row == 3){
       
        MyTaskCell *Cell = (MyTaskCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"me"]))){
            Cell.TaskCreationLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic  valueForKey:@"me"]];
        }
        else{
            Cell.TaskCreationLabel.text  = @"";
        }
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"line_manager"]))){
            Cell.TaskCompletionLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"line_manager"]];
        }
        else{
            Cell.TaskCompletionLabel.text  = @"";
        }
        return Cell;
        
    }
    else if(indexPath.row == 4){
        MyKpiCell *Cell = (MyKpiCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"kpi"]))){
            Cell.KPILabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"kpi"]];
        }
        else{
            Cell.KPILabel.text  = @"";
        }
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"feedback_as_stackholder"]))){
            Cell.FeedbackAsStackLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"feedback_as_stackholder"]];
        }
        else{
            Cell.FeedbackAsStackLabel.text  = @"";
        }
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"myfeedback"]))){
            Cell.FeedbackLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"myfeedback"]];
        }
        else{
            Cell.FeedbackLabel.text  = @"";
        }
        return Cell;
    }
    else if(indexPath.row == 5){
        
        if(SkillArray.count == 0 && ValueArray.count == 0){
            RewardPointsCell *Cell = (RewardPointsCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL5" forIndexPath:indexPath];
            Cell.PointLabel.text = strRewardPoints;
            UITapGestureRecognizer* Gestures = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoRewardPoints)];
            [Cell  setUserInteractionEnabled:YES];
            [Cell  addGestureRecognizer:Gestures];
            return Cell;
        }
        else{
            static NSString *CellIdentifier = @"RecognitionTableViewCell";
            RecognitionTableViewCell *cell = (RecognitionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[RecognitionTableViewCell alloc] initWithFrame:CGRectZero];
            }
            cell.delegate = self;
            cell.cellType = @"Line";
            if(recognitionDataDic.count>0){
                [cell configureCellWithDataWithDict:recognitionDataDic];
            }
            return cell;
        }
    }
    else{
        RewardPointsCell *Cell = (RewardPointsCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL5" forIndexPath:indexPath];
        Cell.PointLabel.text = strRewardPoints;
        UITapGestureRecognizer* Gestures = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoRewardPoints)];
        [Cell  setUserInteractionEnabled:YES];
        [Cell  addGestureRecognizer:Gestures];
        return Cell;
    }
}
-(void)GotoProfilePage{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    ObjViewController.isComeFrom = @"PMS";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GotoRewardPoints{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    PointRewardsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"PointRewardsViewController"];
    ObjViewController.totalRewardsPoint = strRewardPoints;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)SubmitTaskAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    CreateTaskScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateTaskScreen"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)SubmitProgressAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyTaskScreenParentViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTaskScreenParentViewController"];
    viewController.isComeFrom = @"PROGRESS";
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)GiveFeedBackAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    FeedbackListViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackListViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)GotoTaskAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NewTaskScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewTaskScreen"];
    viewController.taskType = 1;
    [[self navigationController] pushViewController:viewController animated:YES];
}
-(IBAction)GotoAssignmentAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NewTaskScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewTaskScreen"];
    viewController.taskType = 2;
    [[self navigationController] pushViewController:viewController animated:YES];
}
-(IBAction)ViewAllTasksAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyTaskScreenParentViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTaskScreenParentViewController"];
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)CompleteAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskStatusScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskStatusScreen"];
    viewController.taskType = 3;
    viewController.isManager = YES;
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)OnSchuduleAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskStatusScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskStatusScreen"];
    viewController.taskType = 1;
    viewController.isManager = YES;
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)BhindAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskStatusScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskStatusScreen"];
    viewController.taskType = 2;
    viewController.isManager = YES;
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)FilterAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    GiveFeedbacFilterViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"GiveFeedbacFilterViewController"];
    ObjController.isComeFrom =@"MYTASK";
    [[self navigationController] presentViewController:ObjController animated:YES completion:nil];
}
-(IBAction)KPIAction:(id)sender{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyKPIParentViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyKPIParentViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
//    MyKPIViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyKPIViewController"];
//    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)FeedbackAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    FeedbackScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackScreen"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)FeedbackAsStackHoderAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    FeedbackStackHolderViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackStackHolderViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

//MARK: - RecognitionTableViewCellProtocol delegate
-(void)cellTappedWithSelectedIndex:(NSInteger)selectedIndex selectedCell:(NSInteger)selectedCell{
    [self callWebServiceForIndex:selectedIndex selectedCell:selectedCell];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RecognitionPopUpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RecognitionPopUpViewController"];
    vc.responseDict = popUpResponseDict;
    if (selectedIndex == 0){
        vc.title = @"Skills";
    }
    else{
        vc.title = @"Value";
    }
    vc.providesPresentationContextTransitionStyle = YES;
    vc.definesPresentationContext = YES;
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:vc animated:false completion:nil];
}
-(void)callWebServiceForIndex:(NSInteger)index selectedCell:(NSUInteger)CellIndex{
    if (index == 0){
        SkillArray = [recognitionDataDic valueForKey:@"skillsData"];
        NSNumber *IdNumber = [[SkillArray objectAtIndex:CellIndex] valueForKey:@"id"];
        [self callWebServiceForSkills:[NSString stringWithFormat:@"%@", IdNumber]];
    }
    else{
        ValueArray = [recognitionDataDic valueForKey:@"valuesData"];
        NSNumber *IdNumber = [[ValueArray objectAtIndex:CellIndex] valueForKey:@"id"];
        [self callWebServiceForValues:[NSString stringWithFormat:@"%@", IdNumber]];
    }
}
-(void)callWebServiceForSkills:(NSString*)id{
    if(delegate.isInternetConnected){
        
         NSDictionary * dict = [Api WebApi:nil Url:[NSString stringWithFormat:@"getSkillUsersData?id=%@&showTeamData=false&employeeId=null",id]];
        
        //NSDictionary * dict = [Api WebApi:nil Url:[NSString stringWithFormat:@"getSkillUsersData?id=%@",id]];
        if(dict == nil){
            [LoadingManager hideLoadingView:self.view];
        }
        else if(([[dict valueForKey:@"statusCode"] intValue]==5) || ([[dict valueForKey:@"statusCode"] intValue]==600)){
            NSDictionary *responseObject = [dict valueForKey:@"object"];
            popUpResponseDict = responseObject;
            [LoadingManager hideLoadingView:self.view];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}

-(void)callWebServiceForValues:(NSString*)id
{
    if(delegate.isInternetConnected){
        
        NSDictionary * dict = [Api WebApi:nil Url:[NSString stringWithFormat:@"getValueUsersData?id=%@&showTeamData=false&employeeId=null",id]];
        
       // NSDictionary * dict = [Api WebApi:nil Url:[NSString stringWithFormat:@"getValueUsersData?id=%@",id]];
        if(dict == nil){
            [LoadingManager hideLoadingView:self.view];
        }
        else if(([[dict valueForKey:@"statusCode"] intValue]==5) || ([[dict valueForKey:@"statusCode"] intValue]==601)){
            NSDictionary *responseObject = [dict valueForKey:@"object"];
            popUpResponseDict = responseObject;
            [LoadingManager hideLoadingView:self.view];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (void)WebApiCellForRecognition
{
    if(delegate.isInternetConnected){
        recognitionDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getRecongnitions?userId=%@",[ApplicationState userId]]];
        if(recognitionDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self WebApiCellForRecognition];
        }
        else if(([[recognitionDic valueForKey:@"statusCode"] intValue]==5) || ([[recognitionDic valueForKey:@"statusCode"] intValue]==602)){
            NSDictionary *responseObject = [recognitionDic valueForKey:@"object"];
            if(IsSafeStringPlus(TrToString(responseObject[@"recognitionData"]))){
                NSDictionary *recognitionData = [responseObject valueForKey:@"recognitionData"];
                recognitionDataDic = recognitionData;
                SkillArray = [recognitionDataDic valueForKey:@"skillsData"];
                ValueArray = [recognitionDataDic valueForKey:@"valuesData"];
            }
            if(IsSafeStringPlus(TrToString(responseObject[@"rewardpoints"]))){
                strRewardPoints = [[responseObject valueForKey:@"rewardpoints"] stringValue];
            }
            [LoadingManager hideLoadingView:self.view];
            [MyTaskTableView reloadData];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)clearfilterUserdefaultData
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"MYTASK"];
    [userDefault synchronize];
}
@end
