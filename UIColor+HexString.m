//
//  UIColor+HexString.m
//  ChartJS
//
//  Created by Michelangelo Chasseur on 23/04/14.
//  Copyright (c) 2014 Touchware. All rights reserved.
//

#import "UIColor+HexString.h"

@implementation UIColor (HexString)

- (NSString *)hexString {
    const CGFloat *components = CGColorGetComponents(self.CGColor);
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    NSString *hexString=[NSString stringWithFormat:@"#%02X%02X%02X", (int)(r * 255), (int)(g * 255), (int)(b * 255)];
    return hexString;
}
+ (UIColor*)TopBarYellowColor{
    return [UIColor colorWithRed:255.0f/255.0f green:112.0f/255.0f blue:76.0f/255.0f alpha:1.0f];
}
+ (UIColor*)TextBlueColor{
    return [UIColor colorWithRed:59.0f/255.0f green:112.0f/255.0f blue:140.0f/255.0f alpha:1.0f];
}
+ (UIColor*)ButtonSkyColor{
   return [UIColor colorWithRed:92.0f/255.0f green:197.0f/255.0f blue:216.0f/255.0f alpha:1.0f];
}
+ (UIColor*)RedColor{
   return [UIColor colorWithRed:242.0f/255.0f green:28.0f/255.0f blue:44.0f/255.0f alpha:1.0f];
}
+ (UIColor*)ButtonGreenColor{
    return [UIColor colorWithRed:150.0f/255.0f green:203.0f/255.0f blue:65.0f/255.0f alpha:1.0f];

}
@end
