//
//  CreateKPIViewController.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 \\b"

class CreateKPIViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate{
    

    @IBOutlet weak var CreateKPITableView: UITableView!
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var pickerViewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var PopupOuter: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var PopupTitleLabel: UILabel!
    @IBOutlet weak var PopupDescriptionLabel: UILabel!
    @IBOutlet weak var HeaderLabel: UILabel!
    var responseDict = NSDictionary()
    var ObjAppDelegate = AppDelegate()
    var Api: WebApiService =  WebApiService()
    
    var txtCategory: UITextField!
    var txtStrategicObjective: UITextField!
    var txtKPI :  UITextField!
    var txtGate1: UITextField!
    var txtGate2: UITextField!
    var txtGate3: UITextField!
    var txtGate4: UITextField!
    var txtGate5: UITextField!
    var txtKetInitiative: UITextField!
    var txtWeightage: UITextField!
    var txtLineManager: UITextField!
    var ResponseDic : NSMutableDictionary! = [:]
    var CategoryNameArray = NSArray()
    var CategoryIDArray = NSArray()
    var pickerSelectedIndex : Int = 0
    var CategoryName : String! = ""
    
    var CheckValue : String! = ""
    var isComeFrom : String! = ""
    var LineManageIDSt : String! = ""
    
    var KPICascading = UserDefaults.standard.string(forKey: "kKPICascading")
    var RemainingValue : Int  = 0
    
    var WeightageStr : String! = ""
    var remainingWeightage : NSNumber!
    
    override func viewDidLoad()
    {
    
        
        if(KPICascading! == "0"){
            btnSubmit.backgroundColor = UIColor.lightGray
            btnSubmit.setTitleColor(UIColor.darkText, for: UIControlState.normal)
        }
        super.viewDidLoad()
        ObjAppDelegate = UIApplication.shared.delegate as! AppDelegate
        CreateKPITableView.estimatedRowHeight = 80;
        CreateKPITableView.backgroundColor = UIColor.clear;
        CreateKPITableView.rowHeight = UITableViewAutomaticDimension;
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        IQKeyboardManager.shared().isEnabled = true
        LineManageIDSt =  UserDefaults.standard.string(forKey:"kLineManagerID")
       
        print("remainingWeightage====\(remainingWeightage!)")
//        let Dic = ResponseDic.value(forKey: "object") as! NSDictionary
//        remainingWeightage = Dic.value(forKey: "remainingWeightage") as! NSNumber
        RemainingValue = Int(NSNumber(integerLiteral: Int(remainingWeightage)));
//        print("RemainingValue====\(RemainingValue)")
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        LoadingManager.showLoading(self.view)
        self.perform(#selector(CreateKPIViewController.GetListOfCategory), with: nil, afterDelay: 0.1)
        self.registerTavleViewCell()
        if(isComeFrom == "EDIT"){
            if ResponseDic != nil {
                self.CreateKPITableView.reloadData()
            }
            HeaderLabel.text = "Update KPI"
            btnSubmit.setTitle("UPDATE", for: UIControlState.normal)
        }
        else{
            HeaderLabel.text = "Create KPI"
            btnSubmit.setTitle("SUBMIT", for: UIControlState.normal)
        }
    }
    func registerTavleViewCell(){
        self.CreateKPITableView.register(UINib(nibName: "CategoryCell", bundle: nil), forCellReuseIdentifier: "CategoryCell")
        self.CreateKPITableView.register(UINib(nibName: "KPIHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "KPIHeaderTableViewCell")
        self.CreateKPITableView.register(UINib(nibName: "SubmitBtnCell", bundle: nil), forCellReuseIdentifier: "SubmitBtnCell")
        self.CreateKPITableView.register(UINib(nibName: "GateCell", bundle: nil), forCellReuseIdentifier: "GateCell")
        self.CreateKPITableView.register(UINib(nibName: "TargetCell", bundle: nil), forCellReuseIdentifier: "TargetCell")
        self.CreateKPITableView.register(UINib(nibName: "LineManagerTableViewCell", bundle: nil), forCellReuseIdentifier: "LineManagerTableViewCell")
        self.CreateKPITableView.register(UINib(nibName: "KPITableViewCell", bundle: nil), forCellReuseIdentifier: "KPITableViewCell")
        self.CreateKPITableView.register(UINib(nibName: "KeyInitiativeTableViewCell", bundle: nil), forCellReuseIdentifier: "KeyInitiativeTableViewCell")
        self.CreateKPITableView.register(UINib(nibName: "WeightageTableViewCell", bundle: nil), forCellReuseIdentifier: "WeightageTableViewCell")
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 12
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
         if indexPath.row == 0{
            let Cell : CategoryCell = tableView.dequeueReusableCell(
                withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
            self.txtCategory = Cell.viewWithTag(102) as! UITextField!
            self.txtCategory.delegate = self
            Cell.dropDownButton.addTarget(self, action: #selector(dropDownButtonTapped(sender:)), for:.touchUpInside)
            if isComeFrom == "EDIT"{
                self.txtCategory.textColor = UIColor.black
                self.txtCategory.text = ResponseDic["category"] as? String
            }
            else{
                if("\(CategoryName!)" != ""){
                     self.txtCategory.text = "\(CategoryName!)"
                }
            }
            return Cell
        }
         else if indexPath.row == 1{
            let Cell : TargetCell = tableView.dequeueReusableCell(
                withIdentifier: "TargetCell", for: indexPath) as! TargetCell
            self.txtStrategicObjective = Cell.viewWithTag(103) as! UITextField!
            self.txtStrategicObjective.delegate = self
            if isComeFrom == "EDIT" || ResponseDic != nil{
                self.txtStrategicObjective.text = ResponseDic["strategicObjective"] as? String
            }
            return Cell
         }
         else if indexPath.row == 2{
            let Cell : KPITableViewCell = tableView.dequeueReusableCell(
                withIdentifier: "KPITableViewCell", for: indexPath) as! KPITableViewCell
            self.txtKPI = Cell.viewWithTag(105) as! UITextField!
            self.txtKPI.delegate = self
            if isComeFrom == "EDIT" || ResponseDic != nil{
                self.txtKPI.text = ResponseDic["kpi"] as? String
            }
            return Cell
        }
        else if indexPath.row == 3{
            let Cell : KPIHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "KPIHeaderTableViewCell", for: indexPath) as! KPIHeaderTableViewCell
            return Cell
        }
        else if indexPath.row == 4{
            let Cell : GateCell = tableView.dequeueReusableCell(
                withIdentifier: "GateCell", for: indexPath) as! GateCell
            Cell.configureCellWithData(index: 1)
            self.txtGate1 = Cell.viewWithTag(101) as! UITextField!
            self.txtGate1.delegate = self
            if isComeFrom == "EDIT" || ResponseDic != nil{
                self.txtGate1.text = ResponseDic["gate1"] as? String
            }
            return Cell
         }
         else if indexPath.row == 5{
            let Cell : GateCell = tableView.dequeueReusableCell(
                withIdentifier: "GateCell", for: indexPath) as! GateCell
            Cell.configureCellWithData(index: 2)
            self.txtGate2 = Cell.viewWithTag(101) as! UITextField!
            self.txtGate2.delegate = self
            if isComeFrom == "EDIT" || ResponseDic != nil{
                self.txtGate2.text = ResponseDic["gate2"] as? String
            }
            return Cell
         }
         else if indexPath.row == 6{
            let Cell : GateCell = tableView.dequeueReusableCell(
                withIdentifier: "GateCell", for: indexPath) as! GateCell
            Cell.configureCellWithData(index: 3)
            self.txtGate3 = Cell.viewWithTag(101) as! UITextField!
            self.txtGate3.delegate = self
            if isComeFrom == "EDIT" || ResponseDic != nil{
                self.txtGate3.text = ResponseDic["gate3"] as? String
            }
            return Cell
         }
         else if indexPath.row == 7{
            let Cell : GateCell = tableView.dequeueReusableCell(
                withIdentifier: "GateCell", for: indexPath) as! GateCell
            Cell.configureCellWithData(index: 4)
            self.txtGate4 = Cell.viewWithTag(101) as! UITextField!
            self.txtGate4.delegate = self
            if isComeFrom == "EDIT" || ResponseDic != nil{
                self.txtGate4.text = ResponseDic["gate4"] as? String
            }
             return Cell
         }
         else if indexPath.row == 8{
            let Cell : GateCell = tableView.dequeueReusableCell(
                withIdentifier: "GateCell", for: indexPath) as! GateCell
            Cell.configureCellWithData(index: 5)
            self.txtGate5 = Cell.viewWithTag(101) as! UITextField!
            self.txtGate5.delegate = self
            if isComeFrom == "EDIT" || ResponseDic != nil{
                self.txtGate5.text = ResponseDic["gate5"] as? String
            }
            return Cell
         }
         else if indexPath.row == 9{
            let Cell : KeyInitiativeTableViewCell = tableView.dequeueReusableCell(
                withIdentifier: "KeyInitiativeTableViewCell", for: indexPath) as! KeyInitiativeTableViewCell
            self.txtKetInitiative = Cell.viewWithTag(106) as! UITextField!
            self.txtKetInitiative.delegate = self
            if isComeFrom == "EDIT" || ResponseDic != nil{
                self.txtKetInitiative.text = ResponseDic["keyInitiative"] as? String
            }
            return Cell
        }
        else if indexPath.row == 10{
            let Cell : WeightageTableViewCell = tableView.dequeueReusableCell(
                withIdentifier: "WeightageTableViewCell", for: indexPath) as! WeightageTableViewCell
            self.txtWeightage = Cell.viewWithTag(107) as! UITextField!
            self.txtWeightage.keyboardType = UIKeyboardType.decimalPad
            self.txtWeightage.delegate = self
            if isComeFrom == "EDIT" || ResponseDic != nil{
                self.txtWeightage.text = ResponseDic["weightage"] as? String
                WeightageStr = ResponseDic["weightage"] as? String
            }
            return Cell
        }
         else{
            let Cell : LineManagerTableViewCell = tableView.dequeueReusableCell(
                withIdentifier: "LineManagerTableViewCell", for: indexPath) as! LineManagerTableViewCell
            self.txtLineManager = Cell.viewWithTag(104) as! UITextField!
            let LineManage =  UserDefaults.standard.string(forKey:"kLineManager")
            self.txtLineManager.text = LineManage!
            return Cell
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(self.CategoryIDArray.count > 0){
            return self.CategoryIDArray.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        if(self.CategoryIDArray.count > 0){
            return "\(self.CategoryNameArray[row])"
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtCategory.textColor = UIColor.black
        self.txtCategory.text = "\(self.CategoryNameArray[row])"
        self.CategoryName = "\(self.CategoryNameArray[row])"
        self.CreateKPITableView.reloadData()
    }
    func dropDownButtonTapped(sender: UIButton){
        self.view.endEditing(true)
        self.pickerView.isHidden = false
    }
    @IBAction func submitButtonTapped(_ sender: UIButton){
        
        if(KPICascading! == "1"){
            if ((self.txtCategory.text == "") || (self.txtStrategicObjective.text == "") || (self.txtKPI.text == "") || (self.txtGate1.text == "") || (self.txtGate2.text == "") || (self.txtGate3.text == "") || (self.txtGate4.text == "") || (self.txtGate5.text == "") || (self.txtLineManager.text == "") || (self.txtWeightage.text == "")){
                self.ShowAlert(MsgTitle: "Please fill required fields and then press SUBMIT", BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
            }
            else{
                if(isComeFrom == "EDIT"){
                    CheckValue = "EDITKPI"
                }
                else {
                    CheckValue = "CREATEKPI"
                }
                 LoadingManager.showLoading(self.view)
                self.perform(#selector(CreateKPIViewController.KPIWebApi), with: nil, afterDelay: 0.5)
            }
        }
    }
    @IBAction func pickerDoneButtonTapped(_ sender: Any){
        if("\(CategoryName!)" == ""){
            self.txtCategory.text = CategoryNameArray[0] as? String
            self.txtCategory.textColor = UIColor.black
        }
        self.pickerView.isHidden = true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtCategory{
            textField.resignFirstResponder()
            self.pickerView.isHidden = false
        }
        else{
            self.pickerView.isHidden = true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if (textField == self.txtCategory){
            return true
        }
        else if ((textField == self.txtStrategicObjective) || (textField == txtKPI)) {
//            
//           
//            
//            
//            
//            let acceptedInput = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS)
//            print("acceptedInput===\(acceptedInput)")
//            if !(string.components(separatedBy: acceptedInput).count > 1) {
//                return false
//            }
//            
//                let  char = string.cString(using: String.Encoding.utf8)!
//                let isBackSpace = strcmp(char, "\\b")
//                if (isBackSpace == -92) {
//                    return true
//                }
                let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
                return newLength <= 1000

            
        }
        else if ((textField == self.txtGate1)||(textField == self.txtGate2)||(textField == self.txtGate3)||(textField == self.txtGate4)||(textField == self.txtGate5)){
//            let acceptedInput = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS)
//            if !(string.components(separatedBy: acceptedInput).count > 1) {
//                return false
//            }
//            else {
                let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
                return newLength <= 200
           // }
        }
        else if (textField == self.txtKetInitiative){
//            let acceptedInput = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS)
//            if !(string.components(separatedBy: acceptedInput).count > 1) {
//                return false
//            }
//            else {
                let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
                return newLength <= 500
           // }
        }
        else if textField == self.txtWeightage{
//            if string.rangeOfCharacter(from: NSCharacterSet.letters) != nil{
//                return false
//            } else {
                let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
                if let text = textField.text as NSString? {
                    let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
                    if(txtAfterUpdate != ""){
                        
                        if(Float(txtAfterUpdate)!<=100){
                            return newLength <= 5
                        }
                        else{
                            self.txtWeightage.text  = "100"
                            self.ShowAlert(MsgTitle: "Weightage can not be greater then 100", BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                        }
                    }
               // }
                return newLength<=3
            }
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == self.txtCategory{
            self.ResponseDic.setValue(textField.text, forKey: "category")
        }
        else if textField == self.txtStrategicObjective{
            self.ResponseDic.setValue(textField.text, forKey: "strategicObjective")
        }
        else if textField == self.txtKPI{
            self.ResponseDic.setValue(textField.text, forKey: "kpi")
        }
        else if textField == self.txtGate1{
            self.ResponseDic.setValue(textField.text, forKey: "gate1")
        }
        else if textField == self.txtGate2{
            self.ResponseDic.setValue(textField.text, forKey: "gate2")
        }
        else if textField == self.txtGate3{
            self.ResponseDic.setValue(textField.text, forKey: "gate3")
        }
        else if textField == self.txtGate4{
            self.ResponseDic.setValue(textField.text, forKey: "gate4")
        }
        else if textField == self.txtGate5{
            self.ResponseDic.setValue(textField.text, forKey: "gate5")
        }
        else if textField == self.txtKetInitiative{
            self.ResponseDic.setValue(textField.text, forKey: "keyInitiative")
        }
        else if textField == self.txtWeightage{
            self.ResponseDic.setValue(textField.text, forKey: "weightage")
        }
        else{
            self.ResponseDic.setValue(textField.text, forKey: "lineManagerId")
        }
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
//        if textField == self.txtCategory{
//            self.ResponseDic.setValue(textField.text, forKey: "category")
//        }
//        else if textField == self.txtStrategicObjective{
//            self.ResponseDic.setValue(textField.text, forKey: "strategicObjective")
//        }
//        else if textField == self.txtKPI{
//            self.ResponseDic.setValue(textField.text, forKey: "kpi")
//        }
//        else if textField == self.txtGate1{
//            self.ResponseDic.setValue(textField.text, forKey: "gate1")
//        }
//        else if textField == self.txtGate2{
//            self.ResponseDic.setValue(textField.text, forKey: "gate2")
//        }
//        else if textField == self.txtGate3{
//            self.ResponseDic.setValue(textField.text, forKey: "gate3")
//        }
//        else if textField == self.txtGate4{
//            self.ResponseDic.setValue(textField.text, forKey: "gate4")
//        }
//        else if textField == self.txtGate5{
//            self.ResponseDic.setValue(textField.text, forKey: "gate5")
//        }
//        else if textField == self.txtKetInitiative{
//            self.ResponseDic.setValue(textField.text, forKey: "keyInitiative")
//        }
//        else if textField == self.txtWeightage{
//            self.ResponseDic.setValue(textField.text, forKey: "weightage")
//        }
//        else{
//            self.ResponseDic.setValue(textField.text, forKey: "lineManagerId")
//        }
//    }
    // end
    //MARK: - APi's
    func GetListOfCategory(){
        if (ObjAppDelegate.isInternetConnected()){
             responseDict = Api.webApi(nil, url: "getCategory") as NSDictionary!
            if(ResponseDic==nil){
                LoadingManager.hideLoading(view)
                 GetListOfCategory()
            }
            else if( responseDict.value(forKey: "statusCode") as! Int == 5){
                LoadingManager.hideLoading(view)
                let dict = responseDict.value(forKey: "object") as! NSDictionary
                let categoryDict = dict.value(forKey: "kpiCategory") as! NSArray
                CategoryNameArray = categoryDict.value(forKey: "category") as! NSArray
                CategoryIDArray = categoryDict.value(forKey: "categoryId") as! NSArray
                self.picker.reloadAllComponents()
            }
            else{
                self.GetListOfCategory()
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func KPIWebApi(){
        if (ObjAppDelegate.isInternetConnected()){
            let userId = UserDefaults.standard.string(forKey: "kUserId")
            if(CheckValue == "CREATEKPI"){
                if(Int(self.txtWeightage.text!)! > RemainingValue){
                    
                    self.ShowAlert(MsgTitle:  NSLocalizedString("REMAIN_WEIGHTAGE_LIMIT", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
                else{
                    let param : NSDictionary! = ["category": self.txtCategory.text!, "gate1": self.txtGate1.text!, "gate2": self.txtGate2.text!, "gate3": self.txtGate3.text!, "gate4": self.txtGate4.text!, "gate5": self.txtGate5.text!, "keyInitiative": self.txtKetInitiative.text!, "kpi": self.txtKPI.text!, "lineManagerId": LineManageIDSt!,"remainingWeightage":remainingWeightage!,"strategicObjective":self.txtStrategicObjective.text!,"userId":userId!,"weightage":self.txtWeightage.text!]
                    
                    responseDict = Api.webApi(param as! [AnyHashable : Any]!, url: "createKpi") as NSDictionary!
                    if( responseDict.value(forKey: "statusCode") as! Int == 5){
                        LoadingManager.hideLoading(view)
                        PopupOuter.isHidden = false
                        PopupTitleLabel.text = "SENT"
                        PopupDescriptionLabel.text = "\(responseDict.value(forKey: "message")!)"
                    }
                    else{
                        LoadingManager.showLoading(self.view)
                        self.KPIWebApi()
                    }
                }
            }
            else if(CheckValue == "EDITKPI"){
                if(Int(self.txtWeightage.text!)! > RemainingValue +  Int(WeightageStr!)!){
                    self.ShowAlert(MsgTitle:  NSLocalizedString("REMAIN_WEIGHTAGE_LIMIT", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
                else{
                    let targerId  = ResponseDic["target_id"] as? Int
                    let param : NSDictionary! = ["category": self.txtCategory.text!, "gate1": self.txtGate1.text!, "gate2": self.txtGate2.text!, "gate3": self.txtGate3.text!, "gate4": self.txtGate4.text!, "gate5": self.txtGate5.text!,"id":ResponseDic["id"] as! Int, "keyInitiative": self.txtKetInitiative.text!, "kpi": self.txtKPI.text!, "lineManagerId": LineManageIDSt!,"strategicObjective":self.txtStrategicObjective.text!,"target_id": targerId!.description,"userId":userId!,"weightage":self.txtWeightage.text!]
                    
                    responseDict = Api.webApi(param as! [AnyHashable : Any]!, url: "editKpi") as NSDictionary!
                    
                    if( responseDict.value(forKey: "statusCode") as! Int == 5){
                        LoadingManager.hideLoading(view)
                        PopupOuter.isHidden = false
                        PopupTitleLabel.text = "UPDATED KPI"
                        PopupDescriptionLabel.text = "\(String(describing: responseDict.value(forKey: "message")!))"
                    }
                    else{
                        LoadingManager.showLoading(self.view)
                        self.KPIWebApi()
                    }
                }
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    @IBAction func BackAction(_ sender: UIButton){
        self.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func HomeAction(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContainerParentViewController") as! ContainerParentViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func CrossOkayBtnAction() {
        PopupOuter.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }

    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: false, completion: nil)
    }
}
