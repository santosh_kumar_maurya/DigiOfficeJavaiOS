//
//  ContainerParentViewController.m
//  iWork
//
//  Created by Shailendra on 21/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "ContainerParentViewController.h"
#import "Header.h"
#import "iWork-Swift.h"

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define kDefaultEdgeInsets UIEdgeInsetsMake(5, 10, 5, 10)

@interface ContainerParentViewController ()<UIScrollViewDelegate>{
    NSArray *btnArray;
    AppDelegate *delegate;
    NSArray *SelectImageArray;
    NSArray *UnSelectImageArray;
    NSArray *controllerArray;
}
@property (weak) IBOutlet UIScrollView *containerScrollView;
@property (weak) IBOutlet UIScrollView *menuScrollView;
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray;
-(void) buttonPressed:(id) sender;
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets;
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr;@end

@implementation ContainerParentViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
   
    self.navigationController.navigationBarHidden = YES;
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    _containerScrollView.pagingEnabled = YES;
     btnArray = [[NSArray alloc]initWithObjects:@"Home",@"My Profile",@"Help", nil];
     SelectImageArray = [[NSArray alloc]initWithObjects:@"HomeSelect",@"ProfileSelect",@"HelpSelect", nil];
     UnSelectImageArray = [[NSArray alloc]initWithObjects:@"HomeUnSelect",@"ProfileUnSelect",@"HelpUnSelect", nil];
    [self addButtonsInScrollMenu:btnArray];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *oneVC = [storyBoard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];

    ProfileViewController *twoVC = [storyBoard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    HelpViewController *threeVC = [storyBoard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    controllerArray = @[oneVC, twoVC,threeVC];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self addChildViewControllersOntoContainer:controllerArray];
   // [[UIApplication sharedApplication] setStatusBarHidden:YES];
}
- (BOOL)prefersStatusBarHidden {
//    if(self.childViewControllers.count > 0) {
//        return [self.childViewControllers.lastObject prefersStatusBarHidden];
//    }
    return NO;// or any other default value
}
#pragma mark - Add Menu Buttons in Menu Scroll View
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray
{
    CGFloat buttonHeight = self.menuScrollView.frame.size.height;
    CGFloat cWidth = 0.0f;
    
    for (int i = 0 ; i<buttonArray.count; i++)
    {
        NSString *tagTitle = [buttonArray objectAtIndex:i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        if(i == 0){
             button.frame = CGRectMake(cWidth + 10, -5, SCREEN_WIDTH/3, buttonHeight);
             [button setTitleEdgeInsets:UIEdgeInsetsMake(5.f, -50.f, -32.f, 0.f)];
        }
        else if(i == 1){
             button.frame = CGRectMake(SCREEN_WIDTH/3+27, -5, SCREEN_WIDTH/3, buttonHeight);
             [button setTitleEdgeInsets:UIEdgeInsetsMake(5.f, -76.f, -32.f, 0.f)];
        }
        else if(i == 2){
            button.frame = CGRectMake(SCREEN_WIDTH/3*2+20, -5, SCREEN_WIDTH/3, buttonHeight);
            [button setTitleEdgeInsets:UIEdgeInsetsMake(5.f, -45.f, -32.f, 0.f)];
        }
        [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[UnSelectImageArray objectAtIndex:i]]] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[SelectImageArray objectAtIndex:i]]] forState:UIControlStateSelected];
        
        [button setTitle:tagTitle forState:UIControlStateNormal];
        button.titleLabel.font = delegate.contentFont;
        [button setTitleColor:delegate.redColor forState:UIControlStateSelected];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        [self.menuScrollView addSubview:button];
        if(i==0){
            button.selected = YES;
        }
    }
}
#pragma mark - Menu Button press action
-(void) buttonPressed:(id) sender
{
    UIButton *senderbtn = (UIButton *) sender;
    float buttonWidth = 0.0f;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        UIButton *btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        
        if (btn.tag == senderbtn.tag)
        {
            if(btn.tag == 1){
                [self PostNotification];
            }
            btn.selected = YES;
            [bottomView setHidden:NO];
        }
        else
        {
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    
    [self.containerScrollView setContentOffset:CGPointMake(SCREEN_WIDTH * senderbtn.tag, 0) animated:YES];
    
    float xx = SCREEN_WIDTH * (senderbtn.tag) * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}
-(void)PostNotification{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CONTAINER" object:nil];
}
#pragma mark - Calculate width of menu button
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets
{
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:10.0f]};
    
    CGSize size = [title sizeWithAttributes:attributes];
    return CGSizeMake(size.width + buttonEdgeInsets.left + buttonEdgeInsets.right, size.height + buttonEdgeInsets.top + buttonEdgeInsets.bottom).width;
}

#pragma mark - Adding all related controllers in to the container
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr
{
    for (int i = 0 ; i < controllersArr.count; i++)
    {
        UIViewController *vc = (UIViewController *)[controllersArr objectAtIndex:i];
        CGRect frame = CGRectMake(0, 0, self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height);
        frame.origin.x = SCREEN_WIDTH * i;
        vc.view.frame = frame;
        
        [self addChildViewController:vc];
        [self.containerScrollView addSubview:vc.view];
        [vc setNeedsStatusBarAppearanceUpdate];
       // [vc didMoveToParentViewController:self];
    }
    
    self.containerScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * controllersArr.count + 1, self.containerScrollView.frame.size.height);
    self.containerScrollView.pagingEnabled = YES;
    self.containerScrollView.delegate = self;
}


#pragma mark - Scroll view delegate methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = (scrollView.contentOffset.x / SCREEN_WIDTH);
    
    UIButton *btn;
    float buttonWidth = 0.0;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        
        if (btn.tag == page)
        {
            btn.selected = YES;
            buttonWidth = btn.frame.size.width;
            [bottomView setHidden:NO];
            if(btn.tag == 1){
                [self PostNotification];
            }
        }
        else
        {
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    float xx = scrollView.contentOffset.x * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)GotoProfilePage:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}


@end
