




#import "MyKPIDetailsScreen.h"
#import "Header.h"
#import "iWork-Swift.h"

@interface MyKPIDetailsScreen ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>{
   
    UIRefreshControl *refreshControl;
    WebApiService *Api;
    AppDelegate *delegate;
    IBOutlet UITableView *tab;
    IBOutlet UILabel *headerlab;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UIView *NoDataView;
    NSMutableArray *dataArray;
    int TagValue;
    NSString *kpiId;
    UIView *CoomentView;
    UITextView *CommentTextView;
    NSDictionary *ResponseDic;
    NSDictionary *params;
    NSString *handsetId;
    int offset;
    int limit;
    
    IBOutlet UIView *ConfirmationView;
    IBOutlet UILabel *ConfirmationTitleLable;
    IBOutlet UILabel *ConfirmationDescriptionLabel;
    IBOutlet UIButton *StartCancelButton;
    IBOutlet UIButton *CancelButton;
    
    IBOutlet UIView *ConfirmedPopupView;
    IBOutlet UILabel *ConfirmedTitleLabel;
    IBOutlet UILabel *ConfirmedDiscripTionLabel;
    IBOutlet UIImageView *ConfirmedImageView;
}

@end
@implementation MyKPIDetailsScreen
@synthesize kpiId,KPIName;

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self EmptyArray];
    Api = [[WebApiService alloc] init];
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    headerlab.text = KPIName;
    TagValue = 0;
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [tab registerNib:Nib forCellReuseIdentifier:@"CELL"];
    Api = [[WebApiService alloc] init];
    tab.estimatedRowHeight = 50;
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tab.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
    [LoadingManager showLoadingView:self.view];
    ConfirmationTitleLable.textColor = [UIColor TextBlueColor];
    [self performSelector:@selector(KPIDatailsApi) withObject:nil afterDelay:0.5];
}
-(void)KPIDatailsApi{
    if(delegate.isInternetConnected){
        params = @ {
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit],
            @"employeeId" : [ApplicationState userId],
            @"id": kpiId
        };
        ResponseDic = [Api WebApi:params Url:@"kpiListDetail"];
        NSLog(@"KPIDeatilsResponseDic=%@",ResponseDic);
        [refreshControl endRefreshing];
        if(ResponseDic==nil){
            [LoadingManager hideLoadingView:self.view];
            [self KPIDatailsApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            NoDataView.hidden = YES;
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    NoDataLabel.hidden = YES;
                     [tab reloadData];
                }
                else if(ResponseArrays.count == 0 && dataArray.count == 0){
                    [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
        
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(dataArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Data Found";
    }
    [tab reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self KPIDatailsApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [dataArray addObject:BindDataDic];
    }
}
- (void)reloadData{
    [self EmptyArray];
    [self KPIDatailsApi];
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    dataArray = [[NSMutableArray alloc] init];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.ViewDetailsBtn.tag = indexPath.row;
    [Cell.ViewDetailsBtn addTarget:self action:@selector(StartCancelDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
   
    if(dataArray.count>0){
        NSDictionary * responseData = [dataArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
        
        
        if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"status"] isEqualToString:@"Submitted"]){
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationLabel.hidden = NO;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.ViewDetailsBtn.hidden = NO;
            Cell.StatusLabel.text = @"Submitted";
            Cell.StausImageView.image = [UIImage imageNamed:@"Completes"];
            [Cell.ViewDetailsBtn setTitle:NSLocalizedString(@"VIEW_DETAILS", nil) forState:UIControlStateNormal];
            [Cell.ViewDetailsBtn setTitleColor:[UIColor ButtonSkyColor] forState:UIControlStateNormal];
            
        }
        else if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"status"] isEqualToString:@"New"]||[[[dataArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"new"]){
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationLabel.hidden = NO;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.ViewDetailsBtn.hidden = NO;
            Cell.StatusLabel.text = @"New";
            Cell.StausImageView.image = [UIImage imageNamed:@"New"];
            [Cell.ViewDetailsBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
            [Cell.ViewDetailsBtn setTitleColor:delegate.redColor forState:UIControlStateNormal];
            
        }
        else if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"status"]  isEqualToString:@"In-Progress"]){
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationLabel.hidden = NO;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.ViewDetailsBtn.hidden = NO;
            Cell.StatusLabel.text = @"In-Progress";
            Cell.StausImageView.image = [UIImage imageNamed:@"WatingForApprove"];
            [Cell.ViewDetailsBtn setTitle:NSLocalizedString(@"VIEW_DETAILS", nil) forState:UIControlStateNormal];
            [Cell.ViewDetailsBtn setTitleColor:[UIColor ButtonSkyColor] forState:UIControlStateNormal];
           
        }
        else if ([[[dataArray objectAtIndex:indexPath.row] valueForKey:@"status"]  isEqualToString:@"Assigned"]){
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationLabel.hidden = NO;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.ViewDetailsBtn.hidden = NO;
            Cell.StatusLabel.text = @"Assigned";
            Cell.StausImageView.image = [UIImage imageNamed:@"Completes"];
            [Cell.ViewDetailsBtn setTitle:NSLocalizedString(@"START", nil) forState:UIControlStateNormal];
            [Cell.ViewDetailsBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
           
        }
        else if ([[[dataArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Declined"] ||([[[dataArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Discarded"])||[[[dataArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Rejected"]){
            
            Cell.hight.constant = 0.0;
            Cell.LineLabel.hidden = YES;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
            Cell.ViewDetailsBtn.hidden = YES;
            Cell.StausImageView.image = [UIImage imageNamed:@"StatusRejected"];
            if([[[dataArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Declined"]){
                Cell.StatusLabel.text = @"Declined";
            }
            else if([[[dataArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Discarded"]){
                Cell.StatusLabel.text = @"Discarded";
            }
            else if([[[dataArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Rejected"]){
                Cell.StatusLabel.text = @"Rejected";
            }
        }
        else if ([[[dataArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Approved"]){
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = NO;
            Cell.RateStaticLabel.hidden = NO;
            Cell.DurationStaticLabel.hidden = YES;
            Cell.DurationLabel.hidden = YES;
            Cell.ViewDetailsBtn.hidden = NO;
            Cell.StatusLabel.text= @"Completed";
            Cell.StausImageView.image = [UIImage imageNamed:@"Completes"];
            [Cell.ViewDetailsBtn setTitle:NSLocalizedString(@"VIEW_DETAILS", nil) forState:UIControlStateNormal];
            [Cell.ViewDetailsBtn setTitleColor:[UIColor ButtonSkyColor] forState:UIControlStateNormal];
        }
        else if ([[[dataArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Cancelled"]){
            Cell.hight.constant = 32.0;
            Cell.LineLabel.hidden = NO;
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
            Cell.ViewDetailsBtn.hidden = NO;
            Cell.StausImageView.image = [UIImage imageNamed:@"StatusRejected"];
            Cell.StatusLabel.text= @"Cancelled";
            [Cell.ViewDetailsBtn setTitleColor:[UIColor ButtonSkyColor] forState:UIControlStateNormal];
            [Cell.ViewDetailsBtn setTitle:NSLocalizedString(@"VIEW_DETAILS", nil) forState:UIControlStateNormal];
        }
    }
    return Cell;
}

-(void)CancelTaskSubmitAction{
    if(delegate.isInternetConnected){
        NSString* TaskID = [[dataArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        if([[[dataArray objectAtIndex:TagValue] objectForKey:@"status"] isEqualToString:@"Assigned"]){
            ResponseDic =  [Api WebApi:nil Url:[NSString stringWithFormat:@"startButton?employeeId=%@&taskId=%@",[ApplicationState userId],TaskID]];
        }
        else{
            params = @ {
                @"comment" : @"",
                @"status" : @"4",
                @"actionBy" : @"3",   // Cancel By Employee
                @"taskId" : TaskID,
                @"user_id" : [ApplicationState userId]
            };
            ResponseDic =  [Api WebApi:params Url:@"cancelTask"];
        }
        if(ResponseDic==nil){
            [LoadingManager hideLoadingView:self.view];
            [self CancelTaskSubmitAction];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            
            if([[[dataArray objectAtIndex:TagValue] objectForKey:@"status"] isEqualToString:@"Assigned"]){
                ConfirmedImageView.image = [UIImage imageNamed:@"ApprovedKpi"];
                ConfirmedTitleLabel.text = @"TASK STARTED";
                ConfirmedDiscripTionLabel.text = [ResponseDic objectForKey:@"message"];
            }
            else{
                ConfirmedImageView.image = [UIImage imageNamed:@"RejectedKpi"];
                ConfirmedTitleLabel.text = @"TASK CANCELLED";
                ConfirmedDiscripTionLabel.text = [ResponseDic objectForKey:@"message"];
            }
            ConfirmedPopupView.hidden = NO;
            [self EmptyArray];
            [self KPIDatailsApi];
            
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)StartCancelDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]){
        ConfirmationView.hidden = NO;
        ConfirmationTitleLable.text = @"Cancel Task";
        ConfirmationDescriptionLabel.text = @"Are you sure to cancel this task?";
        [StartCancelButton setTitleColor:delegate.redColor forState:UIControlStateNormal];
    }
    else if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"START", nil)]){
       
        ConfirmationView.hidden = NO;
        ConfirmationTitleLable.text = @"Start Task";
        ConfirmationDescriptionLabel.text = @"Are you sure to start this task?";
        [StartCancelButton setTitle:@"OK" forState:UIControlStateNormal];
        [StartCancelButton setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    }
    else{
        NSString* str = [[dataArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        if ([[[dataArray objectAtIndex:TagValue] objectForKey:@"status"] isEqualToString:@"In-Progress"]){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
            TaskDetailManagerScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailManagerScreen"];
            viewController.taskID = str;
            viewController.isManager = FALSE;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else{
            HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
            viewController.taskID = str;
            viewController.isManager = TRUE;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
    }
}
-(IBAction)ConfrimationCrossAndCancelAction{
    ConfirmationView.hidden = YES;
}
-(IBAction)OkayAndCrossConfirmedAction{
    ConfirmedPopupView.hidden = YES;
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)StartCancelTaskAction:(id)sender{
    ConfirmationView.hidden = YES;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(CancelTaskSubmitAction) withObject:nil afterDelay:0.1];
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(IBAction)BackAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
//    [[self navigationController] popViewControllerAnimated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
