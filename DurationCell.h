//
//  DurationCell.h
//  iWork
//
//  Created by Shailendra on 02/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DurationCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UITextField *StartDateTextField;
@property(nonatomic,strong)IBOutlet UITextField *EndDateTextField;
@property(nonatomic,strong)IBOutlet UIButton *StartButton;
@property(nonatomic,strong)IBOutlet UIButton *EndButton;
@property(nonatomic,strong)IBOutlet UIView *DateView;

@end
