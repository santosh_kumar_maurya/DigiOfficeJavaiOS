//
//  CommentCell.swift
//  iWork
//
//  Created by Shailendra on 16/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet var UserNameLabel : UILabel!
    @IBOutlet var SubtaskNameLabel : UILabel!
    @IBOutlet var DateLabel : UILabel!
    @IBOutlet var CommnetLabel : UILabel!
    @IBOutlet var ProfileImageView : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        ProfileImageView.layer.borderColor = UIColor.white.cgColor
        ProfileImageView.layer.borderWidth = 1
        ProfileImageView.layer.cornerRadius = 22.5
        ProfileImageView.clipsToBounds = true
        
        SubtaskNameLabel.textColor = UIColor.TextBlueColor()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
