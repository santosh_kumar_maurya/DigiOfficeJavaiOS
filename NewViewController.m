//
//  NewViewController.m
//  iWork
//
//  Created by Shailendra on 08/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "NewViewController.h"
#import "Header.h"
#import "iWork-Swift.h"

@interface NewViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,RejectionDelegate>
{
    IBOutlet UITableView *NewTableView;
    NSMutableArray *NewArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    
    IBOutlet UILabel *NoDataLabel;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    UITextView *CommentTextView;
    IBOutlet UIView *NoDataView;

    IBOutlet UIButton *StartCancelBtn;
    IBOutlet UIButton *CacnelBtn;
    CustomPopView *ObjCustomPopView;
    
    IBOutlet UIView *ConfirmationView;
    IBOutlet UILabel *ConfirmationTitle;
    IBOutlet UILabel *ConfirmationDescriptionLabel;
    
    
    IBOutlet UIView *ConfirmationConfirmedView;
    IBOutlet UILabel *ConfirmationConfirmedTitle;
    IBOutlet UILabel *ConfirmationConfirmedDescriptionLabel;
    IBOutlet UIImageView *ConfirmationConfirmedImageView;

    
    __weak IBOutlet NSLayoutConstraint *comfirmationBottonConstraints;

    IBOutlet UIView *ViewDetailsView;
    IBOutlet UILabel *ApprovedLabel;
    IBOutlet UILabel *TaskNameLabel;
    IBOutlet UILabel *TaskDetailsLabel;
    IBOutlet UILabel *KPINameLabel;
    IBOutlet UILabel *CreationDateLabel;
    IBOutlet UILabel *EndDateLabel;
    IBOutlet UILabel *DurationLabel;
    IBOutlet UILabel *StakeHolderLabel;
    IBOutlet UILabel *TaskDetailsStaticLabel;
    
    NSString *StartCancelStr;
}

@end

@implementation NewViewController

- (void)viewDidLoad {
    [self clearfilterUserdefaultData];
    ObjCustomPopView = [[CustomPopView alloc] init];
    ConfirmationTitle.textColor = [UIColor TextBlueColor];
    
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NewTableView.estimatedRowHeight = 50;
    NewTableView.rowHeight = UITableViewAutomaticDimension;
    NewTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, NewTableView.bounds.size.width, 10.f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [NewTableView addSubview:refreshControl];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"NEWTASK" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ClearFilterData:) name:@"ClearFilteriPM" object:nil];
    [self ClearData];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(NewTaskWebApi) withObject:nil afterDelay:0.5];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self ClearData];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(NewTaskWebApi) withObject:nil afterDelay:0.5];
}
-(void)ClearFilterData:(NSNotification*)notification{
    CheckValue = @"";
    [self ClearData];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(NewTaskWebApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData{
    if(![CheckValue isEqualToString:@"FILTER"]){
        CheckValue = @"Refresh";
        [self NewTaskWebApi];
    }
    [self ClearData];
}
-(void)ClearData{
    offset = 0;
    limit = 50;
    NewArray = [[NSMutableArray alloc] init];
}
- (void)NewTaskWebApi
{
    if(delegate.isInternetConnected)
    {
        if(![CheckValue isEqualToString:@"FILTER"])
        {
            params = @ {
                @"eEndDate" : @"",
                @"eStartDate" :@"",
                @"taskId" :@"",
                @"task_request_status": @"",
                @"task_type": @"",
                @"employeeId" : [ApplicationState userId],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        ResponseDic =  [Api WebApi:params Url:@"viewAllNewDashTask"];
        [refreshControl endRefreshing];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self NewTaskWebApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            NSLog(@"NewTaskResponseDic==%@",ResponseDic);
            NoDataView.hidden = YES;
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    [NewTableView reloadData];
                }
                else if(ResponseArrays.count == 0 && NewArray.count == 0){
                    [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(NewArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Record Found";
    }
    [NewTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self NewTaskWebApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array
{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [NewArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [NewArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AskForProgressCell *Cell = (AskForProgressCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    Cell.StartCancelBtn.tag = indexPath.row;
    Cell.DetailsBtn.tag = indexPath.row;
    [Cell.StartCancelBtn addTarget:self action:@selector(StartCancelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if(NewArray.count>0){
        NSDictionary * responseData = [NewArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
        if ([[[NewArray objectAtIndex:indexPath.row] objectForKey:@"new_status"] isEqualToString:@"New"]||[[[NewArray objectAtIndex:indexPath.row] objectForKey:@"new_status"] isEqualToString:@"new"]){
            Cell.StatusImageView.image = [UIImage imageNamed:@"WatingForApprove"];
            Cell.StatusLabel.text = @"Waiting Approval";
            Cell.StatusLabelHeightConstraints.constant = 24;
            [Cell.StartCancelBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
            [Cell.StartCancelBtn setTitleColor:delegate.redColor forState:UIControlStateNormal];
        }
        else if ([[[NewArray objectAtIndex:indexPath.row] objectForKey:@"new_status"] isEqualToString:@"Assigned"]){
            Cell.StatusImageView.image = [UIImage imageNamed:@"Completes"];
            Cell.StatusLabel.text = @"Approved";
            Cell.StatusLabelHeightConstraints.constant = 22;
            [Cell.StartCancelBtn setTitle:NSLocalizedString(@"START", nil) forState:UIControlStateNormal];
            [Cell.StartCancelBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
        }
    }
    return Cell;
}
-(void)CancelTaskSubmitAction{
    if(delegate.isInternetConnected){
        NSString* TaskID = [[NewArray objectAtIndex:TagValue] objectForKey:@"taskId"];   // Start Stask
        if([[[NewArray objectAtIndex:TagValue] objectForKey:@"new_status"] isEqualToString:@"Assigned"]){
            ResponseDic =  [Api WebApi:nil Url:[NSString stringWithFormat:@"startButton?employeeId=%@&taskId=%@",[ApplicationState userId],TaskID]];
        }
        else{
            params = @ {
                @"comment" : @"",
                @"status" : @"4",
                @"actionBy" : @"3",   // Cancel By Employee
                @"taskId" : TaskID,
                @"user_id" : [ApplicationState userId]
            };
            ResponseDic =  [Api WebApi:params Url:@"cancelTask"];
        }
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self CancelTaskSubmitAction];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            [self ClearData];
            //ConfirmationConfirmedView.hidden = NO;
            if([StartCancelStr isEqualToString:@"STARTTASK"]){
                
                [self ShowAlertApprove:@"TASK STARTED" MsgDiscription:[ResponseDic objectForKey:@"message"] ImageName:@"ApprovedKpi" OpenViewName:@"CONFORMVIEW"];
                
//                ConfirmationConfirmedTitle.text = @"TASK STARTED";
//                ConfirmationConfirmedDescriptionLabel.text = [ResponseDic objectForKey:@"message"];
//                ConfirmationConfirmedImageView.image = [UIImage imageNamed:@"ApprovedKpi"];
            }
            else{
                [self ShowAlertApprove:@"TASK CANCELLED" MsgDiscription:[ResponseDic objectForKey:@"message"] ImageName:@"RejectedKpi" OpenViewName:@"CONFORMVIEW"];
                
//                ConfirmationConfirmedTitle.text = @"TASK CANCELLED";
//                ConfirmationConfirmedDescriptionLabel.text = [ResponseDic objectForKey:@"message"];
//                ConfirmationConfirmedImageView.image = [UIImage imageNamed:@"RejectedKpi"];
            }
            [self NewTaskWebApi];
           
        }
        else{
             [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(IBAction)ConfirmedViewOKCrossAction:(id)sender{
    ConfirmationConfirmedView.hidden = YES;
    //[[self navigationController] popViewControllerAnimated:YES];
}
-(void)StartCancelAction:(UIButton*)sender{
    TagValue = sender.tag;
    //ConfirmationView.hidden = NO;
    
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"START", nil)]){
        [self ShowAlertApprove:@"Start Task" MsgDiscription:@"Are you sure to start this task?" ImageName:@"" OpenViewName:@"STARTTASK"];
        
//        ConfirmationTitle.text = @"Start Task";
//        ConfirmationDescriptionLabel.text = @"Are you sure to start this task?";
//        [StartCancelBtn.titleLabel setText:@"OK"];
//        [StartCancelBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
//        [CacnelBtn.titleLabel setText:@"CANCEL"];
    }
    else if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]){
//        ConfirmationTitle.text = @"Cancel Task";
//        ConfirmationDescriptionLabel.text = @"Are you sure to cancel this task?";
//        [StartCancelBtn.titleLabel setText:@"OK"];
//        [StartCancelBtn setTitleColor:[UIColor RedColor] forState:UIControlStateNormal];
        
        [self ShowAlertApprove:@"Cancel Task" MsgDiscription:@"Are you sure to cancel this task?" ImageName:@"" OpenViewName:@"CANCELTASK"];
    }
}
-(void)StartCancelTaskWithTaskText:(NSString *)TaskText{
    NSLog(@"TaskText===%@",TaskText);
    if([TaskText isEqualToString:@"STARTTASK"]){
        StartCancelStr = @"STARTTASK";
    }
    else if([TaskText isEqualToString:@"CANCELTASK"]){
        StartCancelStr = @"CANCELTASK";
    }
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(CancelTaskSubmitAction) withObject:nil afterDelay:0.5];
    
}
-(IBAction)PopupCancelAction:(id)sender
{
    ConfirmationView.hidden = YES;
    ViewDetailsView.hidden = YES;
    NSDictionary *UserInfo = @ {
        @"isSelected" : @"NO"
    };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ViewDetail" object:nil userInfo:UserInfo];
}

-(IBAction)StartAction:(UIButton*)sender{
    TagValue = sender.tag;
    ConfirmationView.hidden = YES;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(CancelTaskSubmitAction) withObject:nil afterDelay:0.5];
}

-(IBAction)ViewDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    NSDictionary * info= [NewArray objectAtIndex:TagValue];
    NSLog(@"info====%@",info);
    TaskDetailsStaticLabel.textColor = [UIColor TextBlueColor];
    if([[info valueForKey:@"new_status"] isEqualToString:@"New"]){
       ApprovedLabel.text = @"WAITING APPROVAL";
        ApprovedLabel.backgroundColor = [UIColor NeedRevisionBtnColor];
    }
    else{
        ApprovedLabel.text = @"APPROVAL";
        ApprovedLabel.backgroundColor = [UIColor ApproveGreenColor];
    }
    ApprovedLabel.layer.cornerRadius = 15;
    ApprovedLabel.clipsToBounds = YES;
    
    if(IsSafeStringPlus(TrToString(info[@"taskNAme"]))){
        TaskNameLabel.text  = info[@"taskNAme"];
    }
    else{
        TaskNameLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskDetail"]))){
        TaskDetailsLabel.text  = info[@"taskDetail"];
    }
    else{
        TaskDetailsLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"kpi"]))){
        KPINameLabel.text  = info[@"kpi"];
    }
    else{
        KPINameLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"creationDate"]))){
        NSNumber *datenumber = info[@"creationDate"];
        CreationDateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        CreationDateLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"endDate"]))){
        NSNumber *datenumber = info[@"endDate"];
        EndDateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        EndDateLabel.text  = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"duration"]))){
        DurationLabel.text  = info[@"duration"];
    }
    else{
        DurationLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"stakeholder"]))){
        NSArray *StakeHolderName = [[info valueForKey:@"stakeholder"] valueForKey:@"stakeholderName"];
        StakeHolderLabel.text = [StakeHolderName componentsJoinedByString:@", "];
    }
    else{
        StakeHolderLabel.text  = @" ";
    }
    ViewDetailsView.hidden = NO;
    NSDictionary *UserInfo = @ {
        @"isSelected" : @"YES"
    };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ViewDetail" object:nil userInfo:UserInfo];
}

-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
-(void)ShowAlertApprove:(NSString*)MsgTitle MsgDiscription:(NSString*)MsgDiscription ImageName:(NSString*)ImageName OpenViewName:(NSString*)OpenViewName{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    ApprovePopUpViewController *ObjApproveController = [storyboard instantiateViewControllerWithIdentifier:@"ApprovePopUpViewController"];
    ObjApproveController.TitleStr = MsgTitle;
    ObjApproveController.DiscriptionStr = MsgDiscription;
    ObjApproveController.ImageNameStr = ImageName;
    ObjApproveController.NameOpenView = OpenViewName;
    ObjApproveController.delegate = self;
    ObjApproveController.view.backgroundColor = [UIColor clearColor];
    ObjApproveController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjApproveController animated:NO completion:nil];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
-(void)clearfilterUserdefaultData
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"NEW"];
    [userDefault synchronize];
}
@end
