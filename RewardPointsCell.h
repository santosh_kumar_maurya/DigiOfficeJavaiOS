//
//  RewardPointsCell.h
//  iWork
//
//  Created by Shailendra on 19/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RewardPointsCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UIView *TopView;
@property(nonatomic,strong)IBOutlet UIView *PointView;
@property(nonatomic,strong)IBOutlet UILabel *PointLabel;
@property(nonatomic,strong)IBOutlet UILabel *PointTitleLabel;
@property(nonatomic,strong)IBOutlet UILabel *HeaderTitleLabel;
@end
