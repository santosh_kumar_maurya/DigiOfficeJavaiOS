//
//  WebApiService.m
//  iWork
//
//  Created by Shailendra on 06/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "WebApiService.h"
#import "Header.h"
@implementation WebApiService


-(NSDictionary*)WebApi: (NSMutableDictionary*)_params Url:(NSString*)Url {
    
    NSString *refreshedToken =  [ApplicationState currentPushToken];
    if (refreshedToken==nil) {
        refreshedToken =  @"";
    }
    //NSLog(@"refreshedToken==%@",refreshedToken);
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSLog(@"_params===>%@",_params);
    NSLog(@"Url == %@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL_PMS,Url]]);
    NSLog(@"Token====%@",[ApplicationState GetToken]);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL_PMS,Url]]];
    
    [request setHTTPMethod:@"POST"];
    if(_params != nil){
        NSError *requestError = NULL;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_params
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&requestError];
        [request setHTTPBody:jsonData];
        [request setValue:[ApplicationState GetToken] forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
    }
    if([Url containsString:@"ssoAuth"]||[Url containsString:@"userProfileAuth"]){
        [request setValue:[ApplicationState GetToken] forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//        [request setValue:@"ios" forHTTPHeaderField: @"platform"];
//        [request setValue:[delegate GetVersionNumber] forHTTPHeaderField: @"appversion"];
//        [request setValue:refreshedToken forHTTPHeaderField: @"fcm_id"];

    }
    if ([Url containsString:@"getRecongnitions"]||[Url containsString:@"getSkillUsersData"]||[Url containsString:@"getValueUsersData"]){
        [request setValue:[ApplicationState GetToken] forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"ios" forHTTPHeaderField: @"platform"];
        [request setValue:@"2x" forHTTPHeaderField: @"resolution"];
    }
    else{
        [request setValue:[ApplicationState GetToken] forHTTPHeaderField:@"Authorization"];
    }
    
        NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSData *responseData1=[responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *jsonParsingError = nil;
    NSDictionary *reqdataDict = [NSJSONSerialization JSONObjectWithData:responseData1
                                                                options:0 error:&jsonParsingError];
    NSLog(@"reqdataDict===>%@",reqdataDict);
    return reqdataDict;
    ;
    
//    //[self showAlert:NSLocalizedString(@"CONNECTION_ERR_MSG",@"")];
//    NSLog(@"_params===>%@",_params);
//    
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:Url]];
//    
//    NSString *boundary = @"---------------------------14737809831466499882746641449";
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//    NSMutableData *body = [NSMutableData data];
//    
//    // add params (all params are strings)
//    for (NSString *param in _params) {
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
//    }
//    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
//    
//    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
//    
//    [request setHTTPMethod:@"POST"];
//    [request setHTTPBody:body];
//    NSURLResponse *response = NULL;
//    NSError *requestError = NULL;
//    
//    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
//    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//    
//    NSData *responseData1=[responseString dataUsingEncoding:NSUTF8StringEncoding];
//    NSError *jsonParsingError = nil;
//    NSDictionary *reqdataDict = [NSJSONSerialization JSONObjectWithData:responseData1
//                                                                options:0 error:&jsonParsingError];
//    NSLog(@"reqdataDict===>%@",reqdataDict);
//    return reqdataDict;
//    ;
}


@end
