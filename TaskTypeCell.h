//
//  TaskTypeCell.h
//  iWork
//
//  Created by Shailendra on 23/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskTypeCell : UITableViewCell

@property(nonatomic,retain)IBOutlet UIButton *TaskButton;
@property(nonatomic,retain)IBOutlet UIButton *AssignmentButton;
@property(nonatomic,retain)IBOutlet UIButton *BothButton;
@property(nonatomic,retain)IBOutlet UIImageView *TaskImageView;
@property(nonatomic,retain)IBOutlet UIImageView *AssignmentImageView;
@property(nonatomic,retain)IBOutlet UIImageView *BothImageView;

@end
