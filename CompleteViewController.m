//
//  CompleteViewController.m
//  iWork
//
//  Created by Shailendra on 07/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "CompleteViewController.h"
#import "Header.h"
#import "iWork-Swift.h"

@interface CompleteViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *CompleteTableView;
    NSMutableArray *CompleteArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UIView *NoDataView;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
}
@end

@implementation CompleteViewController
@synthesize CompleteDics;

- (void)viewDidLoad {
//    [self clearfilterUserdefaultData];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    CompleteTableView.estimatedRowHeight = 50;
    CompleteTableView.rowHeight = UITableViewAutomaticDimension;
    CompleteTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CompleteTableView.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [CompleteTableView addSubview:refreshControl];

    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [CompleteTableView registerNib:Nib forCellReuseIdentifier:@"CELL"];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"COMPLETE" object:nil];
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ClearFilterData:) name:@"ClearFilteriPM" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if(![CheckValue isEqualToString:@"FILTER"]){
        [self EmptyArray];
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(CompleteApi) withObject:nil afterDelay:0.5];
    }
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self CompleteApi];
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    CompleteArray = [[NSMutableArray alloc] init];
}
- (void)reloadData
{
    if(![CheckValue isEqualToString:@"FILTER"]){
        CheckValue = @"Refresh";
        [self CompleteApi];
    }
    [self EmptyArray];
}
-(void)ClearFilterData:(NSNotification*)notification{
    CheckValue = @"";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self CompleteApi];
}
- (void)CompleteApi {
    
    if(delegate.isInternetConnected){
        if(![CheckValue isEqualToString:@"FILTER"]){
            params = @ {
                @"fromDate": @"",
                @"handsetId":@"",
                @"status": @"4",
                @"taskId": @"",
                @"taskType": @"",
                @"toDate": @"",
                @"user_id"  : [CompleteDics valueForKey:@"empId"],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        ResponseDic =  [Api WebApi:params Url:@"getEmployeeTaskList"];
        [refreshControl endRefreshing];
        if(ResponseDic==nil){
            [LoadingManager hideLoadingView:self.view];
            [self CompleteApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5)
        {
            NSLog(@"CompleteResponseDic==%@",ResponseDic);
            [LoadingManager hideLoadingView:self.view];
            NoDataView.hidden = YES;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]];
                }
                else {
                    [self NoRecordFound];
                }
                [CompleteTableView reloadData];
            }
            else{
                [LoadingManager hideLoadingView:self.view];
                [self NoRecordFound];
            }
            [CompleteTableView reloadData];
        }
        else
        {
            [LoadingManager hideLoadingView:self.view];
            [self NoRecordFound];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoRecordFound{
    if(CompleteArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Record Found";
    }
    [CompleteTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
        NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            if([CheckValue isEqualToString:@"FILTER"]){
                NSMutableDictionary *NewDic = [params mutableCopy];
                for (int i = 0 ; i< [NewDic count];i++){
                    if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                        [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                    }
                    else{
                        [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                    }
                }
                params = [NewDic mutableCopy];
            }
            [self CompleteApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [CompleteArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return   [CompleteArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.tag = indexPath.row;
    Cell.ViewDetailsBtn.tag = indexPath.row;
    [Cell.ViewDetailsBtn addTarget:self action:@selector(GotoEmployeeDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    Cell.RatingView.hidden = NO;
    Cell.DurationStaticLabel.hidden = YES;
    Cell.DurationLabel.hidden = YES;
   
    if(CompleteArray.count>0){
        NSDictionary * responseData = [CompleteArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
         Cell.StatusLabel.text = @"Completed";
    }
    return Cell;
}
-(void)GotoEmployeeDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    NSString* str = [[CompleteArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    if ([[[CompleteArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
        TaskDetailManagerScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailManagerScreen"];
        viewController.taskID = str;
        viewController.isManager = YES;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
    else{
        HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
-(void)clearfilterUserdefaultData
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"Complete"];
    [userDefault synchronize];
}
@end
