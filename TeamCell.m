//
//  TeamCell.m
//  iWork
//
//  Created by Shailendra on 04/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "TeamCell.h"
#import "Header.h"
#import "iWork-Swift.h"

@implementation TeamCell 
@synthesize TeamMemberArray;

- (void)awakeFromNib {
    [super awakeFromNib];
   
    self.backgroundColor = [UIColor clearColor];
    _MyTeamView.layer.cornerRadius = 2.0;
    _MyTeamView.maskView.layer.cornerRadius = 7.0f;
    _MyTeamView.layer.shadowRadius = 3.0f;
    _MyTeamView.layer.shadowColor = [UIColor blackColor].CGColor;
    _MyTeamView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _MyTeamView.layer.shadowOpacity = 0.7f;
    _MyTeamView.layer.masksToBounds = NO;
    _MyTeamLabel.textColor = [UIColor TextBlueColor];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  TeamMemberArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyTeamMemberCell *Cell = (MyTeamMemberCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    NSDictionary *info = TeamMemberArray[indexPath.row];
    if(IsSafeStringPlus(TrToString(info[@"empImg"]))){
        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:info[@"empImg"]]]];
        if(images == nil){
            NSLog(@"image nil");
            Cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
        }
        else{
            NSString *URL = [NSString stringWithFormat:@"%@",info[@"empImg"]];
            [Cell.ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
        }
    }
    else{
        Cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
    }
    if(IsSafeStringPlus(TrToString(info[@"empName"]))){
        Cell.UserNameLabel.text = [NSString stringWithFormat:@"%@", info[@"empName"]];
    }
    else{
        Cell.UserNameLabel.text = @"";
    }
    Cell.tag = indexPath.row;
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GetEmployee:)];
    [Cell setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:gesture];
    return Cell;
}
-(void)GetEmployee:(UIGestureRecognizer *)sender{
    int TagValue = sender.view.tag;
    NSDictionary *Dic = TeamMemberArray[TagValue];
    [self.delegate SelectTeamMemberIndex:Dic];

}
- (void)configureCell:(NSArray *)info{
    [_MyTeamTableView reloadData];
    
}

@end
