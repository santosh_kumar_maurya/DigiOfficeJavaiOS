#import "TaskDetailScreen.h"
#import "Header.h"
#import "iWork-Swift.h"
const UIEdgeInsets textInsetsMine1 = {5, 10, 11, 17};
const UIEdgeInsets textInsetsSomeone1 = {5, 15, 11, 10};

@interface TaskDetailScreen ()<UIPickerViewDelegate,UIPickerViewDataSource,UITextViewDelegate>
{
    UIRefreshControl *refreshControl;
    WebApiService *Api;
    UIButton *screenView;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    NSString *SubTask;
    NSArray* subTaskData;
    NSMutableArray *stakeHolderData;
    float ProgressValue;
    NSString *SelectBtn;
    IBOutlet UIView *CoomentView;
    IBOutlet UIView *AddSubTaskView;
    IBOutlet UILabel *CommentTitleLabel;
    IBOutlet UILabel *SelectSubtaskStaticLabel;
    IBOutlet UILabel *SelectSubtasLabel;
    IBOutlet UIButton *SelectSubtastBtn;
    IBOutlet UITextView *CommentTextView;
    
    IBOutlet NSLayoutConstraint *hight;
    IBOutlet NSLayoutConstraint *hight1;
    
    IBOutlet UIView *PickerViewBg;
    IBOutlet UIPickerView *PickerView;
    UIButton *mainView;
}
@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation TaskDetailScreen
@synthesize isShowDetail,count,taskID,taskType,dataDict, isMe,isComeFrom;

- (void)viewDidLoad{
    [super viewDidLoad];
    ProgressValue = 0.0;
    Api = [[WebApiService alloc] init];
    SelectBtn = @"";
    SubTask = @"";
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    isServerResponded = FALSE;
    isShowDetail= TRUE;
    isMe = TRUE;
    [self ScreenDesign];
    CommentTextView.layer.cornerRadius = 6;
    CommentTextView.layer.borderWidth = 1;
    CommentTextView.layer.borderColor = delegate.borderColor.CGColor;
    AddSubTaskView.layer.cornerRadius = 6;
    AddSubTaskView.layer.borderWidth = 1;
    AddSubTaskView.layer.borderColor = delegate.borderColor.CGColor;
    CommentTextView.clipsToBounds = YES;
    AddSubTaskView.clipsToBounds = YES;
    [LoadingManager showLoadingView:self.view];
    if([isComeFrom isEqualToString:@"NOTIFICATION"]){
       isShowDetail = FALSE;
       [self performSelector:@selector(getConversation) withObject:nil afterDelay:0.5];
    }
    else{
      [self performSelector:@selector(TaskDetailsWebApi) withObject:nil afterDelay:0.5];
    }
    
    CommentTitleLabel.textColor = [UIColor TextBlueColor];
  
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];

}
- (void) ScreenDesign{
    mainScreenView=[[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    mainScreenView.backgroundColor=delegate.BackgroudColor;
    
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor=delegate.headerbgColler;
    [mainScreenView addSubview:headerview];
    
    UIButton *backButton=[[UIButton alloc] initWithFrame:CGRectMake(5, 35, 30, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackBtn"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    backButton.tag=20;
    CGPoint center = backButton.center;
    center.y = headerview.frame.size.height / 2 +3;
    [backButton setCenter:center];
    [headerview addSubview:backButton];
    
    UIButton* HomeButton=[UIButton buttonWithType:UIButtonTypeCustom];
    HomeButton.frame=CGRectMake(delegate.devicewidth-35, 10, 18, 18);
    HomeButton.tag=20;
    center = HomeButton.center;
    center.y = headerview.frame.size.height / 2 + 3;
    [HomeButton setCenter:center];
    [HomeButton addTarget:self action:@selector(GotobackContainer) forControlEvents:UIControlEventTouchUpInside];
    [HomeButton setImage:[UIImage imageNamed:@"iPMhome"]  forState:UIControlStateNormal];
    [headerview addSubview:HomeButton];
    
    UILabel *headerlab=[[UILabel alloc] initWithFrame:CGRectMake(45, backButton.frame.origin.y+12, 200, 20)];
    headerlab.textColor = [UIColor whiteColor];
    headerlab.font=delegate.headFont;
    headerlab.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),taskID];
    headerlab.textAlignment=ALIGN_LEFT;
    [headerview addSubview:headerlab];
    
    UIButton *flatButton=[[UIButton alloc] initWithFrame:CGRectMake(0, delegate.headerheight, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton.backgroundColor=[UIColor whiteColor];
    flatButton_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton_lab.backgroundColor = delegate.headerbgColler;
    flatButton_lab.text = NSLocalizedString(@"DETAILS_CAPS", nil);
    flatButton_lab.textAlignment=ALIGN_CENTER;
    flatButton_lab.font=delegate.normalBold;
    flatButton_lab.textColor=[UIColor whiteColor];
    [flatButton addSubview:flatButton_lab];
    [flatButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    flatButton.tag=300;
    [mainScreenView addSubview:flatButton];
    
    UIButton *flatButton1=[[UIButton alloc] initWithFrame:CGRectMake(mainScreenView.frame.size.width/2,delegate.headerheight,mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton1.backgroundColor=[UIColor whiteColor];
    flatButton1_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton1_lab.text=NSLocalizedString(@"COMMENTS_CAPS", nil);
    flatButton1_lab.backgroundColor = delegate.headerbgColler;
    flatButton1_lab.textAlignment=ALIGN_CENTER;
    flatButton1_lab.font=delegate.normalBold;
    flatButton1_lab.textColor=[UIColor whiteColor];
    [flatButton1 addSubview:flatButton1_lab];
    [flatButton1 addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    flatButton1.tag=301;
    [mainScreenView addSubview:flatButton1];
    
    
   
    if([isComeFrom isEqualToString:@"NOTIFICATION"]){
    sel_lab=[[UILabel alloc] initWithFrame:CGRectMake(mainScreenView.frame.size.width/2, delegate.headerheight+delegate.tabheight-2, mainScreenView.frame.size.width/2, 2)];
    }
    else{
      sel_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, delegate.headerheight+delegate.tabheight-2, mainScreenView.frame.size.width/2, 2)];
    }
     sel_lab.backgroundColor = [UIColor whiteColor];
    [mainScreenView addSubview:sel_lab];
    
    CGRect floatFrame = CGRectMake([UIScreen mainScreen].bounds.size.width - 44 - 20, [UIScreen mainScreen].bounds.size.height - 44 - 20, 44, 44);
    addButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"AddPlus"] andPressedImage:[UIImage imageNamed:@"cross"] withScrollview:nil];
    addButton.imageArray = @[@"fb-icon",@"google-icon"];
    addButton.labelArray = @[@"Assign Task",@"Create Task"];
    addButton.hideWhileScrolling = YES;
    addButton.isCommentScreen = TRUE;
    addButton.delegate = self;
    
    tab=[[UITableView alloc] initWithFrame:CGRectMake(0, delegate.headerheight + delegate.tabheight, mainScreenView.frame.size.width, mainScreenView.frame.size.height - (delegate.headerheight + delegate.tabheight))];
//    tab=[[UITableView alloc] initWithFrame:CGRectMake(0, delegate.headerheight+delegate.tabheight, mainScreenView.frame.size.width,mainScreenView.frame.size.height-100)];
    tab.delegate=self;
    tab.dataSource=self;
    tab.separatorStyle=UITableViewCellSeparatorStyleNone;
    tab.backgroundColor=[UIColor clearColor];
    [mainScreenView addSubview:tab];
    [self.view addSubview:mainScreenView];
    [self AddRefreshControl];
    UINib *Nib = [UINib nibWithNibName:@"CommentCell" bundle:nil];
    [tab registerNib:Nib forCellReuseIdentifier:@"CommentCell"];
    [self.view addSubview:CoomentView];
    [self.view addSubview:PickerViewBg];
}
-(void)AddRefreshControl{
    [refreshControl removeFromSuperview];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
}
-(void)TaskDetailsWebApi{
    if(delegate.isInternetConnected){
        if([SelectBtn isEqualToString:@"MARK"]){
            params = @ {
                @"comment": CommentTextView.text,
                @"taskId": taskID,
                @"employeeId"  : [ApplicationState userId],
            };
            NSLog(@"params==%@",params);
            ResponseDic = [Api WebApi:params Url:@"markCompleted"];
        }
        else if([SelectBtn isEqualToString:@"COMMENT"]||[SelectBtn isEqualToString:@"COMMENTBTN"]){
            params = @ {
                @"comment": CommentTextView.text,
                @"subTask":SubTask,
                @"taskId": taskID,
                @"user_id"  : [ApplicationState userId],
            };
            NSLog(@"params==%@",params);
            ResponseDic = [Api WebApi:params Url:@"addComment"];
        }
        else{
            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getTaskDetail?taskId=%@&userId=%@",taskID,[ApplicationState userId]]];
        }
        isServerResponded = YES;
        [refreshControl endRefreshing];
        if(ResponseDic==nil){
            [LoadingManager hideLoadingView:self.view];
            [self TaskDetailsWebApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                if([SelectBtn isEqualToString:@"MARK"]){
                    [[self navigationController] popViewControllerAnimated:YES];
                   
                }
                else if([SelectBtn isEqualToString:@"COMMENT"]){
                   // [[self navigationController] popViewControllerAnimated:YES];
                    [self BlankValue];
                    
                }
                else if([SelectBtn isEqualToString:@"COMMENTBTN"]){
                    [self getConversation];
                    [self BlankValue];
                }
                else{
                    dataArray = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                    subTaskData = [[ResponseDic valueForKey:@"object"] valueForKey:@"subtask"];
                    stakeHolderData = [[ResponseDic valueForKey:@"object"] valueForKey:@"stakeholderData"];
                    ProgressValue = [[[ResponseDic valueForKey:@"object"] valueForKey:@"percentage"] floatValue];
                }
            }
            else{
                [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
            [tab reloadData];
            [tab setContentOffset:CGPointMake(0.0, 0.0)];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)BlankValue{
    CommentTextView.hidden = YES;
    CommentTextView.text = @"";
    [CommentTextView resignFirstResponder];
}
- (void)reloadData{
    [self TaskDetailsWebApi];
    [self getConversation];
}
-(void) getConversation{
    if([delegate isInternetConnected]){
        ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getComment?taskId=%@&userId=%@",taskID,[ApplicationState userId]]];
        [refreshControl endRefreshing];
        if(ResponseDic==nil){
            [LoadingManager hideLoadingView:self.view];
            [self getConversation];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            isServerResponded = YES;
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                dataConv = [[ResponseDic valueForKey:@"object"] valueForKey:@"commentData"];
                 [tab reloadData];
            }
            else{
                [self noMessageScreen:NSLocalizedString(@"NO_RECORD", nil)];
            }
            [tab reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self noMessageScreen:NSLocalizedString(@"NO_RECORD", nil)];
        }
        [self.view addSubview:addButton];
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
}
-(void)screenTaskDetail:(UITableViewCell *)cell{
    int rightPad = delegate.devicewidth-delegate.margin*2;
    mainView=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight+50)];
    //mainView.backgroundColor = [UIColor yellowColor];
    [cell addSubview:mainView];
    int ypos = 15;
    
    UILabel *createdOnLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/3, 15)];
    createdOnLabel.text = NSLocalizedString(@"CREATED_ON", nil);
    createdOnLabel.font = delegate.contentSmallFont;
    createdOnLabel.textColor = [UIColor blackColor];
    createdOnLabel.textAlignment = ALIGN_LEFT;
    [mainView addSubview:createdOnLabel];
    
    UILabel *createdOn =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos+17, delegate.devicewidth/3, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"creationDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"creationDate"];
        createdOn.text = [self DateFormateChange:datenumber];
    }
    else{
        createdOn.text = @"";
    }
    createdOn.font = delegate.contentSmallFont;
    createdOn.textColor = delegate.dimBlack;
    createdOn.textAlignment = ALIGN_LEFT;
    [mainView addSubview:createdOn];
    
    createdOnLabel.frame = CGRectMake(10, ypos, createdOn.intrinsicContentSize.width, 15);
    
    int xpos = delegate.devicewidth/2-((delegate.devicewidth/3)/2);
    UILabel *startDateLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/3, 15)];
    startDateLabel.text = NSLocalizedString(@"ACTUAL_START_DATE", nil);
    startDateLabel.textColor = [UIColor blackColor];
    startDateLabel.font = delegate.contentSmallFont;
    startDateLabel.textAlignment = ALIGN_LEFT;
    startDateLabel.textColor = [UIColor blackColor];
    [mainView addSubview:startDateLabel];
    
    UILabel *startDate =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos+17, delegate.devicewidth/3, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"actualStartDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"actualStartDate"];
        startDate.text = [self DateFormateChange:datenumber];
    }
    else{
        startDate.text = @"";
    }
    startDate.font = delegate.contentSmallFont;
    startDate.textAlignment = ALIGN_LEFT;
    startDate.textColor = delegate.dimBlack;
    [mainView addSubview:startDate];
    if(![startDate.text isEqualToString:@""]){
        xpos = delegate.devicewidth/2 - (startDate.intrinsicContentSize.width/2);
        startDateLabel.frame = CGRectMake(xpos, ypos, startDate.intrinsicContentSize.width+30, 15);
        startDate.frame = CGRectMake(xpos, ypos+17, startDate.intrinsicContentSize.width, 15);
    }
    
    xpos = delegate.devicewidth-(delegate.devicewidth/3);
    UILabel *dayLeftLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/3, 15)];
    dayLeftLabel.text = NSLocalizedString(@"TIME_LEFT_WITHOUT_COLON", nil);
    dayLeftLabel.textColor = [UIColor blackColor];
    dayLeftLabel.font = delegate.contentSmallFont;
    dayLeftLabel.textAlignment = ALIGN_LEFT;
    [mainView addSubview:dayLeftLabel];
    
    xpos = delegate.devicewidth - dayLeftLabel.intrinsicContentSize.width - 15;
    dayLeftLabel.frame = CGRectMake(xpos, ypos, dayLeftLabel.intrinsicContentSize.width, 15);
    
    UILabel *dayLeft =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos+17, dayLeftLabel.intrinsicContentSize.width, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"timeLeft"]))){
       dayLeft.text = [[dataArray objectAtIndex:0] objectForKey:@"timeLeft"];
    }
    else{
        dayLeft.text = @"";
    }
    dayLeft.font = delegate.contentSmallFont;
    dayLeft.textAlignment = ALIGN_LEFT;
    dayLeft.textColor = delegate.dimBlack;
    [mainView addSubview:dayLeft];
    

    if (dayLeftLabel.intrinsicContentSize.width > dayLeft.intrinsicContentSize.width)
        dayLeft.frame = CGRectMake(xpos, ypos+17, dayLeftLabel.intrinsicContentSize.width, 15);
    else
        dayLeft.frame = CGRectMake(xpos, ypos+17, dayLeft.intrinsicContentSize.width, 15);
    
    if([dayLeft.text containsString:@"-"]){
        dayLeft.text = [dayLeft.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        dayLeftLabel.text = @"Overdue";
        dayLeftLabel.frame = CGRectMake(xpos, ypos, dayLeftLabel.intrinsicContentSize.width+20, 15);
    }
    int saveYPos = ypos = ypos + 30 + 15;
   
    screenView=[[UIButton alloc] initWithFrame:CGRectMake(delegate.margin, ypos, delegate.devicewidth-delegate.margin*2, delegate.deviceheight)];
    screenView.backgroundColor=[UIColor whiteColor];
    screenView.layer.cornerRadius = 2.0;
    screenView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    screenView.layer.borderWidth = 0.5;
    screenView.layer.masksToBounds =YES;
    screenView.backgroundColor=[UIColor whiteColor];
    screenView.layer.shadowRadius = 3.0f;
    screenView.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
    screenView.layer.shadowOpacity = 0.7f;
    screenView.layer.masksToBounds = NO;
//    screenView.layer.cornerRadius = 2.0;
//    screenView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    screenView.layer.borderWidth = 0.5;
//    screenView.layer.shadowRadius = 3.0f;
//    screenView.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
//    screenView.layer.shadowOpacity = 0.7f;
//    screenView.layer.masksToBounds = NO;
    [mainView addSubview:screenView];
    cell.backgroundColor=[UIColor clearColor];
    
    UIView *lineView12 = [[UIView alloc] initWithFrame:CGRectMake(0, 0,delegate.devicewidth-delegate.margin*2, 5)];
    lineView12.backgroundColor = [UIColor appsYellowColor];
    [screenView addSubview:lineView12];

    
    ypos = 15;
    UILabel *status1 =
    [[UILabel alloc] initWithFrame:CGRectMake(rightPad/2, ypos, rightPad/2-delegate.margin, 20)];
    status1.font = delegate.headFont;
    status1.textAlignment = ALIGN_CENTER;
    status1.text = NSLocalizedString(@"IN_PROGRESS", nil);
    status1.textColor = delegate.inprogColor;
    CGPoint center = status1.center;
    center.x = screenView.frame.size.width / 2 + 10;
    [status1 setCenter:center];
    [screenView addSubview:status1];
    
    UIImageView *statusLogo;
    statusLogo=[[UIImageView alloc] initWithFrame:CGRectMake(rightPad/2, ypos, 20, 20)];
    statusLogo.image=[UIImage imageNamed:@"inprogressblue"];
    center = statusLogo.center;
    center.x = screenView.frame.size.width / 2 - 50;
    [statusLogo setCenter:center];
    [screenView addSubview:statusLogo];

    ypos = ypos + statusLogo.frame.size.height + 15;
    UIView *lineView0 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 1.0)];
    lineView0.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView0];

    ypos = ypos + 15;

    UILabel *taskNameLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, rightPad-delegate.margin*2, 15)];
    taskNameLabel.text =NSLocalizedString(@"TASK_NAME_WITH_COLON", nil);
    taskNameLabel.font = delegate.contentFont;
    taskNameLabel.textColor=[UIColor blackColor];
    taskNameLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:taskNameLabel];
    
    NSString *taskInfo = [[dataArray objectAtIndex:0] objectForKey:@"taskName"];
    if (taskInfo.length > 0){
        taskInfo = [NSString stringWithFormat:@"%@%@",[[taskInfo substringToIndex:1] uppercaseString],[taskInfo substringFromIndex:1] ];
    }
    xpos = taskNameLabel.intrinsicContentSize.width + 10;
    UILabel *taskName =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 50)];
    taskName.text = taskInfo;
    taskName.font = delegate.contentFont;
    taskName.textColor=delegate.dimBlack;
    taskName.textAlignment = ALIGN_LEFT;
    taskName.numberOfLines = 0;
    [taskName sizeToFit];
    [screenView addSubview:taskName];
    
    ypos = ypos + taskName.frame.size.height + 15;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, ypos, rightPad, 1.0)];
    lineView.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView];
    ypos = ypos + 15;
    
    UILabel *createdByLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    createdByLabel.text = NSLocalizedString(@"TYPE_WITH_COLON", nil);
    createdByLabel.font = delegate.contentFont;
    createdByLabel.textColor = [UIColor blackColor];
    createdByLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:createdByLabel];
    
    UILabel *createdBy =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos+17, delegate.devicewidth/2, 15)];
    createdBy.text = [[dataArray objectAtIndex:0] objectForKey:@"type"];
    createdBy.font = delegate.contentFont;
    createdBy.textColor = delegate.dimBlack;
    createdBy.textAlignment = ALIGN_LEFT;
    [screenView addSubview:createdBy];
    
    createdBy.frame = CGRectMake(createdByLabel.intrinsicContentSize.width+10, ypos, rightPad, 15);
    createdByLabel.frame = CGRectMake(10, ypos, createdByLabel.intrinsicContentSize.width, 15);
    
    ypos = ypos + createdByLabel.frame.size.height + 15;
    UIView *lineView10 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos, rightPad, 1.0)];
    lineView10.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView10];
    
    ypos = ypos + 15;
    UILabel *StartDateStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    StartDateStaticLabel.text = NSLocalizedString(@"START_DATE_WITH_COLON", nil);
    StartDateStaticLabel.font = delegate.contentFont;
    StartDateStaticLabel.textColor = [UIColor blackColor];
    StartDateStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:StartDateStaticLabel];
    
    xpos = StartDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *StartDateValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"startDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"startDate"];
        StartDateValueLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        StartDateValueLabel.text = @" ";
    }
    StartDateValueLabel.font = delegate.contentFont;
    StartDateValueLabel.textColor = delegate.dimBlack;
    StartDateValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:StartDateValueLabel];
    
    ypos = ypos + StartDateStaticLabel.frame.size.height + 15;
    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 1.0)];
    lineView1.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView1];
    
    ypos = ypos + 15;
    UILabel *EndDateStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    EndDateStaticLabel.text = NSLocalizedString(@"END_DATE_WITH_COLON", nil);
    EndDateStaticLabel.font = delegate.contentFont;
    EndDateStaticLabel.textColor = [UIColor blackColor];
    EndDateStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:EndDateStaticLabel];
    
    xpos = EndDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *EndDateValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"endDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"endDate"];
        EndDateValueLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        EndDateValueLabel.text = @"";
    }
    EndDateValueLabel.font = delegate.contentFont;
    EndDateValueLabel.textColor = delegate.dimBlack;
    EndDateValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:EndDateValueLabel];
    
    ypos = ypos + EndDateValueLabel.frame.size.height + 15;
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 1.0)];
    lineView2.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView2];
    
    ypos = ypos + 15;
    UILabel *taskDurationLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    taskDurationLabel.text = NSLocalizedString(@"DURATION_WITH_COLON", nil);
    taskDurationLabel.textColor = [UIColor blackColor];
    taskDurationLabel.font = delegate.contentFont;
    taskDurationLabel.textAlignment = ALIGN_LEFT;
    taskDurationLabel.textColor = [UIColor blackColor];
    [screenView addSubview:taskDurationLabel];
    
    UILabel *taskDuration =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    taskDuration.text = [[dataArray objectAtIndex:0] objectForKey:@"duration"];
    taskDuration.font = delegate.contentFont;
    taskDuration.textAlignment = ALIGN_LEFT;
    taskDuration.textColor = delegate.dimBlack;
    [screenView addSubview:taskDuration];
    
    taskDuration.frame = CGRectMake(taskDurationLabel.intrinsicContentSize.width + 10, ypos, rightPad, 15);
    ypos = ypos + taskDuration.frame.size.height + 15;
    UIView *lineView11 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 1.0)];
    lineView11.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView11];
    
    ypos = ypos + 15;
    UILabel *kpiLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
    kpiLabel.text = NSLocalizedString(@"KPI_WITH_COLON", nil);
    kpiLabel.textColor = [UIColor blackColor];
    kpiLabel.font = delegate.contentFont;
    createdBy.numberOfLines = 0;
    kpiLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:kpiLabel];
    
    xpos = kpiLabel.intrinsicContentSize.width + 10;
    UILabel *kpiName =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-30, 50)];
    kpiName.text = [NSString stringWithFormat:@"%@", [[dataArray objectAtIndex:0]  objectForKey:@"kpi"]];
    kpiName.textColor = delegate.dimBlack;
    kpiName.font = delegate.contentFont;
    kpiName.numberOfLines = 0;
    [kpiName sizeToFit];
    kpiName.textAlignment = ALIGN_LEFT;
    [screenView addSubview:kpiName];
    
    if (kpiName.text.length > 0)
        ypos = ypos + kpiName.frame.size.height + 15;
    else
        ypos = ypos + kpiLabel.frame.size.height + 15;
    UIView *lineView3 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 1.0)];
    lineView3.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView3];
    
    ypos = ypos + 15;
    UIProgressView *progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    [progressView setFrame:CGRectMake(10, ypos+10, delegate.devicewidth-delegate.margin*4, 15)];
    [progressView setProgress:ProgressValue/100.0];
    progressView.backgroundColor = delegate.ProgressColor;
    [progressView.layer setCornerRadius:10];
    progressView.layer.masksToBounds = TRUE;
    progressView.clipsToBounds = TRUE;
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 5.0f);
    progressView.transform = transform;
    progressView.progressTintColor = delegate.redColor;
    [screenView addSubview: progressView];
    
    ypos = ypos + progressView.frame.size.height + 15;
    int subTaskHeight = 0;
    if (((NSNull*)subTaskData != [NSNull null]) && subTaskData.count > 0 && [subTaskData[0] length] > 0){
        for (int i=0; i < subTaskData.count; i++){
//            UIView *lineView4 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 1.0)];
//            lineView4.backgroundColor = delegate.borderColor;
//            [screenView addSubview:lineView4];
            
            ypos = ypos + 15;
            UILabel *subtaskLabel =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            subtaskLabel.text = NSLocalizedString(@"SUBTASK_WITH_COLON", nil);
            subtaskLabel.font = delegate.contentFont;
            subtaskLabel.textColor = [UIColor blackColor];
            subtaskLabel.textAlignment = ALIGN_LEFT;
            [screenView addSubview:subtaskLabel];
            
            xpos = subtaskLabel.intrinsicContentSize.width + 10;
            UILabel *subtask =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 500)];
            subtask.text = subTaskData[i];
            subtask.font = delegate.contentFont;
            subtask.textColor = delegate.dimBlack;
            subtask.numberOfLines=0;
            subtask.textAlignment = ALIGN_LEFT;
            [subtask sizeToFit];
            [screenView addSubview:subtask];
            subTaskHeight = subTaskHeight + subtask.frame.size.width;
            ypos = ypos + subtask.frame.size.height + 15;
        }
    }
    
    UIView *lineView5 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 1.0)];
    lineView5.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView5];
    
    ypos = ypos + 15;
    UILabel *taskDetailLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth-delegate.margin*2-5, 15)];
    taskDetailLabel.text =NSLocalizedString(@"TASK_DETAILS_WITH_COLON", nil);
    taskDetailLabel.font = delegate.contentFont;
    taskDetailLabel.textColor = [UIColor blackColor];;
    taskDetailLabel.textAlignment = ALIGN_LEFT;
    taskDetailLabel.numberOfLines = 0;
    [taskDetailLabel sizeToFit];
    [screenView addSubview:taskDetailLabel];
    
    xpos = taskDetailLabel.intrinsicContentSize.width + 10;
    UILabel *taskDetail =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
    taskDetail.text = [[dataArray objectAtIndex:0] objectForKey:@"taskDetail"];
    taskDetail.font = delegate.contentFont;
    taskDetail.textColor = delegate.dimBlack;;
    taskDetail.textAlignment = ALIGN_LEFT;
    taskDetail.numberOfLines = 0;
    [taskDetail sizeToFit];
    [screenView addSubview:taskDetail];
   
    if (taskDetail.text.length > 0)
        ypos = ypos + taskDetail.frame.size.height + 15;
    else
        ypos = ypos + taskDetailLabel.frame.size.height + 15;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, ypos, delegate.devicewidth-delegate.margin*2, 40)];
    [screenView addSubview:footerView];
    
    UIView *lineView6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0,delegate.devicewidth-delegate.margin*2, 1)];
    lineView6.backgroundColor = [UIColor darkGrayColor];
    [footerView addSubview:lineView6];
    
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2, 0, footerView.frame.size.width/2, 40)];
    //submitButton.backgroundColor = delegate.redColor;
    submitButton.clipsToBounds = YES;
    submitButton.titleLabel.font = delegate.normalFont;
    [submitButton setTitle:NSLocalizedString(@"ADD_COMMENT", nil) forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    submitButton.tag = 1000;
    [footerView addSubview:submitButton];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width/2, 40)];
    //cancelButton.backgroundColor = delegate.yellowColor;
    cancelButton.clipsToBounds = YES;
    cancelButton.titleLabel.font = delegate.normalFont;
    
//    if ( [[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]) {
//        [cancelButton setTitle:NSLocalizedString(@"CANCEL_TASK", nil) forState:UIControlStateNormal];
//        
//    }
//    else{
        [cancelButton setTitle:NSLocalizedString(@"MARK_COMPETE", nil) forState:UIControlStateNormal];
//    }
    [cancelButton setTitleColor:[UIColor ButtonSkyColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.tag = 2000;
    [footerView addSubview:cancelButton];
    
    UIView *lineView7 = [[UIView alloc] initWithFrame:CGRectMake(cancelButton.frame.origin.x + submitButton.frame.size.width+5, 3,1, 31)];
    lineView7.backgroundColor = [UIColor darkGrayColor];
    [footerView addSubview:lineView7];
    
    int height = ypos + footerView.frame.size.height;
    screenView.frame = CGRectMake(delegate.margin, saveYPos, delegate.devicewidth-delegate.margin*2, height);
    mainView.frame = CGRectMake(0, 0, delegate.devicewidth, height+100);
}
-(IBAction)btnfun:(id)sender{
    PickerViewBg.hidden = YES;
    [CommentTextView resignFirstResponder];
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==20){
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else if(btn.tag==300){
        SelectBtn = @"";
        [addButton removeFromSuperview];
        tab.backgroundColor = delegate.BackgroudColor;
        isShowDetail = TRUE;
        sel_lab.frame=CGRectMake(0, delegate.headerheight+delegate.tabheight-2,  delegate.devicewidth/2, 2);
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(TaskDetailsWebApi) withObject:nil afterDelay:0.5];
    }
    else if(btn.tag==301){
        
        SelectBtn = @"";
        tab.backgroundColor = delegate.BackgroudColor;
        isShowDetail = FALSE;
        sel_lab.frame=CGRectMake(delegate.devicewidth/2, delegate.headerheight+delegate.tabheight-2,  delegate.devicewidth/2, 2);
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(getConversation) withObject:nil afterDelay:0.5];
    }
    else if (2000 == btn.tag){
        SelectBtn = @"MARK";
        CoomentView.hidden = NO;
        hight.constant = 10;
        hight1.constant = 190;
        CommentTitleLabel.text = @"Completion Comment";
        [self HideShowSubTaskView:NO];
    }
    else if (1000 == btn.tag){
        CoomentView.hidden = NO;
        SelectBtn = @"COMMENT";
        CommentTitleLabel.text = @"Add Comment";
        if(((NSNull*)subTaskData == [NSNull null])){
            hight.constant = 10;
            hight1.constant = 190;
            [self HideShowSubTaskView:YES];
        }
        else{
            hight.constant = 85;
            hight1.constant = 270;
            [self HideShowSubTaskView:NO];
        }
    }
}
-(void)HideShowSubTaskView:(BOOL)BoolValue{
    AddSubTaskView.hidden = BoolValue;
    SelectSubtaskStaticLabel.hidden = BoolValue;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (FALSE == isServerResponded)
        return 0;
    [msgvw removeFromSuperview];
    if (isShowDetail){
        return 1;
    }
    else{
        if ([dataConv count] <= 0){
            [self noMessageScreen:@"Sorry \n No Data Found"];
        }
        else
            return [dataConv count];
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (TRUE == isShowDetail){
        return mainView.frame.size.height;
    }
    else{
        tab.estimatedRowHeight = 105;
        return UITableViewAutomaticDimension;
//        int indx=(int)indexPath.row;
//        NSString * str = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
//        UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
//        CGSize size1 = CGSizeMake(230,9999);
//        CGRect textRect = [str boundingRectWithSize:size1 options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
//        CGSize size = textRect.size;
//        int temp = size.height+textInsetsMine1.bottom+textInsetsMine1.top;
//        return temp + 85;
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(delegate.margin, 5, delegate.devicewidth-delegate.margin, 50);
    myLabel.font = delegate.boldFont;
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    return headerView;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
    }
    if (TRUE == isShowDetail){
        [self screenTaskDetail:cell];
    }
    else{
        
        static NSString *CellIdentifier = @"CommentCell";
        CommentCell *cell = (CommentCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[CommentCell alloc] initWithFrame:CGRectZero];
        }
        
        if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:indexPath.row] objectForKey:@"creationDate"]))){
            NSNumber *datenumber = [[dataConv objectAtIndex:indexPath.row] objectForKey:@"creationDate"];
            cell.DateLabel.text = [self CommentDateFormateChange:datenumber];
        }
        else{
            cell.DateLabel.text = @"";
        }
        cell.CommnetLabel.text = [[dataConv objectAtIndex:indexPath.row] objectForKey:@"comment"];
        
        
        SubTask = [[dataConv objectAtIndex:indexPath.row] objectForKey:@"subtask"];
        NSLog(@"SubTask===%@",SubTask);
        if([SubTask isEqual:[NSNull null]]){
            cell.SubtaskNameLabel.text = @"";
        }
        else{
            if([SubTask isEqualToString:@"null"] ||[SubTask isEqualToString:@""]|| [SubTask isEqualToString:@"(null)"]){
                cell.SubtaskNameLabel.text = @"";
            }
            else{
                cell.SubtaskNameLabel.text = SubTask;
            }
        }
        cell.UserNameLabel.text = [[dataConv objectAtIndex:indexPath.row] objectForKey:@"userName"];
        
        if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:indexPath.row] objectForKey:@"userImg"]))){
            UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:[[dataConv objectAtIndex:indexPath.row] objectForKey:@"userImg"]]]];
            if(images == nil){
                NSLog(@"image nil");
                cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
            }
            else{
                NSString *URL = [NSString stringWithFormat:@"%@",[[dataConv objectAtIndex:indexPath.row] objectForKey:@"userImg"]];
                [ cell.ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
            }
        }
        else{
            cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
        }
        return cell;
    }
    return cell;
}
- (void)showCommentAlert {
    CoomentView.hidden = NO;
    SelectBtn = @"COMMENTBTN";
    if(((NSNull*)subTaskData == [NSNull null])){
        hight.constant = 10;
        hight1.constant = 190;
        [self HideShowSubTaskView:YES];
    }
    else{
        hight.constant = 85;
        hight1.constant = 270;
        [self HideShowSubTaskView:NO];
    }
}
-(IBAction)CancelAction{
    SelectBtn = @"";
    CoomentView.hidden = YES;
    CommentTextView.text = @"";
    SelectSubtasLabel.text = @"Select Subtask";
    SelectSubtasLabel.textColor = [UIColor blackColor];
    
}
-(IBAction)SubmitAction{
    if([CommentTextView.text isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_ADD_COMMENT", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        if([SelectBtn isEqualToString:@"MARK"]){
            [LoadingManager showLoadingView:self.view];
            [self performSelector:@selector(TaskDetailsWebApi) withObject:nil afterDelay:0.5];

        }
        else if([SelectBtn isEqualToString:@"COMMENT"]||[SelectBtn isEqualToString:@"COMMENTBTN"]){
            if(((NSNull*)subTaskData == [NSNull null])){
                SubTask = @"";
            }
            else{
                if([SelectSubtasLabel.text isEqualToString:@"Select Subtask"]){
                    SubTask = @"";
                }
            }
            [LoadingManager showLoadingView:self.view];
            [self performSelector:@selector(TaskDetailsWebApi) withObject:nil afterDelay:0.5];
            CoomentView.hidden = YES;
            [CommentTextView resignFirstResponder];
        }
    }
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component; {
    if(((NSNull*)subTaskData == [NSNull null])){
        return 0;
    }
    else{
        if([subTaskData count]>0){
            return [subTaskData count];
        }
    }
    return 0;
}
-(NSString*) pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(((NSNull*)subTaskData == [NSNull null])){
        return 0;
    }
    else{
        if([subTaskData count]>0){
            return [subTaskData objectAtIndex:row];
        }
    }
    return @"";
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;{
    if(((NSNull*)subTaskData == [NSNull null])){
        SubTask = @"";
    }
    else{
        if([subTaskData count]>0){
            SelectSubtasLabel.text = [subTaskData objectAtIndex:row];
            SubTask = [subTaskData objectAtIndex:row];
            SelectSubtasLabel.textColor = [UIColor blackColor];
        }
    }
    
}
-(IBAction)SelectSubTaskAction{
    [CommentTextView resignFirstResponder];
    PickerViewBg.hidden = NO;
    [PickerView reloadAllComponents];
}
-(IBAction)PickerDoneAction:(id)sender{
    PickerViewBg.hidden = YES;
    if(((NSNull*)subTaskData == [NSNull null])){
        SubTask = @"";
    }
    else{
        if([SubTask isEqualToString:@""]){
            SelectSubtasLabel.text = [subTaskData objectAtIndex:0];
            SubTask = [subTaskData objectAtIndex:0];
            SelectSubtasLabel.textColor = [UIColor blackColor];
        }
    }
}
-(void)noMessageScreen:(NSString*)message{
    msgvw=[[UIView alloc] initWithFrame:tab.frame];
    tab.backgroundColor = delegate.BackgroudColor;
    msgvw.backgroundColor = delegate.BackgroudColor;
    UIImageView *nomsgimg;
    UILabel *tab_lab;
    if (delegate.deviceheight <= 460){
        nomsgimg=[[UIImageView alloc] initWithFrame:CGRectMake((msgvw.frame.size.width-100)/2,
                                                               0, 50,59)];
        tab_lab=[[UILabel alloc] initWithFrame:CGRectMake(0,100, delegate.devicewidth, 80)];
    }
    else{
        nomsgimg=[[UIImageView alloc] initWithFrame:CGRectMake((msgvw.frame.size.width-50)/2,
                                                               70, 50,59)];
        tab_lab=[[UILabel alloc] initWithFrame:CGRectMake(0,100, delegate.devicewidth, 80)];
    }
    nomsgimg.image=[UIImage imageNamed:@"no_data"];
    [msgvw addSubview:nomsgimg];
    tab_lab.text = message;
    tab_lab.font=[UIFont systemFontOfSize:13];
    tab_lab.textColor=delegate.dimBlack;
    tab_lab.lineBreakMode = NSLineBreakByWordWrapping;
    tab_lab.numberOfLines = 0;
    tab_lab.textAlignment=ALIGN_CENTER;
    [msgvw addSubview:tab_lab];
    [tab addSubview:msgvw];
//    tab.scrollEnabled=FALSE;
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    PickerViewBg.hidden = YES;
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)CommentDateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm aa"];
    return [dateformate stringFromDate:Newdate];
}
@end
