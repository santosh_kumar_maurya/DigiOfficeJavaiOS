//
//  CustomColor.swift
//  iWork
//
//  Created by Shailendra on 08/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

extension UIColor {

    //iPM
    class func TopBarYellowColor() -> UIColor{
        return UIColor(red:255.0/255.0, green:211.0/255.0 ,blue:72.0/255.0 , alpha:1.0)
    }
    class func TextBlueColor() -> UIColor{
        return UIColor(red:59.0/255.0, green:112.0/255.0 ,blue:140.0/255.0 , alpha:1.0)
    }
    class func ButtonSkyColor() -> UIColor{
        return UIColor(red:92.0/255.0, green:197.0/255.0 ,blue:216.0/255.0 , alpha:1.0)
    }
    class func RedColor() -> UIColor{
        return UIColor(red:242.0/255.0, green:28.0/255.0 ,blue:44.0/255.0 , alpha:1.0)
    }
    class func ButtonGreenColor() -> UIColor{
        return UIColor(red:150.0/255.0, green:203.0/255.0 ,blue:65.0/255.0 , alpha:1.0)
    }
    class func ApproveGreenColor() -> UIColor{
        return UIColor(red:40.0/255.0, green:182.0/255.0 ,blue:84.0/255.0 , alpha:1.0)
    }
    class func RejectBtnColor() -> UIColor{
        return UIColor(red:150.0/255.0, green:150.0/255.0 ,blue:150.0/255.0 , alpha:1.0)
    }
    class func NeedRevisionBtnColor() -> UIColor{
        return UIColor(red:239.0/255.0, green:154.0/255.0 ,blue:61.0/255.0 , alpha:1.0)
    }
    class func StakeHoderSelectColor() -> UIColor{
        return UIColor(red:237.0/255.0, green:148.0/255.0 ,blue:51.0/255.0 , alpha:1.0)
    }
    //iWork
    class func LocationPlaceHolderColor() -> UIColor{
        return UIColor(red:242.0/255.0, green:166.0/255.0 ,blue:166.0/255.0 , alpha:1.0)
    }
    class func ViewRequestBlueColor() -> UIColor{
        return UIColor(red:78.0/255.0, green:194.0/255.0 ,blue:255.0/255.0 , alpha:1.0)
    }
    class func ViewRequestRedColor() -> UIColor{
        return UIColor(red:238.0/255.0, green:28.0/255.0 ,blue:39.0/255.0 , alpha:1.0)
    }
    class func ViewRequestGreenColor() -> UIColor{
        return UIColor(red:59.0/255.0, green:182.0/255.0 ,blue:66.0/255.0 , alpha:1.0)
    }
    class func ViewRequestOrangeColor() -> UIColor{
        return UIColor(red:247.0/255.0, green:116.0/255.0 ,blue:10.0/255.0 , alpha:1.0)
    }
    class func ViewRequestGreyColor() -> UIColor{
        return UIColor(red:214.0/255.0, green:222.0/255.0 ,blue:235.0/255.0 , alpha:1.0)
    }
    class func LineGreyColor() -> UIColor{
        return UIColor(red:228.0/255.0, green:232.0/255.0 ,blue:235.0/255.0 , alpha:1.0)
    }
    class func iWorkRequestGreyColor() -> UIColor{
        return UIColor(red:212.0/255.0, green:216.0/255.0 ,blue:225.0/255.0 , alpha:1.0)
    }
    
    
}
