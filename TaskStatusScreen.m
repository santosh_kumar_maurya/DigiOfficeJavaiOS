#import "TaskStatusScreen.h"
#import "Header.h"

@interface TaskStatusScreen ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    IBOutlet UITableView *tab;
    IBOutlet UILabel *headerlab;
    IBOutlet UILabel *NoDataLabel;
    WebApiService *Api;
    NSString *handelId;
    NSString *reqType;
    NSMutableArray *newArray;
    NSMutableDictionary *dataDict;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    int offset;
    int limit;
    IBOutlet UIView *NoDataView;

}
@end
@implementation TaskStatusScreen
@synthesize taskType,isManager;

- (void)viewDidLoad{
    [super viewDidLoad];
    [self EmptyArray];
    self.view.backgroundColor = delegate.BackgroudColor;
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    handelId = [NSString stringWithFormat:@"%@",[delegate.dataFull objectForKey:@"handsetId"]];
    if (1 == taskType)
        headerlab.text=NSLocalizedString(@"ONGOING_SCHEDULE", nil);
    else if (2 == taskType)
        headerlab.text=NSLocalizedString(@"ONGOING_BHIND_SCHEDULE", nil);
    else
        headerlab.text=NSLocalizedString(@"COMPLETE_TASK", nil);
    Api = [[WebApiService alloc] init];
    tab.estimatedRowHeight = 50;
    tab.rowHeight = UITableViewAutomaticDimension;

    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
    
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [tab registerNib:Nib forCellReuseIdentifier:@"CELL"];
}
-(void) viewWillAppear:(BOOL)animated{
    [LoadingManager showLoadingView:self.view];
    [self EmptyArray];
    [self performSelector:@selector(fetchData) withObject:nil afterDelay:0.5];
}
-(void) fetchData{
    if(delegate.isInternetConnected){
        if(taskType == 3){
            params = @ {
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
            ResponseDic = [Api WebApi:params Url:[NSString stringWithFormat:@"CompletedCountList?employeeID=%@",[ApplicationState userId]]];
            [refreshControl endRefreshing];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self fetchData];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                NSLog(@"NewTaskResponseDic==%@",ResponseDic);
                NoDataLabel.hidden = YES;
                if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                    
                    NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                    if(ResponseArrays.count>0){
                        [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                        NoDataLabel.hidden = YES;
                        [tab reloadData];
                    }
                    else if(ResponseArrays.count == 0 && newArray.count == 0){
                       [self NoDataFound];
                    }
                }
                else{
                   [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
        else if(taskType == 1 || taskType == 2){
            NSString *Status;
            if(taskType == 1){
                Status = @"ongoing";
            }
            else if(taskType == 2){
                Status = @"behind";
            }
            params = @ {
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };

            [refreshControl endRefreshing];
            ResponseDic = [Api WebApi:params Url:[NSString stringWithFormat:@"onBehindCountList?employeeId=%@&status=%@",[ApplicationState userId],Status]];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self fetchData];
            }
            else  if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                NSLog(@"NewTaskResponseDic==%@",ResponseDic);
                NoDataLabel.hidden = YES;
                if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                    
                    NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                    if(ResponseArrays.count>0){
                        [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                        NoDataLabel.hidden = YES;
                        [tab reloadData];
                    }
                    else if(ResponseArrays.count == 0 && newArray.count == 0){
                        [self NoDataFound];
                    }
                }
                else{
                    [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
      [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    if(newArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataView.hidden = NO;
        NoDataLabel.text = @"No Data Found";
    }
    [tab reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self fetchData];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [newArray addObject:BindDataDic];
    }
}
- (void)reloadData{
    [self EmptyArray];
    [self fetchData];
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    newArray = [[NSMutableArray alloc] init];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [newArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.tag = indexPath.row;
    Cell.ViewDetailsBtn.tag = indexPath.row;
    Cell.RatingView.hidden = YES;
    Cell.RateStaticLabel.hidden = YES;
    Cell.DurationStaticLabel.hidden = NO;
    Cell.DurationLabel.hidden = NO;
    [Cell.ViewDetailsBtn addTarget:self action:@selector(GotoTaskDetails:) forControlEvents:UIControlEventTouchUpInside];
    if(newArray.count>0){
        NSDictionary * responseData = [newArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
        if(taskType == 1 || taskType == 2){
            
            Cell.RatingView.hidden = YES;
            Cell.RateStaticLabel.hidden = YES;
            Cell.DurationStaticLabel.hidden = NO;
            Cell.DurationLabel.hidden = NO;
            if(taskType == 1 ){
              Cell.DurationStaticLabel.text = @"Time Left";
                Cell.DurationLabel.text = [responseData valueForKey:@"timeLeft"];
            }
            else{
              Cell.DurationStaticLabel.text = @"Over due";  
            }
            Cell.StatusLabel.text = @"In-Progress";
            Cell.StausImageView.image = [UIImage imageNamed:@"WatingForApprove"];
        }
        else{
            Cell.RatingView.hidden = NO;
            Cell.RateStaticLabel.hidden = NO;
            Cell.DurationStaticLabel.hidden = YES;
            Cell.DurationLabel.hidden = YES;
            Cell.StatusLabel.text = @"Completed";
        }
    }
    return Cell;
}
-(void)GotoTaskDetails:(UIButton*)sender{
    int TagValue = sender.tag;
    if (taskType == 3){
        NSString* task = [[newArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
        viewController.taskID = task;
        viewController.isManager = isManager;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
    else{
        NSString* task = [[newArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        if (TRUE == isManager){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
            TaskDetailManagerScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailManagerScreen"];
            viewController.taskID = task;
            viewController.isManager = FALSE;
            [[self navigationController] pushViewController:viewController animated: YES];
            
        }
        else{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
            TaskDetailScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailScreen"];
            viewController.taskID = task;
            [[self navigationController] pushViewController:viewController animated: YES];

        }
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
