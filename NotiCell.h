//
//  NotiCell.h
//  iWork
//
//  Created by Shailendra on 21/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotiCell : UITableViewCell

@property(nonnull,retain)IBOutlet UILabel *NotificationLabel;
@property(nonnull,retain)IBOutlet UILabel *DateLabel;
@property(nonnull,retain)IBOutlet UIImageView *StatusImageView;
- (void)configureCell:(NSDictionary *_Nullable)info;
@end
