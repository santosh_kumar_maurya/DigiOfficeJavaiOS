//
//  NewTaskRejectOrApproveTableViewCell.h
//  iWork
//
//  Created by Himanshu  Goyal on 30/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewTaskRejectOrApproveTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *rejectButton;
@property (weak, nonatomic) IBOutlet UIButton *approveButton;

@end
