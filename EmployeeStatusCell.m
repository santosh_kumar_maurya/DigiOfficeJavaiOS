//
//  EmployeeStatusCell.m
//  iWork
//
//  Created by Shailendra on 07/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeStatusCell.h"
#import "Header.h"
#import "iWork-Swift.h"

@implementation EmployeeStatusCell

- (void)awakeFromNib {
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [super awakeFromNib];
   
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: self.TaskView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    self.TaskView.layer.mask = maskLayer2;
    
//    CAShapeLayer * maskLayer3 = [CAShapeLayer layer];
//    maskLayer3.path = [UIBezierPath bezierPathWithRoundedRect: self.DetailsBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
//    self.DetailsBtn.layer.mask = maskLayer3;
    
    self.ViewDetailsBtn.backgroundColor = [UIColor whiteColor];
    [self.ViewDetailsBtn setTitleColor:[UIColor ButtonSkyColor] forState:UIControlStateNormal];
    [self.StartBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    
}
- (void)configureCell:(NSDictionary *)info{
  
    if(IsSafeStringPlus(TrToString(info[@"taskId"]))){
        _TaskIdLabel.text  = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),info[@"taskId"]];
    }
    else{
        _TaskIdLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"creationDate"]))){
        if ([info[@"creationDate"] isKindOfClass:[NSString class]]){
            _DateLabel.text  = info[@"creationDate"];
        }
        else{
            NSNumber *datenumber = info[@"creationDate"];
            _DateLabel.text = [self DateFormateChange:datenumber];
        }
    }
    else if(IsSafeStringPlus(TrToString(info[@"creationOn"]))){
        NSNumber *datenumber = info[@"creationOn"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else if(IsSafeStringPlus(TrToString(info[@"creartedOn"]))){
        NSNumber *datenumber = info[@"creartedOn"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else if(IsSafeStringPlus(TrToString(info[@"createdOn"]))){
        NSNumber *datenumber = info[@"createdOn"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else if(IsSafeStringPlus(TrToString(info[@"created_on"]))){
        NSNumber *datenumber = info[@"created_on"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _DateLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskName"]))){
        _TaskNameLabel.text  = info[@"taskName"];
    }
    else if(IsSafeStringPlus(TrToString(info[@"taskNAme"]))){
        _TaskNameLabel.text  = info[@"taskNAme"];
    }
    else if(IsSafeStringPlus(TrToString(info[@"task_name"]))){
        _TaskNameLabel.text  = info[@"task_name"];
    }
    else{
        _TaskNameLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"createdBy"]))){
        _CreatedByLabel.text  = info[@"createdBy"];
    }
//    else if(IsSafeStringPlus(TrToString(info[@"taskType"]))){
//        _CreatedByLabel.text  = info[@"taskType"];
//    }
//    else if(IsSafeStringPlus(TrToString(info[@"task_type"]))){
//        _CreatedByLabel.text  = info[@"task_type"];
//    }
    else{
        _CreatedByLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"duration"]))){
        _DurationLabel.text  = info[@"duration"];
        if([_DurationLabel.text isEqualToString:@"-"]){
            _DurationLabel.text = [_DurationLabel.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
            _DurationStaticLabel.text = @"Overdue";
        }
    }
    else{
        _DurationLabel.text  = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"kpi"]))){
        _KPILabel.text  = info[@"kpi"];
    }
    else{
        _KPILabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskStatus"]))){
        _StatusLabel.text  = info[@"taskStatus"];
    }
    else{
        _StatusLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"rating"]))){
        _RatingView.rating = [info[@"rating"] floatValue];
        _RatingView.tintColor = [UIColor lightGrayColor];
       // _RatingView.value  = [info[@"rating"] floatValue];
        //_RatingView.tintColor = [UIColor NeedRevisionBtnColor];
        _RatingView.starFillColor = [UIColor NeedRevisionBtnColor];
        _RatingView.starSize = 14.0;
    }
    else{
        _RatingView.rating = 0;
        //_RatingView.value  = 0;
        //_RatingView.starSize = 14.0;
    }
}

-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm aa"];
    return [dateformate stringFromDate:Newdate];
}
@end
