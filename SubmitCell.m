//
//  SubmitCell.m
//  iWork
//
//  Created by Shailendra on 05/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "SubmitCell.h"
#import "iWork-Swift.h"

@implementation SubmitCell

- (void)awakeFromNib {
    [super awakeFromNib];
  
    self.backgroundColor = [UIColor clearColor];
    
    _BgView.layer.cornerRadius = 2.0;
    _BgView.maskView.layer.cornerRadius = 7.0f;
    _BgView.layer.shadowRadius = 3.0f;
    _BgView.layer.shadowColor = [UIColor blackColor].CGColor;
    _BgView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _BgView.layer.shadowOpacity = 0.7f;
    _BgView.layer.masksToBounds = NO;
    
    self.TopView.backgroundColor = [UIColor TopBarYellowColor];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: self.TopView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){2.0}].CGPath;
    self.TopView.layer.mask = maskLayer1;
    
    _SubmitTaskStaticLabel.textColor = [UIColor TextBlueColor];
    _SubmitProgressStaticLabel.textColor = [UIColor TextBlueColor];
    _GiveFeedbackStaticLabel.textColor = [UIColor TextBlueColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
