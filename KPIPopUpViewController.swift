//
//  KPIPopUpViewController.swift
//  iWork
//
//  Created by mac book pro on 28/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

protocol KPIPopUpDelgate {
    func popUpOkayButtonTapped()
}

class KPIPopUpViewController: UIViewController {

    @IBOutlet weak var btnOkay: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var popUpView: UIView!
    var strMessage: String!
    var popUpDelagate: KPIPopUpDelgate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.masksToBounds = true
        self.popUpView.layer.borderWidth = 0.5;
        self.popUpView.layer.borderColor = UIColor.clear.cgColor
        self.messageLabel.text = strMessage
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnOkayTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true) { 
            self.popUpDelagate.popUpOkayButtonTapped()
        }
    }
    
    @IBAction func btnCrossTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true) {
            self.popUpDelagate.popUpOkayButtonTapped()
        }
    }
}
