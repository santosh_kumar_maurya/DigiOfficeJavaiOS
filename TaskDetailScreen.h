#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DownPicker.h"
#import "DQAlertView.h"
#import "NYAlertViewController.h"
#import "VCFloatingActionButton.h"
#import "FCAlertView.h"
#import "AFURLSessionManager.h"

@interface TaskDetailScreen : UIViewController <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UINavigationControllerDelegate,floatMenuDelegate, FCAlertViewDelegate,UITextViewDelegate>
{
    AppDelegate *delegate;
    UITableView *tab;
    NSMutableArray *tabdataarray1,*tabdataarray2,*dataArray,*dataConv;
    NSMutableDictionary *dataDict;
    UILabel *flatButton_lab,*flatButton1_lab,*sel_lab,*lab1,*datelab,*flatButton2_lab;
    UITextField *searchtxt,*searchtxt1,*renttext,*phonetext,*desctext;
    BOOL isShowDetail, isMe, isServerResponded;
    int taskType;
    NSString *taskID, *userComment;
    int count;
    UIDatePicker *dateTimePicker;
    UIView *mainScreenView, *msgvw;//, *screenView;
    UIAlertView *alertView;
    UITextField *alertTextField, *alertTextField1;
    VCFloatingActionButton *addButton;
    FCAlertView *alert;
   
}
@property (nonatomic) BOOL isShowDetail,isMe,isManager;
@property (nonatomic) int count, taskType;
@property (nonatomic, retain) NSMutableDictionary *dataDict;
@property (nonatomic, retain) NSString *taskID,*isComeFrom;
- (void) ScreenDesign;
- (void)showCommentAlert;
@end
