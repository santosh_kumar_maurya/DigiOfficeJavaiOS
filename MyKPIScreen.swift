//
//  MyKPIScreen.swift
//  iWork
//
//  Created by Shailendra on 09/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class MyKPIScreen: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var KPIListTableView : UITableView!
    var ResponseDic : NSDictionary!
    var ObjAppDelegate = AppDelegate()
    var Api: WebApiService =  WebApiService()
    var KPIArray = NSArray()
    var EMP_ID : String! = ""
    var isComeFrom  : String! = ""
    var UserId_OR_UserID : String! = ""
    var isSelectedArray : [Bool]! = []
    
    @IBOutlet var MyKpiStaticLabel : UILabel!
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        MyKpiStaticLabel.textColor = UIColor.TextBlueColor()
        ObjAppDelegate = UIApplication.shared.delegate as! AppDelegate
        KPIListTableView.estimatedRowHeight = 200
        KPIListTableView.rowHeight = UITableViewAutomaticDimension;
        KPIListTableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: KPIListTableView.bounds.size.width, height: 0.01))
        LoadingManager.showLoading(self.view)
        self.perform(#selector(MyKPIScreen.GetKPIWebApi), with: nil, afterDelay: 0.5)
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return  KPIArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell = tableView.dequeueReusableCell(
            withIdentifier: "CELL", for: indexPath) as! KPISCell
         Cell.tag = indexPath.row
          let Dic = KPIArray[indexPath.row] as! NSDictionary
          Cell.KPILabel.text = Dic.value(forKey: "kpiDesc") as? String
        
        if (isSelectedArray[indexPath.row] == true)
        {
            Cell.radioButton.image = UIImage(named:"Radio_Green")
        }
        else
        {
            Cell.radioButton.image = UIImage(named:"RadioOff")
        }
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
        Cell.addGestureRecognizer(tapGesture)
        
        return Cell
    }

    func tapBlurButton(_ sender: UITapGestureRecognizer) {
    
        let tagValue = sender.view?.tag
        
        self.isSelectedArray[tagValue!] = true
        KPIListTableView.reloadData()
        let  KPIDic = KPIArray[tagValue!] as! NSDictionary
        let  kpiId =  KPIDic.value(forKey: "kpiId") as? String
        let  kpiDesc =  KPIDic.value(forKey: "kpiDesc") as? String
        let Dic : NSDictionary! = ["CODE": "\(kpiId!)","DESC": "\(kpiDesc!)"]
        NotificationCenter.default.post(name: Notification.Name("KPINOTI"), object: nil, userInfo: Dic as? [AnyHashable : Any])
        self.dismiss(animated: true, completion: nil)
    }
   
    func GetKPIWebApi(){
        if (ObjAppDelegate.isInternetConnected()){
            if(isComeFrom == "ASSIGN"){
               UserId_OR_UserID = EMP_ID
            }
            else{
               UserId_OR_UserID = UserDefaults.standard.string(forKey: "kUserId")
            }
            ResponseDic = Api.webApi(nil, url:"getKPI?user_id=\(UserId_OR_UserID!)") as NSDictionary!
            if ResponseDic != nil{
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    LoadingManager.hideLoading(view)
                    let Dic = ResponseDic.value(forKey: "object") as! NSDictionary
                    if (Dic["kpi"] as? NSArray) != nil {
                        KPIArray = Dic.value(forKey: "kpi") as! NSArray
                        
                        for _ in 0...KPIArray.count - 1
                        {
                            self.isSelectedArray.append(false)
                        }
                        
                    }
                    self.KPIListTableView.reloadData()
                }
                else{
                    LoadingManager.hideLoading(view)
                }
            }
            else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(MyKPIScreen.GetKPIWebApi), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    @IBAction func CrossAction(){
        self.dismiss(animated: true, completion: nil)
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: true, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}
