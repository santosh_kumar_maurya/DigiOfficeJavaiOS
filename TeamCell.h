//
//  TeamCell.h
//  iWork
//
//  Created by Shailendra on 04/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectTeamMember <NSObject>

-(void)SelectTeamMemberIndex:(NSDictionary*)Dic;

@end
@interface TeamCell : UITableViewCell<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)IBOutlet UIView *MyTeamView;
@property(nonatomic,strong)IBOutlet UILabel *MyTeamLabel;
@property(nonatomic,strong)IBOutlet UITableView *MyTeamTableView;
@property(nonatomic,strong)NSArray *TeamMemberArray;
@property(nonatomic,weak) id <SelectTeamMember> delegate;
@property(nonatomic,strong)IBOutlet NSLayoutConstraint *hight;

- (void)configureCell:(NSArray *)info;
@end
