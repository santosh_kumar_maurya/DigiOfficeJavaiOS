//
//  MapDetailsViewController.h
//  iWork
//
//  Created by Shailendra on 16/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
@interface MapDetailsViewController : UIViewController<GMSMapViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UILabel *RequestIDStaticLabel,*RequestIDValueLabel,*EmployeeNameStaticLabel,*EmployeeNameValueLabel,*iWorkDateStaticLabel,*iWorkDateValueLabel;
}
@property(nonatomic,strong)IBOutlet GMSMapView *mapView;
@property(nonatomic,strong)NSDictionary *MapDic;
@property(nonatomic,strong)NSString *RequestID;
@property(nonatomic,strong)NSString *isComeFrom;

@property (weak, nonatomic) IBOutlet UITableView *mapTableView;

@end
