//
//  MapDetailsViewController.m
//  iWork
//
//  Created by Shailendra on 16/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MapDetailsViewController.h"
#import "Header.h"

@interface MapDetailsViewController (){
    AppDelegate *delegate;
    double Lat;
    double Long;
    double Lat1;
    double Long1;
    
    __weak IBOutlet UIView *userInfoView;
    __weak IBOutlet UIView *userView;
}
@end

@implementation MapDetailsViewController
@synthesize MapDic,isComeFrom,RequestID;

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    self.mapTableView.estimatedRowHeight = 260;
    self.mapTableView.rowHeight = UITableViewAutomaticDimension;
    [self registerTableViewCell];
    RequestIDValueLabel.text = @" ";
    iWorkDateValueLabel.text = @" ";
}
-(void)viewWillAppear:(BOOL)animated{
    [self updateUI];
    [self GetData];
}
-(void)updateUI{
    userView.layer.cornerRadius = 6;
    userView.clipsToBounds = YES;
    
    userInfoView.layer.cornerRadius = 6;
    userInfoView.clipsToBounds = YES;
}
-(void)GetData{
    NSLog(@"MapDic==%@",MapDic);
    if(IsSafeStringPlus(TrToString(MapDic[@"locLatitude"]))) {
        Lat = [MapDic[@"locLatitude"] doubleValue];
    }
    else{
        Lat = 0.0;
    }
    if(IsSafeStringPlus(TrToString(MapDic[@"locLongitude"]))) {
        Long = [MapDic[@"locLongitude"] doubleValue];
    }
    else{
        Long = 0.0;
    }
    if(IsSafeStringPlus(TrToString(MapDic[@"checkInLatitude"]))) {
        Lat1 = [MapDic[@"checkInLatitude"] doubleValue];
    }
    else{
        Lat1 = 0.0;
    }
    if(IsSafeStringPlus(TrToString(MapDic[@"checkInLongitude"]))) {
        Long1 = [MapDic[@"checkInLongitude"] doubleValue];
    }
    else{
        Long1 = 0.0;
    }

    NSString *HeaderStr = NSLocalizedString(@"IWORK_LOCATION", nil);
    //HeaderStr = [HeaderStr stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
    GMSMarker *marker1 = [[GMSMarker alloc] init];
    marker1.position = [[[CLLocation alloc]initWithLatitude:Lat longitude:Long] coordinate];
    if([isComeFrom isEqualToString:@"REPORT"]){
        marker1.title = @"Jakarta";
    }
    else{
        marker1.title = HeaderStr;
    }
    marker1.appearAnimation = kGMSMarkerAnimationPop;
    marker1.map = self.mapView;
    
    GMSMarker *marker2 = [[GMSMarker alloc] init];
    marker2.position =[[[CLLocation alloc]initWithLatitude:Long1 longitude:Long1] coordinate];
    marker2.title = @"Checkin Location";
    marker2.appearAnimation = kGMSMarkerAnimationPop;
    //if(![isComeFrom isEqualToString:@"REPORT"]){
    marker2.map = self.mapView;
    // }
    self.mapView.myLocationEnabled = NO;
    self.mapView.mapType = kGMSTypeNormal;
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.myLocationButton = NO;
    self.mapView.delegate = self;
    self.mapView.settings.zoomGestures = YES;
    
    if(IsSafeStringPlus(TrToString(MapDic[@"requestId"]))){
        RequestIDValueLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"REQUEST_ID_WITH_COLON", nil),MapDic[@"requestId"]];
    }
    else{
         RequestIDValueLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(MapDic[@"userName"]))){
        EmployeeNameValueLabel.text = MapDic[@"userName"];
    }
    else{
        EmployeeNameValueLabel.text = @" ";
    }
    
    
    if(IsSafeStringPlus(TrToString(MapDic[@"requestDate"]))) {
        NSNumber *datenumber = MapDic[@"requestDate"];
        iWorkDateValueLabel.text = [self DateFormateChange:datenumber];
        
    } else {
        iWorkDateValueLabel.text = @" ";
    }
    

}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)HomeAction:(id)sender{
    NSArray *array = [self.navigationController viewControllers];
    for (int i = 0 ; i < array.count; i++) {
        UIViewController *ViewController = [array objectAtIndex:i];
        if([ViewController isKindOfClass:[AppContainerViewController class]]){
            [self.navigationController popToViewController:ViewController animated:YES];
        }
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)GetDate:(NSNumber*)Timenumber{
    NSString *DateStr = [NSString stringWithFormat:@"%@",Timenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"HH:mm:ss"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"hh:mm a"];
    return [dateformate stringFromDate:Newdate];
}

-(NSString*)MaxDate:(NSString*)DateString{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString: DateString];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *convertedString = [dateFormatter stringFromDate:date];
    return convertedString;
}


-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

-(void)registerTableViewCell{
     [self.mapTableView registerNib:[UINib nibWithNibName:@"MapDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"MapDetailTableViewCell"];
}

//MARK: - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MapDetailTableViewCell *Cell = (MapDetailTableViewCell*)[self.mapTableView dequeueReusableCellWithIdentifier:@"MapDetailTableViewCell" forIndexPath:indexPath];
    [Cell configureCell:MapDic];
    return Cell;
}


@end
