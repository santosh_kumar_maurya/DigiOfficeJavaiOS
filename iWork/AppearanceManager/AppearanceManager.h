//
//  AppearanceManager.h
//  MangoApp
//
//  Created by Fourbrick on 17/11/16.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppearanceManager : NSObject
+ (void)applyGlobalAppearance;
@end
