//
//  AlertManager.h
//  MangoApp
//
//  Created by Fourbrick on 17/11/16.
//   Copyright © 2017 Fourbrick. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlertManager : NSObject


+ (void)showAlertView:(NSString *)title withMessage:(NSString *)message;
+ (void)showTostAlert:(NSString *)message;
+ (void)showPopupMessageAlert:(NSString *)message  withTitle:(NSString *) title;
@end
