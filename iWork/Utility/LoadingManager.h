//
//  LoadingManager.h
//  MangoApp
//
//  Created by Fourbrick on 17/11/16.
//    Copyright © 2017 Fourbrick. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoadingManager : NSObject

+ (instancetype)sharedLoadingManager;
+ (void)showLoadingView:(UIView *)view;
+ (void)hideLoadingView:(UIView *)view;
+ (void)showBlurLoadingView;

-(void)startAnimation:(UIView *)view;
-(void)stopAnimation:(UIView *)view;
@end
