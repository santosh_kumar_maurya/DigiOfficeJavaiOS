//
//  APIService.m
//  iWork
//
//  Created by Shailendra on 20/07/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "APIService.h"
#import "Header.h"
@implementation APIService

-(NSDictionary*)WebApi: (NSDictionary*)_params Url:(NSString*)Url {
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSLog(@"_params===>%@",_params);
    NSLog(@"Url == %@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL,Url]]);
    NSLog(@"Token====%@",[ApplicationState GetToken]);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL,Url]]];
    
    [request setHTTPMethod:@"POST"];
    if(_params != nil){
        NSError *requestError = NULL;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_params
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&requestError];
        [request setHTTPBody:jsonData];
        [request setValue:[ApplicationState GetToken] forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    }
    if([Url containsString:@"updateStatusOfRequest"]||[Url containsString:@"userProfileAuth"]){
        [request setValue:[ApplicationState GetToken] forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }
    else if([Url containsString:@"appLaunch"]){
       [request addValue:@"ios" forHTTPHeaderField: @"platform"];
       [request addValue:[delegate GetVersionNumber] forHTTPHeaderField: @"appversion"];
    }
    else{
        [request setValue:[ApplicationState GetToken] forHTTPHeaderField:@"Authorization"];

    }
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSData *responseData1=[responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *jsonParsingError = nil;
    NSDictionary *reqdataDict = [NSJSONSerialization JSONObjectWithData:responseData1
                                                                options:0 error:&jsonParsingError];
    NSLog(@"reqdataDict===>%@",reqdataDict);
    return reqdataDict;
    ;
}



@end
