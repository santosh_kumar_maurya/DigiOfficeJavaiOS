//
//  ParentDashbordViewController.m
//  iWork
//
//  Created by Shailendra on 16/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "ParentDashbordViewController.h"
#import "Header.h"

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define kDefaultEdgeInsets UIEdgeInsetsMake(5, 10, 5, 10)

@interface ParentDashbordViewController ()<UIScrollViewDelegate>{
    NSArray *btnArray;
    AppDelegate *delegate;
    IBOutlet UIImageView *ProfileImageView;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UILabel *UserNameLabel;
    IBOutlet UILabel *NatificationLabel;
    NSString *isGetNotification;
    

}
@property (weak) IBOutlet UIScrollView *containerScrollView;
@property (weak) IBOutlet UIScrollView *menuScrollView;
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray;
-(void) buttonPressed:(id) sender;
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets;
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr;
@end

@implementation ParentDashbordViewController
@synthesize isComeFrom,NotiDic;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    self.menuScrollView.backgroundColor = [UIColor clearColor];

    if([isComeFrom isEqualToString:@"NOTIFICATION"]){
        isGetNotification = @"YES";
    }
    UserNameLabel.text = @"";
    NatificationLabel.layer.cornerRadius = 9.0;
    NatificationLabel.clipsToBounds = YES;
    ProfileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    ProfileImageView.layer.borderWidth = 2;
    ProfileImageView.layer.cornerRadius = 27;
    ProfileImageView.clipsToBounds = YES;
    
    
    NSString *MyIWorkStr = NSLocalizedString(@"MY_IWORK", nil);
    //MyIWorkStr = [[MyIWorkStr stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]] uppercaseString];
    
    NSString *HeaderStr = NSLocalizedString(@"IWORK", nil);
    //HeaderStr = [HeaderStr stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    HeaderLabel.text = HeaderStr;
    
    
    btnArray = [[NSArray alloc]initWithObjects:@"My iWork",@"My Team", nil];
    [self addButtonsInScrollMenu:btnArray];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyiWorkViewController *oneVC = [storyBoard instantiateViewControllerWithIdentifier:@"MyiWorkViewController"];
    if([isComeFrom isEqualToString:@"NOTIFICATION"]){
        oneVC.isComeFrom = @"NOTIFICATION";
        oneVC.NotificationDic = NotiDic;
    }
    MyTeamViewController *twoVC = [storyBoard instantiateViewControllerWithIdentifier:@"MyTeamViewController"];
    NSArray *controllerArray = @[oneVC, twoVC];
    [self addChildViewControllersOntoContainer:controllerArray];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateNotifications) name:@"Notification" object:nil];
    // [self WebApiCell];
    
//   NSDictionary *Dic = [ApplicationState getUserLoginData];
//    
//    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]))){
//        UserNameLabel.text = [NSString stringWithFormat:@"Hi, %@", [[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]];
//    }
//    else{
//        UserNameLabel.text = @" ";
//    }
//    
//    NSString *URL = [NSString stringWithFormat:@"%@images/%@",BASE_URL,[[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]];
//    URL = [URL stringByReplacingOccurrencesOfString:@"iworkapi" withString:@"iwork"];
//    
//    NSLog(@"URL==%@",URL);
//    
//    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]))){
//        NSURL *Url = [NSURL URLWithString:URL];
//        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL:Url]];
//        if(images == nil){
//            NSLog(@"image nil");
//            ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
//        }
//        else{
//            
//            [ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
//        }
//    }
//    else{
//        ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
//    }
    
   
}
#pragma mark - Add Menu Buttons in Menu Scroll View
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray
{
//    self.menuScrollView.frame = CGRectMake(0.0f, self.view.frame.size.height - 40, SCREEN_WIDTH, 40);
    _menuScrollView.backgroundColor = [UIColor redColor];
    _menuScrollView.alwaysBounceVertical = NO;
    _menuScrollView.alwaysBounceHorizontal = YES;
    CGFloat buttonHeight = self.menuScrollView.frame.size.height;

    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.frame = CGRectMake(0.0f, 0.0f - buttonHeight/2 , SCREEN_WIDTH/2, buttonHeight);
    
    [button1 setTitle:[buttonArray objectAtIndex:0] forState:UIControlStateNormal];
    button1.titleLabel.font = delegate.contentFont;
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [button1 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button1 setBackgroundColor:[UIColor colorWithRed:(50.0/255.0) green:(63.0/255.0) blue:(72.0/255.0) alpha:1.0f]];
    button1.tag = 0;
    button1.selected = YES;
    [self.menuScrollView addSubview:button1];
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.frame = CGRectMake(SCREEN_WIDTH/2, 0.0f - buttonHeight/2, SCREEN_WIDTH/2, buttonHeight);
    [button2 setTitle:[buttonArray objectAtIndex:1] forState:UIControlStateNormal];
    button2.titleLabel.font = delegate.contentFont;
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [button2 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundColor:[UIColor colorWithRed:(224.0/255.0) green:(224.0/255.0) blue:(224.0/255.0) alpha:1.0f]];
    button2.tag = 1;
    [self.menuScrollView addSubview:button2];
    _menuScrollView.scrollEnabled = NO;

    //self.menuScrollView.contentSize = CGSizeMake(self.menuScrollView.frame.size.width, self.menuScrollView.frame.size.height);
}


/**
 *  Any Of the Top Menu Button Press Action
 *
 *  @param sender id of the button pressed
 */

#pragma mark - Menu Button press action
-(void) buttonPressed:(id) sender
{
    UIButton *senderbtn = (UIButton *) sender;
    float buttonWidth = 0.0f;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        UIButton *btn = (UIButton *) subView;
//        UIView *bottomView = [btn viewWithTag:1001];
        
        if (btn.tag == senderbtn.tag)
        {
            [btn setBackgroundColor:[UIColor colorWithRed:(50.0/255.0) green:(63.0/255.0) blue:(72.0/255.0) alpha:1.0f]];
            btn.selected = YES;
//            [bottomView setHidden:NO];
        }
        else
        {
            [btn setBackgroundColor:[UIColor colorWithRed:(224.0/255.0) green:(224.0/255.0) blue:(224.0/255.0) alpha:1.0f]];
            btn.selected = NO;
            
//            [bottomView setHidden:YES];
        }
    }
    
    [self.containerScrollView setContentOffset:CGPointMake(SCREEN_WIDTH * senderbtn.tag, 0) animated:YES];
    
    float xx = SCREEN_WIDTH * (senderbtn.tag) * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}

/**
 *  Calculating width of button added on top menu
 *
 *  @param title            Title of the Button
 *  @param buttonEdgeInsets Edge Insets for the title
 *
 *  @return Width of button
 */

#pragma mark - Calculate width of menu button
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets
{
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:10.0f]};
    
    CGSize size = [title sizeWithAttributes:attributes];
    return CGSizeMake(size.width + buttonEdgeInsets.left + buttonEdgeInsets.right, size.height + buttonEdgeInsets.top + buttonEdgeInsets.bottom).width;
}


/**
 *  Adding all related controllers in to the container
 *
 *  @param controllersArr Array containing objects of all controllers
 */
#pragma mark - Adding all related controllers in to the container
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr
{
    for (int i = 0 ; i < controllersArr.count; i++)
    {
        UIViewController *vc = (UIViewController *)[controllersArr objectAtIndex:i];
        CGRect frame = CGRectMake(0, 0, self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height);
        frame.origin.x = SCREEN_WIDTH * i;
        vc.view.frame = frame;
        
        [self addChildViewController:vc];
        [self.containerScrollView addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    
    self.containerScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * controllersArr.count + 1, self.containerScrollView.frame.size.height);
    self.containerScrollView.pagingEnabled = YES;
    self.containerScrollView.delegate = self;
}


#pragma mark - Scroll view delegate methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = (scrollView.contentOffset.x / SCREEN_WIDTH);
    
    UIButton *btn;
    float buttonWidth = 0.0;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        
        if (btn.tag == page)
        {
            [btn setBackgroundColor:[UIColor colorWithRed:(50.0/255.0) green:(63.0/255.0) blue:(72.0/255.0) alpha:1.0f]];
            btn.selected = YES;
            buttonWidth = btn.frame.size.width;
            [bottomView setHidden:NO];
        }
        else
        {
            [btn setBackgroundColor:[UIColor colorWithRed:(224.0/255.0) green:(224.0/255.0) blue:(224.0/255.0) alpha:1.0f]];
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    
    float xx = scrollView.contentOffset.x * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction:(id)sender{
    if([isGetNotification isEqualToString:@"YES"]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AppContainerViewController *newView = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else{
      [[self navigationController] popViewControllerAnimated:YES];
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self UpdateNotifications];
}
- (IBAction)notificationTapped:(id)sender {
    [delegate DeleteNotificationCounter:@"IWORK"];
    NSMutableDictionary *Dics = [[NSMutableDictionary alloc] init];
    [Dics setValue:@"IWORK" forKey:@"IWORK"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil userInfo:Dics];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     NotificationViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
     [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)UpdateNotifications{
    NatificationLabel.alpha=0.0;
    NatificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IWORK"]];
    if ([NatificationLabel.text intValue]>0) {
        NatificationLabel.alpha=1.0;
        NatificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IWORK"]];
    }
}
-(IBAction)GotoProfilePage:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    ObjViewController.isComeFrom = @"IWORK";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}

@end
