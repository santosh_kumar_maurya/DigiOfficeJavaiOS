//
//  UIColor+AppColor.h
//  MangoApp
//
//  Created by Fourbrick on 17/11/16.
//    Copyright © 2017 Fourbrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (AppColor)

+(UIColor *)ColorFromRGB:(NSString *)sharpRGB;
+ (instancetype)appsThemeColor;
+ (UIColor *)colorWithHex:(NSString *)string;
+ (instancetype)appsYellowColor;
@end
