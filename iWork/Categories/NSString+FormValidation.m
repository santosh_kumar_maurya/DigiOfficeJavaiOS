//
//  NSString+FormValidation.m
//  Alive
//
//  Created by Sanjay on 25/08/14.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import "NSString+FormValidation.h"

@implementation NSString (FormValidation)

- (BOOL)isValidEmail {
   
    NSString *emailRegEx =
    @"(?:[A-Za-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[A-Za-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[A-Za-z0-9](?:[a-"
    @"z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[A-Za-z0-9-]*[A-Za-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    return [emailPredicate evaluateWithObject:self];
}

- (BOOL)isValidPassword {
    return (self.length >= 6);
}

- (BOOL)isValidName {
    
   NSString* string =  [self stringByTrimmingCharactersInSet:
           [NSCharacterSet whitespaceCharacterSet]];
   return ([string length ]>= 4);
}


-(BOOL) isValidMobileNumber
{
    BOOL retVal=YES;
    if ( [self length] == 0 || [self integerValue]==0 ) {
        retVal=NO;
        return retVal;
    }
    
    if ([self length] !=10 ) {
        
        retVal=NO;
        return retVal;
    }
    
    
    NSString* longStr = @"7000000000";
    NSString* longStr2 = @"9999999999";
    
    long long min = [longStr longLongValue] ;
    long long max = [longStr2 longLongValue] ;
    long long longValue = [self longLongValue];
    
    if(longValue <= min || longValue >=max ){
        
        retVal=NO;
        return retVal;
    }
    
    NSString *regEx = @"[0-9]{10}";
    NSRange r = [self rangeOfString:regEx options:NSRegularExpressionSearch];
    
    if (r.location == NSNotFound) {
        
        retVal=NO;
        return retVal;
    }
    
    return retVal;
    
}

-(BOOL) isValidPIN {
    
    BOOL retVal=YES;
    if ([self length] !=6 ) {
        retVal=NO;
    }
   return retVal;
}

@end
