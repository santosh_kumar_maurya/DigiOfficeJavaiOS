//
//  UITextField+textFiled.h
//  Created by Fourbrick on 25/05/15.
//

#import <UIKit/UIKit.h>

@interface UITextField (textFiled)

- (void)setPlaceholder:(NSString *)placeholder withColor:(UIColor *)color;
@end
