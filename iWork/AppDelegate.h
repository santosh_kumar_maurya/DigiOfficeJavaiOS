//
//  AppDelegate.h
//  iWork
//
//  Created by Fourbrick on 04/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
// New one commited 2

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "SSSnackbar.h"
#import <CoreData/CoreData.h>

//Test

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    int devicewidth,deviceheight,headerheight,barheight,margin,count,tabheight;
    UINavigationController *nav;
    BOOL alertflag,iscorner;
    NSMutableDictionary *dataDict,*dataFull;
    UIFont *boldFont,*normalFont,*normalFont1,*smallFont,*mediumFont,*normalBold,*buttonFont,*smallFont2,*largeFont,*headFont,*contentFont,*contentBigFont,*contentSmallFont,*contentSmallFont1;
    NSString *imsi, *areastr;
    UIColor *bgCollor,*btnbgCollor,*btnColour,*headerbgColler,*borderColor,*blackcolor,*btnbgColur,*redColor,*dimColor,*dimBlack, *yellowColor,*confColor,*susColor,*inprogColor,*pendingColor,*failedColor,*cancelColor,*rejectedColor,*discardedColor,*assignColor;
}

@property (strong, nonatomic) NSMutableArray *userRequestsArray;
@property (strong, nonatomic) NSString *userLat;
@property (strong, nonatomic) NSString *userLong;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *nav;
@property (nonatomic) int devicewidth,deviceheight,headerheight,barheight,margin,count,tabheight;
@property (nonatomic) BOOL alertflag,iscorner;
@property (strong, nonatomic) NSString *imsi,*areastr;
@property (strong, nonatomic) NSMutableDictionary *dataDict,*dataFull;
@property (strong, nonatomic) UIFont *boldFont,*normalFont,*normalFont1,*smallFont,*mediumFont,*normalBold,*buttonFont,*smallFont2,*largeFont,*headFont,*contentFont,*contentBigFont,*contentSmallFont,*contentSmallFont1,*ooredoo;
@property (strong, nonatomic) UIColor*bgColour,*btnbgCollor,*btnCollor,*headerbgColler,*borderColor,*blackcolor,*redColor,*dimColor,*dimBlack,*yellowColor,*confColor,*susColor,*inprogColor,*pendingColor,*failedColor,*cancelColor,*submittedColor,*rejectedColor,*discardedColor,*assignColor,*viewtask,*ProgressColor,*BackgroudColor;


-(bool)isInternetConnected;
-(void)showAlert:(NSString*)alertMessage;
-(NSString*)SSOAuth:(NSMutableDictionary*) _params;
-(NSString*)StackHolder:(NSMutableDictionary*) _params;
-(NSString*)postRequestForLogin: (NSMutableDictionary*) _params;
-(NSString*)postRequestForNewTask: (NSMutableDictionary*) _params;
-(NSString*)postRequestForMyTask: (NSMutableDictionary*) _params;
-(NSString*)postRequestForTaskDetails: (NSMutableDictionary*) _params;
-(NSString*)postRequestForSetStatus: (NSMutableDictionary*) _params;
-(NSString*)postRequestForFollowOn: (NSMutableDictionary*) _params;
-(NSString*)postRequestForGetConversation: (NSMutableDictionary*) _params;
-(NSString*)postRequestForFeedback: (NSMutableDictionary*) _params;
-(NSString*)postRequestForDashboard: (NSMutableDictionary*) _params;
-(NSString*)postRequestForMyRequest: (NSMutableDictionary*) _params;
-(NSString*)postRequestForGetNotifications: (NSMutableDictionary*) _params;
-(NSString*)postRequestForEmployeeDetails: (NSMutableDictionary*) _params;
-(NSString*)postRequestForMyReporties: (NSMutableDictionary*) _params;
-(NSString*)postRequestForDashboardDetails: (NSMutableDictionary*) _params;
-(NSString*)postRequestForFilter: (NSMutableDictionary*) _params;
- (void)registerApplicationForPushNotifications;
- (SSSnackbar *)snackbarForQuickRunningItem:(NSString *)msg;



+(AppDelegate *)appDelegate;
- (void)signedIn;
- (void)signedOut;
-(void)ContainerView;
-(void)PMSViewController;
@property (readonly, strong) NSPersistentContainer *persistentContainer;
- (void)saveContext;
-(void)DeleteNotificationCounter:(NSString*)AppType;
-(int)FetchNotificationCounter:(NSString*)AppType;
-(NSString*)GetVersionNumber;

@end

