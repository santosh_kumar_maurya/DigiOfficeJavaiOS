//
//  BIZPopupViewControllerDelegate.h
#import <Foundation/Foundation.h>


@protocol BIZPopupViewControllerDelegate <NSObject>
@optional
- (void)popupViewControllerWillDismiss;
- (void)popupViewControllerDidDismiss;
@end
