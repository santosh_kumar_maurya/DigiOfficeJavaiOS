//
//  FilterViewController.m
//  iWork
//
//  Created by Shailendra on 25/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "FilterViewController.h"
#import "Header.h"

@interface FilterViewController (){
    
    NSString *req_id;
    NSString *request_status;
    NSString *attendance_status;
    NSString *request_date;
    NSString *iwork_date;
    NSString *location_list_array;
    NSString *check_in_time;
    NSString *check_out_time;
    NSString *action_date;
    NSString *empl_id;
    NSString *empl_name;
    NSString *Selection;
    NSString *DateSelection;
    NSMutableDictionary *UserDic;

    NSMutableArray *LocationIDArray;
    NSMutableArray *LocationNameArray;
    NSMutableArray *FilterArray;
    NSMutableArray *arrayForBool;
    NSMutableArray *SelectedIndex;
    NSMutableArray *SelectedArray;
    NSMutableArray *SelectedLocationIDArray;
    NSMutableArray *RequestStatusArray;
    NSMutableArray *AttendanceStatusArray;
    
    IBOutlet UITableView *FilterTableView;
    IBOutlet UITableView *SelectTableView;
    IBOutlet UIView *SelectView;
    IBOutlet UIView *DatePikerViewBg;
    IBOutlet UIDatePicker *DatePicker;
    IBOutlet UILabel *HeaderLabel;
    
    NSMutableArray *PlusImageArray;
    NSMutableArray *MinusImageArray;
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *CancelFilterBtn;
    IBOutlet UIButton *ApplyFilterBtn;
    AppDelegate *delegate;
    APIService *Api;
    NSDictionary *ResponseDic;
    int offset;
    int limit;
    NSUserDefaults *userDefault;
    BOOL isClearSelected;
    BOOL clearFilterForEmployee;
}
@end

@implementation FilterViewController
@synthesize isComefrom;


- (void)viewDidLoad {
    [super viewDidLoad];
    isClearSelected = NO;
    userDefault = [NSUserDefaults standardUserDefaults];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
    offset = 0;
    limit = 10;
    Api = [[APIService alloc] init];
    [self ClearData];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    SelectedLocationIDArray = [[NSMutableArray alloc] init];
    DatePikerViewBg.backgroundColor = [UIColor whiteColor];
    DatePicker.backgroundColor = [UIColor clearColor];
   
    PlusImageArray = [[NSMutableArray alloc] initWithObjects:@"Plus",@"Plus",@"Plus",@"Plus",@"Plus",@"Plus", nil];
    MinusImageArray = [[NSMutableArray alloc] initWithObjects:@"Minus",@"Minus",@"Minus",@"Minus",@"Minus",@"Minus", nil];
    
    RequestStatusArray = [[NSMutableArray alloc] initWithObjects:NSLocalizedString(@"SELECT_STATUS", nil),NSLocalizedString(@"APPROVED", nil),NSLocalizedString(@"CANCELLED", nil),NSLocalizedString(@"MISSED_BTN", nil),NSLocalizedString(@"NEW_BTN", nil),NSLocalizedString(@"REJECTED", nil), nil];
    
    AttendanceStatusArray = [[NSMutableArray alloc] initWithObjects:NSLocalizedString(@"SELECT_ATTENDENCE", nil),NSLocalizedString(@"ABSENT", nil),NSLocalizedString(@"PRESENT", nil), nil];

    NSString *iWorkLocation = NSLocalizedString(@"IWORK_LOCATION", nil);
   // iWorkLocation = [iWorkLocation stringByReplacingOccurrencesOfString:@"iWork" withString:[@"&#9432;Work" stringByConvertingHTMLToPlainText]];
    
    NSString *iWorkDate = NSLocalizedString(@"IWORK_DATE", nil);
    //iWorkDate = [iWorkDate stringByReplacingOccurrencesOfString:@"iWork" withString:[@"&#9432;Work" stringByConvertingHTMLToPlainText]];
    
    if([isComefrom isEqualToString:@"Rejected"]||[isComefrom isEqualToString:@"New"]||[isComefrom isEqualToString:@"Missed"]){
       FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"REQUEST_ID", nil),iWorkLocation,NSLocalizedString(@"REQUEST_DATE", nil),iWorkDate, nil];
    }
    else if ([isComefrom isEqualToString:@"Approved"]){
       FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"REQUEST_ID", nil),iWorkLocation,NSLocalizedString(@"ATTENDENCE_STATUS", nil),NSLocalizedString(@"REQUEST_DATE", nil),iWorkDate, nil];
    }
    else if ([isComefrom isEqualToString:@"LineManagerApprove"]){
        FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"EMPLOYEE_ID", nil),NSLocalizedString(@"EMPLOYEE_NAME", nil),NSLocalizedString(@"REQUEST_ID", nil),iWorkLocation,NSLocalizedString(@"REQUEST_DATE", nil),iWorkDate, nil];
    }
    else{
        FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"REQUEST_ID", nil),iWorkLocation,NSLocalizedString(@"REQUEST_STATUS", nil),NSLocalizedString(@"ATTENDENCE_STATUS", nil),NSLocalizedString(@"REQUEST_DATE", nil),iWorkDate, nil];
    }

    CancelBtn.layer.borderWidth = 1.0;
    CancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    CancelBtn.layer.cornerRadius = 3.0;
    CancelBtn.clipsToBounds = YES;
    
    CancelFilterBtn.layer.borderWidth = 1.0;
    CancelFilterBtn.layer.borderColor = delegate.redColor.CGColor;
    CancelFilterBtn.layer.cornerRadius = 3.0;
    CancelFilterBtn.clipsToBounds = YES;
   
    ApplyFilterBtn.layer.cornerRadius = 3.0;
    arrayForBool=[[NSMutableArray alloc]init];
    for (int i=0; i<[FilterArray count]; i++) {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    
    UINib *SearchNib = [UINib nibWithNibName:@"SearchCell" bundle:nil];
    [FilterTableView registerNib:SearchNib forCellReuseIdentifier:@"CELL"];
    
    UINib *SelectionNib = [UINib nibWithNibName:@"SelectionCell" bundle:nil];
    [FilterTableView registerNib:SelectionNib forCellReuseIdentifier:@"CELL1"];
    
    UINib *DateSelectionNib = [UINib nibWithNibName:@"DateSelectionCell" bundle:nil];
    [FilterTableView registerNib:DateSelectionNib forCellReuseIdentifier:@"CELL2"];
    
    UINib *CheckboxNib = [UINib nibWithNibName:@"CheckboxCell" bundle:nil];
    [FilterTableView registerNib:CheckboxNib forCellReuseIdentifier:@"CELL3"];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(LocationApi) withObject:nil afterDelay:0.5];
    
}

-(void)getLocationArray:(NSMutableDictionary *)dict
{
    if ([dict valueForKey:@"locationId"] == nil)
    {
    }
    else
    {
        NSArray *locationIdArr = [dict valueForKey:@"locationId"];
        NSMutableArray *locationId = [[NSMutableArray alloc] init];
        for (int i = 0; i< LocationIDArray.count; i++)
        {
            for (int j = 0; j< locationIdArr.count; j++)
            {
                if (LocationIDArray[i] == [NSNumber numberWithInteger:[locationIdArr[j] intValue]])
                {
                    [locationId insertObject:locationIdArr[j] atIndex:i];
                    [SelectedIndex addObject:[NSString stringWithFormat:@"%d",i]];
                    break;
                }
                else
                {
                    [locationId insertObject:@"" atIndex:i];
                }
            }
        }
        SelectedLocationIDArray = [locationIdArr mutableCopy];
    }
}


#pragma mark -
#pragma mark TableView DataSource and Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(tableView == SelectTableView){
        return 1;
    }
    return [FilterArray count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == SelectTableView){
        if([Selection isEqualToString:@"RequestStatus"]){
            return [RequestStatusArray count];
        }
        else if([Selection isEqualToString:@"AttendanseStatus"]){
             return [AttendanceStatusArray count];
        }
    }
    else if(tableView == FilterTableView){
        if ([isComefrom isEqualToString:@"LineManagerApprove"]){
            if(section == 3){
                return [LocationIDArray count];
            }
            else{
                return  1;
            }
        }
        else{
            if(section == 1){
                return [LocationIDArray count];
            }
            else{
                return  1;
            }
        }
    }
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@",self.filterData);
    if(tableView == SelectTableView){
        if([Selection isEqualToString:@"RequestStatus"]){
           StatusSelectionCell * Cell = (StatusSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"StatusSelectionCell" forIndexPath:indexPath];
            Cell.SelectionLabel.text = [RequestStatusArray objectAtIndex:indexPath.row];
            return Cell;
        }
        else {
            StatusSelectionCell * Cell = (StatusSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"StatusSelectionCell" forIndexPath:indexPath];
            Cell.SelectionLabel.text = [AttendanceStatusArray objectAtIndex:indexPath.row];
            return Cell;
        }
    }
    else{
        if ([isComefrom isEqualToString:@"Approved"])
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            dict = [userDefault objectForKey:isComefrom];
            NSLog(@"%@",dict);
            if(indexPath.section == 0 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                [Cell.SearchBtn addTarget:self action:@selector(SearchAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestId"] isEqualToString:@""] || ([dict valueForKey:@"requestId"] == nil))
                    {
                        Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                    }
                    else
                    {
                        Cell.SearchTextField.text = [dict valueForKey:@"requestId"];
                        req_id = Cell.SearchTextField.text;
                    }
                }
                else
                {
                    Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                }
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                return Cell;
            }
            else if(indexPath.section == 1){
                CheckboxCell * Cell = (CheckboxCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
                Cell.CheckBoxBtn.tag = indexPath.row;
                Cell.BlanckBtn.tag = indexPath.row;
                [Cell.CheckBoxBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.BlanckBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                if(LocationNameArray.count>0)
                {
                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
                }
                
                
                if ([dict valueForKey:@"locationId"] == nil)
                {
                    if(LocationNameArray.count>0)
                    {
                        if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                    }
                }
                return Cell;
            }
            else if(indexPath.section == 2 ){
                SelectionCell * Cell = (SelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(AttendanceStatusSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(SelectAttendanceAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"attendanceStatus"] isEqualToString:@""] || ([dict valueForKey:@"attendanceStatus"] == nil))
                    {
                        if ([attendance_status isEqualToString:@""])
                        {
                            attendance_status = @"";
                            Cell.SelectionLabel.text = @"Select Attendance";
                        }
                        else
                        {
                            Cell.SelectionLabel.text = attendance_status;
                        }
                    }
                    else
                    {
                        if ([attendance_status isEqualToString:@""])
                        {
                            Cell.SelectionLabel.text = [dict valueForKey:@"attendanceStatus"];
                            attendance_status = Cell.SelectionLabel.text;
                        }
                        else
                        {
                            Cell.SelectionLabel.text = attendance_status;
                        }
                    }
                }
                else
                {
                    if([attendance_status isEqualToString:@""])
                    {
                        Cell.SelectionLabel.text = @"Select Attendance";
                    }
                    else
                    {
                        Cell.SelectionLabel.text = attendance_status;
                    }
                }
                return Cell;
            }
            else if(indexPath.section == 3 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestDate"] isEqualToString:@""] || ([dict valueForKey:@"requestDate"] == nil))
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            request_date = @"";
                            Cell.DateSelectionLabel.text = @"Request Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                    else
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"requestDate"]];
                            request_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                }
                else
                {
                    if([request_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Request Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = request_date;
                    }
                }
//                if([request_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Request Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = request_date;
//                }
                return Cell;
            }
            else {
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(iWorkDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(iWorkDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"workDate"] isEqualToString:@""] || ([dict valueForKey:@"workDate"] == nil))
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            iwork_date = @"";
                            Cell.DateSelectionLabel.text = @"iWork Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                    else
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"workDate"]];
                            iwork_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                }
                else
                {
                    if([iwork_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"iWork Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = iwork_date;
                    }
                }
//                if([iwork_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"iWork Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = iwork_date;
//                }
                return Cell;
            }
        }
        else if([isComefrom isEqualToString:@"Rejected"]||[isComefrom isEqualToString:@"New"]||[isComefrom isEqualToString:@"Missed"])
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            dict = [userDefault objectForKey:isComefrom];
            NSLog(@"%@",dict);
            if(indexPath.section == 0 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                [Cell.SearchBtn addTarget:self action:@selector(SearchAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestId"] isEqualToString:@""] || ([dict valueForKey:@"requestId"] == nil))
                    {
                        Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                    }
                    else
                    {
                        Cell.SearchTextField.text = [dict valueForKey:@"requestId"];
                        req_id = Cell.SearchTextField.text;
                    }
                }
                else
                {
                    Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                }

//                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                return Cell;
            }
            else if(indexPath.section == 1){
                CheckboxCell * Cell = (CheckboxCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
                Cell.CheckBoxBtn.tag = indexPath.row;
                Cell.BlanckBtn.tag = indexPath.row;
                [Cell.CheckBoxBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.BlanckBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                if(LocationNameArray.count>0)
                {
                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
                }
                
                
                if ([dict valueForKey:@"locationId"] == nil)
                {
                    if(LocationNameArray.count>0)
                    {
                        if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                    }
                }

//                if(LocationNameArray.count>0){
//                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
//                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]]){
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
//                    }
//                    else{
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
//                    }
//                }
                return Cell;
            }
           else if(indexPath.section == 2 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestDateAction) forControlEvents:UIControlEventTouchUpInside];
               if (dict != nil)
               {
                   if ([[dict valueForKey:@"requestDate"] isEqualToString:@""] || ([dict valueForKey:@"requestDate"] == nil))
                   {
                       if ([request_date isEqualToString:@""])
                       {
                           request_date = @"";
                           Cell.DateSelectionLabel.text = @"Request Date";
                       }
                       else
                       {
                           Cell.DateSelectionLabel.text = request_date;
                       }
                   }
                   else
                   {
                       if ([request_date isEqualToString:@""])
                       {
                           Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"requestDate"]];
                           request_date = Cell.DateSelectionLabel.text;
                       }
                       else
                       {
                           Cell.DateSelectionLabel.text = request_date;
                       }
                   }
               }
               else
               {
                   if([request_date isEqualToString:@""])
                   {
                       Cell.DateSelectionLabel.text = @"Request Date";
                   }
                   else
                   {
                       Cell.DateSelectionLabel.text = request_date;
                   }
               }
                return Cell;
            }
            else {
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(iWorkDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(iWorkDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"workDate"] isEqualToString:@""] || ([dict valueForKey:@"workDate"] == nil))
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            iwork_date = @"";
                            Cell.DateSelectionLabel.text = @"iWork Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                    else
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"workDate"]];
                            iwork_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                }
                else
                {
                    if([iwork_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"iWork Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = iwork_date;
                    }
                }
                return Cell;
            }
        }
        else if ([isComefrom isEqualToString:@"LineManagerApprove"])
        {
            if(indexPath.section == 0 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                if (self.filterData != nil)
                {
                    if([[self.filterData valueForKey:@"empId"] isEqualToString:@""] || ([self.filterData valueForKey:@"empId"] == nil))
                    {
                        Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                    }
                    else
                    {
                        Cell.SearchTextField.text = [self.filterData valueForKey:@"empId"];
                        req_id = Cell.SearchTextField.text;
                    }
                }
                else
                {
                    Cell.SearchTextField.text = @"";
                    Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                }
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
                return Cell;
            }
            else if(indexPath.section == 1 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                
                if (self.filterData != nil)
                {
                    if([[self.filterData valueForKey:@"empName"] isEqualToString:@""] || ([self.filterData valueForKey:@"empName"] == nil))
                    {
                        Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                    }
                    else
                    {
                        Cell.SearchTextField.text = [self.filterData valueForKey:@"empName"];
                    }
                }
                else
                {
                    Cell.SearchTextField.text = @"";
                    Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                }
                
                
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeNameSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
                return Cell;
            }
           else if(indexPath.section == 2 )
           {
               SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
               
               if (self.filterData != nil)
               {
                   if([[self.filterData valueForKey:@"requestId"] isEqualToString:@""] || ([self.filterData valueForKey:@"requestId"] == nil))
                   {
                       Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                   }
                   else
                   {
                       Cell.SearchTextField.text = [self.filterData valueForKey:@"requestId"];
                   }
               }
               else
               {
                   Cell.SearchTextField.text = @"";
                   Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
               }
               
              
               [Cell.SearchBtn addTarget:self action:@selector(SearchAction) forControlEvents:UIControlEventTouchUpInside];
               return Cell;
            }
            else if(indexPath.section == 3)
            {
                CheckboxCell * Cell = (CheckboxCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
                Cell.CheckBoxBtn.tag = indexPath.row;
                Cell.BlanckBtn.tag = indexPath.row;
                [Cell.CheckBoxBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.BlanckBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                if(LocationNameArray.count>0)
                {
                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
                }
          
                if ([self.filterData valueForKey:@"locationId"] == nil)
                {
                    if(LocationNameArray.count>0)
                    {
                        if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]]){
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                        }
                        else{
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]]){
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                    }
                    else{
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                    }
                }
                return Cell;
            }
            else if(indexPath.section == 4 )
            {
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestDateAction) forControlEvents:UIControlEventTouchUpInside];
                
                if (self.filterData != nil)
                {
                    if([[self.filterData valueForKey:@"requestDate"] isEqualToString:@""] || ([self.filterData valueForKey:@"requestDate"] == nil))
                    {
                        if([request_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = @"Request Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                    else
                    {
                        if([request_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY:[self.filterData valueForKey:@"requestDate"]];
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                }
                else
                {
                    Cell.DateSelectionLabel.text = @"Request Date";
                }
                
                return Cell;
            }
            else {
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(iWorkDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(iWorkDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (self.filterData != nil)
                {
                    if([[self.filterData valueForKey:@"workDate"] isEqualToString:@""] || ([self.filterData valueForKey:@"workDate"] == nil))
                    {
                        if([iwork_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = @"iWork Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                    else
                    {
                        if([iwork_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY:[self.filterData valueForKey:@"workDate"]];
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                }
                else
                {
                    Cell.DateSelectionLabel.text = @"iWork Date";
                }
                return Cell;
            }
        }
        else{
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            dict = [userDefault objectForKey:isComefrom];
            NSLog(@"%@",dict);
            if(indexPath.section == 0 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestId"] isEqualToString:@""] || ([dict valueForKey:@"requestId"] == nil))
                    {
                        Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                    }
                    else
                    {
                        Cell.SearchTextField.text = [dict valueForKey:@"requestId"];
                        req_id = Cell.SearchTextField.text;
                    }
                }
                else
                {
                    Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                }
                
                [Cell.SearchBtn addTarget:self action:@selector(SearchAction) forControlEvents:UIControlEventTouchUpInside];
                return Cell;
            }
            else if(indexPath.section == 1){
                CheckboxCell * Cell = (CheckboxCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
                Cell.CheckBoxBtn.tag = indexPath.row;
                Cell.BlanckBtn.tag = indexPath.row;
                [Cell.CheckBoxBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.BlanckBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                
                if(LocationNameArray.count>0)
                {
                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
                }
                
                
                if ([dict valueForKey:@"locationId"] == nil)
                {
                    if(LocationNameArray.count>0)
                    {
                        if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                    }
                }
                return Cell;
            }
            else if(indexPath.section == 2 )
            {
                SelectionCell * Cell = (SelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestStatusSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(SelectRequestAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"status"] isEqualToString:@""] || [[dict valueForKey:@"status"] isEqualToString:@"10"] || ([dict valueForKey:@"status"] == nil))
                    {
                        if ([[dict valueForKey:@"status"] isEqualToString:@"10"] && [request_status isEqualToString:@""])
                        {
                            request_status = @"";
                            Cell.SelectionLabel.text = @"Select Status";
                        }
                        else
                        {
                            Cell.SelectionLabel.text = request_status;
                        }
                    }
                    else
                    {
                        if ([request_status isEqualToString:@""])
                        {
                            Cell.SelectionLabel.text = [self getRequestStatusFromRequestId:[dict valueForKey:@"status"]];
                            request_status = Cell.SelectionLabel.text;
                        }
                        else
                        {
                            Cell.SelectionLabel.text = request_status;
                        }
                    }
                }
                else
                {
                    if([request_status isEqualToString:@""])
                    {
                        Cell.SelectionLabel.text = @"Select Status";
                    }
                    else
                    {
                        Cell.SelectionLabel.text = request_status;
                    }
                }
                return Cell;
            }
            else if(indexPath.section == 3 )
            {
                SelectionCell * Cell = (SelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(AttendanceStatusSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(SelectAttendanceAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"attendanceStatus"] isEqualToString:@""] || ([dict valueForKey:@"attendanceStatus"] == nil))
                    {
                        if ([attendance_status isEqualToString:@""])
                        {
                            attendance_status = @"";
                            Cell.SelectionLabel.text = @"Select Attendance";
                        }
                        else
                        {
                            Cell.SelectionLabel.text = attendance_status;
                        }
                    }
                    else
                    {
                        if ([attendance_status isEqualToString:@""])
                        {
                            Cell.SelectionLabel.text = [dict valueForKey:@"attendanceStatus"];
                            attendance_status = Cell.SelectionLabel.text;
                        }
                        else
                        {
                            Cell.SelectionLabel.text = attendance_status;
                        }
                    }
                }
                else
                {
                    if([attendance_status isEqualToString:@""])
                    {
                        Cell.SelectionLabel.text = @"Select Attendance";
                    }
                    else
                    {
                        Cell.SelectionLabel.text = attendance_status;
                    }
                }
                return Cell;
            }
            else if(indexPath.section == 4 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestDateAction) forControlEvents:UIControlEventTouchUpInside];
                
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestDate"] isEqualToString:@""] || ([dict valueForKey:@"requestDate"] == nil))
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            request_date = @"";
                            Cell.DateSelectionLabel.text = @"Request Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                    else
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"requestDate"]];
                            request_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                }
                else
                {
                    if([request_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Request Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = request_date;
                    }
                }
                return Cell;
            }
            else {
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(iWorkDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(iWorkDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"workDate"] isEqualToString:@""] || ([dict valueForKey:@"workDate"] == nil))
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            iwork_date = @"";
                            Cell.DateSelectionLabel.text = @"iWork Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                    else
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"workDate"]];
                            iwork_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                }
                else
                {
                    if([iwork_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"iWork Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = iwork_date;
                    }
                }
                return Cell;
            }
        }
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == SelectTableView){
        if([Selection isEqualToString:@"RequestStatus"]){
           request_status = [RequestStatusArray objectAtIndex:indexPath.row];
        }
        else if([Selection isEqualToString:@"AttendanseStatus"]){
           attendance_status =  [AttendanceStatusArray objectAtIndex:indexPath.row];
        }
        SelectView.hidden = YES;
        [FilterTableView reloadData];
    }
    else{
        [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
        [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == SelectTableView){
        return 30;
    }
    else{
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return 44;
        }
        return 0;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(tableView == FilterTableView){
        return 44;
    }
    return 10;
}
#pragma mark - Creating View for TableView Section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *Views=[[UIView alloc]initWithFrame:CGRectMake(0, 0, FilterTableView.frame.size.width,0)];
    if(tableView != SelectTableView){
        UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, FilterTableView.frame.size.width,44)];
        sectionView.backgroundColor  = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
        sectionView.tag=section;
        UILabel *viewLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, FilterTableView.frame.size.width, 44)];
        viewLabel.backgroundColor=[UIColor clearColor];
        viewLabel.textAlignment = NSTextAlignmentLeft;
        viewLabel.textColor=[UIColor blackColor];
        viewLabel.font=delegate.ooredoo;
        viewLabel.text=[NSString stringWithFormat:@"%@",[FilterArray objectAtIndex:section]];
        [sectionView addSubview:viewLabel];
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, FilterTableView.frame.size.width, 1)];
        separatorLineView.backgroundColor = [UIColor lightGrayColor];
        [sectionView addSubview:separatorLineView];
        
        UIImageView *ImageViews = [[UIImageView alloc] initWithFrame:CGRectMake(FilterTableView.frame.size.width-40, 17, 10, 10)];
        if ([[arrayForBool objectAtIndex:section] boolValue]){
            ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[MinusImageArray objectAtIndex:section]]];
        }
        else{
            ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[PlusImageArray objectAtIndex:section]]];
        }
        [sectionView addSubview:ImageViews];
        UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
        [sectionView addGestureRecognizer:headerTapped];
        return  sectionView;
    }
    return Views;
}
#pragma mark - Table header gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[FilterArray count]; i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
-(void)ClearData{
    req_id = @"";
    request_status = @"";
    attendance_status = @"";
    request_date = @"";
    iwork_date = @"";
    location_list_array = @"";
    check_in_time = @"";
    check_out_time = @"";
    action_date = @"";
    empl_id = @"";
    empl_name = @"";
}
-(void)EmployeeIDSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    SearchCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    empl_id = cell.SearchTextField.text;
    [self FilterNofi];
}
-(void)EmployeeNameSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    SearchCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    empl_name = cell.SearchTextField.text;
    [self FilterNofi];
}
-(void)SearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    SearchCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    req_id = cell.SearchTextField.text;
    [self FilterNofi];
}
-(void)RequestStatusSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
    SelectionCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    request_status = cell.SelectionLabel.text;
    if([request_status isEqualToString:@"Select Status"]){
        request_status = @"";
    }
    [self FilterNofi];
}
-(void)SelectRequestAction{
    Selection = @"RequestStatus";
    SelectView.hidden = NO;
    [SelectTableView reloadData];
}
-(void)AttendanceStatusSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:3];
    SelectionCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    attendance_status = cell.SelectionLabel.text;
    if([attendance_status isEqualToString:@"Select Attendence"]){
        attendance_status = @"";
    }
    [self FilterNofi];
}
-(void)SelectAttendanceAction{
    Selection = @"AttendanseStatus";
    SelectView.hidden = NO;
    [SelectTableView reloadData];
}
-(void)RequestDateSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:4];
    DateSelectionCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    request_date = cell.DateSelectionLabel.text;
    if([request_date isEqualToString:@"Request Date"]){
        request_date = @"";
    }
    [self FilterNofi];
}
-(void)RequestDateAction{
    DateSelection = @"RequestDate";
    DatePikerViewBg.hidden = NO;
    [DatePicker setMaximumDate:[NSDate date]];
}
-(void)iWorkDateSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:4];
    DateSelectionCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    iwork_date = cell.DateSelectionLabel.text;
    if([iwork_date isEqualToString:@"iWork Date"]){
        iwork_date = @"";
    }
    [self FilterNofi];
}
-(void)iWorkDateAction{
    DateSelection = @"iWorkDate";
    DatePikerViewBg.hidden = NO;
    [DatePicker setMaximumDate:nil];
}
-(void)FilterNofi{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if([isComefrom isEqualToString:@"Rejected"]||[isComefrom isEqualToString:@"New"]||[isComefrom isEqualToString:@"Missed"])
    {
        // Request ID
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        SearchCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
        if (cell.SearchTextField.text == nil)
        {
            if ([req_id isEqualToString:@""])
            {
                req_id = cell.SearchTextField.text;
            }
        }
        else
        {
            req_id = cell.SearchTextField.text;
        }
//        req_id = cell.SearchTextField.text;
        
        NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:0 inSection:2];
        DateSelectionCell *cell2 = [FilterTableView cellForRowAtIndexPath:indexPath2];
        if (cell2.DateSelectionLabel.text == nil)
        {
            if ([request_date isEqualToString:@""])
            {
                request_date = cell2.DateSelectionLabel.text;
            }
        }
        else
        {
            request_date = cell2.DateSelectionLabel.text;
        }
        if([request_date isEqualToString:@"Request Date"]){
            request_date = @"";
        }
        else {
            request_date = [self dateToFormatedDate:request_date];
        }
        
        NSIndexPath *indexPath3 = [NSIndexPath indexPathForRow:0 inSection:3];
        DateSelectionCell *cell3 = [FilterTableView cellForRowAtIndexPath:indexPath3];
        if (cell3.DateSelectionLabel.text == nil)
        {
            if ([iwork_date isEqualToString:@""])
            {
                iwork_date = cell3.DateSelectionLabel.text;
            }
        }
        else
        {
            iwork_date = cell3.DateSelectionLabel.text;
        }
//        iwork_date = cell3.DateSelectionLabel.text;
        if([iwork_date isEqualToString:@"iWork Date"]){
            iwork_date = @"";
        }
        else {
            iwork_date = [self dateToFormatedDate:iwork_date];
        }
        
        UserDic = [[NSMutableDictionary alloc] init];
        if(![req_id isEqualToString:@""]){
            [UserDic setValue:req_id forKey:@"requestId"];
        }
//        if(![location_list_array isEqualToString:@""]){
            [UserDic setValue:SelectedLocationIDArray forKey:@"locationId"];
       // }
        if(![request_date isEqualToString:@""]){
            [UserDic setValue:request_date forKey:@"requestDate"];
        }
        if(![iwork_date isEqualToString:@""]){
            [UserDic setValue:iwork_date forKey:@"workDate"];
        }

    }
    else if([isComefrom isEqualToString:@"Approved"]){
        // Request ID
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        SearchCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
        if (cell.SearchTextField.text == nil)
        {
            if ([request_status isEqualToString:@""])
            {
                req_id = cell.SearchTextField.text;
            }
        }
        else
        {
            req_id = cell.SearchTextField.text;
        }
//        req_id = cell.SearchTextField.text;
        
        // Attandeance
        NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:0 inSection:2];
        SelectionCell *cell2 = [FilterTableView cellForRowAtIndexPath:indexPath2];
        if (cell2.SelectionLabel.text == nil)
        {
            if ([attendance_status isEqualToString:@""])
            {
                attendance_status = cell2.SelectionLabel.text;
            }
            
        }
        else
        {
            attendance_status = cell2.SelectionLabel.text;
        }
//        attendance_status = cell2.SelectionLabel.text;
        if([attendance_status isEqualToString:@"Select Attendance"]){
            attendance_status = @"";
        }
        
        NSIndexPath *indexPath3 = [NSIndexPath indexPathForRow:0 inSection:3];
        DateSelectionCell *cell3 = [FilterTableView cellForRowAtIndexPath:indexPath3];
        if (cell3.DateSelectionLabel.text == nil)
        {
            if ([request_date isEqualToString:@""])
            {
                request_date = cell3.DateSelectionLabel.text;
            }
            
        }
        else
        {
            request_date = cell3.DateSelectionLabel.text;
        }
//        request_date = cell3.DateSelectionLabel.text;
        if([request_date isEqualToString:@"Request Date"]){
            request_date = @"";
        }
        else {
            request_date = [self dateToFormatedDate:request_date];
        }
        
        NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:0 inSection:4];
        DateSelectionCell *cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
        if (cell4.DateSelectionLabel.text == nil)
        {
            if ([iwork_date isEqualToString:@""])
            {
                iwork_date = cell4.DateSelectionLabel.text;
            }
            
        }
        else
        {
            iwork_date = cell4.DateSelectionLabel.text;
        }
//        iwork_date = cell4.DateSelectionLabel.text;
        if([iwork_date isEqualToString:@"iWork Date"]){
            iwork_date = @"";
        }
        else {
            iwork_date = [self dateToFormatedDate:iwork_date];
        }
        
        UserDic = [[NSMutableDictionary alloc] init];
        if(![req_id isEqualToString:@""]){
            [UserDic setValue:req_id forKey:@"requestId"];
        }
       // if(![location_list_array isEqualToString:@""]){
            [UserDic setValue:SelectedLocationIDArray forKey:@"locationId"];
       // }
        if(![attendance_status isEqualToString:@""]){
            if([attendance_status isEqualToString:@"Present"]){
                attendance_status = @"Present";
            }
            else if([attendance_status isEqualToString:@"Absent"]){
                attendance_status = @"Absent";
            }
            [UserDic setValue:attendance_status forKey:@"attendanceStatus"];
        }
        if(![request_date isEqualToString:@""]){
            [UserDic setValue:request_date forKey:@"requestDate"];
        }
        if(![iwork_date isEqualToString:@""]){
            [UserDic setValue:iwork_date forKey:@"workDate"];
        }
    }
    else if([isComefrom isEqualToString:@"LineManagerApprove"]){
        
        //Employee ID
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        SearchCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
        if (cell.SearchTextField.text == nil)
        {
            if ([empl_id isEqualToString:@""])
            {
                empl_id = cell.SearchTextField.text;
            }
        }
        else
        {
            empl_id = cell.SearchTextField.text;
        }

        //Employee Name
        NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:0 inSection:1];
        SearchCell *cell1 = [FilterTableView cellForRowAtIndexPath:indexPath1];
        if (cell1.SearchTextField.text == nil)
        {
            if ([empl_name isEqualToString:@""])
            {
                empl_name = cell1.SearchTextField.text;
            }
        }
        else
        {
            empl_name = cell1.SearchTextField.text;
        }

        // Request ID
        NSIndexPath * indexPath2 = [NSIndexPath indexPathForRow:0 inSection:2];
        SearchCell *cell2 = [FilterTableView cellForRowAtIndexPath:indexPath2];
        if (cell2.SearchTextField.text == nil)
        {
            if ([req_id isEqualToString:@""])
            {
                req_id = cell2.SearchTextField.text;
            }
        }
        else
        {
            req_id = cell2.SearchTextField.text;
        }
 
        NSIndexPath *indexPath3 = [NSIndexPath indexPathForRow:0 inSection:4];
        DateSelectionCell *cell3 = [FilterTableView cellForRowAtIndexPath:indexPath3];
        if (cell3.DateSelectionLabel.text == nil)
        {
            if ([request_date isEqualToString:@""])
            {
                request_date = cell3.DateSelectionLabel.text;
            }
        }
        else
        {
            request_date = cell3.DateSelectionLabel.text;
        }
        if([request_date isEqualToString:@"Request Date"]){
            request_date = @"";
        }
        else {
            request_date = [self dateToFormatedDate:request_date];
        }
        
        NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:0 inSection:5];
        DateSelectionCell *cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
        if (cell4.DateSelectionLabel.text == nil)
        {
            if ([iwork_date isEqualToString:@""])
            {
                iwork_date = cell4.DateSelectionLabel.text;
            }
        }
        else
        {
            iwork_date = cell4.DateSelectionLabel.text;
        }
        if([iwork_date isEqualToString:@"iWork Date"]){
            iwork_date = @"";
        }
        else {
            iwork_date = [self dateToFormatedDate:iwork_date];
        }
        
        
        UserDic = [[NSMutableDictionary alloc] init];
        if(![empl_id isEqualToString:@""]){
            [UserDic setValue:empl_id forKey:@"empId"];
        }
        if(![empl_name isEqualToString:@""]){
            [UserDic setValue:empl_name forKey:@"empName"];
        }
        if(![req_id isEqualToString:@""]){
            [UserDic setValue:req_id forKey:@"requestId"];
        }
       // if(![location_list_array isEqualToString:@""]){
            [UserDic setValue:SelectedLocationIDArray forKey:@"locationId"];
      //  }
        if(![request_date isEqualToString:@""]){
            [UserDic setValue:request_date forKey:@"requestDate"];
        }
        if(![iwork_date isEqualToString:@""]){
            [UserDic setValue:iwork_date forKey:@"workDate"];
        }
    }
    else{
        // Request ID
        NSIndexPath *indexPath;
        indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        SearchCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
        if (cell.SearchTextField.text == nil)
        {
            if ([request_status isEqualToString:@""])
            {
                req_id = cell.SearchTextField.text;
            }
        }
        else
        {
            req_id = cell.SearchTextField.text;
        }
        
        // Request Status
        NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:0 inSection:2];
        SelectionCell *cell1 = [FilterTableView cellForRowAtIndexPath:indexPath1];
        if (cell1.SelectionLabel.text == nil)
        {
            if ([request_status isEqualToString:@""])
            {
                request_status = cell1.SelectionLabel.text;
            }
            
        }
        else
        {
            request_status = cell1.SelectionLabel.text;
        }
        
        if([request_status isEqualToString:@"Select Status"]){
            request_status = @"";
        }
        
        // Attandeance
        NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:0 inSection:3];
        SelectionCell *cell2 = [FilterTableView cellForRowAtIndexPath:indexPath2];
        if (cell2.SelectionLabel.text == nil)
        {
            if ([attendance_status isEqualToString:@""])
            {
                attendance_status = cell2.SelectionLabel.text;
            }
            
        }
        else
        {
            attendance_status = cell2.SelectionLabel.text;
        }
        if([attendance_status isEqualToString:@"Select Attendance"]){
            attendance_status = @"";
        }
        
        NSIndexPath *indexPath3 = [NSIndexPath indexPathForRow:0 inSection:4];
        DateSelectionCell *cell3 = [FilterTableView cellForRowAtIndexPath:indexPath3];
        if (cell3.DateSelectionLabel.text == nil)
        {
            if ([request_date isEqualToString:@""])
            {
                request_date = cell3.DateSelectionLabel.text;
            }
            
        }
        else
        {
            request_date = cell3.DateSelectionLabel.text;
        }
        
        if([request_date isEqualToString:@"Request Date"]){
            request_date = @"";
        }
        else {
            request_date = [self dateToFormatedDate:request_date];
        }
        
        NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:0 inSection:5];
        DateSelectionCell *cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
        
        if (cell4.DateSelectionLabel.text == nil)
        {
            if ([iwork_date isEqualToString:@""])
            {
                iwork_date = cell4.DateSelectionLabel.text;
            }
            
        }
        else
        {
            iwork_date = cell4.DateSelectionLabel.text;
        }

        if([iwork_date isEqualToString:@"iWork Date"]){
            iwork_date = @"";
        }
        else {
            iwork_date = [self dateToFormatedDate:iwork_date];
        }
        
        UserDic = [[NSMutableDictionary alloc] init];
        if(![req_id isEqualToString:@""]){
            [UserDic setValue:req_id forKey:@"requestId"];
        }
        //if(![location_list_array isEqualToString:@""]){
            [UserDic setValue:SelectedLocationIDArray forKey:@"locationId"];
       // }
        if(![request_status isEqualToString:@""]){
            if([request_status isEqualToString:@"New"]){
                request_status = @"0";
            }
            else if ([request_status isEqualToString:@"Approved"]){
                request_status = @"1";
            }
            else if ([request_status isEqualToString:@"Rejected"]){
                request_status = @"2";
            }
            else if ([request_status isEqualToString:@"Cancelled"]){
                request_status = @"3";
            }
            else if ([request_status isEqualToString:@"Discarded"]){
                request_status = @"4";
            }
            else if ([request_status isEqualToString:@"Decliend"]){
                request_status = @"5";
            }
            else if ([request_status isEqualToString:@"Missed"]){
                request_status = @"6";
            }
            [UserDic setValue:request_status forKey:@"status"];
        }
        if(![attendance_status isEqualToString:@""]){
            if([attendance_status isEqualToString:@"Present"]){
                attendance_status = @"Present";
            }
            else if([attendance_status isEqualToString:@"Absent"]){
                attendance_status = @"Absent";
            }
            [UserDic setValue:attendance_status forKey:@"attendanceStatus"];
        }
        if(![request_date isEqualToString:@""]){
            [UserDic setValue:request_date forKey:@"requestDate"];
        }
        if(![iwork_date isEqualToString:@""]){
            [UserDic setValue:iwork_date forKey:@"workDate"];
        }

    }
    [UserDic setValue:[ApplicationState userId] forKey:@"userId"];
    [UserDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
    [UserDic setValue:[NSNumber numberWithInt:limit] forKey:@"limit"];
    if([isComefrom isEqualToString:@"Approved"]){
        if([request_status isEqualToString:@""]||[request_status isEqualToString:@"()"]|| request_status == nil){
            [UserDic setValue:@"1" forKey:@"status"];
        }
        [userDefault setObject:UserDic forKey:@"Approved"];
        [userDefault synchronize];
        
       [[NSNotificationCenter defaultCenter] postNotificationName:@"Approved" object:nil userInfo:UserDic];
    }
    else if([isComefrom isEqualToString:@"Rejected"]){
        if([request_status isEqualToString:@""]||[request_status isEqualToString:@"()"]|| request_status == nil){
            [UserDic setValue:@"2" forKey:@"status"];
        }
        [userDefault setObject:UserDic forKey:@"Rejected"];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Rejected" object:nil userInfo:UserDic];
    }
    else if([isComefrom isEqualToString:@"New"]){
        if([request_status isEqualToString:@""]||[request_status isEqualToString:@"()"]|| request_status == nil){
            [UserDic setValue:@"0" forKey:@"status"];
        }
        [userDefault setObject:UserDic forKey:@"New"];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"New" object:nil userInfo:UserDic];
    }
    else if([isComefrom isEqualToString:@"Missed"]){
        if([request_status isEqualToString:@""]||[request_status isEqualToString:@"()"]|| request_status == nil){
            [UserDic setValue:@"6" forKey:@"status"];
        }
        [userDefault setObject:UserDic forKey:@"Missed"];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Missed" object:nil userInfo:UserDic];
    }

    else if([isComefrom isEqualToString:@"All"]){
        if([request_status isEqualToString:@""]||[request_status isEqualToString:@"()"]|| request_status == nil){
            [UserDic setValue:@"10" forKey:@"status"];
        }
        [userDefault setObject:UserDic forKey:@"All"];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"All" object:nil userInfo:UserDic];
    }
    else if([isComefrom isEqualToString:@"Employee"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Filter" object:nil userInfo:UserDic];
    }
    else if([isComefrom isEqualToString:@"LineManagerApprove"]){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LineManagerFilter" object:nil userInfo:UserDic];
    }
}
-(void)LocationApi{
    SelectedIndex = [[NSMutableArray alloc] init];
    SelectedArray = [[NSMutableArray alloc] init];
    LocationIDArray = [[NSMutableArray alloc] init];
    LocationNameArray = [[NSMutableArray alloc] init];
    if(delegate.isInternetConnected){
        ResponseDic = [Api WebApi:nil Url:@"locationsList"];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            if([ResponseDic[@"object"] isKindOfClass:[NSDictionary class]]){
                NSArray * locations = [ResponseDic[@"object"] valueForKey:@"locationData"];
                if(locations.count>0){
                    for (int i = 0; i< locations.count; i++){
                        NSDictionary *Dic = [locations objectAtIndex:i];
                        NSString *LocID = [Dic valueForKey:@"iwork_locations_id"];
                        NSString *Name = @"";
                        if(IsSafeStringPlus(TrToString(Dic[@"office_name"]))){
                            Name = [Dic valueForKey:@"office_name"];
                        }
                        else{
                            Name = @"";
                        }
                        [LocationIDArray addObject:LocID];
                        [LocationNameArray addObject:Name];
                    }
                    for (int i = 0; i< [LocationIDArray count]; i++) {
                        NSString *TagValue = [NSString stringWithFormat:@"%d",i];
                        [SelectedArray addObject:TagValue];
                    }
                    
                    if([isComefrom isEqualToString:@"LineManagerApprove"])
                    {
                        [self getLocationArray:self.filterData];
                    }
                    else
                    {
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                        dict = [userDefault objectForKey:isComefrom];
                        [self getLocationArray:dict];
                    }
                    [FilterTableView reloadData];
                }
            }
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:NSLocalizedString(@"NO_LOCATION_FROM_SERVER", nil) ButtonTitle:NSLocalizedString(@"OK", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }

}
-(IBAction)TapAction:(id)sender{
     SelectView.hidden = NO;
}
-(IBAction)DatePickerCancelAction:(id)sender{
    DatePikerViewBg.hidden = YES;
}
-(IBAction)DatePickerOKAction:(id)sender{
    DatePikerViewBg.hidden = YES;
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    df.dateFormat = @"dd-MM-yyyy";
    if([DateSelection isEqualToString:@"RequestDate"]){
       request_date = [df stringFromDate:DatePicker.date];
    }
    else if([DateSelection isEqualToString:@"iWorkDate"]){
        iwork_date = [df stringFromDate:DatePicker.date];
    }
    DateSelection = @"";
    [FilterTableView reloadData];
}

-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

-(IBAction)ClearFilterAction:(id)sender
{
    isClearSelected = YES;
    [self ClearData];
    [FilterTableView reloadData];
    if ([isComefrom isEqualToString:@"LineManagerApprove"])
    {
        self.filterData = nil;
        clearFilterForEmployee = NO;
         [[NSNotificationCenter defaultCenter] postNotificationName:@"LineManagerClearFilter" object:nil userInfo:nil];
    }
    else
    {
        [userDefault removeObjectForKey:isComefrom];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EmployeeClearFilterApi" object:nil userInfo:nil];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)CancelAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)FilterAction:(id)sender
{
    [self FilterNofi];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)CheckUnCheckAction:(UIButton*)sender{
    int Tagvalue = sender.tag;
    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%d",Tagvalue]]){
        [SelectedIndex removeObject:[NSString stringWithFormat:@"%d",Tagvalue]];
        [SelectedLocationIDArray removeObject:[NSString stringWithFormat:@"%@",[LocationIDArray objectAtIndex:Tagvalue]]];
    }
    else{
        [SelectedIndex addObject:[NSString stringWithFormat:@"%d",Tagvalue]];
        [SelectedLocationIDArray addObject:[NSString stringWithFormat:@"%@",[LocationIDArray objectAtIndex:Tagvalue]]];
    }
    [FilterTableView reloadData];
}
-(NSString *)dateToFormatedDate:(NSString *)dateStr {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:date];
}
-(NSString *)dateToFormatedDateDDMMYYYY:(NSString *)dateStr {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    return [dateFormatter stringFromDate:date];
}
-(NSString *)getRequestStatusFromRequestId:(NSString *)requestId
{
    if ([requestId isEqualToString:@"0"])
    {
        return @"New";
    }
    else if ([requestId isEqualToString:@"1"])
    {
        return @"Approved";
    }
    else if ([requestId isEqualToString:@"2"])
    {
        return @"Rejected";
    }
    else if ([requestId isEqualToString:@"3"])
    {
        return @"Cancelled";
    }
    else if ([requestId isEqualToString:@"4"])
    {
        return @"Discarded";
    }
    else if ([requestId isEqualToString:@"5"])
    {
        return @"Decliend";
    }
    else
    {
        return @"Missed";
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
@end
