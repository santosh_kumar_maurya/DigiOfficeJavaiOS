//
//  SignInViewController.m
//  MangoApp
//
//  Created by Fourbrick on 18/11/16.
//    Copyright © 2017 Fourbrick. All rights reserved.
//

#import "SignInViewController.h"
#import "Header.h"

@import Firebase;
@interface SignInViewController () <UITextFieldDelegate>{

    IBOutlet UIButton *EyeButton;
    IBOutlet UITextField *passwordTextField;
    IBOutlet UITextField *userNameTextField;
    IBOutlet UIButton *loginButton;
    BOOL rememberMeStatus;
    BOOL isPasswordShow;
    NSDictionary *params;
}
@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [userNameTextField setPlaceholder:NSLocalizedString(@"USER_NAME", nil)];
    [passwordTextField setPlaceholder:NSLocalizedString(@"PASSWORD", nil)];
    [loginButton setTitle:NSLocalizedString(@"LOGON_BUTTON", nil) forState:UIControlStateNormal];
    isPasswordShow = NO;
    
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
}
- (IBAction)loginButtonDidClicked:(id)sender {
    [self userDidTapScreen:nil];
    NSString *errMessage  = [self validateForm];
    if (!errMessage) {
        [self performSelector:@selector(performLoginProcess) withObject:nil afterDelay:0.5];
//       [self performLoginProcess];
    } else {
        [self ShowAlert:errMessage ButtonTitle:NSLocalizedString(@"OKAY", nil)];
    }
}
- (NSString *)validateForm {
    NSString *errorMessage;
    if (![userNameTextField.text isValidName]) {
        errorMessage = NSLocalizedString(@"USER_ID_VALIDATION", nil);
    }
    else if (![passwordTextField.text isValidName]) {
        errorMessage = NSLocalizedString(@"USER_ID_VALIDATION", nil);    }
    return errorMessage;
}
- (void)performLoginProcess {
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    if (refreshedToken !=nil) {
        [ApplicationState setPushToken:refreshedToken];
    }
//    else{
//        refreshedToken = @"";
//    }
    params = @ {
        @"role" : @"",
        @"uname" : userNameTextField.text,
        @"password" : passwordTextField.text,
        @"fcm_id" : refreshedToken
    };
    NSLog(@"params==%@",params);
    [LoadingManager showLoadingView:self.view];
    [[NetworkInterface sharedNetworkManager]fetchDataWithRequestTypePOST:[NSString stringWithFormat:@"%@%@",BASE_URL,@"userAuth"] parameters:params successBlock:^(id responseObject) {
        NSLog(@"responseObject====%@",responseObject);
        [LoadingManager hideLoadingView:self.view];
        if(responseObject == nil){
            [LoadingManager hideLoadingView:self.view];
            [self performLoginProcess];
        }
        else if([[[responseObject valueForKey:@"responseObj"] valueForKey:@"statusCode"] intValue]==5){
             NSDictionary *Dic = [[responseObject valueForKey:@"responseObj"] valueForKey:@"object"];
             [ApplicationState setUserId:Dic[@"user_id"]];
             [ApplicationState setUserName:Dic[@"name"]];
             [ApplicationState setToken:responseObject[@"token"]];
             [ApplicationState setUserLoginData:responseObject];
             [ApplicationState setUserEmailId:userNameTextField.text];
             [ApplicationState setUserIsLoggedIn];
            ContainerParentViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
            [self.navigationController pushViewController:newView animated:YES];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[[responseObject valueForKey:@"responseObj"] valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
        
    } failure:^(NSError *err) {
         [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_RESPONSE_SERVER", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }];
}
- (IBAction)userDidTapScreen:(id)sender {
    [userNameTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)PasswordShow {
    if(isPasswordShow == NO){
        isPasswordShow = YES;
        passwordTextField.secureTextEntry = NO ;
        [EyeButton setImage:[UIImage imageNamed:@"visibility_on"] forState:UIControlStateNormal];
    }
    else if(isPasswordShow == YES){
        isPasswordShow = NO;
        passwordTextField.secureTextEntry = YES ;
        [EyeButton setImage:[UIImage imageNamed:@"visibility_off"] forState:UIControlStateNormal];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
@end
