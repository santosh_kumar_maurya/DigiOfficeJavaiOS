//
//  LineManageMissedViewController.m
//  iWork
//
//  Created by Shailendra on 30/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LineManageMissedViewController.h"
#import "Header.h"

@interface LineManageMissedViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *MissedTableView;
    NSMutableArray *MissedArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    IBOutlet UILabel *NoDataLabel;
    NSDictionary *params;
    
    APIService *Api;
    NSDictionary *ResponseDic;
    NSString *requestsId;
    int offset;
    int limit;
    int TagValue;
    IBOutlet UIImageView *NoDataImageView;
}
@end

@implementation LineManageMissedViewController

- (void)viewDidLoad {
    CheckValue = @"MISSED";
    [self EmptyArray];
    TagValue = 0;
    Api = [[APIService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor whiteColor];
    MissedTableView.backgroundColor = [UIColor whiteColor];
    MissedTableView.estimatedRowHeight = 500;
    MissedTableView.rowHeight = UITableViewAutomaticDimension;
    MissedTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, MissedTableView.bounds.size.width, 0.00f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [MissedTableView addSubview:refreshControl];
    
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkLineManagerMissed) withObject:nil afterDelay:0.5];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MissedApi:) name:@"LineManagerMissed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClearFilterApi:) name:@"LMClearFilterApi" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)EmptyArray{
    offset = 0;
    limit = 10;
    MissedArray = [[NSMutableArray alloc]init];
}
- (void)reloadData
{
    if ([CheckValue isEqualToString:@"FILTER"])
    {
        CheckValue = @"FILTER";
    }
    else{
        CheckValue = @"Refresh";
    }
    [self EmptyArray];
    [self GetiWorkLineManagerMissed];
}
#pragma mark  -UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [MissedArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MissedCell *Cell = (MissedCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
    Cell.tag = indexPath.section;
    [Cell  setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:Gesture];
    if(MissedArray.count>0){
        NSDictionary * responseData = [MissedArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
        if([responseData[@"status"] isEqualToString:@"MISSED"] ){
            if ([Cell.ActionDateLabel.text isEqualToString:@" "]){
                Cell.ActionDateStaticLabel.text = @" ";
            }
        }
    }
    if(indexPath.section == 0){
        Cell.UpperLineLabel.hidden = YES;
    }
    else{
        Cell.UpperLineLabel.hidden = NO;
    }
   
    return Cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
    if(ArraysValue.count>0){
        offset = offset + 10;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self GetiWorkLineManagerMissed];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [MissedArray addObject:BindDataDic];
    }
}
-(void)MapPage{
    NSDictionary *Dic = [MissedArray objectAtIndex:TagValue];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
    ObjViewController.MapDic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GotoMapView:(UIButton*)Sender{
    TagValue = Sender.tag;
    [self MapPage];
}
-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    TagValue = sender.view.tag;
    [self MapPage];
}
- (void)GetiWorkLineManagerMissed {
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"MISSED"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"status": @6,
                    @"userId": [ApplicationState userId],
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"viewTeamReport"];
            [refreshControl endRefreshing];
            [LoadingManager hideLoadingView:self.view];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self GetiWorkLineManagerMissed];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                NSLog(@"MissedMissedMissedMissedMissed");
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
                if(ResponseArrays.count>0){
                    NoDataLabel.hidden = YES;
                     NoDataImageView.hidden = YES;
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"]];
                    [MissedTableView  reloadData];
                }
                else if(MissedArray.count == 0){
                    [self NoIworkRequest];
                }
            }
            else{
                if(MissedArray.count == 0){
                    [self NoIworkRequest];
                }
            }
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoIworkRequest{
    NoDataLabel.hidden = NO;
     NoDataImageView.hidden = NO;
    NoDataLabel.text = NSLocalizedString(@"NO_IWORK_REQUEST_FOUND", nil);
//    NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    [MissedTableView  reloadData];
}
-(void)MissedApi:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [self GetiWorkLineManagerMissed];
}
-(void)ClearFilterApi:(NSNotification*)notification
{
    [self EmptyArray];
    CheckValue = @"MISSED";
    [LoadingManager showLoadingView:self.view];
    [self GetiWorkLineManagerMissed];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
