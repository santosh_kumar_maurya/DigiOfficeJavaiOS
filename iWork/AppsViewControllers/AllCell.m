//
//  AllCell.m
//  iWork
//
//  Created by Shailendra on 18/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "AllCell.h"
#import "Header.h"
#import "iWork-Swift.h"

@implementation AllCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.circleView.layer.cornerRadius = self.circleView.frame.size.width/2;
    self.circleView.layer.masksToBounds =YES;
    self.circleView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.circleView.layer.borderWidth = 0.5;
    
    self.CheckInButton.layer.cornerRadius = 15;
    self.CheckInButton.clipsToBounds = YES;
    // Initialization code
}

- (void)configureCell:(NSDictionary *)info {
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if(IsSafeStringPlus(TrToString(info[@"userId"]))) {
        _employeeId.text = [NSString stringWithFormat:@"%@",info[@"userId"]];
    } else {
        _employeeId.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"userName"]))) {
        _employeeName.text = info[@"userName"];
    } else {
        _employeeName.text = @" ";
    }
    _RequestIdLabel.text = @"";
    if(IsSafeStringPlus(TrToString(info[@"requestId"]))) {
        _RequestIdLabel.text = [NSString stringWithFormat:@"%@",info[@"requestId"]];
    } else {
        _RequestIdLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"dateTime"]))) {
        NSNumber *datenumber = info[@"dateTime"];
        _RequestDateLabel.text = [self DateFormateChange:datenumber];
        
    } else {
        _RequestDateLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"task"]))) {
        _TaskLabel.text = info[@"task"];
    } else {
        _TaskLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"location"]))) {
        _iWorkLocationLabel.text = info[@"location"];
    } else {
        _iWorkLocationLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"requestDate"]))) {
        NSNumber *datenumber = info[@"requestDate"];
        _iWorkDateLabel.text = [self DateFormateChange:datenumber];
    } else {
        _iWorkDateLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"actionDate"]))) {
        NSNumber *datenumber = info[@"actionDate"];
        _ActionDateLabel.text = [self DateFormateChange:datenumber];
    } else {
        _ActionDateLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"checkInTime"]))) {
        NSNumber *Timenumber = info[@"checkInTime"];
        _CheckInLabel.text = [self GetDate:Timenumber];
    } else {
        _CheckInLabel.text = @"- -:- -";
    }
    if(IsSafeStringPlus(TrToString(info[@"checkOutTime"]))) {
        NSNumber *Timenumber = info[@"checkOutTime"];
        _CheckoutLabel.text = [self GetDate:Timenumber];
    } else {
        _CheckoutLabel.text = @"- -:- -";
    }
    _HoursLabel.text = @" ";
    if(IsSafeStringPlus(TrToString(info[@"hour"]))) {
        NSNumber *Timenumber = info[@"hour"];
        _HoursLabel.text = [self GetDate:Timenumber];
    }
    else{
        _HoursLabel.text = @"- -:- -";
    }
    if(IsSafeStringPlus(TrToString(info[@"maxCheckInTime"]))) {
        _MaxCheckInLabel.text = [self MaxDate:info[@"maxCheckInTime"]];
    }else{
        _MaxCheckInLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"locType"]))) {
        _iWorkLocationTypeLabel.text = info[@"locType"];
    } else {
        _iWorkLocationTypeLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"attendanceStatus"]))) {
        _AttandenceStatusLabel.text = info[@"attendanceStatus"];
        if([_AttandenceStatusLabel.text isEqualToString:@"-"]){
            _AttandenceStatusLabel.text = @" ";
        }
    }else{
        _AttandenceStatusLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"status"]))) {
        
        if ([info[@"status"] isEqualToString:@"PENDING"]) {
            _StatusLabel.textColor = [UIColor ViewRequestOrangeColor];
            _circleView.backgroundColor = [UIColor ViewRequestOrangeColor];
            _StatusLabel.text = @"New";
            _StatusImageView.image = [UIImage imageNamed:@"NewStatus"];
            _CheckInButton.hidden = YES;
            _checkInBottomConstraints.constant = 8;
            
        } else if ([info[@"status"] isEqualToString:@"APPROVED"]) {
            
            _StatusLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:88.0/255.0 blue:45.0/255.0 alpha:1.0];
            _circleView.backgroundColor = [UIColor ViewRequestGreenColor];
            _StatusLabel.text = @"Approved";
            _StatusImageView.image = [UIImage imageNamed:@"Approved"];
            _CheckInButton.hidden = YES;
            _checkInBottomConstraints.constant = 8;
        }
        else if ([info[@"status"] isEqualToString:@"REJECTED"]) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _circleView.backgroundColor = [UIColor ViewRequestRedColor];
            _StatusLabel.text = @"Rejected";
            _StatusImageView.image = [UIImage imageNamed:@"rejected_new"];
            _CheckInButton.hidden = YES;
            _checkInBottomConstraints.constant = 8;
            
        }
        else if ([info[@"status"] isEqualToString:@"CANCELLED"] ) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _circleView.backgroundColor = [UIColor ViewRequestRedColor];
            _StatusLabel.text = @"Cancelled";
            _StatusImageView.image = [UIImage imageNamed:@"CancelledStatus"];
            _CheckInButton.hidden = YES;
            _checkInBottomConstraints.constant = 8;
            
        }
        else if ([info[@"status"] isEqualToString:@"DISCARDED"]) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _StatusLabel.text = @"Discarded";
            _circleView.backgroundColor = [UIColor ViewRequestRedColor];
            _StatusImageView.image = [UIImage imageNamed:@"DiscardedStatus"];
            _CheckInButton.hidden = YES;
            _checkInBottomConstraints.constant = 8;
            
        }
        else if ([info[@"status"]isEqualToString:@"DECLINED"] ) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _StatusLabel.text = @"Declined";
            _circleView.backgroundColor = [UIColor ViewRequestRedColor];
            _StatusImageView.image = [UIImage imageNamed:@"DeclinedStatus"];
            _CheckInButton.hidden = YES;
            _checkInBottomConstraints.constant = 8;
            
        }
    }
    
    if(IsSafeStringPlus(TrToString(info[@"checkIn"]))){
        if([info[@"checkIn"] intValue]==1){
            [_CheckInButton setTitle:NSLocalizedString(@"CHECK_IN_BUTTON", nil) forState:UIControlStateNormal];
            [_CheckInButton setBackgroundColor:delegate.redColor];
            _CheckInButton.hidden = NO;
            _checkInBottomConstraints.constant = 44;
        }
        else{
            _CheckInButton.hidden = YES;
            _checkInBottomConstraints.constant = 8;
        }
    }
    if(IsSafeStringPlus(TrToString(info[@"checkOut"]))){
        if([info[@"checkOut"] intValue]==1){
            [_CheckInButton setTitle:NSLocalizedString(@"CHECK_OUT_BUTTON", nil) forState:UIControlStateNormal];
            [_CheckInButton setBackgroundColor:delegate.redColor];
            _CheckInButton.hidden = NO;
            _checkInBottomConstraints.constant = 44;
        }
        else{
            _CheckInButton.hidden = YES;
            _checkInBottomConstraints.constant = 8;
        }
    }
    
    
    if(IsSafeStringPlus(TrToString(info[@"status"]))){
        if([info[@"status"] isEqualToString:@"APPROVED"]){
            if(IsSafeStringPlus(TrToString(info[@"attendanceStatus"])))
            {
                _AttandenceStatusStaticLabel.text = NSLocalizedString(@"ATTENDANCE_STATUS", nil);
                if([info[@"attendanceStatus"] isEqualToString:@"Present"])
                {
                    _CheckInStaticLabel.text = @"Check-in";
                    _CheckoutStaticLabel.text = @"Checkout";
                    _HoursStaticLabel.text = NSLocalizedString(@"Hours", nil);
                    _HoursLabel.text = info[@"hour"];
                    if(IsSafeStringPlus(TrToString(info[@"checkOutTime"]))) {
                        NSNumber *Timenumber = info[@"checkOutTime"];
                        _CheckoutLabel.text = [self GetDate:Timenumber];
                    } else {
                        _CheckoutLabel.text = @"- -:- -";
                    }
                    if(IsSafeStringPlus(TrToString(info[@"checkInTime"]))) {
                        NSNumber *Timenumber = info[@"checkInTime"];
                        _CheckInLabel.text = [self GetDate:Timenumber];
                    } else {
                        _CheckInLabel.text = @"- -:- -";
                    }
                    if(IsSafeStringPlus(TrToString(info[@"hour"]))) {
                        NSNumber *Timenumber = info[@"hour"];
                        _HoursLabel.text = [self GetDate:Timenumber];
                    }
                    else{
                        _HoursLabel.text = @"- -:- -";
                    }
                }
                else{
                    _CheckInStaticLabel.text = @"";
                    _CheckoutStaticLabel.text = @"";
                    _HoursStaticLabel.text = @"";
                    _CheckInLabel.text = @"";
                    _CheckoutLabel.text = @"";
                    _HoursLabel.text = @"";
                    _checkInBottomConstraints.constant = 0.0;
                    
                }
            }
            else
            {
                _CheckInStaticLabel.text = @"";
                _CheckoutStaticLabel.text = @"";
                _HoursStaticLabel.text = @"";
                _CheckInLabel.text = @"";
                _CheckoutLabel.text = @"";
                _HoursLabel.text = @"";
                _AttandenceStatusStaticLabel.text = @"";
                _AttandenceStatusLabel.text = @"";
                
            }
        }
        else{
            _CheckInStaticLabel.text = @"";
            _CheckoutStaticLabel.text = @"";
            _HoursStaticLabel.text = @"";
            _CheckInLabel.text = @"";
            _CheckoutLabel.text = @"";
            _HoursLabel.text = @"";
            _AttandenceStatusStaticLabel.text = @"";
            _AttandenceStatusLabel.text = @"";
        }
    }
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)GetDate:(NSNumber*)Timenumber{
    NSString *DateStr = [NSString stringWithFormat:@"%@",Timenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"HH:mm:ss"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"hh:mm a"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)MaxDate:(NSString*)DateString{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString: DateString];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *convertedString = [dateFormatter stringFromDate:date];
    return convertedString;
}
@end
