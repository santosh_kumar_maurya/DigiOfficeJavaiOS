//
//  EmployeeParentViewController.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeParentViewController.h"
#import "Header.h"

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define kDefaultEdgeInsets UIEdgeInsetsMake(5, 10, 5, 10)

@interface EmployeeParentViewController ()
{
    NSArray *btnArray;
    AppDelegate *delegate;
    IBOutlet UILabel *HeaderLabel;
    int BtnTagValue;

}
@property (weak) IBOutlet UIScrollView *containerScrollView;
@property (weak) IBOutlet UIScrollView *menuScrollView;
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray;
-(void) buttonPressed:(id) sender;
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets;
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr;
@end

@implementation EmployeeParentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSString *HeaderStr = NSLocalizedString(@"I_WORK_REQUESTS_HEADER", nil);
   // HeaderStr = [HeaderStr stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    HeaderLabel.text = HeaderStr;
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    btnArray = [[NSArray alloc]initWithObjects:[NSLocalizedString(@"ALL_BTN", nil) uppercaseString],[NSLocalizedString(@"NEW_BTN", nil) uppercaseString], [NSLocalizedString(@"APPROVED_BTN", nil) uppercaseString],[NSLocalizedString(@"REJ_ECTED_BTN", nil) uppercaseString],[NSLocalizedString(@"MISSED_BTN", nil) uppercaseString], nil];
    //
    [self addButtonsInScrollMenu:btnArray];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EmployeeApprovedViewController *threeVC = [storyBoard instantiateViewControllerWithIdentifier:@"EmployeeApprovedViewController"];
    EmployeeRejectedViewController *fourVC = [storyBoard instantiateViewControllerWithIdentifier:@"EmployeeRejectedViewController"];
    EmployeeNewViewController *twoVC = [storyBoard instantiateViewControllerWithIdentifier:@"EmployeeNewViewController"];
    EmployeeAllViewController *oneVC = [storyBoard instantiateViewControllerWithIdentifier:@"EmployeeAllViewController"];
    EmployeeMissedViewController *fiveVC = [storyBoard instantiateViewControllerWithIdentifier:@"EmployeeMissedViewController"];
    NSArray *controllerArray = @[oneVC, twoVC,threeVC,fourVC,fiveVC];
    [self addChildViewControllersOntoContainer:controllerArray];
    [self clearFilterUserDefault];
    // Do any additional setup after loading the view.
}

#pragma mark - Add Menu Buttons in Menu Scroll View
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray
{
    CGFloat buttonHeight = self.menuScrollView.frame.size.height;
    CGFloat cWidth = 12.0f;
    
    for (int i = 0 ; i<buttonArray.count; i++)
    {
        NSString *tagTitle = [buttonArray objectAtIndex:i];
        
        CGFloat buttonWidth = [self widthForMenuTitle:tagTitle buttonEdgeInsets:kDefaultEdgeInsets];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.frame = CGRectMake(cWidth, 0.0f, SCREEN_WIDTH/4, buttonHeight);

        [button setTitle:tagTitle forState:UIControlStateNormal];
        button.titleLabel.font = delegate.contentFont;
        
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [button setTitleColor:[UIColor colorWithRed:(248.0/255.0) green:(117.0/255.0) blue:(177.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        [self.menuScrollView addSubview:button];
        
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height - 2, button.frame.size.width, 2)];
        bottomView.backgroundColor = [UIColor whiteColor];
        bottomView.tag = 1001;
        [button addSubview:bottomView];
        if (i == 0)
        {
            button.selected = YES;
            [bottomView setHidden:NO];
        }
        else
        {
            [bottomView setHidden:YES];
        }

        cWidth = cWidth + SCREEN_WIDTH/4;
//        if(i == 0){
//            cWidth = cWidth + SCREEN_WIDTH/4+20;
//        }
//        else if(i == 2){
//            cWidth = cWidth + SCREEN_WIDTH/4-20;
//        }
//        else{
//            cWidth = cWidth + SCREEN_WIDTH/4;
//        }
//        if(i == 0){
//           cWidth = cWidth + SCREEN_WIDTH/5+15;
//        }
//        else if(i == 1){
//            cWidth = cWidth + SCREEN_WIDTH/5+10;
//        }
//        else if(i == 2){
//           cWidth = cWidth + SCREEN_WIDTH/5;
//        }
//        else if(i == 3){
//            cWidth = cWidth + SCREEN_WIDTH/5-10;
//        }
//        else{
//           cWidth = cWidth + SCREEN_WIDTH/5;
//        }
    }
    
    NSLog(@"scroll menu width->%f",cWidth);
    self.menuScrollView.contentSize = CGSizeMake(cWidth+20, self.menuScrollView.frame.size.height);
}


/**
 *  Any Of the Top Menu Button Press Action
 *
 *  @param sender id of the button pressed
 */

#pragma mark - Menu Button press action
-(void) buttonPressed:(id) sender
{
    UIButton *senderbtn = (UIButton *) sender;
    float buttonWidth = 0.0f;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        UIButton *btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        
        if (btn.tag == senderbtn.tag)
        {
            BtnTagValue = btn.tag;
            btn.selected = YES;
            [bottomView setHidden:NO];
        }
        else
        {
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    
    [self.containerScrollView setContentOffset:CGPointMake(SCREEN_WIDTH * senderbtn.tag, 0) animated:YES];
    
    float xx = SCREEN_WIDTH * (senderbtn.tag) * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    
    if (senderbtn.tag == 0){
        [self.menuScrollView scrollRectToVisible:CGRectMake(0, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
    }
    else{
        [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH+100, self.menuScrollView.frame.size.height) animated:YES];
    }
    
}

/**
 *  Calculating width of button added on top menu
 *
 *  @param title            Title of the Button
 *  @param buttonEdgeInsets Edge Insets for the title
 *
 *  @return Width of button
 */

#pragma mark - Calculate width of menu button
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets
{
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:10.0f]};
    
    CGSize size = [title sizeWithAttributes:attributes];
    return CGSizeMake(size.width + buttonEdgeInsets.left + buttonEdgeInsets.right, size.height + buttonEdgeInsets.top + buttonEdgeInsets.bottom).width;
}


/**
 *  Adding all related controllers in to the container
 *
 *  @param controllersArr Array containing objects of all controllers
 */
#pragma mark - Adding all related controllers in to the container
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr
{
    for (int i = 0 ; i < controllersArr.count; i++)
    {
        UIViewController *vc = (UIViewController *)[controllersArr objectAtIndex:i];
        CGRect frame = CGRectMake(0, 0, self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height);
        frame.origin.x = SCREEN_WIDTH * i;
        vc.view.frame = frame;
        
        [self addChildViewController:vc];
        [self.containerScrollView addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    
    self.containerScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * controllersArr.count + 1, self.containerScrollView.frame.size.height);
    self.containerScrollView.pagingEnabled = YES;
    self.containerScrollView.delegate = self;
}


#pragma mark - Scroll view delegate methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = (scrollView.contentOffset.x / SCREEN_WIDTH);
    
    UIButton *btn;
    float buttonWidth = 0.0;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        
        if (btn.tag == page)
        {
            BtnTagValue = btn.tag;
            btn.selected = YES;
            buttonWidth = btn.frame.size.width;
            [bottomView setHidden:NO];
        }
        else
        {
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    
    float xx = scrollView.contentOffset.x * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction:(id)sender
{
    [self clearFilterUserDefault];
    [[self navigationController] popViewControllerAnimated:YES];
}
-(void)clearFilterUserDefault
{
    NSUserDefaults *userDefault =  [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"All"];
    [userDefault removeObjectForKey:@"New"];
    [userDefault removeObjectForKey:@"Approved"];
    [userDefault removeObjectForKey:@"Rejected"];
    [userDefault removeObjectForKey:@"RejeMissedcted"];
}
- (IBAction)HomeAction:(id)sender {
    NSArray *array = [self.navigationController viewControllers];
    for (int i = 0 ; i < array.count; i++) {
        UIViewController *ViewController = [array objectAtIndex:i];
        if([ViewController isKindOfClass:[ContainerParentViewController class]]){
            [self.navigationController popToViewController:ViewController animated:YES];
        }
    }
}
- (IBAction)FilterBtnAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FilterViewController *ObjFilterViewController = [storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    if(BtnTagValue == 0){
        ObjFilterViewController.isComefrom =@"All";
    }
    else if(BtnTagValue == 1){
        ObjFilterViewController.isComefrom =@"New";
    }
    else if(BtnTagValue == 2){
        ObjFilterViewController.isComefrom =@"Approved";
    }
    else if(BtnTagValue == 3){
        ObjFilterViewController.isComefrom =@"Rejected";
    }
    else if(BtnTagValue == 4){
        ObjFilterViewController.isComefrom =@"Missed";
    }
    [self presentViewController:ObjFilterViewController animated:YES completion:nil];
}


@end
