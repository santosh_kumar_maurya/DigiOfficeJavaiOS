//
//  MyWorkLocationCell.h
//  iWork
//
//  Created by Shailendra on 20/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWorkLocationCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UITextField *LocationTextField;
@property(nonatomic,strong)IBOutlet UIView *TextFieldBG;

@end
