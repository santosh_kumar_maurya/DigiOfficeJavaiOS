//
//  MyTeamCell.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyTeamCell.h"
#import "NSString+HTML.h"

@implementation MyTeamCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.CellBgView.layer.cornerRadius = 5.0;
    self.MyTeamLabel.text = NSLocalizedString(@"MY_TEAM_ON_IWORK", nil);
   // self.MyTeamLabel.text = [self.MyTeamLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.CellBgView.bounds];
    self.CellBgView.layer.masksToBounds = NO;
    self.CellBgView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.CellBgView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.CellBgView.layer.shadowOpacity = 0.3f;
    self.CellBgView.layer.shadowPath = shadowPath.CGPath;


}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
