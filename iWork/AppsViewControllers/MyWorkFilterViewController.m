//
//  MyWorkFilterViewController.m
//  iWork
//
//  Created by Shailendra on 20/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyWorkFilterViewController.h"
#import "Header.h"

@interface MyWorkFilterViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrayForBool;
    NSMutableArray *FilterArray;
    IBOutlet UITableView *MyWorkTableView;
    BOOL RadioSelect;
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *CancelFilterBtn;
    IBOutlet UIButton *ApplyFilterBtn;
    AppDelegate *delegate;
    IBOutlet UIView *BottomView;
    NSMutableArray *PlusImageArray;
    NSMutableArray *MinusImageArray;
    
    NSIndexPath *indexPath3;
    MyWorkEmployeeCell *Cell3;
    NSIndexPath *indexPath4;
    MyWorkLocationCell *Cell4;
    NSUserDefaults *defauls;
    NSString *LocationSting;
    NSString *iWorkemp_idStr;
    NSString *iWorkemp_nameStr;
    BOOL isClearTapped;

}
@end

@implementation MyWorkFilterViewController

- (void)viewDidLoad {
    isClearTapped = NO;
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
    defauls = [NSUserDefaults standardUserDefaults];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    RadioSelect = YES;
    PlusImageArray = [[NSMutableArray alloc] initWithObjects:@"Plus",@"Plus", nil];
    MinusImageArray = [[NSMutableArray alloc] initWithObjects:@"Minus",@"Minus", nil];
    
    arrayForBool = [[NSMutableArray alloc] init];
    FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"EMPLOYEE_FILTER", nil),NSLocalizedString(@"LOCATION_FILTER", nil), nil];
    
    arrayForBool=[[NSMutableArray alloc]init];
    for (int i=0; i<[FilterArray count]; i++) {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    MyWorkTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, MyWorkTableView.bounds.size.width, 0.01f)];
    
    UINib *SearchNib = [UINib nibWithNibName:@"MyWorkEmployeeCell" bundle:nil];
    [MyWorkTableView registerNib:SearchNib forCellReuseIdentifier:@"CELL"];
    
    UINib *SelectionNib = [UINib nibWithNibName:@"MyWorkLocationCell" bundle:nil];
    [MyWorkTableView registerNib:SelectionNib forCellReuseIdentifier:@"CELL1"];
    
    CancelBtn.layer.borderWidth = 1.0;
    CancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    CancelBtn.layer.cornerRadius = 3.0;
    CancelBtn.clipsToBounds = YES;
    
    CancelFilterBtn.layer.borderWidth = 1.0;
    CancelFilterBtn.layer.borderColor = delegate.redColor.CGColor;
    CancelFilterBtn.layer.cornerRadius = 3.0;
    CancelFilterBtn.clipsToBounds = YES;
    //CancelFilterBtn.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];

    ApplyFilterBtn.layer.cornerRadius = 3.0;

    //BottomView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
   
    self.view.backgroundColor = [UIColor whiteColor];
    MyWorkTableView.backgroundColor = [UIColor whiteColor];

    if([iWorkemp_idStr isEqualToString:@""]){
        RadioSelect = YES;
    }
    else if([iWorkemp_nameStr isEqualToString:@""]){
        RadioSelect = YES;
    }
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [FilterArray count];
}

//MARK: - TableView Delegate ---------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0 )
    {
        MyWorkEmployeeCell * Cell = (MyWorkEmployeeCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        if(RadioSelect==YES)
        {
            Cell.EmployeeNameRadioImage.image = [UIImage imageNamed:@"RadioOn"];
            Cell.EmployeeIDRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            if ([[self.filterData valueForKey:@"empName"] isEqualToString:@""])
            {
                Cell.EmployeeIDTextField.text = @"";
                Cell.EmployeeIDTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME_FILTER", nil);
            }
            else
            {
                Cell.EmployeeIDTextField.text = [self.filterData valueForKey:@"empName"];
            }
//Cell.EmployeeIDTextField.keyboardType = UIKeyboardTypeDefault;
            [Cell.EmployeeIDTextField becomeFirstResponder];
        }
        else
        {
            Cell.EmployeeNameRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            Cell.EmployeeIDRadioImage.image = [UIImage imageNamed:@"RadioOn"];
            if ([[self.filterData valueForKey:@"empId"] isEqualToString:@""])
            {
                Cell.EmployeeIDTextField.text = @"";
                Cell.EmployeeIDTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID_FILTER", nil);
            }
            else
            {
                Cell.EmployeeIDTextField.text = [self.filterData valueForKey:@"empId"];
            }
            //Cell.EmployeeIDTextField.keyboardType = UIKeyboardTypeNumberPad;
            [Cell.EmployeeIDTextField becomeFirstResponder];
        }
         [Cell.EmployeeNameBtn addTarget:self action:@selector(RadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
         [Cell.EmployeeIDBtn addTarget:self action:@selector(RadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
        return Cell;
    }
    else
    {
        MyWorkLocationCell * Cell = (MyWorkLocationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        if ([[self.filterData valueForKey:@"location"] isEqualToString:@""])
        {
            
        }
        else
        {
            Cell.LocationTextField.text = [self.filterData valueForKey:@"location"];
        }
        return Cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue])
        {
            return 150;
        }
    }
    else if(indexPath.section == 1)
    {
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue])
        {
            return 70;
        }
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *Views=[[UIView alloc]initWithFrame:CGRectMake(0, 0, MyWorkTableView.frame.size.width,0)];
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, MyWorkTableView.frame.size.width,44)];
    sectionView.tag=section;
    sectionView.backgroundColor  = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
    UILabel *viewLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, MyWorkTableView.frame.size.width, 44)];
    viewLabel.backgroundColor=[UIColor clearColor];
    viewLabel.textAlignment = NSTextAlignmentLeft;
    viewLabel.textColor=[UIColor blackColor];
    viewLabel.font=delegate.ooredoo;
    viewLabel.text=[NSString stringWithFormat:@"%@",[FilterArray objectAtIndex:section]];
    [sectionView addSubview:viewLabel];

    UIImageView *ImageViews = [[UIImageView alloc] initWithFrame:CGRectMake(MyWorkTableView.frame.size.width-40, 17, 10, 10)];
    if ([[arrayForBool objectAtIndex:section] boolValue])
    {
        ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[MinusImageArray objectAtIndex:section]]];
    }
    else
    {
      ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[PlusImageArray objectAtIndex:section]]];
    }
    [sectionView addSubview:ImageViews];
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    return  sectionView;
    return Views;
}
#pragma mark - Table header gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[FilterArray count]; i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        [MyWorkTableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)RadioBtnOnOff:(UIButton*)Sender{
    switch (Sender.tag) {
        case 0:
            RadioSelect = YES;
            [MyWorkTableView reloadData];
            break;
        case 1:
            RadioSelect = NO;
            [MyWorkTableView reloadData];
            break;
        default:
            break;
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    MyWorkEmployeeCell *Cell = [MyWorkTableView cellForRowAtIndexPath:indexPath];
    [Cell.EmployeeIDTextField resignFirstResponder];
}
-(IBAction)CancelAction:(id)sender{
    [self GetCellValue];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)ClearAction:(id)sender{
    [self GetCellValue];
    isClearTapped = YES;
    [self.filterData removeAllObjects];
    [MyWorkTableView reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"iWorkClearFilter" object:nil userInfo:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)ApplyFilter:(id)sender
{
//    if (isClearTapped == YES)
//    {
//        isClearTapped = NO;
//        [self GetCellValue];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"iWorkClearFilter" object:nil userInfo:nil];
//    }
//    else
//    {
        [self GetCellValue];
        NSString *EmployeeID = @"";
        NSString *EmployeeName = @"";
        if(RadioSelect == YES){
            EmployeeName = Cell3.EmployeeIDTextField.text;
        }
        else if(RadioSelect == NO){
            EmployeeID = Cell3.EmployeeIDTextField.text;
        }
        
        NSString *Locations = @"";
        Locations = Cell4.LocationTextField.text;
        NSDictionary *params = @ {
            @"empId": EmployeeID,
            @"empName": EmployeeName,
            @"location": Locations,
            @"userId": [ApplicationState userId]
        };
        
        [defauls setValue:Locations forKey:@"iWorkLoation"];
        [defauls setValue:EmployeeID forKey:@"iWorkemp_id"];
        [defauls setValue:EmployeeName forKey:@"iWorkemp_name"];
        [defauls synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"iWorkFilter" object:nil userInfo:params];

//    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)GetCellValue{
    indexPath3 = [NSIndexPath indexPathForRow:0 inSection:0];
    Cell3 = [MyWorkTableView cellForRowAtIndexPath:indexPath3];
    [Cell3.EmployeeIDTextField resignFirstResponder];
    indexPath4 = [NSIndexPath indexPathForRow:0 inSection:1];
    Cell4 = [MyWorkTableView cellForRowAtIndexPath:indexPath4];
    [Cell4.LocationTextField resignFirstResponder];
}

@end
