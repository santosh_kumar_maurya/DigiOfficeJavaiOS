//
//  SelectionCell.m
//  iWork
//
//  Created by Shailendra on 25/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "StatusSelectionCell.h"

@implementation StatusSelectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
