//
//  RecognitionPopUpViewController.swift
//  iWork
//
//  Created by mac book pro on 18/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class RecognitionPopUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - Properties
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet var TableHight: NSLayoutConstraint!
    //@IBOutlet var InnerViewHight: NSLayoutConstraint!
    
    var responseDict : NSDictionary!
    var response: [NSDictionary]!
    // end
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateUI()
        tableView.register(UINib(nibName: "RecognitionPopUpTableViewCell", bundle: nil), forCellReuseIdentifier: "RecognitionPopUpTableViewCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if responseDict.count > 0{
            
            
            if (responseDict["name"] as? String) != nil {
                self.headerLabel.text = responseDict["name"] as? String
            }
            else {
                self.headerLabel.text = ""
            }
            
            if (responseDict["iconDp"] as? String) != nil {
                let Url = URL(string: (responseDict["iconDp"] as? String)!)
                if self.title == "Skills"{
                    self.headerImageView.sd_setImage(with: Url, placeholderImage: UIImage(named: "Skill_placeholder"))
                }else{
                    self.headerImageView.sd_setImage(with: Url, placeholderImage: UIImage(named: "Value_placeholder"))
                }
                self.response = responseDict["userData"] as? [NSDictionary]
            }
            else {
               self.headerImageView.image = UIImage(named: "")
            }
        }
    }
    //MARK: - updateUI Function is used to Update Custom UI
    func updateUI(){
        self.headerImageView.layer.cornerRadius = self.headerImageView.frame.width/2
        self.headerImageView.layer.masksToBounds = true
        self.headerImageView.backgroundColor = UIColor.white
        self.headerImageView.layer.borderWidth = 0.5;
        self.headerImageView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.innerView.layer.cornerRadius = 5
        self.innerView.layer.masksToBounds = true
        self.innerView.layer.borderWidth = 0.5;
        self.innerView.layer.borderColor =  UIColor.clear.cgColor
    }
    override func viewWillLayoutSubviews() {
//        TableHight.constant = CGFloat(40 * response.count) + 45
//        InnerViewHight.constant = CGFloat(40 * response.count) + 80
        tableView.reloadData()
    }
    //MARK: - TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(response != nil){
            if response.count > 0{
                return response.count
            }
            else{
                return 0
            }
        }
        else{
            return 0
        }
        
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 30
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RecognitionPopUpTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "RecognitionPopUpTableViewCell", for: indexPath) as! RecognitionPopUpTableViewCell
        if (self.response[indexPath.row]["userName"] as? String) != nil{
            cell.dataLabel.text = self.response[indexPath.row]["userName"] as? String
        }
        else{
           cell.dataLabel.text = ""
        }
        if (self.response[indexPath.row]["userDp"] as? String) != nil{
            let Url = URL(string: (self.response[indexPath.row]["userDp"] as? String)!)
            cell.imgView.sd_setImage(with: Url, placeholderImage: UIImage(named: "profile_image_default"))
        }
        else{
           cell.imgView.image = UIImage(named: "profile_image_default")
        }
        
        
        TableHight.constant = CGFloat(40 * response.count)
       // cell.hight.constant = CGFloat(40 * response.count)
        //TableHight.constant = CGFloat(40 * response.count) + 45
        return cell
    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 30
//    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 30))
//        let headerLabel = UILabel(frame: CGRect(x: 15, y: 2, width: self.tableView.frame.width, height: 25))
//        headerLabel.font = UIFont.systemFont(ofSize: 13)
//        headerLabel.text = "Recognitions given by"
//        view.addSubview(headerLabel)
//        return view
//    }
    @IBAction func btnCrossClicked(_ sender: Any){
        self.dismiss(animated: false, completion: nil)
    }
}
