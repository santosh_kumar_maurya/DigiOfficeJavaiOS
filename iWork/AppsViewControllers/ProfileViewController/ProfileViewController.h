//
//  ProfileViewController.h
//  iWork
//
//  Created by Fourbrick on 05/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic,strong)NSString *isComeFrom;
@property (weak, nonatomic) IBOutlet UITableView *profileTableView;

@end
