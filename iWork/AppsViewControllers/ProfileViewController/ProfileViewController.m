        //
//  ProfileViewController.m
//  iWork
//
//  Created by Fourbrick on 05/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import "ProfileViewController.h"
#import "Header.h"
#import "iWork-Swift.h"

@interface ProfileViewController ()<RecognitionTableViewCellProtocol>{
    
    AppDelegate *delegate;
    NSDictionary *params;
    APIService *Api;
    WebApiService *pmsApi;
    NSDictionary *ResponseDic;
    NSDictionary *recognitionDic;
    NSDictionary *recognitionDataDic;
    NSDictionary *popUpResponseDict;
    
    NSArray *profileOtherStaticLabelArray;
    NSArray *profileOtherImageArray;
    NSMutableArray *profileOtherDetailArray;
    NSString *strName;
    NSString *strImageName;
    NSString *strWorkLimitLabel;
    NSString *strConsumedLabel;
   
    IBOutlet UIButton *HomeBtn;
    IBOutlet UIButton *BackBtn;
    IBOutlet UIButton *LogoutBtn;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UILabel *DigiOfficeHeaderLabel;
    IBOutlet UIView *LogoutView;
    NSArray *SkillArray;
    NSArray *ValueArray;
    IBOutlet UIView *bgView;
    NSDictionary *Dic;
    IBOutlet UILabel *userNameLabel;
    IBOutlet UILabel *LocationLabel;
    IBOutlet UIImageView *profileImageView;

    
}
@end

@implementation ProfileViewController
@synthesize isComeFrom;

- (void)viewDidLoad {
    [super viewDidLoad];
    userNameLabel.text = @"";
    LocationLabel.text = @"";
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager]setEnable:NO];
    profileOtherStaticLabelArray = [NSArray arrayWithObjects:NSLocalizedString(@"EMAIL_ID", nil),NSLocalizedString(@"DEPARTMENT", nil),NSLocalizedString(@"REPORTING_MANAGER", nil),NSLocalizedString(@"CONTACT_NUMBER", nil), nil];
    
    _profileTableView.estimatedRowHeight = 50.0;
    _profileTableView.rowHeight = UITableViewAutomaticDimension;
    [_profileTableView setNeedsLayout];
    _profileTableView.backgroundColor = [UIColor whiteColor];
    
    profileOtherImageArray = [NSArray arrayWithObjects:@"ProfileMail",@"ic_department",@"ic_manager",@"ContactUs", nil];
    profileOtherDetailArray = [[NSMutableArray alloc] init];
    Api = [[APIService alloc] init];
    pmsApi = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [LoadingManager showLoadingView:self.view];
   
    //self.view.backgroundColor = delegate.BackgroudColor;
  
    //isComeFrom = @"CONTAINER";
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self registerCell];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationRecive) name:@"CONTAINER" object:nil];
   
    [self performSelector:@selector(performUserRequestData) withObject:nil afterDelay:1.0];
    
    if([isComeFrom isEqualToString:@"PMS"]){
        [HomeBtn setImage:[UIImage imageNamed:@"iPMhome"] forState:UIControlStateNormal];
    }
    else{
        [HomeBtn setImage:[UIImage imageNamed:@"HomeImage"] forState:UIControlStateNormal];
    }
    [self SetBGView];
    [self ProfileNameAndImage];
}
-(void)ProfileNameAndImage{
    
    profileImageView.layer.cornerRadius =30;
    profileImageView.clipsToBounds = YES;
    
    Dic = [ApplicationState getUserLoginData];
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]))){
        userNameLabel.text = [NSString stringWithFormat:@"%@", [[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]];
    }
    else{
        userNameLabel.text = @" ";
    }
    NSString *URL = [NSString stringWithFormat:@"%@",[[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]];
    NSLog(@"URL==%@",URL);
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]))){
        NSURL *Url = [NSURL URLWithString:URL];
        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL:Url]];
        if(images == nil){
            profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
        }
        else{
            [profileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
        }
    }
    else{
        profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
    }
}
-(void)SetBGView{
    bgView.layer.cornerRadius = 2.0;
    bgView.maskView.layer.cornerRadius = 7.0f;
    bgView.layer.shadowRadius = 3.0f;
    bgView.layer.shadowColor = [UIColor blackColor].CGColor;
    bgView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    bgView.layer.shadowOpacity = 0.7f;
    bgView.layer.masksToBounds = NO;
}
-(void)NotificationRecive{
    BackBtn.hidden = YES;
    HomeBtn.hidden = YES;
    LogoutBtn.hidden = NO;
    DigiOfficeHeaderLabel.hidden = NO;
    DigiOfficeHeaderLabel.text = NSLocalizedString(@"DIGI_OFFICE", nil);
    HeaderLabel.hidden = YES;
}

-(void)viewDidLayoutSubviews{
    _profileTableView.estimatedRowHeight = 50;
    _profileTableView.backgroundColor = [UIColor clearColor];
    _profileTableView.rowHeight = UITableViewAutomaticDimension;
}
- (void)performUserRequestData
{
    
    if(delegate.isInternetConnected)
    {
//        if([isComeFrom isEqualToString:@"PMS"]||[isComeFrom isEqualToString:@"CONTAINER"])
//        {
            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"userProfileAuth?userId=%@",[ApplicationState userId]]];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self performUserRequestData];
            }
            else{
                if([[ResponseDic valueForKey:@"statusCode"] intValue]==5)
                {
                    [self WebApiCellForRecognition];
                    [LoadingManager hideLoadingView:self.view];
                    NSDictionary *responseObject = [[ResponseDic valueForKey:@"object"] valueForKey:@"employeeDetail"];
                    if(IsSafeStringPlus(TrToString(responseObject[@"name"])))
                    {
                        strName = [NSString stringWithFormat:@"%@", responseObject[@"name"]];
                    }
                    if(IsSafeStringPlus(TrToString(responseObject[@"email_id"])))
                    {
                        [profileOtherDetailArray addObject:[NSString stringWithFormat:@"%@", responseObject[@"email_id"]]];
                    }
                    if(IsSafeStringPlus(TrToString(responseObject[@"department"])))
                    {
                        if ([responseObject[@"department"] isEqualToString:@"0"])
                        {
                            [profileOtherDetailArray addObject:@""];
                        }
                        else if ([responseObject[@"department"] isEqualToString:@"1"])
                        {
                            [profileOtherDetailArray addObject:@""];
                        }
                        else
                        {
                            [profileOtherDetailArray addObject:[NSString stringWithFormat:@"%@", responseObject[@"department"]]];
                        }
                    }
                    
                    
                    if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"lineManagerDetail"]))) {
                        [profileOtherDetailArray addObject:[NSString stringWithFormat:@"%@", [[[ResponseDic valueForKey:@"object"] valueForKey:@"lineManagerDetail"] valueForKey:@"name"]]];
                        
                        
                        
                        //                    reportingManagerLabel.text = [NSString stringWithFormat:@"%@", [[[ResponseDic valueForKey:@"object"] valueForKey:@"lineManagerDetail"] valueForKey:@"name"]];
                        //                    ReportingView.hidden = NO;
                        
                    } else {
                        [profileOtherDetailArray addObject:@""];
                        //                    reportingManagerLabel.text = [NSString stringWithFormat:@"%@", @""] ;
                        //                    ReportingView.hidden = YES;
                    }
                    //[profileOtherDetailArray addObject:@""];
                    NSString *MobileString = @"";
                    if(IsSafeStringPlus(TrToString(responseObject[@"mobile_no"])))
                    {
                        MobileString = [NSString stringWithFormat:@"%@", responseObject[@"mobile_no"]];
                    }
                    if(IsSafeStringPlus(TrToString(responseObject[@"contact2"])))
                    {
                        MobileString = [MobileString stringByAppendingString:[NSString stringWithFormat:@" | %@",responseObject[@"contact2"]]];
                    }
                    [profileOtherDetailArray addObject:MobileString];
                    
                    LocationLabel.text = [NSString stringWithFormat:@"%@",  [responseObject valueForKey:@"company"]];
                    
                    if(IsSafeStringPlus(TrToString(responseObject[@"dp"]))){
                        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:responseObject[@"dp"]]]];
                        if(images == nil){
                            strImageName = @"profile_image_default";
                            //                        profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
                        }
                        else{
                            NSString *URL = [NSString stringWithFormat:@"%@",responseObject[@"dp"]];
                            //                        [profileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
                            strImageName = URL;
                            
                        }
                    }
                    else{
                        //                    profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
                        strImageName = @"profile_image_default";
                    }
                }
                else {
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowAlert:ResponseDic[@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                }
            }
//        }
//        else
//        {
//            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"userProfileData?user_id=%@",[ApplicationState userId]]];
//            if(ResponseDic == nil){
//                [LoadingManager hideLoadingView:self.view];
//                [self performUserRequestData];
//            }
//            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
//                [LoadingManager hideLoadingView:self.view];
//                NSDictionary *responseObject = [ResponseDic valueForKey:@"object"];
//                if(IsSafeStringPlus(TrToString(responseObject[@"name"]))){
//                    strName = [NSString stringWithFormat:@"%@", responseObject[@"name"]];
//                }
//                if(IsSafeStringPlus(TrToString(responseObject[@"email_id"]))){
//                    
//                    [profileOtherDetailArray addObject:[NSString stringWithFormat:@"%@", responseObject[@"email_id"]]];
//                    
//                }
//                if(IsSafeStringPlus(TrToString(responseObject[@"department"]))) {
//                    if ([responseObject[@"department"] isEqualToString:@"0"]) {
//                        [profileOtherDetailArray addObject:[NSString stringWithFormat:@"%@", responseObject[@""]]];
//                    }
//                    else if ([responseObject[@"department"] isEqualToString:@"1"]) {
//                        
//                        [profileOtherDetailArray addObject:@""];
//                    }
//                    else {
//                        [profileOtherDetailArray addObject:[NSString stringWithFormat:@"%@", responseObject[@"department"]]];
//                    }
//                } else {
//                    
//                    [profileOtherDetailArray addObject:@""];
//                }
//                
//                if(IsSafeStringPlus(TrToString(responseObject[@"reporting_manager"]))) {
//                    if (![responseObject[@"reporting_manager"] isEqualToString:@"0"]) {
//                        
//                        [profileOtherDetailArray addObject:[NSString stringWithFormat:@"%@", responseObject[@"reporting_manager"]]];
//                        
//                        //                        reportingManagerLabel.text = [NSString stringWithFormat:@"%@", responseObject[@"reporting_manager"]];
//                        //                        ReportingView.hidden = NO;
//                    } else {
//                        [profileOtherDetailArray addObject:@""];
//                        //                        reportingManagerLabel.text = [NSString stringWithFormat:@"%@", @""] ;
//                        //                        ReportingView.hidden = YES;
//                    }
//                } else {
//                    [profileOtherDetailArray addObject:@""];
//                    //                    reportingManagerLabel.text = [NSString stringWithFormat:@"%@", @""] ;
//                    //                    ReportingView.hidden = YES;
//                }
//                [profileOtherDetailArray addObject:@" "];
//                
//                
//                if(IsSafeStringPlus(TrToString(responseObject[@"iwork_limit"]))) {
//                    NSString *Str = NSLocalizedString(@"I_WORK_LIMIT", nil);
//                   // Str = [Str stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
//                    strWorkLimitLabel = [NSString stringWithFormat:@"%@ %d",Str, [responseObject[@"iwork_limit"]intValue]];
//                } else {
//                    strWorkLimitLabel = @"" ;
//                }
//                
//                if(IsSafeStringPlus(TrToString(responseObject[@"comsumed"]))){
//                    strConsumedLabel = [NSString stringWithFormat:@"Used : %@", responseObject[@"comsumed"]];
//                }
//                else {
//                    strConsumedLabel = @" ";
//                }
//                if(IsSafeStringPlus(TrToString(responseObject[@"dp"]))){
//                    UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:responseObject[@"dp"]]]];
//                    if(images == nil){
//                        strImageName = @"profile_image_default";
//                        //                        profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
//                    }
//                    else{
//                        NSString *URL = [NSString stringWithFormat:@"%@",responseObject[@"dp"]];
//                        //                        [profileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
//                        strImageName = URL;
//                    }
//                }
//                else{
//                    strImageName = @"profile_image_default";
//                    //                    profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
//                }
//            }
//            else {
//                [LoadingManager hideLoadingView:self.view];
//                [self ShowAlert:ResponseDic[@"message"] ButtonTitle:NSLocalizedString(@"OKAY", nil)];
//            }
//        }
        [self.profileTableView reloadData];
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}

- (IBAction)HomeAction:(id)sender {
//    if([isComeFrom isEqualToString:@"PMS"]){
//        
//    }
//    else{
//        
//    }
    NSArray *array = [self.navigationController viewControllers];
    for (int i = 0 ; i < array.count; i++) {
        UIViewController *ViewController = [array objectAtIndex:i];
        if([ViewController isKindOfClass:[ContainerParentViewController class]]){
            [self.navigationController popToViewController:ViewController animated:YES];
        }
    }
}
-(IBAction)backButtonDidClicked:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}


//MARK:- UITableView Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if([isComeFrom isEqualToString:@"PMS"]||[isComeFrom isEqualToString:@"CONTAINER"])
//    {
//        if(SkillArray.count == 0 && ValueArray.count == 0){
//          return 4;
//        }
//         return 5;
//    }
//    else
//    {
//        return 4;
//    }
    return 4;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row == 0){
//        static NSString *CellIdentifier = @"ProfileDetailTableViewCell";
//        ProfileDetailTableViewCell *cell = (ProfileDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//        if (cell == nil) {
//            cell = [[ProfileDetailTableViewCell alloc] initWithFrame:CGRectZero];
//        }
//        if([isComeFrom isEqualToString:@"PMS"]||[isComeFrom isEqualToString:@"CONTAINER"]){
//            cell.WelcomeStaticLabel.text = NSLocalizedString(@"WELCOME_TO_INDOSAT", nil);
//            cell.workLimitLabel.hidden = YES;
//            cell.consumedLabel.hidden = YES;
//        }
//        else{
//            cell.workLimitLabel.hidden = NO;
//            cell.consumedLabel.hidden = NO;
//            cell.WelcomeStaticLabel.text = NSLocalizedString(@"WELCOME_TO_I_WORK", nil);
//            cell.WelcomeStaticLabel.text = [cell.WelcomeStaticLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
//            cell.workLimitLabel.text = strWorkLimitLabel;
//            cell.consumedLabel.text = strConsumedLabel;
//        }
//        
//        if ([strImageName isEqualToString:@"profile_image_default"]){
//            cell.profileImageView.image = [UIImage imageNamed:strImageName];
//        }
//        else{
//            [cell.profileImageView  sd_setImageWithURL:[NSURL URLWithString:strImageName]];
//        }
//        
//        cell.nameLabel.text = strName;
//        
//        
//        return cell;
//    }
//    else if (indexPath.row  == 4){
//        static NSString *CellIdentifier = @"RecognitionTableViewCell";
//        RecognitionTableViewCell *cell = (RecognitionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//
//        if (cell == nil) {
//            cell = [[RecognitionTableViewCell alloc] initWithFrame:CGRectZero];
//        }
//        cell.delegate = self;
//        cell.cellType = @"Profile";
//        if(recognitionDataDic.count>0){
//            [cell configureCellWithDataWithDict:recognitionDataDic];
//        }
//        return cell;
//    }
//    
//    else{
        static NSString *CellIdentifier = @"ProfileOtherDetailTableViewCell";
        ProfileOtherDetailTableViewCell *cell = (ProfileOtherDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[ProfileOtherDetailTableViewCell alloc] initWithFrame:CGRectZero];
        }
        cell.detailImageView.image = [UIImage imageNamed:profileOtherImageArray[indexPath.row]];
        cell.detailStaticLabel.text = profileOtherStaticLabelArray[indexPath.row];
        if (profileOtherDetailArray.count > 0){
            cell.detailValueLabel.text = profileOtherDetailArray[indexPath.row];
        }
        
        return cell;
    //}
}

-(void)registerCell{
    
    UINib *profileNib = [UINib nibWithNibName:@"ProfileDetailTableViewCell" bundle:nil];
    [_profileTableView registerNib:profileNib forCellReuseIdentifier:@"ProfileDetailTableViewCell"];
    
    UINib *otherProfileNib = [UINib nibWithNibName:@"ProfileOtherDetailTableViewCell" bundle:nil];
    [_profileTableView registerNib:otherProfileNib forCellReuseIdentifier:@"ProfileOtherDetailTableViewCell"];
    
    UINib *recognitionNib = [UINib nibWithNibName:@"RecognitionTableViewCell" bundle:nil];
    [_profileTableView registerNib:recognitionNib forCellReuseIdentifier:@"RecognitionTableViewCell"];
    
}

//MARK: - RecognitionTableViewCellProtocol delegate

-(void)cellTappedWithSelectedIndex:(NSInteger)selectedIndex selectedCell:(NSInteger)selectedCell{

    [self callWebServiceForIndex:selectedIndex selectedCell:selectedCell];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RecognitionPopUpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RecognitionPopUpViewController"];
    if(popUpResponseDict.count>0){
        vc.responseDict = popUpResponseDict;
        if (selectedIndex == 0){
            vc.title = @"Skills";
           
        }
        else{
            vc.title = @"Value";
        }
        vc.providesPresentationContextTransitionStyle = YES;
        vc.definesPresentationContext = YES;
        [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        [self presentViewController:vc animated:false completion:nil];
    }
}

// end

-(void)callWebServiceForIndex:(NSInteger)index selectedCell:(NSUInteger)CellIndex{
    if (index == 0){
        SkillArray = [recognitionDataDic valueForKey:@"skillsData"];
        NSNumber *IdNumber = [[SkillArray objectAtIndex:CellIndex] valueForKey:@"id"];
        [self callWebServiceForSkills:[NSString stringWithFormat:@"%@", IdNumber]];
    }
    else{
        ValueArray = [recognitionDataDic valueForKey:@"valuesData"];
        NSNumber *IdNumber = [[ValueArray objectAtIndex:CellIndex] valueForKey:@"id"];
        [self callWebServiceForValues:[NSString stringWithFormat:@"%@", IdNumber]];
    }
}

-(void)callWebServiceForSkills:(NSString*)idStr{
   
    if(delegate.isInternetConnected)
    {
        NSDictionary * dict = [pmsApi WebApi:nil Url:[NSString stringWithFormat:@"getSkillUsersData?id=%@",idStr]];
        if(([[dict valueForKey:@"statusCode"] intValue]==5) || ([[dict valueForKey:@"statusCode"] intValue]==600))
        {
            NSDictionary *responseObject = [dict valueForKey:@"object"];
            popUpResponseDict = responseObject;
            [LoadingManager hideLoadingView:self.view];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}

-(void)callWebServiceForValues:(NSString*)id
{
    if(delegate.isInternetConnected)
    {
       
        NSDictionary * dict = [pmsApi WebApi:nil Url:[NSString stringWithFormat:@"getValueUsersData?id=%@",id]];
        if(([[dict valueForKey:@"statusCode"] intValue]==5) || ([[dict valueForKey:@"statusCode"] intValue]==601))
        {
            NSDictionary *responseObject = [dict valueForKey:@"object"];
            popUpResponseDict = responseObject;
            [LoadingManager hideLoadingView:self.view];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}



- (void)WebApiCellForRecognition
{
    if(delegate.isInternetConnected)
    {
        recognitionDic = [pmsApi WebApi:nil Url:[NSString stringWithFormat:@"getRecongnitions?userId=%@",[ApplicationState userId]]];
        if(([[recognitionDic valueForKey:@"statusCode"] intValue]==5) || ([[recognitionDic valueForKey:@"statusCode"] intValue]==602))
        {
            NSDictionary *responseObject = [recognitionDic valueForKey:@"object"];
            if(IsSafeStringPlus(TrToString(responseObject[@"recognitionData"])))
            {
                NSDictionary *recognitionData = [responseObject valueForKey:@"recognitionData"];
                recognitionDataDic = recognitionData;
                SkillArray = [recognitionDataDic valueForKey:@"skillsData"];
                ValueArray = [recognitionDataDic valueForKey:@"valuesData"];
            }
            [LoadingManager hideLoadingView:self.view];
        }
        [_profileTableView reloadData];
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (IBAction)LogoutAction {
    LogoutView.hidden = NO;
}
- (IBAction)CancelAction {
    LogoutView.hidden = YES;
}

- (IBAction)YesAction {
    [delegate DeleteNotificationCounter:@"IWORK"];
    [delegate DeleteNotificationCounter:@"IPM"];
    [ApplicationState setUserIsLoggedOut];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isLoggedIn"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Key"];
    SignInViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}
- (BOOL)prefersStatusBarHidden {
    return NO;
}


@end
