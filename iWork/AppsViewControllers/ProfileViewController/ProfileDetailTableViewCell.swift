//
//  ProfileDetailTableViewCell.swift
//  iWork
//
//  Created by mac book pro on 15/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit
import Foundation

class ProfileDetailTableViewCell: UITableViewCell {

    //MARK: - Properties
    @IBOutlet weak var ProfileView: UIView!
    @IBOutlet weak var WelcomeStaticLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var consumedLabel: UILabel!
    @IBOutlet weak var workLimitLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    //end
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.updateUI()
    }
    //MARK: - updateUI Function is used to Update Custom UI
    func updateUI(){
        self.profileImageView.layer.cornerRadius = 30.0
        self.profileImageView.layer.masksToBounds = true
        self.workLimitLabel.text = ""
        self.consumedLabel.text = ""
        self.nameLabel.text = ""
        
        self.ProfileView.layer.cornerRadius = 5
        self.ProfileView.layer.masksToBounds = true
        self.ProfileView.backgroundColor = UIColor.white
        self.ProfileView.layer.borderWidth = 0.5;
        self.ProfileView.layer.borderColor = UIColor.lightGray.cgColor
    }
    //MARK: - ConfigureCellWithData Function is to Populate data on UITableViewCell
    func configureCellWithData(info: Dictionary<String,String>){
        
    }
}
