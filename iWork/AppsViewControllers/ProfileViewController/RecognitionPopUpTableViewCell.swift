//
//  RecognitionPopUpTableViewCell.swift
//  iWork
//
//  Created by mac book pro on 19/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class RecognitionPopUpTableViewCell: UITableViewCell {

    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    //@IBOutlet var hight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }    
}
