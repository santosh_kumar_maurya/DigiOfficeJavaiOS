//
//  ProfileOtherDetailTableViewCell.swift
//  iWork
//
//  Created by mac book pro on 15/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit
import Foundation

class ProfileOtherDetailTableViewCell: UITableViewCell {

    //MARK : - Properties
    @IBOutlet weak var detailValueLabel: UILabel!
    @IBOutlet weak var detailStaticLabel: UILabel!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var ProfileView: UIView!
    // end
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func configureCellWithData(info: Dictionary<String,String>){
    }
}
