//
//  RecognitionTableViewCell.swift
//  iWork
//
//  Created by mac book pro on 18/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

@objc public protocol RecognitionTableViewCellProtocol {
    @objc func cellTapped(selectedIndex: Int, selectedCell: Int)
}

class RecognitionTableViewCell: UITableViewCell, SJSegmentedViewControllerDelegate,RecognitionViewCProtocol {


    @IBOutlet var RecoginitionStaticLabel: UILabel!
    @IBOutlet var segmentView: UIView!
    @IBOutlet var bgView:UIView!
    var selectedSegment: SJSegmentTab?
    var segmentController = SJSegmentedViewController()
    var delegate : RecognitionTableViewCellProtocol?
    var responseDict : NSDictionary!
    var selectedIndex : Int = 0
    var Api : WebApiService = WebApiService()
    var responseCellDict: NSDictionary!
    var appDelegate : AppDelegate!
    var cellType: String!
    @IBOutlet var NoRecordLabel : UILabel!
    @IBOutlet var NoRecordImageView : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.appDelegate = UIApplication.shared.delegate as? AppDelegate
        RecoginitionStaticLabel.textColor = UIColor.TextBlueColor()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int){
        self.selectedIndex = index
    }
    func didSelectSegmentAtIndex(_ index: Int) {
        self.selectedIndex = index
    }
    func configureCell(){
        
        self.bgView.layer.cornerRadius = 2
        self.bgView.backgroundColor = UIColor.white
        self.bgView.layer.borderWidth = 0.5
        self.bgView.layer.borderColor = UIColor.lightGray.cgColor
        if self.cellType == "Line"{
            self.bgView.layer.cornerRadius = 2.0
            self.bgView.mask?.layer.cornerRadius = 7.0
            self.bgView.layer.shadowRadius = 3.0
            self.bgView.layer.shadowColor = UIColor.black.cgColor
            self.bgView.layer.shadowOffset = CGSize(width: -1, height: 2)
            self.bgView.layer.shadowOpacity = 0.7
            self.bgView.layer.masksToBounds = false
        }
        else{
            
        }
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let firstViewController = storyBoard.instantiateViewController(withIdentifier: "RecognitionViewController") as! RecognitionViewController
        firstViewController.model = self.responseDict["skillsData"] as? [NSDictionary]
        firstViewController.delegate = self
        firstViewController.title = "Skills"

        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "RecognitionViewController") as! RecognitionViewController
        secondViewController.model = self.responseDict["valuesData"] as? [NSDictionary]
        secondViewController.delegate = self
        secondViewController.title = "Values"
        segmentController.segmentControllers = [firstViewController,
                                                secondViewController]
        segmentController.selectedSegmentViewHeight = 2
        segmentController.selectedSegmentViewColor = UIColor(red: 237.0/255, green: 61.0/255, blue: 47.0/255, alpha: 1.0)
        self.segmentView.addSubview(segmentController.view)
        segmentController.segmentShadow = SJShadow.light()
        segmentController.view.frame = self.segmentView.bounds
        segmentController.delegate = self

    }
    func cellTapped(index: Int){
        self.delegate?.cellTapped(selectedIndex: selectedIndex, selectedCell: index)
    }
    func configureCellWithData(dict: NSDictionary!){
        responseDict = dict
        self.configureCell()
    }
}
