//
//  RecognitionViewController.swift
//  iWork
//
//  Created by mac book pro on 18/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

protocol RecognitionViewCProtocol {
    func cellTapped(index: Int)
}

class RecognitionViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    var delegate : RecognitionViewCProtocol?
    var model: [NSDictionary]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 150;
        tableView.rowHeight = UITableViewAutomaticDimension;
        tableView.register(UINib(nibName: "RecognitionChildTableViewCell", bundle: nil), forCellReuseIdentifier: "RecognitionChildTableViewCell")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if model.count > 0{
            return model.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecognitionChildTableViewCell", for: indexPath) as! RecognitionChildTableViewCell
        cell.dataLabel.text = model[indexPath.row]["title"] as? String
        //cell.btnCount.setTitle("\(model[indexPath.row]["count"]!)", for: UIControlState.normal)
        cell.CountLabel.text = "\(model[indexPath.row]["count"]!)"
        if self.title == "Skills"{
            let Urls = URL(string: (model[indexPath.row]["skillDp"] as? String)!)
            cell.imgView.sd_setImage(with: Urls!, placeholderImage: UIImage(named: "Skill_placeholder"))
        }else{
             let Urls = URL(string: (model[indexPath.row]["valueDp"] as? String)!)
            cell.imgView.sd_setImage(with: Urls!, placeholderImage: UIImage(named: "Value_placeholder"))
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.delegate?.cellTapped(index: indexPath.row)
    }

}
