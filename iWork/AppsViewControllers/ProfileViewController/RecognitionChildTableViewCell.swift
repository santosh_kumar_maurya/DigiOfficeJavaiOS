//
//  RecognitionChildTableViewCell.swift
//  iWork
//
//  Created by mac book pro on 18/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class RecognitionChildTableViewCell: UITableViewCell {

    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var CountLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configureCell()
    }
    func configureCell(){
        self.imgView.layer.cornerRadius = 4
        self.imgView.layer.masksToBounds = true
        self.CountLabel.layer.cornerRadius = 1
        self.CountLabel.layer.masksToBounds = true
        self.CountLabel.backgroundColor = UIColor.ButtonSkyColor()
    }
    
}
