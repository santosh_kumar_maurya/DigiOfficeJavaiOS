//
//  LineManageApprovedViewController.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LineManageApprovedViewController.h"
#import "ApprovedCell.h"
#import "Header.h"

@interface LineManageApprovedViewController ()
{
    IBOutlet UITableView *ApprovedTableView;
    NSMutableArray *ApprovedArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSString *cortex;
    NSString *user_requestsId;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    
    APIService *Api;
    NSDictionary *ResponseDic;
    NSString *requestsId;
    int offset;
    int limit;
    int TagValue;
    IBOutlet UIImageView *NoDataImageView;
}
@end

@implementation LineManageApprovedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CheckValue = @"APPROVED";
    [self EmptyArray];
    TagValue = 0;
    Api = [[APIService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor whiteColor];
     ApprovedTableView.backgroundColor = [UIColor whiteColor];
    ApprovedTableView.estimatedRowHeight = 500;
    ApprovedTableView.rowHeight = UITableViewAutomaticDimension;
    ApprovedTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, ApprovedTableView.bounds.size.width, 0.0f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [ApprovedTableView addSubview:refreshControl];
    
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkLineManagerApproved) withObject:nil afterDelay:0.5];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ApprovedApi:) name:@"LineManagerApproved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClearFilterApi:) name:@"LMClearFilterApi" object:nil];
    UINib *reportNib = [UINib nibWithNibName:@"ReportsTableViewCell" bundle:nil];
    [ApprovedTableView registerNib:reportNib forCellReuseIdentifier:@"ReportsTableViewCell"];
    
    
    // Do any additional setup after loading the view.
}
-(void)EmptyArray{
    offset = 0;
    limit = 10;
    ApprovedArray = [[NSMutableArray alloc]init];
}
- (void)reloadData{
    if ([CheckValue isEqualToString:@"FILTER"])
    {
        CheckValue = @"FILTER";
    }
    else{
        CheckValue = @"Refresh";
    }
    [self EmptyArray];
    [self GetiWorkLineManagerApproved];
}
#pragma mark  -UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [ApprovedArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ReportsTableViewCell * projectCell = (ReportsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ReportsTableViewCell" forIndexPath:indexPath];
    projectCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [projectCell.ViewDetailsBtn addTarget:self action:@selector(GotoMapView:) forControlEvents:UIControlEventTouchUpInside];
    projectCell.ViewDetailsBtn.tag = indexPath.section;
    
    projectCell.tag  = indexPath.section;
    UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
    [projectCell  setUserInteractionEnabled:YES];
    [projectCell addGestureRecognizer:RequestIWork];
    if(ApprovedArray.count>0){
        [projectCell configureReportCell:ApprovedArray[indexPath.section]];
    }
    if(indexPath.section == 0){
        projectCell.UpperLineLabel.hidden = YES;
    }
    else{
        projectCell.UpperLineLabel.hidden = NO;
    }
    return projectCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
    if(ArraysValue.count>0){
        offset = offset + 10;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self GetiWorkLineManagerApproved];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [ApprovedArray addObject:BindDataDic];
    }
}
-(void)MapPage{
    NSDictionary *Dic = [ApprovedArray objectAtIndex:TagValue];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
    ObjViewController.MapDic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GotoMapView:(UIButton*)Sender{
    TagValue = Sender.tag;
    [self MapPage];
}
-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    TagValue = sender.view.tag;
    [self MapPage];
}
- (void)GetiWorkLineManagerApproved {
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"APPROVED"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"status": @1,
                    @"userId": [ApplicationState userId],
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"viewTeamReport"];
            [refreshControl endRefreshing];
            [LoadingManager hideLoadingView:self.view];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self GetiWorkLineManagerApproved];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                NSLog(@"ApprovedApprovedApprovedApproved");
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
                if(ResponseArrays.count>0){
                    NoDataLabel.hidden = YES;
                     NoDataImageView.hidden = YES
                    ;
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"]];
                    [ApprovedTableView  reloadData];
                }
                else if(ApprovedArray.count == 0){
                    [self NoIworkRequest];
                }
            }
            else{
                if(ApprovedArray.count == 0){
                    [self NoIworkRequest];
                }
            }
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoIworkRequest{
     NoDataImageView.hidden = NO;
    NoDataLabel.hidden = NO;
   NoDataLabel.text = NSLocalizedString(@"NO_IWORK_REQUEST_FOUND", nil);
//    NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
     [ApprovedTableView  reloadData];
}
-(void)ApprovedApi:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [self GetiWorkLineManagerApproved];
}
-(void)ClearFilterApi:(NSNotification*)notification
{
    [self EmptyArray];
    CheckValue = @"APPROVED";
    [LoadingManager showLoadingView:self.view];
    [self GetiWorkLineManagerApproved];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}


@end
