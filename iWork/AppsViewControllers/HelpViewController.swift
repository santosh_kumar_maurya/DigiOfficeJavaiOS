//
//  HelpViewController.swift
//  iWork
//
//  Created by Shailendra on 05/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController,UITextViewDelegate {

    @IBOutlet var BgView : UIView!
    @IBOutlet var SubmitBtn : UIButton!
    @IBOutlet var CancelBtn : UIButton!
    @IBOutlet var HelpTextView : UITextView!
    @IBOutlet var LogoutView : UIView!
    @IBOutlet var HeaderLabel : UILabel!
    var ResponseDic : NSDictionary!
    var Api: APIService =  APIService()
    var delegate = AppDelegate()
    
    var cornerRadius: CGFloat = 2
    var shadowOffsetWidth: Int = 0
    var shadowOffsetHeight: Int = 2
    var shadowColor: UIColor? = UIColor.black
    var shadowOpacity: Float = 0.4
    var maxLength = 500
    var newString : NSString! = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HeaderLabel.text =  "\(NSLocalizedString("DIGI_OFFICE", comment: "") as NSString)"
        
        delegate = UIApplication.shared.delegate as! AppDelegate
        self.view.backgroundColor = delegate.backgroudColor;
        HelpTextView.delegate = self
        BgViewSubviews()
        let notificationName = Notification.Name("CONTAINER")
        NotificationCenter.default.addObserver(self, selector: #selector(HelpViewController.NotificationRecive), name: notificationName, object: nil)
    }
    
    func BgViewSubviews() {
        
        BgView.layer.cornerRadius = 6.0
        BgView.mask?.layer.cornerRadius = 7.0
        BgView.layer.shadowRadius = 3.0;
        BgView.layer.shadowColor = UIColor.black.cgColor
        BgView.layer.shadowOffset = CGSize(width: -1.0, height: 2.0)
        BgView.layer.shadowOpacity = 0.7
        BgView.layer.masksToBounds = false
        SubmitBtn.layer.cornerRadius = 20.0
        CancelBtn.layer.cornerRadius = 20.0
        HelpTextView.textColor = UIColor.lightGray
    }
    @IBAction func LogoutAction(){
        LogoutView.isHidden = false
    }
    @IBAction func NoAction(){
        LogoutView.isHidden = true
    }
    override var prefersStatusBarHidden : Bool {
        return false
    }
    @IBAction func YesAction(){
        
        let userDefault = UserDefaults.standard
        userDefault.removeObject(forKey: "isLoggedIn")
        userDefault.removeObject(forKey: "Key")
        delegate.deleteNotificationCounter("IPM")
        delegate.deleteNotificationCounter("IWORK")
        ApplicationState.setUserIsLoggedOut()
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    @IBAction func SubmitAction(){
        keyBoardHide()
        if(HelpTextView.text == "" || HelpTextView.text == "Please share your thoughts here"){
            ShowAlert(MsgTitle: "Please share your thoughts here.", BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
        else{
           self.perform(#selector(HelpViewController.HelpWebApi), with: nil, afterDelay: 0.5)
        }
    }
    
    func HelpWebApi(){
        if (delegate.isInternetConnected()){
            let param : NSDictionary! = ["feedback": "\(HelpTextView.text!)"]
            ResponseDic = Api.webApi(param as! [AnyHashable : Any]!, url: "help") as NSDictionary!
            if ResponseDic != nil{
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    LoadingManager.hideLoading(view)
                    HelpTextView.text = "Please share your thoughts here"
                    HelpTextView.textColor = UIColor.lightGray
                    ShowAlert(MsgTitle: "\(String(describing: ResponseDic.value(forKey: "message")!))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: ResponseDic.value(forKey: "message")!))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(HelpViewController.HelpWebApi), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    @IBAction func CancelAction(){
        keyBoardHide()
        HelpTextView.text = "Please share your thoughts here"
        HelpTextView.textColor = UIColor.lightGray
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: true, completion: nil)
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        if(numberOfChars == maxLength){
           self.ShowAlert(MsgTitle: "Characters limit Exceed", BtnTitle: "OK")
        }
        return numberOfChars < maxLength
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(HelpTextView.text == "Please share your thoughts here"){
            HelpTextView.text = ""
            HelpTextView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Please share your thoughts here"
            textView.textColor = UIColor.lightGray
        }
    }
    func NotificationRecive(){
        HelpTextView.resignFirstResponder()

    }
    override func viewWillDisappear(_ animated: Bool) {
        HelpTextView.resignFirstResponder()
    }
    @IBAction func keyBoardHide(){
       HelpTextView.resignFirstResponder()
    }
}
