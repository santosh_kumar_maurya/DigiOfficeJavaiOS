//
//  EmployeeDetailsCell.m
//  iWork
//
//  Created by Shailendra on 18/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeDetailsCell.h"
#import "Header.h"
#import "iWork-Swift.h"

@implementation EmployeeDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius =6;
    self.layer.masksToBounds =YES;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 0.5;
}

- (void)configureCell:(NSDictionary *)info {
    
    _RequestIdLabel.text = @"";
    if(IsSafeStringPlus(TrToString(info[@"requestId"]))) {
        _RequestIdLabel.text = [NSString stringWithFormat:@"%@",info[@"requestId"]];
    } else {
        _RequestIdLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"dateTime"]))) {
        NSNumber *datenumber = info[@"dateTime"];
        _RequestDateLabel.text = [self DateFormateChange:datenumber];
        
    } else {
        _RequestDateLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"userId"]))) {
        _EmployeeIDLabel.text = info[@"userId"];
    } else {
        _EmployeeIDLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"userName"]))) {
        _EmployeeNameLabel.text = info[@"userName"];
    } else {
        _EmployeeNameLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"task"]))) {
        _TaskLabel.text = info[@"task"];
    } else {
        _TaskLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"requestDate"]))) {
        NSNumber *datenumber = info[@"requestDate"];
        _iWorkDateLabel.text = [self DateFormateChange:datenumber];
    } else {
        _iWorkDateLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"actionDate"]))) {
        NSNumber *datenumber = info[@"actionDate"];
        _ActionDateLabel.text = [self DateFormateChange:datenumber];
    } else {
        _ActionDateLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"location"]))) {
        _iWorkLocationLabel.text = info[@"location"];
    } else {
        _iWorkLocationLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"locType"]))) {
        _iWorkLocationTypeLabel.text = info[@"locType"];
    } else {
        _iWorkLocationTypeLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"checkInTime"]))) {
        NSNumber *Timenumber = info[@"checkInTime"];
        _CheckInLabel.text = [self GetDate:Timenumber];
    } else {
        _CheckInLabel.text = @"- -:- -";
    }
    if(IsSafeStringPlus(TrToString(info[@"checkOutTime"]))) {
        NSNumber *Timenumber = info[@"checkOutTime"];
        _CheckoutLabel.text = [self GetDate:Timenumber];
    } else {
        _CheckoutLabel.text = @"- -:- -";
    }
    _HoursLabel.text = @" ";
    if(IsSafeStringPlus(TrToString(info[@"hour"]))) {
        _HoursLabel.text = info[@"hour"];
    }
    else{
        _HoursLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"maxCheckInTime"]))) {
        _MaxCheckInLabel.text = [self MaxDate:info[@"maxCheckInTime"]];
    }else{
        _MaxCheckInLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"attendanceStatus"]))) {
        _AttandenceStatusLabel.text = info[@"attendanceStatus"];
        if([_AttandenceStatusLabel.text isEqualToString:@"-"]){
            _AttandenceStatusLabel.text = @"";
        }
    }else{
        _AttandenceStatusLabel.text = @"";
    }
    
    
   
    if(IsSafeStringPlus(TrToString(info[@"status"]))) {
        
        if ([info[@"status"] isEqualToString:@"PENDING"]) {
            _StatusLabel.textColor = [UIColor colorWithRed:1.0 green:0.83 blue:00.0 alpha:1.0];
            
            _StatusLabel.text = @"New";
            _StatusImageView.image = [UIImage imageNamed:@"NewStatus"];
            
            _CheckInLabel.hidden = YES;
            _CheckoutLabel.hidden = YES;
            _HoursLabel.hidden = YES;
            _AttandenceStatusLabel.hidden = YES;
            
        } else if ([info[@"status"] isEqualToString:@"APPROVED"]) {
            
            _StatusLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:88.0/255.0 blue:45.0/255.0 alpha:1.0];
            _StatusLabel.text = @"Approved";
            _StatusImageView.image = [UIImage imageNamed:@"Approved"];
//            if(([_AttandenceStatusLabel.text isEqualToString:@"Present"]) || [_AttandenceStatusLabel.text isEqualToString:@"Absent"]){
//                _CheckInLabel.hidden = NO;
//                _CheckoutLabel.hidden = NO;
//                _HoursLabel.hidden = NO;
//                _AttandenceStatusLabel.hidden = NO;
//                _ActionDateStaticLabel.text = @"";
//            }
//            else{
//                _CheckInLabel.hidden = YES;
//                _CheckoutLabel.hidden = YES;
//                _HoursLabel.hidden = YES;
//                _AttandenceStatusLabel.hidden = YES;
//            }
            
        }
        else if ([info[@"status"] isEqualToString:@"REJECTED"]) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _StatusLabel.text = @"Rejected";
            _StatusImageView.image = [UIImage imageNamed:@"Rejected"];
//            
//            _CheckInLabel.hidden = YES;
//            _CheckoutLabel.hidden = YES;
//            _HoursLabel.hidden = YES;
//            _AttandenceStatusLabel.hidden = YES;
            
        }
        else if ([info[@"status"] isEqualToString:@"CANCELLED"] ) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _StatusLabel.text = @"Cancelled";
            _StatusImageView.image = [UIImage imageNamed:@"CancelledStatus"];
            
//            _CheckInLabel.hidden = YES;
//            _CheckoutLabel.hidden = YES;
//            _HoursLabel.hidden = YES;
//            _AttandenceStatusLabel.hidden = YES;
//            
        }
        else if ([info[@"status"] isEqualToString:@"DISCARDED"]) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _StatusLabel.text = @"Discarded";
            _StatusImageView.image = [UIImage imageNamed:@"DiscardedStatus"];
//            
//            _CheckInLabel.hidden = YES;
//            _CheckoutLabel.hidden = YES;
//            _HoursLabel.hidden = YES;
//            _AttandenceStatusLabel.hidden = YES;
            
        }
        else if ([info[@"status"]isEqualToString:@"DECLINED"] ) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _StatusLabel.text = @"Declined";
            _StatusImageView.image = [UIImage imageNamed:@"DeclinedStatus"];
            
//            _CheckInLabel.hidden = YES;
//            _CheckoutLabel.hidden = YES;
//            _HoursLabel.hidden = YES;
//            _AttandenceStatusLabel.hidden = YES;
        }
    }
    
    
}

-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)GetDate:(NSNumber*)Timenumber{
    NSString *DateStr = [NSString stringWithFormat:@"%@",Timenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"HH:mm:ss"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"hh:mm a"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)MaxDate:(NSString*)DateString{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString: DateString];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *convertedString = [dateFormatter stringFromDate:date];
    return convertedString;
}

@end
