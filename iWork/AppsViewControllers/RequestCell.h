//
//  RequestCell.h
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestCell : UITableViewCell{
    
}

@property (strong, nonatomic) IBOutlet UIView *iWorkView;
@property (strong, nonatomic) IBOutlet UIImageView *iWorkImageView;
@property (strong, nonatomic) IBOutlet UIView *RequestView;
@property (strong, nonatomic) IBOutlet UILabel *RequestIWorkLabel;
@property (strong, nonatomic) IBOutlet UILabel *ViewRequestLabel;
@end
