//
//  EmployeeCell.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeCell.h"

@implementation EmployeeCell

- (void)awakeFromNib {
    [super awakeFromNib];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)configureCell:(UIColor *)color backGround:(UIColor *)backGroundColor
{
    self.YellowView.layer.cornerRadius = self.YellowView.frame.size.height/2;
    self.YellowView.backgroundColor = backGroundColor;
    self.YellowView.layer.borderWidth = 2.0;
    self.YellowView.layer.borderColor = color.CGColor;
    self.YellowView.clipsToBounds = YES;

}
@end
