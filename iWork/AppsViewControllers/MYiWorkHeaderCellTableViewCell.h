//
//  MYiWorkHeaderCellTableViewCell.h
//  iWork
//
//  Created by mac book pro on 05/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MYiWorkHeaderCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end
