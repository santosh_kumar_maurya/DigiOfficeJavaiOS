//
//  MyTeamHeaderViewTableViewCell.h
//  iWork
//
//  Created by mac book pro on 09/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTeamHeaderViewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@end
