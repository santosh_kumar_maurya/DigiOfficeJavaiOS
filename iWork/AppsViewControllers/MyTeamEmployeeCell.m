//
//  MyTeamEmployeeCell.m
//  iWork
//
//  Created by Shailendra on 21/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyTeamEmployeeCell.h"

@implementation MyTeamEmployeeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.TextFieldBG.layer.borderColor = [UIColor colorWithRed:230/255.0 green:229/255.0 blue:230/255.0 alpha:1].CGColor;
    self.TextFieldBG.layer.cornerRadius = 5;
    self.TextFieldBG.layer.borderWidth = 0.5;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
