//
//  LMViewRequestApproveTableViewCell.h
//  iWork
//
//  Created by Himanshu  Goyal on 01/11/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

@interface LMViewRequestApproveTableViewCell : UITableViewCell{
    AppDelegate *delegate;
}
@property (weak, nonatomic) IBOutlet UILabel *employeeId;
@property (weak, nonatomic) IBOutlet UILabel *employeeName;
@property (weak, nonatomic) IBOutlet UILabel *requestId;
@property (weak, nonatomic) IBOutlet UILabel *requestDate;
@property (weak, nonatomic) IBOutlet UILabel *EmployeeIDStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *EmployeeNameStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *iworkDate;
@property (weak, nonatomic) IBOutlet UILabel *sepratorLabel;
@property (weak, nonatomic) IBOutlet UILabel *location;
@property (weak, nonatomic) IBOutlet UIButton *approveButton;
@property (weak, nonatomic) IBOutlet UIButton *rejectButton;
@property (weak, nonatomic) IBOutlet UILabel *TaskLabel;
@property (weak, nonatomic) IBOutlet UILabel *iWorkLocationTypeStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *iWorkLocationTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *MaxCheckInStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *MaxCheckInLabel;
@property (strong, nonatomic) IBOutlet UILabel *UpperLineLabel;
@property (strong, nonatomic) IBOutlet UILabel *CercilLabel;
@property (weak, nonatomic) IBOutlet UILabel *StatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *StatusStaticLabel;
- (void)configureApprovalCell:(NSDictionary *)info;

@end
