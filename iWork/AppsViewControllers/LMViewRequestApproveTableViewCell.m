//
//  LMViewRequestApproveTableViewCell.m
//  iWork
//
//  Created by Himanshu  Goyal on 01/11/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LMViewRequestApproveTableViewCell.h"
#import "Shared.h"
#import "iWork-Swift.h"

@implementation LMViewRequestApproveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.CercilLabel.layer.cornerRadius = self.CercilLabel.frame.size.width/2;
    self.CercilLabel.clipsToBounds = YES;
    
    self.approveButton.layer.cornerRadius = 17;
    self.approveButton.clipsToBounds = YES;
    
    self.rejectButton.layer.borderWidth = 1.0;
    self.rejectButton.layer.borderColor =  delegate.redColor.CGColor;
    self.rejectButton.layer.cornerRadius = 17;
    self.rejectButton.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)configureApprovalCell:(NSDictionary *)info {
    
    if(IsSafeStringPlus(TrToString(info[@"userId"]))) {
        _employeeId.text = [NSString stringWithFormat:@"%@",info[@"userId"]];
    } else {
        _employeeId.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"userName"]))) {
        _employeeName.text = info[@"userName"];
    } else {
        _employeeName.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"requestId"]))) {
        _requestId.text = [NSString stringWithFormat:@"%@",info[@"requestId"]];
        
    } else {
        _requestId.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"requestDate"]))) {
        NSNumber *datenumber = info[@"requestDate"];
        _requestDate.text = [self DateFormateChange:datenumber];
    } else {
        _requestDate.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"locType"]))) {
        _iWorkLocationTypeLabel.text = info[@"locType"];
    } else {
        _iWorkLocationTypeLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"dateTime"]))) {
        NSNumber *datenumber = info[@"dateTime"];
        _iworkDate.text = [self DateFormateChange:datenumber];
    } else {
        _iworkDate.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"location"]))) {
        _location.text = info[@"location"];
    } else {
        _location.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"task"]))) {
        _TaskLabel.text = info[@"task"];
    } else {
        _TaskLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"maxCheckInTime"]))) {
        _MaxCheckInLabel.text = [self MaxDate:info[@"maxCheckInTime"]];
    }else{
        _MaxCheckInLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"status"]))) {
        
        if ([info[@"status"] isEqualToString:@"PENDING"]) {
            _StatusLabel.textColor = [UIColor ViewRequestOrangeColor];
            _StatusLabel.text = @"New";
            _CercilLabel.backgroundColor = [UIColor ViewRequestOrangeColor];
            
            
        } else if ([info[@"status"] isEqualToString:@"APPROVED"]) {
            
            _StatusLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:88.0/255.0 blue:45.0/255.0 alpha:1.0];
            _CercilLabel.backgroundColor = [UIColor ViewRequestGreenColor];
            _StatusLabel.text = @"Approved";
            
            
        }
        else if ([info[@"status"] isEqualToString:@"REJECTED"]) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _StatusLabel.text = @"Rejected";
            _CercilLabel.backgroundColor = [UIColor ViewRequestRedColor];
            
        }
        else if ([info[@"status"] isEqualToString:@"CANCELLED"] ) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _StatusLabel.text = @"Cancelled";
            _CercilLabel.backgroundColor = [UIColor ViewRequestRedColor];
            
        }
        else if ([info[@"status"] isEqualToString:@"DISCARDED"]) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _StatusLabel.text = @"Discarded";
            _CercilLabel.backgroundColor = [UIColor ViewRequestRedColor];
            
        }
        else if ([info[@"status"]isEqualToString:@"DECLINED"] ) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _StatusLabel.text = @"Declined";
            _CercilLabel.backgroundColor = [UIColor ViewRequestRedColor];
            
        }
    }
    
}

-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)MaxDate:(NSString*)DateString{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString: DateString];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *convertedString = [dateFormatter stringFromDate:date];
    return convertedString;
}


@end
