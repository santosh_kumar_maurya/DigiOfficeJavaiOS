//
//  CheckInCell.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "CheckInCell.h"
#import "NSString+HTML.h"

@implementation CheckInCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.CellBgView.layer.cornerRadius = 5.0;
    self.CongratsLabel.text = NSLocalizedString(@"CONGRATS_YOU_ARE_ON", nil);
    //self.CongratsLabel.text = [self.CongratsLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
    
    self.NoIWorkLabel.text = NSLocalizedString(@"NO_IWORK_REQUEST", nil);
    //self.NoIWorkLabel.text = [self.NoIWorkLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
//    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
//    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: self.RedView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft cornerRadii: (CGSize){10.0}].CGPath;
//     self.RedView.layer.mask = maskLayer1;
//    
//    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
//    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: self.DateView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
//    self.DateView.layer.mask = maskLayer2;
    self.CheckInButton.layer.cornerRadius = 14.0;
    self.CheckInButton.clipsToBounds = YES;
  
//    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.CellBgView.bounds];
//    self.CellBgView.layer.masksToBounds = NO;
//    self.CellBgView.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.CellBgView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
//    self.CellBgView.layer.shadowOpacity = 0.3f;
//    self.CellBgView.layer.shadowPath = shadowPath.CGPath;
    
//    self.DateView.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255 blue:241.0/255.0 alpha:1];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(UIColor *)color backGround:(UIColor *)backGroundColor
{
    self.RedView.layer.cornerRadius = self.RedView.frame.size.height/2;
    self.RedView.backgroundColor = backGroundColor;
    self.RedView.layer.borderWidth = 2.0;
    self.RedView.layer.borderColor = color.CGColor;
    self.RedView.clipsToBounds = YES;
}


@end
