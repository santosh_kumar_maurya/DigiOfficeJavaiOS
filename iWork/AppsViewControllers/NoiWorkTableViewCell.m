//
//  NoiWorkTableViewCell.m
//  iWork
//
//  Created by mac book pro on 17/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "NoiWorkTableViewCell.h"

@implementation NoiWorkTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.RedView.layer.cornerRadius = self.RedView.frame.size.height/2;
    self.RedView.layer.borderWidth = 2.0;
    self.RedView.backgroundColor = [UIColor whiteColor];
    self.RedView.layer.borderColor = [UIColor redColor].CGColor;
    self.RedView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
