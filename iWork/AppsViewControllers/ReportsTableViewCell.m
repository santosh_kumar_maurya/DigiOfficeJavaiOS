//
//  ReportsTableViewCell.m
//  iWork
//
//  Created by Fourbrick on 18/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import "ReportsTableViewCell.h"
#import "Shared.h"
#import "iWork-Swift.h"

@implementation ReportsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.circleView.layer.cornerRadius = self.circleView.frame.size.width/2;
    self.circleView.layer.masksToBounds = YES;
    self.circleView.layer.borderColor = [UIColor clearColor].CGColor;
    self.circleView.layer.borderWidth = 0.0;
}

- (void)configureReportCell:(NSDictionary *)info {
    
    if(IsSafeStringPlus(TrToString(info[@"userId"]))) {
        _EmployeeIDLabel.text = [NSString stringWithFormat:@"%@",info[@"userId"]];
    } else {
        _EmployeeIDLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"userName"]))) {
        _EmployeeNameLabel.text = [NSString stringWithFormat:@"%@",info[@"userName"]];
    } else {
        _EmployeeNameLabel.text = @" ";
    }
    
    _RequestIdLabel.text = @"";
    if(IsSafeStringPlus(TrToString(info[@"requestId"]))) {
        _RequestIdLabel.text = [NSString stringWithFormat:@"%@",info[@"requestId"]];
    } else {
        _RequestIdLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"dateTime"]))) {
        NSNumber *datenumber = info[@"dateTime"];
        _RequestDateLabel.text = [self DateFormateChange:datenumber];
        
    } else {
        _RequestDateLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"task"]))) {
        _TaskLabel.text = info[@"task"];
    } else {
        _TaskLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"location"]))) {
        _iWorkLocationLabel.text = info[@"location"];
    } else {
        _iWorkLocationLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"requestDate"]))) {
        NSNumber *datenumber = info[@"requestDate"];
        _iWorkDateLabel.text = [self DateFormateChange:datenumber];
    } else {
        _iWorkDateLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"actionDate"]))) {
        NSNumber *datenumber = info[@"actionDate"];
        _ActionDateLabel.text = [self DateFormateChange:datenumber];
    } else {
        _ActionDateLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"checkInTime"]))) {
        NSNumber *Timenumber = info[@"checkInTime"];
        _CheckInLabel.text = [self GetDate:Timenumber];
    } else {
        _CheckInLabel.text = @"- -:- -";
    }
    if(IsSafeStringPlus(TrToString(info[@"checkOutTime"]))) {
        NSNumber *Timenumber = info[@"checkOutTime"];
        _CheckoutLabel.text = [self GetDate:Timenumber];
    } else {
        _CheckoutLabel.text = @"- -:- -";
    }
    _HoursLabel.text = @" ";
    if(IsSafeStringPlus(TrToString(info[@"hour"]))) {
        _HoursLabel.text = info[@"hour"];
    }
    else{
        _HoursLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"locType"]))) {
        _iWorkLocationTypeLabel.text = info[@"locType"];
    } else {
        _iWorkLocationTypeLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"attendanceStatus"]))) {
        _AttandenceStatusLabel.text = info[@"attendanceStatus"];
        if([_AttandenceStatusLabel.text isEqualToString:@"-"]){
            _AttandenceStatusLabel.text = @"";
        }
    }else{
        _AttandenceStatusLabel.text = @"";
    }
    if(IsSafeStringPlus(TrToString(info[@"maxCheckInTime"]))) {
        _MaxCheckInLabel.text = [self MaxDate:info[@"maxCheckInTime"]];
    }else{
        _MaxCheckInLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"status"]))) {
        
        if ([info[@"status"] isEqualToString:@"PENDING"]) {
            _StatusLabel.textColor = [UIColor ViewRequestOrangeColor];
            _circleView.backgroundColor = [UIColor ViewRequestOrangeColor];
            _StatusLabel.text = @"New";
            _StatusImageView.image = [UIImage imageNamed:@"NewStatus"];
            
        } else if ([info[@"status"] isEqualToString:@"APPROVED"]) {
            
            _StatusLabel.textColor = [UIColor ViewRequestGreenColor];
            _circleView.backgroundColor = [UIColor ViewRequestGreenColor];
            _StatusLabel.text = @"Approved";
            _StatusImageView.image = [UIImage imageNamed:@"Approved"];
            
        }
        else if ([info[@"status"] isEqualToString:@"REJECTED"]) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _circleView.backgroundColor = [UIColor ViewRequestRedColor];
            _StatusLabel.text = @"Rejected";
            _StatusImageView.image = [UIImage imageNamed:@"Rejected"];
            
        }
        else if ([info[@"status"] isEqualToString:@"CANCELLED"] ) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _circleView.backgroundColor = [UIColor ViewRequestRedColor];
            _StatusLabel.text = @"Cancelled";
            _StatusImageView.image = [UIImage imageNamed:@"CancelledStatus"];
            
        }
        else if ([info[@"status"] isEqualToString:@"DISCARDED"]) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _circleView.backgroundColor = [UIColor ViewRequestRedColor];
            _StatusLabel.text = @"Discarded";
            _StatusImageView.image = [UIImage imageNamed:@"DiscardedStatus"];
            
        }
        else if ([info[@"status"]isEqualToString:@"DECLINED"] ) {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _circleView.backgroundColor = [UIColor ViewRequestRedColor];
            _StatusLabel.text = @"Declined";
            _StatusImageView.image = [UIImage imageNamed:@"DeclinedStatus"];
            
        }
    }
    
    if(IsSafeStringPlus(TrToString(info[@"status"]))){
        if([info[@"status"] isEqualToString:@"APPROVED"]){
            if(IsSafeStringPlus(TrToString(info[@"attendanceStatus"])))
            {
                
                _AttandenceStatusStaticLabel.text = NSLocalizedString(@"ATTENDANCE_STATUS", nil);
                if([info[@"attendanceStatus"] isEqualToString:@"Present"])
                {
                    _attendanceStatusVerticalConstraints.constant = 10;
                    _maximumCheckInVerticalContraints.constant = 10;
                    _CheckInStaticLabel.text = @"Check-in";
                    _CheckoutStaticLabel.text = @"Checkout";
                    _HoursStaticLabel.text = NSLocalizedString(@"Hours", nil);
                    _HoursLabel.text = info[@"hour"];
                    NSNumber *Timenumber = info[@"checkOutTime"];
                    _CheckoutLabel.text = [self GetDate:Timenumber];
                    NSNumber *Timenumber1 = info[@"checkInTime"];
                    _CheckInLabel.text = [self GetDate:Timenumber1];
                }
                else{
                    _CheckInStaticLabel.text = @"";
                    _CheckoutStaticLabel.text = @"";
                    _HoursStaticLabel.text = @"";
                    _CheckInLabel.text = @"";
                    _CheckoutLabel.text = @"";
                    _HoursLabel.text = @"";
                    _maximumCheckInVerticalContraints.constant = 0;
                }
            }
            else
            {
                _CheckInStaticLabel.text = @"";
                _CheckoutStaticLabel.text = @"";
                _HoursStaticLabel.text = @"";
                _CheckInLabel.text = @"";
                _CheckoutLabel.text = @"";
                _HoursLabel.text = @"";
                _AttandenceStatusStaticLabel.text = @"";
                _AttandenceStatusLabel.text = @"";
                _attendanceStatusVerticalConstraints.constant = 0;
                _maximumCheckInVerticalContraints.constant = 0;
            }
        }
        else{
            _CheckInStaticLabel.text = @"";
            _CheckoutStaticLabel.text = @"";
            _HoursStaticLabel.text = @"";
            _CheckInLabel.text = @"";
            _CheckoutLabel.text = @"";
            _HoursLabel.text = @"";
            _AttandenceStatusStaticLabel.text = @"";
            _AttandenceStatusLabel.text = @"";
            _attendanceStatusVerticalConstraints.constant = 0;
            _maximumCheckInVerticalContraints.constant = 0;
        }
        
    }

    
    
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)GetDate:(NSNumber*)Timenumber{
    NSString *DateStr = [NSString stringWithFormat:@"%@",Timenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"HH:mm:ss"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"hh:mm a"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)MaxDate:(NSString*)DateString{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString: DateString];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *convertedString = [dateFormatter stringFromDate:date];
    return convertedString;
}

@end
