//
//  RequestCell.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "RequestCell.h"

@implementation RequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.iWorkView.layer.cornerRadius = 5.0;
    self.RequestView.layer.cornerRadius = 5.0;
    
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.iWorkView.bounds];
    self.iWorkView.layer.masksToBounds = NO;
    self.iWorkView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.iWorkView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.iWorkView.layer.shadowOpacity = 0.3f;
    self.iWorkView.layer.shadowPath = shadowPath.CGPath;
    
    UIBezierPath *shadowPath1 = [UIBezierPath bezierPathWithRect:self.RequestView.bounds];
    self.RequestView.layer.masksToBounds = NO;
    self.RequestView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.RequestView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.RequestView.layer.shadowOpacity = 0.3f;
    self.RequestView.layer.shadowPath = shadowPath1.CGPath;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
