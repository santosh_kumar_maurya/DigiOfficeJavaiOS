//
//  EmployeeRejectedViewController.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeRejectedViewController.h"
#import "RejectedCell.h"
#import "Header.h"

@interface EmployeeRejectedViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *RejectedTableView;
    NSMutableArray *RejectedArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *dalegate;
    IBOutlet UILabel *NoDataLabel;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    APIService *Api;
    int offset;
    int limit;
    IBOutlet UIImageView *NoDataImageView;
}
@end

@implementation EmployeeRejectedViewController


- (void)viewDidLoad {
    offset = 0;
    limit = 10;
    Api = [[APIService alloc] init];
    RejectedArray = [[NSMutableArray alloc] init];
    dalegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor whiteColor];
    RejectedTableView.backgroundColor = [UIColor whiteColor];
    RejectedTableView.estimatedRowHeight = 500;
    RejectedTableView.rowHeight = UITableViewAutomaticDimension;
    RejectedTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, RejectedTableView.bounds.size.width, 0.0f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [RejectedTableView addSubview:refreshControl];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkUserRejected) withObject:nil afterDelay:0.5];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RejectedApi:) name:@"Rejected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClearFilterApi:) name:@"EmployeeClearFilterApi" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)reloadData{
    if ([CheckValue isEqualToString:@"FILTER"])
    {
        CheckValue = @"FILTER";
    }
    else{
        CheckValue = @"Refresh";
    }
    [self EmptyArray];
    [self GetiWorkUserRejected];
}
-(void)EmptyArray{
    offset = 0;
    limit = 10;
    RejectedArray = [[NSMutableArray alloc] init];
    [RejectedTableView reloadData];
}
- (void)GetiWorkUserRejected {
    if(dalegate.isInternetConnected){
        if(![CheckValue isEqualToString:@"FILTER"]){
            params = @ {
                @"attendanceStatus": @"",
                @"requestDate": @"",
                @"requestId": @0,
                @"status": @2,
                @"userId": [ApplicationState userId],
                @"workDate": @"",
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        ResponseDic = [Api WebApi:params Url:@"myIworkRequest"];
        [LoadingManager hideLoadingView:self.view];
        [refreshControl endRefreshing];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self GetiWorkUserRejected];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
            if(ResponseArrays.count>0){
                [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                NoDataLabel.hidden = YES;
                NoDataImageView.hidden = YES;
            }
            else if(RejectedArray.count == 0){
                [self NoIworkRequest];
            }
            [RejectedTableView reloadData];
        }
        else{
            if(RejectedArray.count == 0){
               [self NoIworkRequest];
            }
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoIworkRequest{
    NoDataLabel.hidden = NO;
    NoDataImageView.hidden = NO;
    NoDataLabel.text = NSLocalizedString(@"NO_IWORK_REQUEST_FOUND", nil);
    //NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
    if(ArraysValue.count>0){
        offset = offset + 10;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self GetiWorkUserRejected];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
   
    for (int i = 0 ; i<Array.count ; i++){
         NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [RejectedArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [RejectedArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RejectedCell *Cell = (RejectedCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
    Cell.tag = indexPath.section;
    [Cell  setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:Gesture];
    if(RejectedArray.count>0){
        NSDictionary * responseData = [RejectedArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
    }
    if(indexPath.section == 0){
        Cell.UpperLineLabel.hidden = YES;
    }
    else{
        Cell.UpperLineLabel.hidden = NO;
    }
    return Cell;
}

-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    if(dalegate.isInternetConnected){
        int TagValue = sender.view.tag;
        NSDictionary *Dics = [RejectedArray objectAtIndex:TagValue];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
        ObjViewController.MapDic = Dics;
        [[self navigationController] pushViewController:ObjViewController animated:YES];
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)RejectedApi:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkUserRejected) withObject:nil afterDelay:0.4];
}
-(void)ClearFilterApi:(NSNotification*)notification
{
    [self EmptyArray];
    CheckValue = @"Rejected";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkUserRejected) withObject:nil afterDelay:0.4];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
