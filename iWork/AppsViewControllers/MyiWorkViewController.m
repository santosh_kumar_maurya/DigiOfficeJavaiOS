 //
//  MyiWorkViewController.m
//  iWork
//
//  Created by Shailendra on 16/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyiWorkViewController.h"
#import "Header.h"
//#import <ASExtendedCircularMenu/ASExtendedCircularMenu-Swift.h>
#import "iWork-Swift.h"


@interface MyiWorkViewController ()<UITableViewDelegate,UITableViewDataSource, RainbowColorSource>
{
    IBOutlet UITableView *MyiWorkTableView;
    AppDelegate *delegate;
    NSString *CheckValue;
    NSMutableArray *TemaiworkArray;
    NSDictionary *ResponseDic;
    BOOL useriWorkStatus;
  
    NSDictionary *params;
    NSString *iWorkIdStr;
    UIRefreshControl *refreshControl;
    
    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UIButton *ApproveBtn;
    IBOutlet UIButton *RejectBtn;
   
    NSString *Type;
    NSString *user_requests_id;
    
    IBOutlet UILabel *ApproveRejactLabel;
    IBOutlet UIView *ApproveRejactView;
    IBOutlet UIImageView *ApproveImageView;
    IBOutlet UILabel *ApproveTitleLabel;
    IBOutlet UIButton *ApproveCloseBtn;
    
    IBOutlet UIView *CheckInOuterView;
    IBOutlet UIView *CheckInInnerView;
    IBOutlet UILabel *CheckInLabel;
    IBOutlet UIButton *DoneBtn;
    APIService *Api;
    NSString *empId;
    NSString *empName;
    NSString *location;
    RainbowNavigation *rainbowNavigation;
    UINavigationBar* navbar;
    
    IBOutlet UIButton *circularButton;
    IBOutlet UIButton *viewReportButton;
    IBOutlet UIButton *createReportButton;
    UILabel *notificationLabel;
    UIImageView *profilePic;
    UILabel *lblUserName;
    UILabel *lblAddress;
    NSDictionary *recentResponseDict;
    NSMutableArray *recentDataArray;
    NSString *headerStrForMyTeam;
    UIButton *profileButton;
    int TagValue;

//    ASCircularMenuButton *circularMenu;
    
//    UIWindow *window;
}

@end

@implementation MyiWorkViewController
@synthesize isComeFrom,NotificationDic;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureNavigationBar];
    [self configureHeaderView];
    [self configureTableViewCell];
    
    Api = [[APIService alloc] init];
    empId = @"";
    empName = @"";
    location = @"";
    CheckValue = @"MY_IWORK";
//    self.view.backgroundColor = delegate.BackgroudColor;
//    MyiWorkTableView.backgroundColor = delegate.BackgroudColor;
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    MyiWorkTableView.estimatedRowHeight = 200;
    [MyiWorkTableView setNeedsLayout];
    MyiWorkTableView.bounces = NO;
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
//    [MyiWorkTableView addSubview:refreshControl];
    [LoadingManager showLoadingView:self.view];
  
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
    [self performSelector:@selector(callWebServiceForRecentiWorkRequest) withObject:nil afterDelay:0.1];
    [self CheckInCheckOutView];
    
    if([isComeFrom isEqualToString:@"NOTIFICATION"]){
        MsgLabel.text = [NotificationDic valueForKey:@"msg"];
        if([[NotificationDic valueForKey:@"type"]integerValue] ==6){
            CheckInOuterView.hidden = NO;
            [DoneBtn setTitle:NSLocalizedString(@"CHECK_IN_BTN_CAPS", nil) forState:UIControlStateNormal];
            [DoneBtn addTarget:self action:@selector(CheckInChekcOut:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if([[NotificationDic valueForKey:@"type"]integerValue] ==7){
            CheckInOuterView.hidden = NO;
            [DoneBtn setTitle:NSLocalizedString(@"CHECK_OUT_BTN_CAPS", nil) forState:UIControlStateNormal];
            [DoneBtn addTarget:self action:@selector(CheckInChekcOut:) forControlEvents:UIControlEventTouchUpInside];
        }
        else{
           MsgOuterView.hidden = NO;
        }
    }
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iWorkFilterApi:) name:@"iWorkFilter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateNotifications) name:@"Notification" object:nil];
  
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self UpdateNotifications];
    
}

-(void)configureTableViewCell
{
    [MyiWorkTableView registerNib:[UINib nibWithNibName:@"MYiWorkHeaderCellTableViewCell" bundle:nil] forCellReuseIdentifier:@"MYiWorkHeaderCellTableViewCell"];
    [MyiWorkTableView registerNib:[UINib nibWithNibName:@"RecentiWorkRequestTableViewCell" bundle:nil] forCellReuseIdentifier:@"RecentiWorkRequestTableViewCell"];
    [MyiWorkTableView registerNib:[UINib nibWithNibName:@"NoiWorkTableViewCell" bundle:nil] forCellReuseIdentifier:@"NoiWorkTableViewCell"];
}

-(void)configureNavigationBar{
    navbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 44)];
    [navbar setBackgroundImage:[UIImage new]
                 forBarMetrics:UIBarMetricsDefault];
    navbar.shadowImage = [UIImage new];
    navbar.translucent = YES;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 20.0f, self.view.frame.size.width, navbar.frame.size.height)];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(-5.0f, 17.0f, 15.0f, 15.0f)];
    [backButton addTarget:self action:@selector(BackAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"new_ic_back"] forState:UIControlStateNormal];
    backButton.imageView.contentMode = UIViewContentModeCenter;
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 100.0f, 30.0f)];
//    NSString *MyIWorkStr = NSLocalizedString(@"MY_IWORK", nil);
    //MyIWorkStr = [[MyIWorkStr stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]] uppercaseString];
//    title.enabled = true;
    [title setFont:[UIFont boldSystemFontOfSize:17]];

    title.text = NSLocalizedString(@"IWORK", nil);
//    title.font = [delegate headFont];
    title.textColor = [UIColor whiteColor];
    
    UITapGestureRecognizer* titleGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(BackAction:)];
    [title  setUserInteractionEnabled:YES];
    [title addGestureRecognizer:titleGesture];
    
    [customView addSubview:backButton];
    [customView addSubview:title];

    UIButton *notificationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [notificationButton setFrame:CGRectMake(self.view.frame.size.width - 60, 10.0f, 30.0f, 30.0f)];
    [notificationButton addTarget:self action:@selector(notificationTapped:) forControlEvents:UIControlEventTouchUpInside];
    [notificationButton setImage:[UIImage imageNamed:@"new_ic_notification"] forState:UIControlStateNormal];
    notificationButton.imageView.contentMode = UIViewContentModeCenter;
    
    notificationLabel = [[UILabel alloc] initWithFrame:CGRectMake(notificationButton.frame.origin.x + 15, 5.0f, 20.0f, 20.0f)];
    notificationLabel.text = @"0";
    notificationLabel.layer.cornerRadius = 10;
    notificationLabel.clipsToBounds = YES;
    notificationLabel.backgroundColor = [UIColor colorWithRed:(236.0/255.0) green:(27.0/255.0) blue:(33.0/255.0) alpha:1.0f];
    notificationLabel.textAlignment = NSTextAlignmentCenter;
    notificationLabel.textColor = [UIColor whiteColor];
    
    
    UIButton *HeaderProfileButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [HeaderProfileButton setFrame:CGRectMake(self.view.frame.size.width - 100, 10.0f, 30.0f, 25.0f)];
    [HeaderProfileButton addTarget:self action:@selector(openProfile) forControlEvents:UIControlEventTouchUpInside];
    [HeaderProfileButton setImage:[UIImage imageNamed:@"New_Profile"] forState:UIControlStateNormal];
    HeaderProfileButton.imageView.contentMode = UIViewContentModeCenter;
    
    
    
    
    UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [homeButton setFrame:CGRectMake(self.view.frame.size.width - 140, 10.0f, 30.0f, 25.0f)];
    [homeButton addTarget:self action:@selector(BackAction:) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setImage:[UIImage imageNamed:@"HomeImage"] forState:UIControlStateNormal];
    homeButton.imageView.contentMode = UIViewContentModeCenter;
    
    [customView addSubview:homeButton];
    [customView addSubview:HeaderProfileButton];
    [customView addSubview:notificationButton];
    [customView addSubview:notificationLabel];
    
    UINavigationItem* navItem = [[UINavigationItem alloc] init];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:customView];
    
    navItem.leftBarButtonItem = leftButton;
    //    navItem.rightBarButtonItem = rightButton;
    [navbar setItems:@[navItem]];
    
    
    [self.view addSubview:navbar];

}

-(void)configureHeaderView
{
    NSDictionary *Dic = [ApplicationState getUserLoginData];
    
    rainbowNavigation = [[RainbowNavigation alloc] init];
    [createReportButton setHidden:YES];
    [viewReportButton setHidden:YES];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIImageView *imageView =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.width * 0.65)];
    imageView.image=[UIImage imageNamed:@"My_iWork_Dashboard_Navigation"];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    
    //Yellow Header
    UIView *yellowBorder = [[UIView alloc] initWithFrame:CGRectMake(0, imageView.frame.size.height - 6, self.view.frame.size.width, 6)];
    yellowBorder.backgroundColor = [UIColor LineGreyColor];
    [imageView addSubview:yellowBorder];
    
    //ProfilePic
    profilePic = [[UIImageView alloc] initWithFrame:CGRectMake(10 , (imageView.frame.size.height - yellowBorder.frame.size.height - 55), 50, 50)];
    profilePic.layer.cornerRadius = profilePic.frame.size.height/2;
    profilePic.clipsToBounds = YES;

    
    NSString *URL = [NSString stringWithFormat:@"%@",[[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]];
    NSLog(@"URL==%@",URL);
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]))){
        NSURL *Url = [NSURL URLWithString:URL];
        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL:Url]];
        if(images == nil){
            profilePic.image = [UIImage imageNamed:@"profile_image_default"];
        }
        else{
            [profilePic sd_setImageWithURL:[NSURL URLWithString:URL]];
        }
    }
    else{
        profilePic.image = [UIImage imageNamed:@"profile_image_default"];
    }
    [imageView addSubview:profilePic];
    
   
    
    //UserName
    lblUserName = [[UILabel alloc] initWithFrame:CGRectMake(profilePic.frame.size.width + 20 , profilePic.frame.origin.y + 10, (self.view.frame.size.width - (profilePic.frame.size.width + 30)), 25)];
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]))){
        lblUserName.text = [NSString stringWithFormat:@"%@", [[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]];
    }
    else{
        lblUserName.text = @" ";
    }
    [lblUserName setFont:[UIFont systemFontOfSize:14]];
    lblUserName.textColor = [UIColor whiteColor];
    [imageView addSubview:lblUserName];
    
    //Location Icon
    UIImageView *locationImageView =[[UIImageView alloc] initWithFrame:CGRectMake(profilePic.frame.size.width + 20, lblUserName.frame.origin.y + lblUserName.frame.size.height , 15, 15)];
    locationImageView.image=[UIImage imageNamed:@"new_ic_map"];
    [locationImageView setContentMode:UIViewContentModeScaleAspectFill];
    [imageView addSubview:locationImageView];
    
    //Address
    lblAddress = [[UILabel alloc] initWithFrame:CGRectMake(profilePic.frame.size.width + 20 + 20 , lblUserName.frame.origin.y + lblUserName.frame.size.height - 3 , (self.view.frame.size.width - (profilePic.frame.size.width + 30)), 20)];

        lblAddress.text = [NSString stringWithFormat:@"%@", [ApplicationState getLocationName]];
    [lblAddress setFont:[UIFont systemFontOfSize:12]];
    lblAddress.textColor = [UIColor whiteColor];
    [imageView addSubview:lblAddress];
    
    profilePic.multipleTouchEnabled = YES;
    profilePic.userInteractionEnabled = YES;

    profileButton = [[UIButton alloc] initWithFrame:CGRectMake(10 , (imageView.frame.size.height - yellowBorder.frame.size.height - 55), 50, 50)];

    [profileButton addTarget:self action:@selector(openProfile) forControlEvents:UIControlEventTouchUpInside];
    imageView.userInteractionEnabled = YES;
    imageView.multipleTouchEnabled =YES;
    profileButton.userInteractionEnabled = YES;
    profileButton.multipleTouchEnabled =YES;
    
    [imageView addSubview:profileButton];
    MyiWorkTableView.tableHeaderView = imageView;
    
    
}

-(void)CheckInCheckOutView{
    CheckInInnerView.layer.cornerRadius = 5;
    [DoneBtn setBackgroundColor:[UIColor colorWithRed:0.85 green:0.69 blue:0.14 alpha:1.0]];
    DoneBtn.layer.cornerRadius = 5;
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self WebApiCell];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1;
    }
    else
    {
        if(recentDataArray.count>0)
        {
            return recentDataArray.count;
        }
        else
        {
            return 1;
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        CheckInCell *Cell = (CheckInCell*)[MyiWorkTableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        Cell.backgroundColor = [UIColor clearColor];
        
        Cell.CheckInButton.backgroundColor = delegate.redColor;
        [Cell.CheckInButton addTarget:self action:@selector(CheckInChekcOut:) forControlEvents:UIControlEventTouchUpInside];
        
        Cell.NoIWorkLabel.hidden = YES;
        NSString *GetDayStr = [self GetDayWithDate];
        NSString *GetTimeStr = [self GetCurrentDate];
        NSString *myRequestForToday = [ResponseDic[@"object"] valueForKey:@"myRequestForToday"];
        NSString *finalDataAndTime;
        
        if([myRequestForToday integerValue] == 1){
            NSMutableDictionary *myRequestDic = [ResponseDic[@"object"] valueForKey:@"myRequest"];
            NSString *check_in = [myRequestDic valueForKey:@"checkIn"];
            NSString *check_out = [myRequestDic valueForKey:@"checkOut"];
            
            if([check_in integerValue] == 1){
                [Cell.CheckInButton setTitle:NSLocalizedString(@"CHECK_IN_BTN_CAPS", nil) forState:UIControlStateNormal];
                [Cell.CheckInButton setBackgroundColor:[UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f]];
                [Cell configureCell:[UIColor colorWithRed:(247.0/255.0) green:(43.0/255.0) blue:(44.0/255.0) alpha:1.0f] backGround:[UIColor whiteColor]];
                
                finalDataAndTime = [NSString stringWithFormat: @"%@  %@",GetTimeStr,GetDayStr];
                Cell.CheckInButton.hidden = NO;
                Cell.checkInButtonHeightConstraints.constant = 26.0;
//                Cell.checkInButtonBottonConstraints.constant = 2.0;
                Cell.DateLabel.hidden = NO;
                //                Cell.TimeLabel.hidden = NO;
                //                Cell.AM_PM_Label.hidden = NO;
                Cell.CongratsLabel.hidden = NO;
                Cell.NoIWorkLabel.hidden = YES;
                useriWorkStatus =YES;
            }
            else if([check_out integerValue] == 1){
                [Cell.CheckInButton setTitle:NSLocalizedString(@"CHECK_OUT_BTN_CAPS", nil) forState:UIControlStateNormal];
                [Cell.CheckInButton setBackgroundColor:[UIColor colorWithRed:(247.0/255.0) green:(43.0/255.0) blue:(44.0/255.0) alpha:1.0f]];
  
                [Cell configureCell:[UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f] backGround:[UIColor whiteColor]];
                finalDataAndTime = [NSString stringWithFormat: @"%@  %@",GetTimeStr,GetDayStr];
                Cell.CheckInButton.hidden = NO;
                Cell.checkInButtonHeightConstraints.constant = 26.0;
//                Cell.checkInButtonBottonConstraints.constant = 2.0;
                Cell.DateLabel.hidden = NO;
                //                Cell.TimeLabel.hidden = NO;
                //                Cell.AM_PM_Label.hidden = NO;
                Cell.CongratsLabel.hidden = NO;
                Cell.NoIWorkLabel.hidden = YES;
            }
            else{
                [Cell configureCell:[UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f] backGround:[UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f]];
                finalDataAndTime = [NSString stringWithFormat: @"%@  %@",GetTimeStr,GetDayStr];
                Cell.CheckInButton.hidden = YES;
                Cell.checkInButtonHeightConstraints.constant = 0.0;
//                Cell.checkInButtonBottonConstraints.constant = 28.0;
                Cell.DateLabel.hidden = YES;
                //                Cell.AM_PM_Label.hidden = YES;
                Cell.CongratsLabel.hidden = YES;
                //                Cell.TimeLabel.hidden = YES;
                Cell.NoIWorkLabel.hidden = NO;
            }
            if ([myRequestDic valueForKey:@"checkIn"] == [NSNull null] && [myRequestDic valueForKey:@"checkOut"] == [NSNull null])
            {
                [Cell configureCell:[UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f] backGround:[UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f]];
                Cell.CheckInButton.hidden = YES;
                Cell.checkInButtonHeightConstraints.constant = 0.0;
//                Cell.checkInButtonBottonConstraints.constant = 28.0;
            }
        }
        else{
            [Cell configureCell:[UIColor colorWithRed:(247.0/255.0) green:(43.0/255.0) blue:(44.0/255.0) alpha:1.0f] backGround:[UIColor whiteColor]];
            finalDataAndTime = [NSString stringWithFormat: @"%@", GetDayStr];
            //            Cell.TimeLabel.hidden = YES;
            //            Cell.AM_PM_Label.hidden = YES;
            Cell.CongratsLabel.hidden = YES;
            Cell.CheckInButton.hidden = YES;
            Cell.checkInButtonHeightConstraints.constant = 0.0;
//            Cell.checkInButtonBottonConstraints.constant = 28.0;
            Cell.NoIWorkLabel.hidden = NO;
            Cell.DateLabel.hidden = YES;
        }
        Cell.DateLabel.text = finalDataAndTime;

        return Cell;
    }
    else
    {
        if(recentDataArray.count>0)
        {
            RecentiWorkRequestTableViewCell *Cell = (RecentiWorkRequestTableViewCell*)[MyiWorkTableView dequeueReusableCellWithIdentifier:@"RecentiWorkRequestTableViewCell" forIndexPath:indexPath];
            [Cell configureWith:[recentDataArray objectAtIndex:indexPath.row] indexPath:indexPath.row];
            UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
            Cell.tag = indexPath.row;
            [Cell  setUserInteractionEnabled:YES];
            [Cell addGestureRecognizer:Gesture];
            
            return Cell;
        }
        else
        {
            NoiWorkTableViewCell *Cell = (NoiWorkTableViewCell*)[MyiWorkTableView dequeueReusableCellWithIdentifier:@"NoiWorkTableViewCell" forIndexPath:indexPath];
            Cell.HorizentalLineLabel.hidden = YES;
            return Cell;
        }
       
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MYiWorkHeaderCellTableViewCell *Cell = (MYiWorkHeaderCellTableViewCell*)[MyiWorkTableView dequeueReusableCellWithIdentifier:@"MYiWorkHeaderCellTableViewCell"];
    if (section == 0)
    {
        Cell.headerLabel.text = @"Today";
    }
    else
    {
        Cell.headerLabel.text = @"My iWork Request";
        //Cell.headerLabel.text = [[Cell.headerLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]] uppercaseString];
    }
    return Cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0.0;
    }
    return 80.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *sampleView = [[UIView alloc] init];
    sampleView.frame = CGRectMake(0, 0, self.view.frame.size.width, 80);
    sampleView.backgroundColor = [UIColor clearColor];
    return sampleView;
}


-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    TagValue = sender.view.tag;
    NSDictionary *Dic = [recentDataArray objectAtIndex:TagValue];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
    ObjViewController.MapDic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
#pragma mark -
- (IBAction)ApproveBtnAction:(UIButton *)sender {
    CheckValue = @"APPROVE";
    Type = @"1";
    user_requests_id = [NotificationDic valueForKey:@"user_requests_id"];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
}
- (IBAction)RejectBtnAction:(UIButton *)sender {
    CheckValue = @"REJECT";
    Type = @"2";
    user_requests_id = [NotificationDic valueForKey:@"user_requests_id"];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
}
-(void)ShowErrorTitle:(NSString*)Title Message:(NSString*)Msg ImageName:(NSString*)ImageName BtnTitle:(NSString*)BtnTilte{
    ApproveRejactView.hidden = NO;
    ApproveTitleLabel.text = Title;
    ApproveRejactLabel.text = Msg;
    ApproveImageView.image = [UIImage imageNamed:ImageName];
    [ApproveCloseBtn setTitle:BtnTilte forState:UIControlStateNormal];
    
}
-(void)GoToRequestIWork{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateWorkRequestViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateWorkRequestViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GoToViewRequest{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EmployeeParentViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"EmployeeParentViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)CheckInChekcOut:(UIButton*)sender{
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CHECK_IN_BTN_CAPS", nil)]){
        CheckValue = @"CHECKIN";
    }
    else if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CHECK_OUT_BTN_CAPS", nil)]){
        CheckValue = @"CHECKOUT";
    }
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.4];
}
-(void)FilterAction{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyWorkFilterViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyWorkFilterViewController"];
    [self presentViewController:ObjViewController animated:YES completion:nil];
    
}
- (void)WebApiCell {
    
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"MY_IWORK"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"IWORKFILTER"]){
            if(![CheckValue isEqualToString:@"IWORKFILTER"]){
                params = @ {
                    @"empId": empId,
                    @"empName": empName,
                    @"location": location,
                    @"userId": [ApplicationState userId]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"myIwork"];
//            [refreshControl endRefreshing];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self WebApiCell];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                
                
               
                if(IsSafeStringPlus(TrToString(ResponseDic[@"object"]))){
                    
                    iWorkIdStr = [[ResponseDic[@"object"]valueForKey:@"myRequest"] valueForKey:@"requestId"];
                    if(IsSafeStringPlus(TrToString([ResponseDic[@"object"]valueForKey:@"teamRequestList"]))){
                        TemaiworkArray = [ResponseDic[@"object"]valueForKey:@"teamRequestList"];
                    }
                }
                else{
                    iWorkIdStr = @"";
                }
                [MyiWorkTableView reloadData];
            }
            else{
                [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
        else if ([CheckValue isEqualToString:@"CHECKIN"]||[CheckValue isEqualToString:@"CHECKOUT"]){
            if([delegate.userLat doubleValue] ==0.0 || [delegate.userLong doubleValue] == 0.0){
                [AlertManager showPopupMessageAlert:NSLocalizedString(@"TURN_ON_LOCATION", nil) withTitle:nil];
            }
            //else{
                delegate.userLat = @"";
                delegate.userLong = @"";
           // }
            NSString *Lat = [NSString stringWithFormat:@"%@",delegate.userLat];
            NSString *Logn = [NSString stringWithFormat:@"%@",delegate.userLong];
            if([Lat isEqualToString:@"(null)"]){
                Lat = @"";
                Logn = @"";
            }
            //            else{
            if([CheckValue isEqualToString:@"CHECKIN"]){
                params = @ {
                    @"mapping" : @"apis",
                    @"userId" : [ApplicationState userId],
                    @"requestId" : iWorkIdStr,
                    @"latitude" : Lat,
                    @"longitude" : Logn
                };
                ResponseDic = [Api WebApi:params Url:@"checkIn"];
                NSLog(@"ResponseDic==%@",ResponseDic);
                
                if(ResponseDic == nil){
                    [LoadingManager hideLoadingView:self.view];
                    [self WebApiCell];
                }
                else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                    [LoadingManager hideLoadingView:self.view];
                    MsgOuterView.hidden = YES;
                    [self ShowErrorTitle:NSLocalizedString(@"CHECK_IN_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"TITME_COMPLETE" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                    CheckValue = @"MY_IWORK";
                    [MyiWorkTableView reloadData];
                    [self WebApiCell];
                }
                else{
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowAlert:NSLocalizedString(ResponseDic[@"message"], nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                }
            }
            else if([CheckValue isEqualToString:@"CHECKOUT"]){
                params = @ {
                    @"mapping" : @"apis",
                    @"userId" : [ApplicationState userId],
                    @"requestId" : iWorkIdStr,
                    @"latitude" : Lat,
                    @"longitude" : Logn
                };
                ResponseDic = [Api WebApi:params Url:@"checkOut"];
                if(ResponseDic == nil){
                    [LoadingManager hideLoadingView:self.view];
                    [self WebApiCell];
                }
                else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                    [LoadingManager hideLoadingView:self.view];
                    MsgOuterView.hidden = YES;
                    [self ShowErrorTitle:NSLocalizedString(@"CHECK_OUT_TITME", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"TITME_COMPLETE" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                    [MyiWorkTableView reloadData];
                    CheckValue = @"MY_IWORK";
                    [self WebApiCell];
                }
                else if([ResponseDic[@"statusCode"]intValue]==105) // Automatic Checkout
                {
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowErrorTitle:NSLocalizedString(@"HOURS_NOT_COMPLETE_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"CUT_OFF_TIME" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                    [MyiWorkTableView reloadData];
                    CheckValue = @"MY_IWORK";
                    [self WebApiCell];
                }
                else if([ResponseDic[@"statusCode"]intValue]==106) // Automatic Checkout
                {
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowErrorTitle:NSLocalizedString(@"MAXIMUM_CHECKED_OUT_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"CHECKOUT_TIME" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                    [MyiWorkTableView reloadData];
                    CheckValue = @"MY_IWORK";
                    [self WebApiCell];
                }
                else if([ResponseDic[@"statusCode"]intValue]==205)
                {
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowErrorTitle:NSLocalizedString(@"HOURS_NOT_COMPLETE_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"CUT_OFF_TIME" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                    [MyiWorkTableView reloadData];
                    CheckValue = @"MY_IWORK";
                    [self WebApiCell];
                }
                else{
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowAlert:NSLocalizedString(ResponseDic[@"message"], nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                    [MyiWorkTableView reloadData];
                    CheckValue = @"MY_IWORK";
                    [self WebApiCell];
                }
            }
            // }
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(NSString*)GetDayWithDate{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM YYYY"];
    NSDateComponents *component = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSString *day;
    switch ([component weekday]) {
        case 1:
            day = NSLocalizedString(@"SUNDAY", nil);
            break;
        case 2:
            day = NSLocalizedString(@"MONDAY", nil);
            break;
        case 3:
            day = NSLocalizedString(@"TUESDAY", nil);
            break;
        case 4:
            day = NSLocalizedString(@"WEDNESDAY", nil);
            break;
        case 5:
            day = NSLocalizedString(@"THERSDAY", nil);
            break;
        case 6:
            day = NSLocalizedString(@"FRIDAY", nil);
            break;
        case 7:
            day = NSLocalizedString(@"SATURDAY", nil);
            break;
        default:
            break;
    }
    return  [NSString stringWithFormat:@"%@, %@",day,[dateFormat stringFromDate:[NSDate date]]];
}
-(void)iWorkFilterApi:(NSNotification*)notification{
    TemaiworkArray = [[NSMutableArray alloc] init];
    params = notification.userInfo;
    CheckValue = @"IWORKFILTER";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
}
-(NSString*)GetDate:(NSNumber*)Timenumber{
    NSString *DateStr = [NSString stringWithFormat:@"%@",Timenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"HH:mm:ss"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"hh:mm a"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)GetCurrentDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [NSDate date];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return  formattedDate;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    UIColor *themeColor  = [UIColor colorWithRed:(205.0/255.0) green:(25.0/255.0) blue:(24.0/255.0) alpha:1.0f];
    CGFloat offsetY;
    offsetY = scrollView.contentOffset.y;
    if (offsetY >= 0)
    {
        CGFloat height;
        CGFloat maxOffset;
        CGFloat progress;
        height = MyiWorkTableView.tableHeaderView.frame.size.height;
        maxOffset = height - 64;
        progress = (scrollView.contentOffset.y - 64) / maxOffset;
        progress = MIN(progress, 1);
        navbar.rb.backgroundColor = [themeColor colorWithAlphaComponent:progress];
        
//        [circularButton setAlpha:0.0];
//        [viewReportButton setAlpha:0.0];
//        [createReportButton setAlpha:0.0];
        
    }
    else{
//        [circularButton setAlpha:1.0];
//        [viewReportButton setAlpha:1.0];
//        [createReportButton setAlpha:1.0];
    }
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    [circularButton setAlpha:1.0];
//    [viewReportButton setAlpha:1.0];
//    [createReportButton setAlpha:1.0];
}


- (UIColor * _Nonnull)navigationBarInColor{
    return [UIColor clearColor];
}

- (IBAction)circularMenuButtonTapped:(UIButton *)sender
{
    if (circularButton.isSelected)
    {
        [circularButton setImage:[UIImage imageNamed:@"ic_ic_fav_add"] forState: UIControlStateNormal];

        CATransition *transition = [CATransition animation];
        [transition setDuration:0.5];
        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transition setType:kCATransitionPush];
        [transition setSubtype:kCATransitionFromBottom];
        [[viewReportButton layer] addAnimation:transition forKey:nil];
        
        CATransition *transition1 = [CATransition animation];
        [transition1 setDuration:0.5];
        [transition1 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transition1 setType:kCATransitionPush];
        [transition1 setSubtype:kCATransitionFromLeft];
        [[createReportButton layer] addAnimation:transition1 forKey:nil];
        
        createReportButton.hidden = YES;
        viewReportButton.hidden = YES;

    
        [circularButton setSelected:NO];
    }
    else
    {
        [circularButton setImage:[UIImage imageNamed:@"new_ic_fav_close"] forState: UIControlStateNormal];
        [createReportButton setHidden:NO];
        [viewReportButton setHidden:NO];
        
        CATransition *transition = [CATransition animation];
        [transition setDuration:0.5];
        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transition setType:kCATransitionPush];
        [transition setSubtype:kCATransitionFromTop];
        [[viewReportButton layer] addAnimation:transition forKey:nil];
        
        CATransition *transition1 = [CATransition animation];
        [transition1 setDuration:0.5];
        [transition1 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transition1 setType:kCATransitionPush];
        [transition1 setSubtype:kCATransitionFromRight];
        [[createReportButton layer] addAnimation:transition1 forKey:nil];
        
        [circularButton setSelected:YES];
    }
    
}

- (IBAction)createRequestButtonTapped:(UIButton *)sender
{
     [self GoToRequestIWork];
   
}
- (IBAction)viewRequestButtonTapped:(UIButton *)sender
{
    [self GoToViewRequest];
}

-(IBAction)BackAction:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

-(void)callWebServiceForRecentiWorkRequest
{
    if(delegate.isInternetConnected){
        params = @ {
            @"userId": [ApplicationState userId],
        };
        NSDictionary *response = [Api WebApi:params Url:@"recentIworkRequest"];
        if([[response valueForKey:@"statusCode"] intValue]==5){
            
            [LoadingManager hideLoadingView:self.view];
            
            
            if ([response objectForKey:@"object"] != nil){
                recentDataArray = [[response objectForKey:@"object"] objectForKey:@"recentIworks"];
            }
            [MyiWorkTableView reloadData];

        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}

- (IBAction)notificationTapped:(id)sender {
    [delegate DeleteNotificationCounter:@"IWORK"];
    NSMutableDictionary *Dics = [[NSMutableDictionary alloc] init];
    [Dics setValue:@"IWORK" forKey:@"IWORK"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil userInfo:Dics];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotificationViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)UpdateNotifications{
    notificationLabel.alpha=0.0;
    notificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IWORK"]];
    if ([notificationLabel.text intValue]>0) {
        notificationLabel.alpha=1.0;
        notificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IWORK"]];
    }
}

-(void)openProfile{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    ObjViewController.isComeFrom = @"IWORK";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
- (IBAction)ApproveCloseButtonAction:(id)sender
{
    ApproveRejactView.hidden = YES;
}

@end
