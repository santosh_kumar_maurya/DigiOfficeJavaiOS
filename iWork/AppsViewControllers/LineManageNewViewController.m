//
//  LineManageNewViewController.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LineManageNewViewController.h"
#import "NewCell.h"
#import "Header.h"

#define MAX_LENGTH 10
@interface LineManageNewViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *NewTableView;
    NSMutableArray *NewArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSString *user_requests_id;
    IBOutlet UILabel *NoDataLabel;
    NSDictionary *params;
    
    IBOutlet UIView *RejectionOuterView;
    IBOutlet UIView *RejectionInnerView;
    IBOutlet UITextField *RejectionTextView;
    IBOutlet UILabel *RejectionTextViewLine;
    IBOutlet UIButton *RejectionYesBtn;
    IBOutlet UIButton *RejectionNoBtn;
    IBOutlet UIImageView *RequireImageView;
    
    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UIButton *YesBtn;
    IBOutlet UIButton *NoBtn;
    APIService *Api;
    NSDictionary *ResponseDic;
    NSString *requestsId;
    int offset;
    int limit;
    int TagValue;
    IBOutlet UIImageView *NoDataImageView;
}

@end

@implementation LineManageNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CheckValue = @"NEW";
    [self EmptyArray];
    TagValue = 0;
    Api = [[APIService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor whiteColor];
    NewTableView.backgroundColor = [UIColor whiteColor];
    NewTableView.estimatedRowHeight = 500;
    NewTableView.rowHeight = UITableViewAutomaticDimension;
    NewTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, NewTableView.bounds.size.width, 0.0f)];
    
    NewTableView.delegate = self;
    NewTableView.dataSource = self;
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [NewTableView addSubview:refreshControl];
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkLineManagerNew) withObject:nil afterDelay:0.5];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NewApi:) name:@"LineManagerNew" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClearFilterApi:) name:@"LMClearFilterApi" object:nil];
    UINib *reportNib = [UINib nibWithNibName:@"ApprovalsTableViewCell" bundle:nil];
    [NewTableView registerNib:reportNib forCellReuseIdentifier:@"ApprovalsTableViewCell"];
    [self MessageContentView];
    [self RejectionContentView];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
    
    // Do any additional setup after loading the view.
}
- (void)reloadData{
    if ([CheckValue isEqualToString:@"FILTER"])
    {
        CheckValue = @"FILTER";
    }
    else{
        CheckValue = @"Refresh";
    }
    [self EmptyArray];
    [self GetiWorkLineManagerNew];
}
-(void)MessageContentView{
    MsgInnerView.layer.cornerRadius = 5;
    [YesBtn setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
    [YesBtn setBackgroundColor:delegate.redColor];
    [NoBtn setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
//    [NoBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: YesBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
    YesBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: NoBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    NoBtn.layer.mask = maskLayer2;
}
-(void)RejectionContentView{
    RejectionInnerView.layer.cornerRadius = 5;
    RejectionInnerView.clipsToBounds =YES;
    [RejectionYesBtn setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
    [RejectionYesBtn setBackgroundColor:delegate.redColor];
    [RejectionNoBtn setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
//    [RejectionNoBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: RejectionYesBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
    RejectionYesBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: RejectionNoBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    RejectionNoBtn.layer.mask = maskLayer2;
}
-(void)EmptyArray{
    offset = 0;
    limit = 10;
    NewArray = [[NSMutableArray alloc]init];
}
#pragma mark  -UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [NewArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ApprovalsTableViewCell * projectCell = (ApprovalsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ApprovalsTableViewCell" forIndexPath:indexPath];
    projectCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *responseData = NewArray [indexPath.section];
    [projectCell configureApprovalCell:responseData];
    UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
    [projectCell  setUserInteractionEnabled:YES];
    [projectCell addGestureRecognizer:RequestIWork];
    projectCell.approveButton.tag = indexPath.section;
    projectCell.rejectButton.tag = indexPath.section;
    [projectCell.approveButton addTarget:self action:@selector(approveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [projectCell.rejectButton addTarget:self action:@selector(rejectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    if(indexPath.section == 0){
        projectCell.UpperLineLabel.hidden = YES;
    }
    else{
        projectCell.UpperLineLabel.hidden = NO;
    }
    
    return projectCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
    if(ArraysValue.count>0){
        offset = offset + 10;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self GetiWorkLineManagerNew];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [NewArray addObject:BindDataDic];
    }
}
-(void)MapPage{
    NSDictionary *Dic = [NewArray objectAtIndex:TagValue];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
    ObjViewController.MapDic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GotoMapView:(UIButton*)Sender{
    TagValue = Sender.tag;
    [self MapPage];
}
-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    TagValue = sender.view.tag;
    [self MapPage];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField.text.length >= MAX_LENGTH && range.length == 0){
        [self ShowAlert:@"Character Limit Exceed" ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        return NO; // return NO to not change text
    }
    else{
        return YES;
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    RejectionTextView.text = @"";
    RequireImageView.hidden =YES;
    return YES;
}

#pragma mark -
- (void)approveButtonClicked:(UIButton *)sender {
    MsgLabel.text = NSLocalizedString(@"APPROVE_MSG", nil);
    TagValue = sender.tag;
    CheckValue = @"APPROVE";
    MsgOuterView.hidden =NO;
}
- (void)rejectButtonClicked:(UIButton *)sender {
    TagValue = sender.tag;
    CheckValue = @"REJECT";
    RejectionOuterView.hidden =NO;
}
- (void)GetiWorkLineManagerNew{
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"NEW"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"status": @0,
                    @"userId": [ApplicationState userId],
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"viewTeamReport"];
            [refreshControl endRefreshing];
            [LoadingManager hideLoadingView:self.view];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self GetiWorkLineManagerNew];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                NSLog(@"NewNewNewNewNewNewNewNew");
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
                if(ResponseArrays.count>0){
                    NoDataLabel.hidden = YES;
                     NoDataImageView.hidden = YES;
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"]];
                    [NewTableView  reloadData];
                }
                else if(NewArray.count == 0){
                    [self NoIworkRequest];
                }
                [NewTableView  reloadData];
                
            } 
            else{
                if(NewArray.count == 0){
                    [self NoIworkRequest];
                }
            }
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoIworkRequest{
    NoDataImageView.hidden = NO;
    NoDataLabel.hidden = NO;
    NoDataLabel.text =NSLocalizedString(@"NO_IWORK_REQUEST_FOUND", nil);
//    NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
   // [NewTableView  reloadData];
}
-(IBAction)YESBtnAction:(id)sender{
    NSDictionary * responseData = NewArray [TagValue];
    requestsId = [NSString stringWithFormat:@"%@",[responseData objectForKey:@"requestId"]];
    NSLog(@"requestsId==%@",requestsId);
    [self ApproveRejectWebApi];
}
-(void)ApproveRejectWebApi{
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"REJECT"]){
            NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
            NSString *RemarkStr = [RejectionTextView.text stringByAddingPercentEncodingWithAllowedCharacters:set];
            NSString *UrlStr = [NSString stringWithFormat:@"updateStatusOfRequest?status=2&requestId=%@&rejectionRemark=%@",requestsId,RemarkStr];
            NSLog(@"UrlStr==%@",UrlStr);
            ResponseDic = [Api WebApi:nil Url:UrlStr];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self ApproveRejectWebApi];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                MsgOuterView.hidden =YES;
                RejectionOuterView.hidden =YES;
                [self EmptyArray];
                [self GetiWorkLineManagerNew];
                
            }
            else{
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
        else if([CheckValue isEqualToString:@"APPROVE"]){
            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"updateStatusOfRequest?status=1&requestId=%@",requestsId]];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self ApproveRejectWebApi];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                MsgOuterView.hidden =YES;
                [self EmptyArray];
                [self GetiWorkLineManagerNew];
            }
            else{
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
}
-(IBAction)NoBtnAction:(id)sender{
    MsgOuterView.hidden =YES;
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)RejectionYesAction:(id)sender{
    [RejectionTextView resignFirstResponder];
    NSDictionary * responseData = NewArray [TagValue];
    requestsId = [NSString stringWithFormat:@"%@",[responseData objectForKey:@"requestId"]];
    if([RejectionTextView.text isEqualToString:@""]){
        RequireImageView.hidden =NO;
    }
    else if([RejectionTextView.text isEqualToString:@""]){
        RequireImageView.hidden =NO;
    }
    else{
        [self ApproveRejectWebApi];
    }
}
-(IBAction)RejectionNoCrossAction:(id)sender{
    RejectionTextView.text = @"";
//    RejectionTextView.textColor = delegate.borderColor;
    [RejectionTextView resignFirstResponder];
    CheckValue = @"";
    RejectionOuterView.hidden = YES;
//    RejectionTextView.layer.borderColor = delegate.borderColor.CGColor;
    RequireImageView.hidden =YES;
}
-(void)NewApi:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [self GetiWorkLineManagerNew];
}
-(void)ClearFilterApi:(NSNotification*)notification
{
    [self EmptyArray];
    CheckValue = @"NEW";
    [LoadingManager showLoadingView:self.view];
    [self GetiWorkLineManagerNew];
}
- (IBAction)FilterAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FilterViewController *ObjFilterViewController = [storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    ObjFilterViewController.isComefrom =@"LineManagerApprove";
    [self presentViewController:ObjFilterViewController animated:YES completion:nil];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
