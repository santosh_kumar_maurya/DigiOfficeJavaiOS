//
//  LineManagerFilterViewController.h
//  iWork
//
//  Created by Shailendra on 26/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LineManagerFilterViewController : UIViewController

@property(nonatomic,strong)NSString *isComeFrom;
@property(nonatomic,strong)NSString *EmployeeID;
@property(nonatomic,strong)NSMutableDictionary *filterData;

@end
