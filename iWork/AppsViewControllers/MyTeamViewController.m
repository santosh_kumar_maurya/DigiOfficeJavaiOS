//
//  MyTeamViewController.m
//  iWork
//
//  Created by Shailendra on 16/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyTeamViewController.h"
#import "Header.h"
//#import <ASExtendedCircularMenu/ASExtendedCircularMenu-Swift.h>
#import "iWork-Swift.h"

@interface MyTeamViewController ()<UITableViewDelegate,UITableViewDataSource, RainbowColorSource>
{
    IBOutlet UITableView *MyTeamTableView;
    AppDelegate *delegate;
    NSString *CheckValue;
    NSMutableArray *TemaiworkArray;
    NSMutableArray *MyTeamiWorkArrayToday;
    NSDictionary *ResponseDic;
    NSDictionary *params;
    NSDictionary *iWorkParams;
    NSString *SelectDuration;
    UIRefreshControl *refreshControl;
    APIService *Api;
    RainbowNavigation *rainbowNavigation;
    UINavigationBar* navbar;
    IBOutlet UIButton *circularButton;
    IBOutlet UIButton *viewReportButton;
    IBOutlet UIButton *approveReportButton;
    __weak IBOutlet NSLayoutConstraint *approveButtonTrailingConstraints;
    __weak IBOutlet NSLayoutConstraint *viewReoprtButtonBottomConstraints;
    UILabel *notificationLabel;
    UIImageView *profilePic;
    UILabel *lblUserName;
    UILabel *lblAddress;
    NSString *empId;
    NSString *empName;
    NSString *location;
    BOOL isTodayFilter;
    BOOL isMonthFilter;
    NSString *headerStrForMyTeam;
    UIButton *profileButton;
    NSMutableDictionary *filterData;
}
@end

@implementation MyTeamViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureNavigationBar];
    [self configureHeaderView];
    [self configureTableViewCell];
    Api = [[APIService alloc] init];
    filterData = [[NSMutableDictionary alloc] init];
    CheckValue = @"MY_TEAM";
    SelectDuration = @"";
    empId = @"";
    empName = @"";
    location = @"";
    isTodayFilter = NO;
    isMonthFilter = NO;
//    self.view.backgroundColor = delegate.BackgroudColor;
//    MyTeamTableView.backgroundColor = delegate.BackgroudColor;
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    MyTeamTableView.estimatedRowHeight = 200;
    [MyTeamTableView setNeedsLayout];
    MyTeamTableView.bounces = NO;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(callWebSeriveForiWorkToday) withObject:nil afterDelay:0.5];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];

    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
//    [MyTeamTableView addSubview:refreshControl];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MyTeamFilterApi:) name:@"MyTeamFilter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iWorkFilterApi:) name:@"iWorkFilter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetDuration:) name:@"SelectDurations" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateNotifications) name:@"Notification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iWorkClearFilter:) name:@"iWorkClearFilter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(TeamClearFilter:) name:@"TeamClearFilter" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self UpdateNotifications];
    
}

//-(void)viewWillDisappear:(BOOL)animated
//{
//    NSLog(@"Disappear");
//}

-(void)configureTableViewCell{
    [MyTeamTableView registerNib:[UINib nibWithNibName:@"MyTeamHeaderViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"MyTeamHeaderViewTableViewCell"];
    [MyTeamTableView registerNib:[UINib nibWithNibName:@"NoiWorkTableViewCell" bundle:nil] forCellReuseIdentifier:@"NoiWorkTableViewCell"];
}

-(void)configureNavigationBar{
    navbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 44)];
    [navbar setBackgroundImage:[UIImage new]
                 forBarMetrics:UIBarMetricsDefault];
    navbar.shadowImage = [UIImage new];
    navbar.translucent = YES;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 20.0f, self.view.frame.size.width, navbar.frame.size.height)];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(-5.0f, 17.0f, 15.0f, 15.0f)];
    [backButton addTarget:self action:@selector(BackAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"new_ic_back"] forState:UIControlStateNormal];
    backButton.imageView.contentMode = UIViewContentModeCenter;
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 100.0f, 30.0f)];
    //    NSString *MyIWorkStr = NSLocalizedString(@"MY_IWORK", nil);
    //MyIWorkStr = [[MyIWorkStr stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]] uppercaseString];
    [title setFont:[UIFont boldSystemFontOfSize:17]];
    title.text = NSLocalizedString(@"IWORK", nil);
    title.textColor = [UIColor whiteColor];
    
    UITapGestureRecognizer* titleGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(BackAction:)];
    [title  setUserInteractionEnabled:YES];
    [title addGestureRecognizer:titleGesture];
    
    [customView addSubview:backButton];
    [customView addSubview:title];
    
    UIButton *notificationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [notificationButton setFrame:CGRectMake(self.view.frame.size.width - 60, 10.0f, 30.0f, 30.0f)];
    [notificationButton addTarget:self action:@selector(notificationTapped:) forControlEvents:UIControlEventTouchUpInside];
    [notificationButton setImage:[UIImage imageNamed:@"new_ic_notification"] forState:UIControlStateNormal];
    notificationButton.imageView.contentMode = UIViewContentModeCenter;
    
    notificationLabel = [[UILabel alloc] initWithFrame:CGRectMake(notificationButton.frame.origin.x + 15, 5.0f, 20.0f, 20.0f)];
    notificationLabel.text = @"0";
    notificationLabel.layer.cornerRadius = 10;
    notificationLabel.clipsToBounds = YES;
    notificationLabel.backgroundColor = [UIColor colorWithRed:(236.0/255.0) green:(27.0/255.0) blue:(33.0/255.0) alpha:1.0f];
    notificationLabel.textAlignment = NSTextAlignmentCenter;
    notificationLabel.textColor = [UIColor whiteColor];
    
    
    UIButton *HeaderProfileButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [HeaderProfileButton setFrame:CGRectMake(self.view.frame.size.width - 100, 10.0f, 30.0f, 25.0f)];
    [HeaderProfileButton addTarget:self action:@selector(openProfile) forControlEvents:UIControlEventTouchUpInside];
    [HeaderProfileButton setImage:[UIImage imageNamed:@"New_Profile"] forState:UIControlStateNormal];
    HeaderProfileButton.imageView.contentMode = UIViewContentModeCenter;
    
    
    
    
    UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [homeButton setFrame:CGRectMake(self.view.frame.size.width - 140, 10.0f, 30.0f, 25.0f)];
    [homeButton addTarget:self action:@selector(BackAction:) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setImage:[UIImage imageNamed:@"HomeImage"] forState:UIControlStateNormal];
    homeButton.imageView.contentMode = UIViewContentModeCenter;
    
    [customView addSubview:homeButton];
    [customView addSubview:HeaderProfileButton];
    [customView addSubview:notificationButton];
    [customView addSubview:notificationLabel];
    
    UINavigationItem* navItem = [[UINavigationItem alloc] init];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:customView];
    
    navItem.leftBarButtonItem = leftButton;
    //    navItem.rightBarButtonItem = rightButton;
    [navbar setItems:@[navItem]];
    
    
    [self.view addSubview:navbar];
    
}

-(void)configureHeaderView
{
    NSDictionary *Dic = [ApplicationState getUserLoginData];
    
    rainbowNavigation = [[RainbowNavigation alloc] init];
    [approveReportButton setHidden:YES];
    [viewReportButton setHidden:YES];
    approveButtonTrailingConstraints.constant = 0.0;
    viewReoprtButtonBottomConstraints.constant = 0.0;
   
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIImageView *imageView =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.width * 0.65)];
    imageView.image=[UIImage imageNamed:@"My_iWork_Dashboard_Navigation"];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    
    //Yellow Header
    UIView *yellowBorder = [[UIView alloc] initWithFrame:CGRectMake(0, imageView.frame.size.height - 6, self.view.frame.size.width, 6)];
    yellowBorder.backgroundColor = [UIColor LineGreyColor];;
    [imageView addSubview:yellowBorder];
    
    //ProfilePic
    profilePic = [[UIImageView alloc] initWithFrame:CGRectMake(10 , (imageView.frame.size.height - yellowBorder.frame.size.height - 55), 50, 50)];
    profilePic.layer.cornerRadius = profilePic.frame.size.height/2;
    profilePic.clipsToBounds = YES;
    
    NSString *URL = [NSString stringWithFormat:@"%@",[[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]];
    NSLog(@"URL==%@",URL);
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]))){
        NSURL *Url = [NSURL URLWithString:URL];
        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL:Url]];
        if(images == nil){
            profilePic.image = [UIImage imageNamed:@"profile_image_default"];
        }
        else{
            [profilePic sd_setImageWithURL:[NSURL URLWithString:URL]];
        }
    }
    else{
        profilePic.image = [UIImage imageNamed:@"profile_image_default"];
    }
    [imageView addSubview:profilePic];
    
    //UserName
    lblUserName = [[UILabel alloc] initWithFrame:CGRectMake(profilePic.frame.size.width + 20 , profilePic.frame.origin.y + 10, (self.view.frame.size.width - (profilePic.frame.size.width + 30)), 25)];
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]))){
        lblUserName.text = [NSString stringWithFormat:@"%@", [[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]];
    }
    else{
        lblUserName.text = @" ";
    }
    [lblUserName setFont:[UIFont fontWithName:@"ooredoo-Bold" size:14]];
    lblUserName.textColor = [UIColor whiteColor];
    [imageView addSubview:lblUserName];
    
    //Location Icon
    UIImageView *locationImageView =[[UIImageView alloc] initWithFrame:CGRectMake(profilePic.frame.size.width + 20, lblUserName.frame.origin.y + lblUserName.frame.size.height , 15, 15)];
    locationImageView.image=[UIImage imageNamed:@"new_ic_map"];
    [locationImageView setContentMode:UIViewContentModeScaleAspectFill];
    [imageView addSubview:locationImageView];
    
    //Address
    lblAddress = [[UILabel alloc] initWithFrame:CGRectMake(profilePic.frame.size.width + 20 + 20 , lblUserName.frame.origin.y + lblUserName.frame.size.height - 3 , (self.view.frame.size.width - (profilePic.frame.size.width + 30)), 20)];
    lblAddress.text = [NSString stringWithFormat:@"%@", [ApplicationState getLocationName]];
    [lblAddress setFont:[UIFont fontWithName:@"Helvetica Neue" size:12]];
    lblAddress.textColor = [UIColor whiteColor];
    [imageView addSubview:lblAddress];
    
    //Button
    profilePic.multipleTouchEnabled = YES;
    profilePic.userInteractionEnabled = YES;
    
    profileButton = [[UIButton alloc] initWithFrame:CGRectMake(10 , (imageView.frame.size.height - yellowBorder.frame.size.height - 55), 50, 50)];
    
    [profileButton addTarget:self action:@selector(openProfile) forControlEvents:UIControlEventTouchUpInside];
    imageView.userInteractionEnabled = YES;
    imageView.multipleTouchEnabled =YES;
    profileButton.userInteractionEnabled = YES;
    profileButton.multipleTouchEnabled =YES;
    
    [imageView addSubview:profileButton];
    
    MyTeamTableView.tableHeaderView = imageView;
}

- (void)reloadData{
    CheckValue = @"Refresh";
    [self WebApiCell];
    [self callWebSeriveForiWorkToday];
}
- (void)WebApiCell {
    if(delegate.isInternetConnected){
        
        if([CheckValue isEqualToString:@"MY_TEAM"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"MYTEAMFILTER"]){
            if(![CheckValue isEqualToString:@"MYTEAMFILTER"]){
                params = @ {
                    @"duration": @"month",
                    @"empId": @"",
                    @"empName": @"",
                    @"endDate": @"",
                    @"startDate": @"",
                    @"userId": [ApplicationState userId],
                };
            }
        }
        NSLog(@"params===%@",params);
        ResponseDic = [Api WebApi:params Url:@"myTeam"];
        TemaiworkArray = [[NSMutableArray alloc] init];
//        [refreshControl endRefreshing];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self WebApiCell];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5)
        {
            [LoadingManager hideLoadingView:self.view];
            if([CheckValue isEqualToString:@"MYTEAMFILTER"]){
                isMonthFilter = YES;
            }
            else{
                isMonthFilter = NO;
            }
            TemaiworkArray = [ResponseDic valueForKey:@"object"];
            [MyTeamTableView reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        if(MyTeamiWorkArrayToday.count>0)
        {
            return MyTeamiWorkArrayToday.count;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        if(TemaiworkArray.count>0)
        {
            return TemaiworkArray.count;
        }
        else
        {
            return 1;
        }
    }
//    return  [TemaiworkArray count] +2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0)
    {
        if (MyTeamiWorkArrayToday.count > 0)
        {
            EmployeeCell *Cell = (EmployeeCell*)[MyTeamTableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
            
            Cell.backgroundColor = [UIColor clearColor];
            Cell.CellBgView.tag = indexPath.row;
            UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iWorkGoToMapPage:)];
            [Cell.CellBgView  setUserInteractionEnabled:YES];
            [Cell.CellBgView addGestureRecognizer:RequestIWork];
            NSDictionary *Dic = [MyTeamiWorkArrayToday objectAtIndex:indexPath.row];
            if(IsSafeStringPlus(TrToString(Dic[@"userName"]))){
                Cell.UserNameLabel.text = Dic[@"userName"];
            }
            else{
                Cell.UserNameLabel.text = @"";
            }
            if(IsSafeStringPlus(TrToString(Dic[@"checkInTime"]))){
                NSNumber *Timenumber = Dic[@"checkInTime"];
                Cell.CheckInValueLabel.text = [self GetDate:Timenumber];
            }
            else{
                Cell.CheckInValueLabel.text = @"__:__";
            }
            if(IsSafeStringPlus(TrToString(Dic[@"checkOutTime"]))){
                NSNumber *Timenumber = Dic[@"checkOutTime"];
                Cell.CheckOutValueLabel.text =[self GetDate:Timenumber];
                
            }
            else{
                Cell.CheckOutValueLabel.text = @"__:__";
            }
            
            if(IsSafeStringPlus(TrToString(Dic[@"location"]))){
                Cell.LacationLabel.text = Dic[@"location"];
            }
            else{
                Cell.LacationLabel.text = @"";
            }
            if(IsSafeStringPlus(TrToString(Dic[@"dp"]))){
                UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:Dic[@"dp"]]]];
                if(images == nil){
                    Cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
                }
                else{
                    NSString *URL = [NSString stringWithFormat:@"%@",Dic[@"dp"]];
                    [Cell.ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
                }
            }
            else{
                Cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
            }
            if (indexPath.row == 0){
                [Cell.topLine setHidden:YES];
            }
            else
            {
                [Cell.topLine setHidden:NO];
            }
            Cell.ProfileImageView.layer.cornerRadius = 22.5;
            Cell.ProfileImageView.clipsToBounds =YES;
            
            
            if ((IsSafeStringPlus(TrToString(Dic[@"checkInTime"]))) && (IsSafeStringPlus(TrToString(Dic[@"checkOutTime"]))))
            {
                [Cell configureCell:[UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f] backGround:[UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f]];
            }
            else if ((IsSafeStringPlus(TrToString(Dic[@"checkInTime"]))) && (!IsSafeStringPlus(TrToString(Dic[@"checkOutTime"]))))
            {
                [Cell configureCell:[UIColor colorWithRed:(247.0/255.0) green:(43.0/255.0) blue:(44.0/255.0) alpha:1.0f] backGround:[UIColor whiteColor]];
            }
            else if ((!IsSafeStringPlus(TrToString(Dic[@"checkInTime"]))) && (IsSafeStringPlus(TrToString(Dic[@"checkOutTime"]))))
            {
                [Cell configureCell:[UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f] backGround:[UIColor whiteColor]];
            }
            else
            {
                [Cell configureCell:[UIColor colorWithRed:(247.0/255.0) green:(43.0/255.0) blue:(44.0/255.0) alpha:1.0f] backGround:[UIColor whiteColor]];
            }
            return Cell;
        }
        else
        {
            NoiWorkTableViewCell *Cell = (NoiWorkTableViewCell*)[MyTeamTableView dequeueReusableCellWithIdentifier:@"NoiWorkTableViewCell" forIndexPath:indexPath];
            Cell.NoIworkRequestLabel.text = @"No team iWork request planned for today";
            Cell.HorizentalLineLabel.hidden = NO;
            return Cell;
        }
    }
    else
    {
        if(TemaiworkArray.count>0)
        {
            ApproveRejectCell *Cell = (ApproveRejectCell*)[MyTeamTableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
            if (indexPath.row == 0)
            {
                Cell.upperLine.hidden = YES;
            }
            else
            {
                Cell.upperLine.hidden = NO;
            }
            Cell.backgroundColor = [UIColor clearColor];
            Cell.CellBgView.tag = indexPath.row;
            UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToEmpDetails:)];
            [Cell.CellBgView  setUserInteractionEnabled:YES];
            [Cell.CellBgView addGestureRecognizer:RequestIWork];
            NSDictionary *Dic;
            if(TemaiworkArray.count>0){
                Dic = [TemaiworkArray objectAtIndex:indexPath.row];
                if(IsSafeStringPlus(TrToString(Dic[@"dayApproved"]))){
                    Cell.ApproveValueLabel.text = [NSString stringWithFormat:@"%d",[Dic[@"dayApproved"] intValue]];
                }
                else{
                    Cell.ApproveValueLabel.text = @"";
                }
                if(IsSafeStringPlus(TrToString(Dic[@"dayRejected"]))){
                    Cell.RejectValueLabel.text =  [NSString stringWithFormat:@"%d",[Dic[@"dayRejected"] intValue]];
                }
                else{
                    Cell.RejectValueLabel.text = @"";
                }
                if(IsSafeStringPlus(TrToString(Dic[@"uName"]))){
                    Cell.UserNameLabel.text = Dic[@"uName"];
                }
                else{
                    Cell.UserNameLabel.text = @"";
                }
                if(IsSafeStringPlus(TrToString(Dic[@"dp"]))){
                    UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:Dic[@"dp"]]]];
                    if(images == nil){
                        NSLog(@"image nil");
                        Cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
                    }
                    else{
                        NSString *URL = [NSString stringWithFormat:@"%@",Dic[@"dp"]];
                        [Cell.ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
                    }
                }
                else{
                    Cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
                }
            }
            Cell.ProfileImageView.layer.cornerRadius = 22.5;
            Cell.ProfileImageView.clipsToBounds =YES;
            return Cell;

        }
        else
        {
            NoiWorkTableViewCell *Cell = (NoiWorkTableViewCell*)[MyTeamTableView dequeueReusableCellWithIdentifier:@"NoiWorkTableViewCell" forIndexPath:indexPath];
            Cell.NoIworkRequestLabel.text = @"No iWork request planned";
            Cell.HorizentalLineLabel.hidden = YES;
            return Cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MyTeamHeaderViewTableViewCell *Cell = (MyTeamHeaderViewTableViewCell*)[MyTeamTableView dequeueReusableCellWithIdentifier:@"MyTeamHeaderViewTableViewCell"];
//    Cell.btnFilter.layer.cornerRadius = 7.0;
//    Cell.btnFilter.layer.borderWidth = 1.5;
//    Cell.btnFilter.layer.borderColor = [UIColor clearColor].CGColor;
//    [Cell.btnFilter setBackgroundColor:[UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f]];
//    [Cell.btnFilter setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
 
    Cell.btnFilter.clipsToBounds = YES;
    Cell.imgView.image = [UIImage imageNamed:@"new_ic_filter"];
    
    if (section == 0)
    {
        Cell.headerLabel.text = @"My Team on iWork Today";
       // Cell.headerLabel.text = [[Cell.headerLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]] uppercaseString];
        if (isTodayFilter == YES)
        {
//            Cell.btnFilter.layer.cornerRadius = 7.0;
//            Cell.btnFilter.layer.borderWidth = 0.0;
//            Cell.btnFilter.layer.borderColor = [UIColor clearColor].CGColor;
//            [Cell.btnFilter setBackgroundColor:[UIColor colorWithRed:(241.0/255.0) green:(241.0/255.0) blue:(241.0/255.0) alpha:1.0f]];
//            [Cell.btnFilter setTitleColor:[UIColor colorWithRed:(186.0/255.0) green:(190.0/255.0) blue:(193.0/255.0) alpha:1.0] forState:UIControlStateNormal];
//            Cell.btnFilter.clipsToBounds = YES;
//            Cell.imgView.image = [UIImage imageNamed:@"ic_new_gray_filter"];
            [Cell.btnFilter setImage:[UIImage imageNamed:@"ic_new_gray_filter"] forState:UIControlStateNormal];
        }
        [Cell.btnFilter addTarget:self action:@selector(iWorkFilterAction) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        if ([CheckValue isEqualToString:@"MYTEAMFILTER"])
        {
            if ([SelectDuration isEqualToString:@""])
            {
                Cell.headerLabel.text = @"iWork for Specify Date";
            }
            else
            {
                Cell.headerLabel.text = [NSString stringWithFormat:@"iWork this %@",SelectDuration];
            }
        }
        else
        {
            Cell.headerLabel.text = @"iWork this Month!";
        }

        
       // Cell.headerLabel.text = [[Cell.headerLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]] uppercaseString];
        if (isMonthFilter == YES)
        {
//            Cell.btnFilter.layer.cornerRadius = 7.0;
//            Cell.btnFilter.layer.borderWidth = 0.0;
//            Cell.btnFilter.layer.borderColor = [UIColor clearColor].CGColor;
//            [Cell.btnFilter setBackgroundColor:[UIColor colorWithRed:(241.0/255.0) green:(241.0/255.0) blue:(241.0/255.0) alpha:1.0f]];
//            [Cell.btnFilter setTitleColor:[UIColor colorWithRed:(186.0/255.0) green:(190.0/255.0) blue:(193.0/255.0) alpha:1.0] forState:UIControlStateNormal];
//            Cell.btnFilter.clipsToBounds = YES;
//            Cell.imgView.image = [UIImage imageNamed:@"ic_new_gray_filter"];
            [Cell.btnFilter setImage:[UIImage imageNamed:@"ic_new_gray_filter"] forState:UIControlStateNormal];
        }
        [Cell.btnFilter addTarget:self action:@selector(FilterAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return Cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0.0;
    }
    return 80.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *sampleView = [[UIView alloc] init];
    sampleView.frame = CGRectMake(0, 0, self.view.frame.size.width, 80);
    sampleView.backgroundColor = [UIColor clearColor];
    return sampleView;
}


-(void)GoToRequestIWork{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LineManagerRequestViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"LineManagerRequestViewController"];
     ObjViewController.isComeFrom = @"Approvel";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GoToViewRequest{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    LineManagerRequestViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"LineManagerRequestViewController"];
//    ObjViewController.isComeFrom = @"Report";
//    [[self navigationController] pushViewController:ObjViewController animated:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LineManagerParentViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"LineManagerParentViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)FilterAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyTeamFilterViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTeamFilterViewController"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict = [params mutableCopy];
    if ([CheckValue isEqualToString:@"MY_TEAM"] )
    {
       [dict setValue:@"" forKey:@"duration"];
    }
    else if ([CheckValue isEqualToString:@"MYTEAMFILTER"])
    {
        if ([[dict valueForKey:@"startDate"] isEqualToString:@""])
        {
        }
        else
        {
            [dict setValue:@"SpecificDate" forKey:@"duration"];
        }
    }
    ObjViewController.filterData = dict;
    [self presentViewController:ObjViewController animated:YES completion:nil];
}
-(void)iWorkFilterAction{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyWorkFilterViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyWorkFilterViewController"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict = [iWorkParams mutableCopy];
    if ([CheckValue isEqualToString:@"IWORKFILTER"])
    {
        NSLog(@"Filter");
    }
    else
    {
        NSLog(@"Without_Filter");
    }
    ObjViewController.filterData = dict;
    [self presentViewController:ObjViewController animated:YES completion:nil];
    
}
-(void)iWorkFilterApi:(NSNotification*)notification{
    MyTeamiWorkArrayToday = [[NSMutableArray alloc] init];
    iWorkParams = notification.userInfo;
    CheckValue = @"IWORKFILTER";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(callWebSeriveForiWorkToday) withObject:nil afterDelay:0.5];
}
-(void)MyTeamFilterApi:(NSNotification*)notification{
    TemaiworkArray = [[NSMutableArray alloc] init];
    params = notification.userInfo;
    CheckValue = @"MYTEAMFILTER";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
}
-(void)GetDuration:(NSNotification*)notification{
    NSDictionary *Dic = notification.userInfo;
    SelectDuration = [Dic valueForKey:@"Duration"];
}
-(void)GoToEmpDetails:(UIGestureRecognizer*)sender{
    int TagValue = sender.view.tag;
    NSDictionary *Dic = TemaiworkArray[TagValue];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EmployeeDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"EmployeeDetailsViewController"];
    ObjViewController.Dic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    UIColor *themeColor  = [UIColor colorWithRed:(205.0/255.0) green:(25.0/255.0) blue:(24.0/255.0) alpha:1.0f];
    CGFloat offsetY;
    offsetY = scrollView.contentOffset.y;
    if (offsetY >= 0)
    {
        CGFloat height;
        CGFloat maxOffset;
        CGFloat progress;
        height = MyTeamTableView.tableHeaderView.frame.size.height;
        maxOffset = height - 64;
        progress = (scrollView.contentOffset.y - 64) / maxOffset;
        progress = MIN(progress, 1);
        navbar.rb.backgroundColor = [themeColor colorWithAlphaComponent:progress];
        
//        [circularButton setAlpha:0.0];
//        [viewReportButton setAlpha:0.0];
//        [approveReportButton setAlpha:0.0];
        
    }
    else{
//        [circularButton setAlpha:1.0];
//        [viewReportButton setAlpha:1.0];
//        [approveReportButton setAlpha:1.0];
    }
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    [circularButton setAlpha:1.0];
//    [viewReportButton setAlpha:1.0];
//    [approveReportButton setAlpha:1.0];
}


- (UIColor * _Nonnull)navigationBarInColor{
    return [UIColor clearColor];
}

- (IBAction)circularMenuButtonTapped:(UIButton *)sender
{
    if (circularButton.isSelected)
    {
        [circularButton setImage:[UIImage imageNamed:@"ic_ic_fav_add"] forState: UIControlStateNormal];

        viewReoprtButtonBottomConstraints.constant = 0.0;
        approveButtonTrailingConstraints.constant = 0.0;
        CATransition *transition = [CATransition animation];
        [transition setDuration:0.5];
        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transition setType:kCATransitionPush];
        [transition setSubtype:kCATransitionFromBottom];
        [[approveReportButton layer] addAnimation:transition forKey:nil];
        
        CATransition *transition1 = [CATransition animation];
        [transition1 setDuration:0.5];
        [transition1 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transition1 setType:kCATransitionPush];
        [transition1 setSubtype:kCATransitionFromLeft];
        [[viewReportButton layer] addAnimation:transition1 forKey:nil];
        
        approveReportButton.hidden = YES;
        viewReportButton.hidden = YES;

        [circularButton setSelected:NO];
    }
    else
    {
        [circularButton setImage:[UIImage imageNamed:@"new_ic_fav_close"] forState: UIControlStateNormal];
      
        approveReportButton.hidden = NO;
        viewReportButton.hidden = NO;
        viewReoprtButtonBottomConstraints.constant = 20.0;
        approveButtonTrailingConstraints.constant = 15.0;
        
        CATransition *transition = [CATransition animation];
        [transition setDuration:0.5];
        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transition setType:kCATransitionPush];
        [transition setSubtype:kCATransitionFromTop];
        [[approveReportButton layer] addAnimation:transition forKey:nil];
        
        CATransition *transition1 = [CATransition animation];
        [transition1 setDuration:0.5];
        [transition1 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transition1 setType:kCATransitionPush];
        [transition1 setSubtype:kCATransitionFromRight];
        [[viewReportButton layer] addAnimation:transition1 forKey:nil];
        
        [circularButton setSelected:YES];
    }
    
}

- (IBAction)approveReportButtonTapped:(UIButton *)sender
{
    
    [self GoToViewRequest];
}

- (IBAction)viewReportButtonTapped:(UIButton *)sender
{
    [self GoToRequestIWork];
    
}

-(IBAction)BackAction:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

-(NSString*)GetDate:(NSNumber*)Timenumber{
    NSString *DateStr = [NSString stringWithFormat:@"%@",Timenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"HH:mm:ss"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"hh:mm a"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)GetCurrentDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [NSDate date];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return  formattedDate;
}
-(NSString*)GetDayWithDate{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM YYYY"];
    NSDateComponents *component = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSString *day;
    switch ([component weekday]) {
        case 1:
            day = NSLocalizedString(@"SUNDAY", nil);
            break;
        case 2:
            day = NSLocalizedString(@"MONDAY", nil);
            break;
        case 3:
            day = NSLocalizedString(@"TUESDAY", nil);
            break;
        case 4:
            day = NSLocalizedString(@"WEDNESDAY", nil);
            break;
        case 5:
            day = NSLocalizedString(@"THERSDAY", nil);
            break;
        case 6:
            day = NSLocalizedString(@"FRIDAY", nil);
            break;
        case 7:
            day = NSLocalizedString(@"SATURDAY", nil);
            break;
        default:
            break;
    }
    return  [NSString stringWithFormat:@"%@, %@",day,[dateFormat stringFromDate:[NSDate date]]];
}
-(void)callWebSeriveForiWorkToday{
    if(delegate.isInternetConnected){
        if(![CheckValue isEqualToString:@"IWORKFILTER"])
        {
            iWorkParams = @ {
                @"empId": empId,
                @"empName": empName,
                @"location": location,
                @"userId": [ApplicationState userId]
            };
        }
        NSLog(@"%@",iWorkParams);
        NSDictionary *response = [Api WebApi:iWorkParams Url:@"myIwork"];
        if(response == nil){
            [LoadingManager hideLoadingView:self.view];
            [self callWebSeriveForiWorkToday];
        }
        else if([[response valueForKey:@"statusCode"] intValue]==5){
            
            [LoadingManager hideLoadingView:self.view];
            if([CheckValue isEqualToString:@"IWORKFILTER"])
            {
                isTodayFilter = YES;
            }
            else{
                isTodayFilter = NO;
            }
            if(IsSafeStringPlus(TrToString([response[@"object"]valueForKey:@"teamRequestList"])))
            {
                MyTeamiWorkArrayToday = [response[@"object"]valueForKey:@"teamRequestList"];
                
            }
            [MyTeamTableView reloadData];

        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}

-(void)iWorkGoToMapPage:(UIGestureRecognizer*)sender{
    int TagValue = sender.view.tag;
    NSDictionary *Dic = [MyTeamiWorkArrayToday objectAtIndex:TagValue];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
    ObjViewController.MapDic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}

- (IBAction)notificationTapped:(id)sender {
    [delegate DeleteNotificationCounter:@"IWORK"];
    NSMutableDictionary *Dics = [[NSMutableDictionary alloc] init];
    [Dics setValue:@"IWORK" forKey:@"IWORK"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil userInfo:Dics];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotificationViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)UpdateNotifications{
    notificationLabel.alpha=0.0;
    notificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IWORK"]];
    if ([notificationLabel.text intValue]>0) {
        notificationLabel.alpha=1.0;
        notificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IWORK"]];
    }
}
-(void)openProfile{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    ObjViewController.isComeFrom = @"IWORK";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)iWorkClearFilter:(NSNotification*)notification
{
    MyTeamiWorkArrayToday = [[NSMutableArray alloc] init];
    CheckValue = @"";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(callWebSeriveForiWorkToday) withObject:nil afterDelay:0.5];
}
-(void)TeamClearFilter:(NSNotification*)notification
{
    TemaiworkArray = [[NSMutableArray alloc] init];
    CheckValue = @"MY_TEAM";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
}
@end
