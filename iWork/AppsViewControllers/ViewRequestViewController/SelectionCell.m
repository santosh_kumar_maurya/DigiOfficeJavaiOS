//
//  ButtonCell.m
//  iWork
//
//  Created by Shailendra on 17/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "SelectionCell.h"

@implementation SelectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
