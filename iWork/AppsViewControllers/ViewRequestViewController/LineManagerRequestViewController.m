//
//  LineManagerRequestViewController.m
//  iWork
//
//  Created by Fourbrick on 18/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import "LineManagerRequestViewController.h"
#import "Header.h"
#import "iWork-Swift.h"

#define MAX_LENGTH 200
@interface LineManagerRequestViewController ()<BIZPopupViewControllerDelegate,UIScrollViewDelegate, UITextFieldDelegate>
{
    IBOutlet UILabel *HeaderLabel;
    NSString *CheckValue;
    int TagValue;
    AppDelegate *delegate;
    
    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UIButton *YesBtn;
    IBOutlet UIButton *NoBtn;
    APIService *Api;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    NSString *requestsId;
    int offset;
    int limit;
    
    IBOutlet UIView *RejectionOuterView;
    IBOutlet UIView *RejectionInnerView;
    IBOutlet UITextField *RejectionTextView;
    IBOutlet UILabel *RejectionTextViewLine;
    IBOutlet UIButton *RejectionYesBtn;
    IBOutlet UIButton *RejectionNoBtn;
    IBOutlet UIImageView *RequireImageView;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UIImageView *NoDataImageView;
    
    
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (strong, nonatomic) NSMutableArray *requestArray;
@property (strong, nonatomic) NSMutableArray *approvalArray;
@property (strong, nonatomic) NSMutableArray *reportArray;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation LineManagerRequestViewController
@synthesize isComeFrom;

- (void)viewDidLoad {
    [super viewDidLoad];
    requestsId = @"";
    Api = [[APIService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    //self.tableView.tableHeaderView = [[UIView alloc]init];
    //self.tableView.backgroundColor = delegate.BackgroudColor;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.01f)];
    self.view.backgroundColor = [UIColor whiteColor];
   
//    UINib *approvalNib = [UINib nibWithNibName:@"ApprovalsTableViewCell" bundle:nil];
    UINib *approvalNib = [UINib nibWithNibName:@"LMViewRequestApproveTableViewCell" bundle:nil];
    UINib *reportNib = [UINib nibWithNibName:@"ReportsTableViewCell" bundle:nil];
    [self.tableView registerNib:approvalNib forCellReuseIdentifier:@"LMViewRequestApproveTableViewCell"];
//    [self.tableView registerNib:approvalNib forCellReuseIdentifier:@"ApprovalsTableViewCell"];
    [self.tableView registerNib:reportNib forCellReuseIdentifier:@"ReportsTableViewCell"];
  
    _tableView.estimatedRowHeight = 500;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(reloadData) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:self.refreshControl];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LineManageFilterApi:) name:@"LineManagerFilter" object:nil];
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LineManageClearFilterApi:) name:@"LineManagerClearFilter" object:nil];
    [self MessageContentView];
    [self RejectionContentView];
    if([isComeFrom isEqualToString:@"Approvel"]){
        HeaderLabel.text = @"Approval";
    }
    else if([isComeFrom isEqualToString:@"Report"]){
         HeaderLabel.text = @"Report";
    }
    [self EmptyArray];
    [self retriveDataFromServer];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];

}
-(void)MessageContentView{
    MsgInnerView.layer.cornerRadius = 5;
   // [YesBtn setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
   // [YesBtn setBackgroundColor:delegate.redColor];
   // [NoBtn setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
   // [NoBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: YesBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
    YesBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: NoBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    NoBtn.layer.mask = maskLayer2;
}
-(void)RejectionContentView{
    RejectionInnerView.layer.cornerRadius = 5;
    RejectionInnerView.clipsToBounds =YES;
//    [RejectionYesBtn setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
    [RejectionYesBtn setBackgroundColor:delegate.redColor];
//    [RejectionNoBtn setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
//    [RejectionNoBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: RejectionYesBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
    RejectionYesBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: RejectionNoBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    RejectionNoBtn.layer.mask = maskLayer2;
    RejectionTextView.delegate = self;
  
}
-(void)EmptyArray{
    offset = 0;
    limit = 10;
    _reportArray = [[NSMutableArray alloc]init];
    _approvalArray = [[NSMutableArray alloc]init];
    [self.tableView reloadData];
}
- (void)reloadData{
    if([CheckValue isEqualToString:@"APPROVEFILTER"]){
        CheckValue = @"APPROVEFILTER";
    }
    else{
        if([isComeFrom isEqualToString:@"Approvel"]){
            CheckValue = @"Refersh";
        }
        else if([isComeFrom isEqualToString:@"Report"]){
            CheckValue = @"Refershs";
        }
    }
    [self EmptyArray];
    [self retriveDataFromServer];
}
#pragma mark  -UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if([isComeFrom isEqualToString:@"Approvel"]){
        return 1;
    }
    else if([isComeFrom isEqualToString:@"Report"]){
        return [_reportArray count];
    }
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_approvalArray count];;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *Cell;
    if([isComeFrom isEqualToString:@"Approvel"])
    {
        LMViewRequestApproveTableViewCell * projectCell = (LMViewRequestApproveTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"LMViewRequestApproveTableViewCell" forIndexPath:indexPath];

//        ApprovalsTableViewCell * projectCell = (ApprovalsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ApprovalsTableViewCell" forIndexPath:indexPath];
        projectCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(indexPath.row == 0){
            projectCell.UpperLineLabel.hidden = YES;
        }
        else{
            projectCell.UpperLineLabel.hidden = NO;
        }
        
        NSDictionary *responseData = _approvalArray [indexPath.row];
        [projectCell configureApprovalCell:responseData];
        projectCell.approveButton.tag = indexPath.row;
        projectCell.rejectButton.tag = indexPath.row;
        [projectCell.approveButton addTarget:self action:@selector(approveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [projectCell.rejectButton addTarget:self action:@selector(rejectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        projectCell.CercilLabel.backgroundColor = [UIColor orangeColor];
        projectCell.sepratorLabel.hidden = YES;
        return projectCell;
    }
    else {
        ReportsTableViewCell * projectCell = (ReportsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ReportsTableViewCell" forIndexPath:indexPath];
        projectCell.selectionStyle = UITableViewCellSelectionStyleNone;
        [projectCell.ViewDetailsBtn addTarget:self action:@selector(GotoMapView:) forControlEvents:UIControlEventTouchUpInside];
        projectCell.ViewDetailsBtn.tag = indexPath.section;
        projectCell.tag  = indexPath.section;
        UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
        [projectCell  setUserInteractionEnabled:YES];
        [projectCell addGestureRecognizer:RequestIWork];
        if(_reportArray.count>0){
            [projectCell configureReportCell:_reportArray[indexPath.section]];
        }
        return projectCell;
    }
    return Cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 8;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if([isComeFrom isEqualToString:@"Approvel"]){
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 10;
            if([CheckValue isEqualToString:@"APPROVEFILTER"]){
                NSMutableDictionary *NewDic = [params mutableCopy];
                for (int i = 0 ; i< [NewDic count];i++){
                    if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                        [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                    }
                    else{
                        [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                    }
                }
                params = [NewDic mutableCopy];
            }
            [self getiWorkUserRequest:@"myTeamWorkForApproval"];
        }
    }
    else if([isComeFrom isEqualToString:@"Report"]){
        NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
        if(ArraysValue.count>0){
            offset = offset + 10;
            if([CheckValue isEqualToString:@"REPORTFILTER"]){
                NSMutableDictionary *NewDic = [params mutableCopy];
                for (int i = 0 ; i< [NewDic count];i++){
                    if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                        [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                    }
                    else{
                        [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                    }
                }
                params = [NewDic mutableCopy];
            }
            [self getiWorkUserRequest:@"viewTeamReport"];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        if([isComeFrom isEqualToString:@"Approvel"]){
            [_approvalArray addObject:BindDataDic];
        }
        else if ([isComeFrom isEqualToString:@"Report"]){
           [_reportArray addObject:BindDataDic];
        }
    }
}
-(void)MapPage{
    NSDictionary *Dic = [_reportArray objectAtIndex:TagValue];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
    ObjViewController.MapDic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GotoMapView:(UIButton*)Sender{
    TagValue = Sender.tag;
    [self MapPage];
}
-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    TagValue = sender.view.tag;
    [self MapPage];
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField.text.length >= MAX_LENGTH && range.length == 0){
        [self ShowAlert:@"Character Limit Exceed" ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        return NO; // return NO to not change text
    }
    else{
        return YES;
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    RejectionTextView.text = @"";
    RequireImageView.hidden =YES;
    return YES;
}


#pragma mark -
- (void)approveButtonClicked:(UIButton *)sender {
    MsgLabel.text = NSLocalizedString(@"APPROVE_MSG", nil);
    TagValue = sender.tag;
    CheckValue = @"APPROVE";
    MsgOuterView.hidden =NO;
}
- (void)rejectButtonClicked:(UIButton *)sender {
    TagValue = sender.tag;
    CheckValue = @"REJECT";
    RejectionOuterView.hidden =NO;
}
- (void)retriveDataFromServer {
    if([isComeFrom isEqualToString:@"Approvel"]){
        if ([_approvalArray count]>0) {
            [self.tableView reloadData];
        } else {
            [self getiWorkUserRequest:@"myTeamWorkForApproval"];
        }
    }
    else if([isComeFrom isEqualToString:@"Report"]){
         if ([_reportArray count]>0) {
            [self.tableView reloadData];
        } else {
            [self getiWorkUserRequest:@"viewTeamReport"];
        }
    }
}
- (void)getiWorkUserRequest:(NSString *)Type
{
    if(delegate.isInternetConnected)
    {
        if([isComeFrom isEqualToString:@"Approvel"]||[CheckValue isEqualToString:@"Refersh"]||[CheckValue isEqualToString:@"APPROVEFILTER"]){
            if(![CheckValue isEqualToString:@"APPROVEFILTER"]){
                params = @ {
                    @"userId": [ApplicationState userId],
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            NSLog(@"%@",params);
            ResponseDic = [Api WebApi:params Url:Type];
            [_refreshControl endRefreshing];
            NoDataLabel.hidden = YES;
            NoDataImageView.hidden = YES;
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
            
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                }
                else if(_approvalArray.count == 0){
                    [self NoApproveRequest];
                }
                [self.tableView reloadData];
            }
            else{
                if(_approvalArray.count == 0){
                    [self NoApproveRequest];
                }
            }
        }
        else if([isComeFrom isEqualToString:@"Report"]||[CheckValue isEqualToString:@"Refershs"]||[CheckValue isEqualToString:@"REPORTFILTER"]){
            if(![CheckValue isEqualToString:@"REPORTFILTER"]){
                params = @ {
                    @"status": @10,
                    @"userId": [ApplicationState userId],
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            NSLog(@"%@",params);
            NoDataLabel.hidden = YES;
             NoDataImageView.hidden = YES;
            ResponseDic = [Api WebApi:params Url:Type];
            [_refreshControl endRefreshing];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"]];
                }
                else if(_reportArray.count == 0){
                    [self NoReportRequest];
                }
                [self.tableView reloadData];
            }
            else{
                if(_reportArray.count == 0){
                    [self NoReportRequest];
                }
            }
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoReportRequest{
    [LoadingManager hideLoadingView:self.view];
    NoDataLabel.hidden = NO;
    NoDataImageView.hidden = NO;
    NoDataLabel.text = NSLocalizedString(@"NO_IWORK_REQUEST_FOUND", nil);
    [self.tableView reloadData];
}
-(void)NoApproveRequest{
    [LoadingManager hideLoadingView:self.view];
    NoDataLabel.hidden = NO;
    NoDataImageView.hidden = NO;
    NoDataLabel.text = NSLocalizedString(@"NO_APPROVE_REQUEST", nil);
    [self.tableView reloadData];
}
-(IBAction)YESBtnAction:(id)sender{
    NSDictionary * responseData = _approvalArray [TagValue];
    requestsId = [NSString stringWithFormat:@"%@",[responseData objectForKey:@"requestId"]];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(ApproveRejectWebApi) withObject:nil afterDelay:0.5];
}
-(void)ApproveRejectWebApi{
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"REJECT"]){
            NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
            NSString *RemarkStr = [RejectionTextView.text stringByAddingPercentEncodingWithAllowedCharacters:set];
            NSString *UrlStr = [NSString stringWithFormat:@"updateStatusOfRequest?status=2&requestId=%@&rejectionRemark=%@",requestsId,RemarkStr];
            NSLog(@"UrlStr==%@",UrlStr);
            ResponseDic = [Api WebApi:nil Url:UrlStr];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self ApproveRejectWebApi];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                MsgOuterView.hidden =YES;
                RejectionOuterView.hidden =YES;
                [self EmptyArray];
                [self getiWorkUserRequest:@"myTeamWorkForApproval"];
            }
            else{
                [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
        else if([CheckValue isEqualToString:@"APPROVE"]){
            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"updateStatusOfRequest?status=1&requestId=%@",requestsId]];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self ApproveRejectWebApi];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
              [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                MsgOuterView.hidden =YES;
                [self EmptyArray];
                [self getiWorkUserRequest:@"myTeamWorkForApproval"];
            }
            else{
                 [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(IBAction)NoBtnAction:(id)sender{
    MsgOuterView.hidden =YES;
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)RejectionYesAction:(id)sender{
    [RejectionTextView resignFirstResponder];
    NSDictionary * responseData = _approvalArray [TagValue];
    requestsId = [NSString stringWithFormat:@"%@",[responseData objectForKey:@"requestId"]];
    if([RejectionTextView.text isEqualToString:@""]){
        RequireImageView.hidden =NO;
    }
    else if([RejectionTextView.text isEqualToString:@""]){
        RequireImageView.hidden =NO;
    }
    else{
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(ApproveRejectWebApi) withObject:nil afterDelay:0.5];
    }
}
-(IBAction)RejectionNoCrossAction:(id)sender{
    RejectionTextView.text = @"";
    [RejectionTextView resignFirstResponder];
    CheckValue = @"";
    RejectionOuterView.hidden = YES;
    RequireImageView.hidden =YES;
}
-(void)LineManageFilterApi:(NSNotification*)notification{
    params = notification.userInfo;
    [self EmptyArray];
    if([isComeFrom isEqualToString:@"Approvel"]){
        CheckValue = @"APPROVEFILTER";
         [self getiWorkUserRequest:@"myTeamWorkForApproval"];
    }
    else if([isComeFrom isEqualToString:@"Report"]){
        CheckValue = @"REPORTFILTER";
        [self getiWorkUserRequest:@"viewTeamReport"];
    }
}
-(void)LineManageClearFilterApi:(NSNotification*)notification{
    [self EmptyArray];
    if([isComeFrom isEqualToString:@"Approvel"]){
        CheckValue = @"Approvel";
        [self getiWorkUserRequest:@"myTeamWorkForApproval"];
    }
    else if([isComeFrom isEqualToString:@"Report"]){
        CheckValue = @"Report";
        [self getiWorkUserRequest:@"viewTeamReport"];
    }
}
- (IBAction)FilterAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if([isComeFrom isEqualToString:@"Approvel"]){
        FilterViewController *ObjFilterViewController = [storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
        ObjFilterViewController.isComefrom = @"LineManagerApprove";
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict = [params mutableCopy];
        if ([CheckValue isEqualToString:@"Approvel"] )
        {
           
        }
        else if ([CheckValue isEqualToString:@"APPROVEFILTER"])
        {
            if ([[dict valueForKey:@"startDate"] isEqualToString:@""])
            {
            }
            else
            {
                [dict setValue:@"SpecificDate" forKey:@"duration"];
            }
        }
        ObjFilterViewController.filterData = dict;
        [self presentViewController:ObjFilterViewController animated:YES completion:nil];
    }
    else if([isComeFrom isEqualToString:@"Report"]){
        LineManagerFilterViewController *ObjLineManagerFilterViewController = [storyboard instantiateViewControllerWithIdentifier:@"LineManagerFilterViewController"];
        [self presentViewController:ObjLineManagerFilterViewController animated:YES completion:nil];
    }
}
- (IBAction)BackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)HomeAction:(id)sender {
    NSArray *array = [self.navigationController viewControllers];
    for (int i = 0 ; i < array.count; i++) {
        UIViewController *ViewController = [array objectAtIndex:i];
        if([ViewController isKindOfClass:[ContainerParentViewController class]]){
            [self.navigationController popToViewController:ViewController animated:YES];
        }
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
