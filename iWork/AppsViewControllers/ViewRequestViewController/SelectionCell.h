//
//  ButtonCell.h
//  iWork
//
//  Created by Shailendra on 17/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectionCell : UITableViewCell{
    
}
@property(nonnull,retain)IBOutlet UIButton *SelectionButton;
@property(nonnull,retain)IBOutlet UIButton *SearchButton;
@property(nonnull,retain)IBOutlet UILabel *SelectionLabel;
@end
