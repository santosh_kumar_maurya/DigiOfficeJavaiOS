//
//  AlertController.m
//  iWork
//
//  Created by Shailendra on 27/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "AlertController.h"

@interface AlertController (){
    IBOutlet UILabel *MessageLabel;
    IBOutlet UILabel *AttributedMessageLabel;
    IBOutlet UIButton *MessageBtn;
}
@end

@implementation AlertController
@synthesize MessageTitleStr,MessageBtnStr,AttributedString,CancelBtn,YellowView,GraylineLabel;


- (void)viewDidLoad {
    [super viewDidLoad];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    if([_isCome isEqualToString:@"KPI"]){
        AttributedMessageLabel.attributedText =  AttributedString;
        AttributedMessageLabel.hidden = false;
        MessageLabel.hidden = true;
        CancelBtn.backgroundColor = [UIColor whiteColor];
        GraylineLabel.hidden = NO;
        YellowView.hidden = NO;
        [CancelBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    else{
        
        MessageLabel.text =  MessageTitleStr;
        AttributedMessageLabel.hidden = true;
        MessageLabel.hidden = false;
        YellowView.hidden = YES;
        GraylineLabel.hidden = YES;
        CancelBtn.backgroundColor = [UIColor redColor];
        [CancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    [MessageBtn setTitle:MessageBtnStr forState:UIControlStateNormal];
    
}

- (IBAction)BackAction:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
