//
//  MyTeamEmployeeCell.h
//  iWork
//
//  Created by Shailendra on 21/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTeamEmployeeCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UITextField *EmployeeIDTextField;
@property(nonatomic,strong)IBOutlet UIImageView *EmployeeNameRadioImage;
@property(nonatomic,strong)IBOutlet UIImageView *EmployeeIDRadioImage;
@property(nonatomic,strong)IBOutlet UIImageView *ALLRadioImage;
@property(nonatomic,strong)IBOutlet UILabel *EmployeeNameLabel;
@property(nonatomic,strong)IBOutlet UILabel *EmployeeIDLabel;
@property(nonatomic,strong)IBOutlet UILabel *ALLLabel;
@property(nonatomic,strong)IBOutlet UIButton *EmployeeNameBtn;
@property(nonatomic,strong)IBOutlet UIButton *EmployeeIDBtn;
@property(nonatomic,strong)IBOutlet UIButton *ALLBtn;
@property(nonatomic,strong)IBOutlet UIView *TextFieldBG;

@end
