//
//  LineManagerFilterViewController.m
//  iWork
//
//  Created by Shailendra on 26/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LineManagerFilterViewController.h"
#import "Header.h"

@interface LineManagerFilterViewController ()
{
    NSMutableArray *FilterReportsArray;
    NSMutableArray *SelectedIndex;
    NSMutableArray *SelectedArray;
    NSMutableArray *SelectedLocationIDArray;
    NSMutableArray *LocationIDArray;
    NSMutableArray *LocationNameArray;
    NSMutableArray *FilterArray;
    NSMutableArray *arrayForBool;
    NSMutableArray *RequestStatusArray;
    NSMutableArray *AttendanceStatusArray;
    
    NSString *req_id;
    NSString *request_status;
    NSString *attendance_status;
    NSString *request_date;
    NSString *iwork_date;
    NSString *location_list_array;
    
    NSString *check_in_time;
    NSString *check_out_time;
    NSString *action_date;
    NSString *empl_id;
    NSString *empl_name;
    NSString *Selection;
    NSString *DateSelection;
    
    IBOutlet UITableView *FilterTableView;
    IBOutlet UITableView *SelectTableView;
    IBOutlet UIView *SelectView;
    IBOutlet UIView *DatePikerViewBg;
    IBOutlet UIDatePicker *DatePicker;
    IBOutlet UILabel *HeaderLabel;
    
    NSMutableArray *PlusImageArray;
    NSMutableArray *MinusImageArray;
    
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *CancelFilterBtn;
    IBOutlet UIButton *ApplyFilterBtn;
    AppDelegate *delegate;
    APIService *Api;
    NSDictionary *ResponseDic;
    int offset;
    int limit;
    
    NSString *CheckInAMPM;
    NSString *CheckInHour;
    NSString *CheckInMin;
    NSString *CheckOutAMPM;
    NSString *CheckOutHour;
    NSString *CheckOutMin;
    NSUserDefaults * defaults;
    NSObject *object;
    BOOL isClearSelected;
    BOOL clearFilterForEmployee;
    NSUserDefaults *userDefault;
}

@end

@implementation LineManagerFilterViewController
@synthesize isComeFrom,EmployeeID;
    
- (void)viewDidLoad {
    [super viewDidLoad];
    isClearSelected = NO;
    userDefault = [NSUserDefaults standardUserDefaults];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
    offset = 0;
    limit = 10;
    defaults = [NSUserDefaults standardUserDefaults];

    [self ClearData];
    Api = [[APIService alloc] init];
    SelectedLocationIDArray = [[NSMutableArray alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
   
    PlusImageArray = [[NSMutableArray alloc] initWithObjects:@"Plus",@"Plus",@"Plus",@"Plus",@"Plus",@"Plus",@"Plus",@"Plus",@"Plus",@"Plus",@"Plus", nil];
    MinusImageArray = [[NSMutableArray alloc] initWithObjects:@"Minus",@"Minus",@"Minus",@"Minus",@"Minus",@"Minus",@"Minus",@"Minus",@"Minus",@"Minus",@"Minus", nil];
        
    NSString *iWorkLocation = NSLocalizedString(@"IWORK_LOCATION", nil);
    //iWorkLocation = [iWorkLocation stringByReplacingOccurrencesOfString:@"iWork" withString:[@"&#9432;Work" stringByConvertingHTMLToPlainText]];
    
    NSString *iWorkDate = NSLocalizedString(@"IWORK_DATE", nil);
    //iWorkDate = [iWorkDate stringByReplacingOccurrencesOfString:@"iWork" withString:[@"&#9432;Work" stringByConvertingHTMLToPlainText]];
    
    
    if([isComeFrom isEqualToString:@"EmployeeDetail"]){
        FilterReportsArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"REQUEST_ID", nil),iWorkLocation,NSLocalizedString(@"REQUEST_STATUS", nil),NSLocalizedString(@"ATTENDENCE_STATUS", nil),NSLocalizedString(@"CHECK_IN_TIME", nil),NSLocalizedString(@"CHECK_OUT_TIME", nil),NSLocalizedString(@"REQUEST_DATE", nil),NSLocalizedString(@"ACTION_DATE", nil),iWorkDate, nil];
    }
    else if([isComeFrom isEqualToString:@"LineManagerRejected"]){
        
        FilterReportsArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"EMPLOYEE_ID", nil),NSLocalizedString(@"EMPLOYEE_NAME", nil),NSLocalizedString(@"REQUEST_ID", nil),iWorkLocation,NSLocalizedString(@"CHECK_IN_TIME", nil),NSLocalizedString(@"CHECK_OUT_TIME", nil),NSLocalizedString(@"REQUEST_DATE", nil),NSLocalizedString(@"ACTION_DATE", nil),iWorkDate, nil];
        
    }
    else if([isComeFrom isEqualToString:@"LineManagerNew"]||[isComeFrom isEqualToString:@"LineManagerMissed"]){
        FilterReportsArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"EMPLOYEE_ID", nil),NSLocalizedString(@"EMPLOYEE_NAME", nil),NSLocalizedString(@"REQUEST_ID", nil),iWorkLocation,NSLocalizedString(@"CHECK_IN_TIME", nil),NSLocalizedString(@"CHECK_OUT_TIME", nil),NSLocalizedString(@"REQUEST_DATE", nil),iWorkDate, nil];
    }
    else if([isComeFrom isEqualToString:@"LineManagerApproved"]){
        FilterReportsArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"EMPLOYEE_ID", nil),NSLocalizedString(@"EMPLOYEE_NAME", nil),NSLocalizedString(@"REQUEST_ID", nil),iWorkLocation,NSLocalizedString(@"ATTENDENCE_STATUS", nil),NSLocalizedString(@"CHECK_IN_TIME", nil),NSLocalizedString(@"CHECK_OUT_TIME", nil),NSLocalizedString(@"REQUEST_DATE", nil),NSLocalizedString(@"ACTION_DATE", nil),iWorkDate, nil];
    }
    else{
       FilterReportsArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"EMPLOYEE_ID", nil),NSLocalizedString(@"EMPLOYEE_NAME", nil),NSLocalizedString(@"REQUEST_ID", nil),iWorkLocation,NSLocalizedString(@"REQUEST_STATUS", nil),NSLocalizedString(@"ATTENDENCE_STATUS", nil),
                              NSLocalizedString(@"CHECK_IN_TIME", nil),NSLocalizedString(@"CHECK_OUT_TIME", nil),NSLocalizedString(@"REQUEST_DATE", nil),NSLocalizedString(@"ACTION_DATE", nil),iWorkDate, nil];
    }

    CancelBtn.layer.borderWidth = 1.0;
    CancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    CancelBtn.layer.cornerRadius = 3.0;
    CancelBtn.clipsToBounds = YES;
    
    
    CancelFilterBtn.layer.borderWidth = 1.0;
    CancelFilterBtn.layer.borderColor = delegate.redColor.CGColor;
    CancelFilterBtn.layer.cornerRadius = 3.0;
    CancelFilterBtn.clipsToBounds = YES;
    
    ApplyFilterBtn.layer.cornerRadius = 3.0;
    
    DatePikerViewBg.backgroundColor = [UIColor whiteColor];
    DatePicker.backgroundColor = [UIColor clearColor];
    
    RequestStatusArray = [[NSMutableArray alloc] initWithObjects:NSLocalizedString(@"SELECT_STATUS", nil),NSLocalizedString(@"APPROVED", nil),NSLocalizedString(@"MISSED_BTN", nil),NSLocalizedString(@"NEW_BTN", nil),NSLocalizedString(@"REJECTED", nil), nil];
    
//    RequestStatusArray = [[NSMutableArray alloc] initWithObjects:NSLocalizedString(@"SELECT_STATUS", nil),NSLocalizedString(@"NEW_BTN", nil),NSLocalizedString(@"APPROVED", nil),NSLocalizedString(@"REJECTED", nil),NSLocalizedString(@"MISSED_BTN", nil), nil];
    
    AttendanceStatusArray = [[NSMutableArray alloc] initWithObjects:NSLocalizedString(@"SELECT_ATTENDENCE", nil),NSLocalizedString(@"ABSENT", nil),NSLocalizedString(@"PRESENT", nil), nil];
    
    arrayForBool=[[NSMutableArray alloc]init];
    for (int i=0; i<[FilterReportsArray count]; i++) {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    UINib *SearchNib = [UINib nibWithNibName:@"SearchCell" bundle:nil];
    [FilterTableView registerNib:SearchNib forCellReuseIdentifier:@"CELL"];
    
    UINib *SelectionNib = [UINib nibWithNibName:@"SelectionCell" bundle:nil];
    [FilterTableView registerNib:SelectionNib forCellReuseIdentifier:@"CELL1"];
    
    UINib *DateSelectionNib = [UINib nibWithNibName:@"DateSelectionCell" bundle:nil];
    [FilterTableView registerNib:DateSelectionNib forCellReuseIdentifier:@"CELL2"];
    
    UINib *CheckboxNib = [UINib nibWithNibName:@"CheckboxCell" bundle:nil];
    [FilterTableView registerNib:CheckboxNib forCellReuseIdentifier:@"CELL3"];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(LocationApi) withObject:nil afterDelay:0.5];
    // Do any additional setup after loading the view.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(tableView == SelectTableView){
        return 1;
    }
    return [FilterReportsArray count];
}
#pragma mark -
#pragma mark TableView DataSource and Delegate Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == SelectTableView){
        if([Selection isEqualToString:@"RequestStatus"]){
            return [RequestStatusArray count];
        }
        else if([Selection isEqualToString:@"AttendanseStatus"]){
            return [AttendanceStatusArray count];
        }
    }
    else if(tableView == FilterTableView){
        if([isComeFrom isEqualToString:@"EmployeeDetail"]){
            if(section == 1){
               return [LocationIDArray count]; 
            }
        }
        else{
            if(section == 3){
                return [LocationIDArray count];
            }
            else{
                return  1;
            }
        }
        
    }
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == SelectTableView){
        if([Selection isEqualToString:@"RequestStatus"]){
            StatusSelectionCell * Cell = (StatusSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"StatusSelectionCell" forIndexPath:indexPath];
            Cell.SelectionLabel.text = [RequestStatusArray objectAtIndex:indexPath.row];
            return Cell;
        }
        else {
            StatusSelectionCell * Cell = (StatusSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"StatusSelectionCell" forIndexPath:indexPath];
            Cell.SelectionLabel.text = [AttendanceStatusArray objectAtIndex:indexPath.row];
            return Cell;
        }
    }
    else{
        if ([isComeFrom isEqualToString:@"EmployeeDetail"])
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            dict = [userDefault objectForKey:isComeFrom];
            NSLog(@"%@",dict);
            if(indexPath.section == 0 )
            {
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                [Cell.SearchBtn addTarget:self action:@selector(RequestIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestId"] isEqualToString:@""] || ([dict valueForKey:@"requestId"] == nil))
                    {
                        if ([req_id isEqualToString: @""])
                        {
                            req_id = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                    else
                    {
                        if ([req_id isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"requestId"]];
                            req_id = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                }
                else
                {
                    if([req_id isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);

                            }
                            else
                            {
                                req_id = Cell.SearchTextField.text;
                            }
                            
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = req_id;
                    }
                }
                return Cell;
            }
            else if(indexPath.section == 1)
            {
                CheckboxCell * Cell = (CheckboxCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
                Cell.CheckBoxBtn.tag = indexPath.row;
                Cell.BlanckBtn.tag = indexPath.row;
                [Cell.CheckBoxBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.BlanckBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                if(LocationNameArray.count>0)
                {
                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
                }
                
                
                if ([dict valueForKey:@"locationId"] == nil)
                {
                    if(LocationNameArray.count>0)
                    {
                        if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                    }
                }
                return Cell;
            }
            else if(indexPath.section == 2 ){
                SelectionCell * Cell = (SelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestStatusSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestStatusAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if (([[dict valueForKey:@"status"] integerValue] == 10) || ([dict valueForKey:@"status"] == nil))
                    {
                        if (([[dict valueForKey:@"status"] integerValue]== 10) && [request_status isEqualToString: @""])
                        {
                            request_status = @"";
                            Cell.SelectionLabel.text = @"Select Status";
                        }
                        else
                        {
                            Cell.SelectionLabel.text = request_status;
                        }
                    }
                    else
                    {
                        if ([request_status isEqualToString:@""])
                        {
                            Cell.SelectionLabel.text = [self getRequestStatusFromRequestId:[[dict valueForKey:@"status"] integerValue]];
                            request_status = Cell.SelectionLabel.text;
                        }
                        else
                        {
                            Cell.SelectionLabel.text = request_status;
                        }
                    }
                }
                else
                {
                    if([request_status isEqualToString:@""])
                    {
                        Cell.SelectionLabel.text = @"Select Status";
                    }
                    else
                    {
                        Cell.SelectionLabel.text = request_status;
                    }
                }
                return Cell;
            }
            else  if(indexPath.section == 3 ){
                SelectionCell * Cell = (SelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(AttendanceStatusSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(AttendanceStatusAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"attendanceStatus"] isEqualToString:@""] || ([dict valueForKey:@"attendanceStatus"] == nil))
                    {
                        if ([attendance_status isEqualToString:@""])
                        {
                            attendance_status = @"";
                            Cell.SelectionLabel.text = @"Select Attendance";
                        }
                        else
                        {
                            Cell.SelectionLabel.text = attendance_status;
                        }
                    }
                    else
                    {
                        if ([attendance_status isEqualToString:@""])
                        {
                            Cell.SelectionLabel.text = [dict valueForKey:@"attendanceStatus"];
                            attendance_status = Cell.SelectionLabel.text;
                        }
                        else
                        {
                            Cell.SelectionLabel.text = attendance_status;
                        }
                    }
                }
                else
                {
                    if([attendance_status isEqualToString:@""])
                    {
                        Cell.SelectionLabel.text = @"Select Attendance";
                    }
                    else
                    {
                        Cell.SelectionLabel.text = attendance_status;
                    }
                }
                return Cell;
            }
            else  if(indexPath.section == 4 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckInSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckInAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkInMin"] isEqualToString:@""] || ([dict valueForKey:@"checkInMin"] == nil))
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            check_in_time = @"";
                            Cell.DateSelectionLabel.text = @"Check-in Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                    else
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkInHour"],[dict valueForKey:@"checkInMin"],[dict valueForKey:@"checkInTimeType"]];
                            check_in_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                }
                else
                {
                    if([check_in_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Check-in Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_in_time;
                    }
                }
                return Cell;
            }
            else  if(indexPath.section == 5 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckOutSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckOutAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkOutMin"] isEqualToString:@""] || ([dict valueForKey:@"checkOutMin"] == nil))
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            check_out_time = @"";
                            Cell.DateSelectionLabel.text = @"Checkout Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                    else
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkOutHour"],[dict valueForKey:@"checkOutMin"],[dict valueForKey:@"checkOutTimeType"]];
                            check_out_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                }
                else
                {
                    if([check_out_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Checkout Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_out_time;
                    }
                }

//                if([check_out_time isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Checkout Time";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = check_out_time;
//                }
                
                return Cell;
            }
            
            else  if(indexPath.section == 6 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestDate"] isEqualToString:@""] || ([dict valueForKey:@"requestDate"] == nil))
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            request_date = @"";
                            Cell.DateSelectionLabel.text = @"Request Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                    else
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"requestDate"]];
                            request_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                }
                else
                {
                    if([request_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Request Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = request_date;
                    }
                }
//                if([request_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Request Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = request_date;
//                }
                return Cell;
            }
            else  if(indexPath.section == 7 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(ActionDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(ActionDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"actionDate"] isEqualToString:@""] || ([dict valueForKey:@"actionDate"] == nil))
                    {
                        if ([action_date isEqualToString:@""])
                        {
                            action_date = @"";
                            Cell.DateSelectionLabel.text = @"Action Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = action_date;
                        }
                    }
                    else
                    {
                        if ([action_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"actionDate"]];
                            action_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = action_date;
                        }
                    }
                }
                else
                {
                    if([action_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Action Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = action_date;
                    }
                }

//                if([action_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Action Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = action_date;
//                }
                return Cell;
            }
            else {
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(iWorkDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(iWorkDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"workDate"] isEqualToString:@""] || ([dict valueForKey:@"workDate"] == nil))
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            iwork_date = @"";
                            Cell.DateSelectionLabel.text = @"iWork Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                    else
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"workDate"]];
                            iwork_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                }
                else
                {
                    if([iwork_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"iWork Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = iwork_date;
                    }
                }

//                if([iwork_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"iWork Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = iwork_date;
//                }
                return Cell;
            }
        }
        else if([isComeFrom isEqualToString:@"LineManagerRejected"])
        {
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            dict = [userDefault objectForKey:isComeFrom];
            NSLog(@"%@",dict);
            if(indexPath.section == 0 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"empId"] isEqualToString:@""] || ([dict valueForKey:@"empId"] == nil))
                    {
                        if ([empl_id isEqualToString: @""])
                        {
                            empl_id = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_id;
                        }
                    }
                    else
                    {
                        if ([empl_id isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"empId"]];
                            empl_id = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_id;
                        }
                    }
                }
                else
                {
                    if([empl_id isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                                
                            }
                            else
                            {
                                empl_id = Cell.SearchTextField.text;
                            }
                            
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = empl_id;
                    }
                }
//                Cell.SearchTextField.text = empl_id;
                return Cell;
            }
            else if(indexPath.section == 1 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeNameSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"empName"] isEqualToString:@""] || ([dict valueForKey:@"empName"] == nil))
                    {
                        if ([empl_name isEqualToString: @""])
                        {
                            empl_name = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_name;
                        }
                    }
                    else
                    {
                        if ([empl_name isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"empName"]];
                            empl_name = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_name;
                        }
                    }
                }
                else
                {
                    if([empl_name isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                                
                            }
                            else
                            {
                                empl_name = Cell.SearchTextField.text;
                            }
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = empl_name;
                    }
                }

//                Cell.SearchTextField.text = empl_name;
                return Cell;
            }
            else if(indexPath.section == 2 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                [Cell.SearchBtn addTarget:self action:@selector(RequestIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
//                Cell.SearchTextField.text = req_id;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestId"] isEqualToString:@""] || ([dict valueForKey:@"requestId"] == nil))
                    {
                        if ([req_id isEqualToString: @""])
                        {
                            req_id = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                    else
                    {
                        if ([req_id isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"requestId"]];
                            req_id = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                }
                else
                {
                    if([req_id isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                                
                            }
                            else
                            {
                                req_id = Cell.SearchTextField.text;
                            }
                            
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = req_id;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 3)
            {
                CheckboxCell * Cell = (CheckboxCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
                Cell.CheckBoxBtn.tag = indexPath.row;
                Cell.BlanckBtn.tag = indexPath.row;
                [Cell.CheckBoxBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.BlanckBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
//                if(LocationNameArray.count>0){
//                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
//                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]]){
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
//                    }
//                    else{
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
//                    }
//                }
                if(LocationNameArray.count>0)
                {
                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
                }
                
                
                if ([dict valueForKey:@"locationId"] == nil)
                {
                    if(LocationNameArray.count>0)
                    {
                        if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                    }
                }

                return Cell;
            }
            else  if(indexPath.section == 4 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckInSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckInAction) forControlEvents:UIControlEventTouchUpInside];
//                if([check_in_time isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Check-in Time";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = check_in_time;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkInMin"] isEqualToString:@""] || ([dict valueForKey:@"checkInMin"] == nil))
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            check_in_time = @"";
                            Cell.DateSelectionLabel.text = @"Check-in Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                    else
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkInHour"],[dict valueForKey:@"checkInMin"],[dict valueForKey:@"checkInTimeType"]];
                            check_in_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                }
                else
                {
                    if([check_in_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Check-in Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_in_time;
                    }
                }

                
                return Cell;
            }
            else  if(indexPath.section == 5 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckOutSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckOutAction) forControlEvents:UIControlEventTouchUpInside];
//                if([check_out_time isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Checkout Time";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = check_out_time;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkOutMin"] isEqualToString:@""] || ([dict valueForKey:@"checkOutMin"] == nil))
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            check_out_time = @"";
                            Cell.DateSelectionLabel.text = @"Checkout Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                    else
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkOutHour"],[dict valueForKey:@"checkOutMin"],[dict valueForKey:@"checkOutTimeType"]];
                            check_out_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                }
                else
                {
                    if([check_out_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Checkout Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_out_time;
                    }
                }

                return Cell;
            }
            else  if(indexPath.section == 6 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestDateAction) forControlEvents:UIControlEventTouchUpInside];
//                if([request_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Request Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = request_date;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestDate"] isEqualToString:@""] || ([dict valueForKey:@"requestDate"] == nil))
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            request_date = @"";
                            Cell.DateSelectionLabel.text = @"Request Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                    else
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"requestDate"]];
                            request_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                }
                else
                {
                    if([request_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Request Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = request_date;
                    }
                }

                return Cell;
            }
            else  if(indexPath.section == 7 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(ActionDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(ActionDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"actionDate"] isEqualToString:@""] || ([dict valueForKey:@"actionDate"] == nil))
                    {
                        if ([action_date isEqualToString:@""])
                        {
                            action_date = @"";
                            Cell.DateSelectionLabel.text = @"Action Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = action_date;
                        }
                    }
                    else
                    {
                        if ([action_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"actionDate"]];
                            action_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = action_date;
                        }
                    }
                }
                else
                {
                    if([action_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Action Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = action_date;
                    }
                }
//                if([action_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Action Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = action_date;
//                }
                return Cell;
            }
            else {
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(iWorkDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(iWorkDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"workDate"] isEqualToString:@""] || ([dict valueForKey:@"workDate"] == nil))
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            iwork_date = @"";
                            Cell.DateSelectionLabel.text = @"iWork Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                    else
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"workDate"]];
                            iwork_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                }
                else
                {
                    if([iwork_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"iWork Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = iwork_date;
                    }
                }

//                if([iwork_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"iWork Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = iwork_date;
//                }
                return Cell;
            }
        }
        else if([isComeFrom isEqualToString:@"LineManagerNew"])
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            dict = [userDefault objectForKey:isComeFrom];
            NSLog(@"%@",dict);
            if(indexPath.section == 0 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
//                Cell.SearchTextField.text = empl_id;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"empId"] isEqualToString:@""] || ([dict valueForKey:@"empId"] == nil))
                    {
                        if ([empl_id isEqualToString: @""])
                        {
                            empl_id = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_id;
                        }
                    }
                    else
                    {
                        if ([empl_id isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"empId"]];
                            empl_id = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_id;
                        }
                    }
                }
                else
                {
                    if([empl_id isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                                
                            }
                            else
                            {
                                empl_id = Cell.SearchTextField.text;
                            }
                            
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = empl_id;
                    }
                }
                return Cell;
            }
            else if(indexPath.section == 1 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeNameSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
//                Cell.SearchTextField.text = empl_name;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"empName"] isEqualToString:@""] || ([dict valueForKey:@"empName"] == nil))
                    {
                        if ([empl_name isEqualToString: @""])
                        {
                            empl_name = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_name;
                        }
                    }
                    else
                    {
                        if ([empl_name isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"empName"]];
                            empl_name = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_name;
                        }
                    }
                }
                else
                {
                    if([empl_name isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                                
                            }
                            else
                            {
                                empl_name = Cell.SearchTextField.text;
                            }
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = empl_name;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 2 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                [Cell.SearchBtn addTarget:self action:@selector(RequestIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
//                Cell.SearchTextField.text = req_id;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestId"] isEqualToString:@""] || ([dict valueForKey:@"requestId"] == nil))
                    {
                        if ([req_id isEqualToString: @""])
                        {
                            req_id = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                    else
                    {
                        if ([req_id isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"requestId"]];
                            req_id = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                }
                else
                {
                    if([req_id isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                                
                            }
                            else
                            {
                                req_id = Cell.SearchTextField.text;
                            }
                            
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = req_id;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 3){
                CheckboxCell * Cell = (CheckboxCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
                Cell.CheckBoxBtn.tag = indexPath.row;
                Cell.BlanckBtn.tag = indexPath.row;
                [Cell.CheckBoxBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.BlanckBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
//                if(LocationNameArray.count>0){
//                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
//                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]]){
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
//                    }
//                    else{
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
//                    }
//                }
                if(LocationNameArray.count>0)
                {
                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
                }
                
                
                if ([dict valueForKey:@"locationId"] == nil)
                {
                    if(LocationNameArray.count>0)
                    {
                        if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                    }
                }

                return Cell;
            }
            else  if(indexPath.section == 4 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckInSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckInAction) forControlEvents:UIControlEventTouchUpInside];
//                if([check_in_time isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Check-in Time";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = check_in_time;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkInMin"] isEqualToString:@""] || ([dict valueForKey:@"checkInMin"] == nil))
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            check_in_time = @"";
                            Cell.DateSelectionLabel.text = @"Check-in Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                    else
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkInHour"],[dict valueForKey:@"checkInMin"],[dict valueForKey:@"checkInTimeType"]];
                            check_in_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                }
                else
                {
                    if([check_in_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Check-in Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_in_time;
                    }
                }

                return Cell;
            }
            else  if(indexPath.section == 5 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckOutSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckOutAction) forControlEvents:UIControlEventTouchUpInside];
//                if([check_out_time isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Checkout Time";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = check_out_time;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkOutMin"] isEqualToString:@""] || ([dict valueForKey:@"checkOutMin"] == nil))
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            check_out_time = @"";
                            Cell.DateSelectionLabel.text = @"Checkout Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                    else
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkOutHour"],[dict valueForKey:@"checkOutMin"],[dict valueForKey:@"checkOutTimeType"]];
                            check_out_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                }
                else
                {
                    if([check_out_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Checkout Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_out_time;
                    }
                }

                return Cell;
            }
            
            else  if(indexPath.section == 6 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestDateAction) forControlEvents:UIControlEventTouchUpInside];
//                if([request_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Request Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = request_date;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestDate"] isEqualToString:@""] || ([dict valueForKey:@"requestDate"] == nil))
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            request_date = @"";
                            Cell.DateSelectionLabel.text = @"Request Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                    else
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"requestDate"]];
                            request_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                }
                else
                {
                    if([request_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Request Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = request_date;
                    }
                }

                return Cell;
            }
            else {
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(iWorkDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(iWorkDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"workDate"] isEqualToString:@""] || ([dict valueForKey:@"workDate"] == nil))
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            iwork_date = @"";
                            Cell.DateSelectionLabel.text = @"iWork Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                    else
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"workDate"]];
                            iwork_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                }
                else
                {
                    if([iwork_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"iWork Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = iwork_date;
                    }
                }

//                if([iwork_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"iWork Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = iwork_date;
//                }
                return Cell;
            }
        }
        else if([isComeFrom isEqualToString:@"LineManagerMissed"])
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            dict = [userDefault objectForKey:isComeFrom];
            NSLog(@"%@",dict);
            if(indexPath.section == 0 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
//                Cell.SearchTextField.text = empl_id;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"empId"] isEqualToString:@""] || ([dict valueForKey:@"empId"] == nil))
                    {
                        if ([empl_id isEqualToString: @""])
                        {
                            empl_id = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_id;
                        }
                    }
                    else
                    {
                        if ([empl_id isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"empId"]];
                            empl_id = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_id;
                        }
                    }
                }
                else
                {
                    if([empl_id isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                                
                            }
                            else
                            {
                                empl_id = Cell.SearchTextField.text;
                            }
                            
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = empl_id;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 1 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeNameSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
//                Cell.SearchTextField.text = empl_name;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"empName"] isEqualToString:@""] || ([dict valueForKey:@"empName"] == nil))
                    {
                        if ([empl_name isEqualToString: @""])
                        {
                            empl_name = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_name;
                        }
                    }
                    else
                    {
                        if ([empl_name isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"empName"]];
                            empl_name = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_name;
                        }
                    }
                }
                else
                {
                    if([empl_name isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                                
                            }
                            else
                            {
                                empl_name = Cell.SearchTextField.text;
                            }
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = empl_name;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 2 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                [Cell.SearchBtn addTarget:self action:@selector(RequestIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
//                Cell.SearchTextField.text = req_id;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestId"] isEqualToString:@""] || ([dict valueForKey:@"requestId"] == nil))
                    {
                        if ([req_id isEqualToString: @""])
                        {
                            req_id = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                    else
                    {
                        if ([req_id isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"requestId"]];
                            req_id = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                }
                else
                {
                    if([req_id isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                                
                            }
                            else
                            {
                                req_id = Cell.SearchTextField.text;
                            }
                            
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = req_id;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 3){
                CheckboxCell * Cell = (CheckboxCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
                Cell.CheckBoxBtn.tag = indexPath.row;
                Cell.BlanckBtn.tag = indexPath.row;
                [Cell.CheckBoxBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.BlanckBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
//                if(LocationNameArray.count>0){
//                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
//                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]]){
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
//                    }
//                    else{
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
//                    }
//                }
                
                if(LocationNameArray.count>0)
                {
                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
                }
                
                
                if ([dict valueForKey:@"locationId"] == nil)
                {
                    if(LocationNameArray.count>0)
                    {
                        if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                    }
                }

                
                return Cell;
            }
            else  if(indexPath.section == 4 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckInSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckInAction) forControlEvents:UIControlEventTouchUpInside];
//                if([check_in_time isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Check-in Time";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = check_in_time;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkInMin"] isEqualToString:@""] || ([dict valueForKey:@"checkInMin"] == nil))
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            check_in_time = @"";
                            Cell.DateSelectionLabel.text = @"Check-in Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                    else
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkInHour"],[dict valueForKey:@"checkInMin"],[dict valueForKey:@"checkInTimeType"]];
                            check_in_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                }
                else
                {
                    if([check_in_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Check-in Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_in_time;
                    }
                }

                return Cell;
            }
            else  if(indexPath.section == 5 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckOutSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckOutAction) forControlEvents:UIControlEventTouchUpInside];
//                if([check_out_time isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Checkout Time";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = check_out_time;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkOutMin"] isEqualToString:@""] || ([dict valueForKey:@"checkOutMin"] == nil))
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            check_out_time = @"";
                            Cell.DateSelectionLabel.text = @"Checkout Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                    else
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkOutHour"],[dict valueForKey:@"checkOutMin"],[dict valueForKey:@"checkOutTimeType"]];
                            check_out_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                }
                else
                {
                    if([check_out_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Checkout Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_out_time;
                    }
                }

                
                return Cell;
            }
            
            else  if(indexPath.section == 6 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestDateAction) forControlEvents:UIControlEventTouchUpInside];
//                if([request_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Request Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = request_date;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestDate"] isEqualToString:@""] || ([dict valueForKey:@"requestDate"] == nil))
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            request_date = @"";
                            Cell.DateSelectionLabel.text = @"Request Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                    else
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"requestDate"]];
                            request_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                }
                else
                {
                    if([request_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Request Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = request_date;
                    }
                }

                return Cell;
            }
            else {
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(iWorkDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(iWorkDateAction) forControlEvents:UIControlEventTouchUpInside];
//                if([iwork_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"iWork Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = iwork_date;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"workDate"] isEqualToString:@""] || ([dict valueForKey:@"workDate"] == nil))
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            iwork_date = @"";
                            Cell.DateSelectionLabel.text = @"iWork Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                    else
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"workDate"]];
                            iwork_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                }
                else
                {
                    if([iwork_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"iWork Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = iwork_date;
                    }
                }

                return Cell;
            }
        }
        else if([isComeFrom isEqualToString:@"LineManagerApproved"])
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            dict = [userDefault objectForKey:isComeFrom];
            NSLog(@"%@",dict);
            if(indexPath.section == 0 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
//                Cell.SearchTextField.text = empl_id;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"empId"] isEqualToString:@""] || ([dict valueForKey:@"empId"] == nil))
                    {
                        if ([empl_id isEqualToString: @""])
                        {
                            empl_id = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_id;
                        }
                    }
                    else
                    {
                        if ([empl_id isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"empId"]];
                            empl_id = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_id;
                        }
                    }
                }
                else
                {
                    if([empl_id isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                                
                            }
                            else
                            {
                                empl_id = Cell.SearchTextField.text;
                            }
                            
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = empl_id;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 1 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeNameSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
//                Cell.SearchTextField.text = empl_name;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"empName"] isEqualToString:@""] || ([dict valueForKey:@"empName"] == nil))
                    {
                        if ([empl_name isEqualToString: @""])
                        {
                            empl_name = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_name;
                        }
                    }
                    else
                    {
                        if ([empl_name isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"empName"]];
                            empl_name = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_name;
                        }
                    }
                }
                else
                {
                    if([empl_name isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                                
                            }
                            else
                            {
                                empl_name = Cell.SearchTextField.text;
                            }
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = empl_name;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 2 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                [Cell.SearchBtn addTarget:self action:@selector(RequestIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
//                Cell.SearchTextField.text = req_id;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestId"] isEqualToString:@""] || ([dict valueForKey:@"requestId"] == nil))
                    {
                        if ([req_id isEqualToString: @""])
                        {
                            req_id = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                    else
                    {
                        if ([req_id isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"requestId"]];
                            req_id = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                }
                else
                {
                    if([req_id isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                                
                            }
                            else
                            {
                                req_id = Cell.SearchTextField.text;
                            }
                            
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = req_id;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 3){
                CheckboxCell * Cell = (CheckboxCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
                Cell.CheckBoxBtn.tag = indexPath.row;
                Cell.BlanckBtn.tag = indexPath.row;
                [Cell.CheckBoxBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.BlanckBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
//                if(LocationNameArray.count>0){
//                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
//                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]]){
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
//                    }
//                    else{
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
//                    }
//                }
                
                if(LocationNameArray.count>0)
                {
                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
                }
                
                
                if ([dict valueForKey:@"locationId"] == nil)
                {
                    if(LocationNameArray.count>0)
                    {
                        if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                    }
                }

                
                return Cell;
            }
            else  if(indexPath.section == 4 ){
                SelectionCell * Cell = (SelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(AttendanceStatusSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(AttendanceStatusAction) forControlEvents:UIControlEventTouchUpInside];
//                if([attendance_status isEqualToString:@""]){
//                    Cell.SelectionLabel.text = @"Select Attendance";
//                }
//                else{
//                    Cell.SelectionLabel.text = attendance_status;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"attendanceStatus"] isEqualToString:@""] || ([dict valueForKey:@"attendanceStatus"] == nil))
                    {
                        if ([attendance_status isEqualToString:@""])
                        {
                            attendance_status = @"";
                            Cell.SelectionLabel.text = @"Select Attendance";
                        }
                        else
                        {
                            Cell.SelectionLabel.text = attendance_status;
                        }
                    }
                    else
                    {
                        if ([attendance_status isEqualToString:@""])
                        {
                            Cell.SelectionLabel.text = [dict valueForKey:@"attendanceStatus"];
                            attendance_status = Cell.SelectionLabel.text;
                        }
                        else
                        {
                            Cell.SelectionLabel.text = attendance_status;
                        }
                    }
                }
                else
                {
                    if([attendance_status isEqualToString:@""])
                    {
                        Cell.SelectionLabel.text = @"Select Attendance";
                    }
                    else
                    {
                        Cell.SelectionLabel.text = attendance_status;
                    }
                }

                return Cell;
            }
            else  if(indexPath.section == 5 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckInSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckInAction) forControlEvents:UIControlEventTouchUpInside];
//                if([check_in_time isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Check-in Time";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = check_in_time;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkInMin"] isEqualToString:@""] || ([dict valueForKey:@"checkInMin"] == nil))
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            check_in_time = @"";
                            Cell.DateSelectionLabel.text = @"Check-in Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                    else
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkInHour"],[dict valueForKey:@"checkInMin"],[dict valueForKey:@"checkInTimeType"]];
                            check_in_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                }
                else
                {
                    if([check_in_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Check-in Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_in_time;
                    }
                }

                return Cell;
            }
            else  if(indexPath.section == 6 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckOutSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckOutAction) forControlEvents:UIControlEventTouchUpInside];
//                if([check_out_time isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Checkout Time";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = check_out_time;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkOutMin"] isEqualToString:@""] || ([dict valueForKey:@"checkOutMin"] == nil))
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            check_out_time = @"";
                            Cell.DateSelectionLabel.text = @"Checkout Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                    else
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkOutHour"],[dict valueForKey:@"checkOutMin"],[dict valueForKey:@"checkOutTimeType"]];
                            check_out_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                }
                else
                {
                    if([check_out_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Checkout Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_out_time;
                    }
                }

                return Cell;
            }
            
            else  if(indexPath.section == 7 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestDateAction) forControlEvents:UIControlEventTouchUpInside];
//                if([request_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Request Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = request_date;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestDate"] isEqualToString:@""] || ([dict valueForKey:@"requestDate"] == nil))
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            request_date = @"";
                            Cell.DateSelectionLabel.text = @"Request Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                    else
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"requestDate"]];
                            request_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                }
                else
                {
                    if([request_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Request Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = request_date;
                    }
                }

                return Cell;
            }
            else  if(indexPath.section == 8 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(ActionDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(ActionDateAction) forControlEvents:UIControlEventTouchUpInside];
//                if([action_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Action Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = action_date;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"actionDate"] isEqualToString:@""] || ([dict valueForKey:@"actionDate"] == nil))
                    {
                        if ([action_date isEqualToString:@""])
                        {
                            action_date = @"";
                            Cell.DateSelectionLabel.text = @"Action Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = action_date;
                        }
                    }
                    else
                    {
                        if ([action_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"actionDate"]];
                            action_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = action_date;
                        }
                    }
                }
                else
                {
                    if([action_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Action Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = action_date;
                    }
                }

                return Cell;
            }
            else {
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(iWorkDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(iWorkDateAction) forControlEvents:UIControlEventTouchUpInside];
//                if([iwork_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"iWork Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = iwork_date;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"workDate"] isEqualToString:@""] || ([dict valueForKey:@"workDate"] == nil))
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            iwork_date = @"";
                            Cell.DateSelectionLabel.text = @"iWork Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                    else
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"workDate"]];
                            iwork_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                }
                else
                {
                    if([iwork_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"iWork Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = iwork_date;
                    }
                }

                return Cell;
            }
        }
        else {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            dict = [userDefault objectForKey:isComeFrom];
            NSLog(@"%@",dict);
            if(indexPath.section == 0 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
//                Cell.SearchTextField.text = empl_id;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"empId"] isEqualToString:@""] || ([dict valueForKey:@"empId"] == nil))
                    {
                        if ([empl_id isEqualToString: @""])
                        {
                            empl_id = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_id;
                        }
                    }
                    else
                    {
                        if ([empl_id isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"empId"]];
                            empl_id = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_id;
                        }
                    }
                }
                else
                {
                    if([empl_id isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID", nil);
                                
                            }
                            else
                            {
                                empl_id = Cell.SearchTextField.text;
                            }
                            
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = empl_id;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 1 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                [Cell.SearchBtn addTarget:self action:@selector(EmployeeNameSearchAction) forControlEvents:UIControlEventTouchUpInside];
                Cell.SearchTextField.keyboardType = UIKeyboardTypeDefault;
//                Cell.SearchTextField.text = empl_name;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"empName"] isEqualToString:@""] || ([dict valueForKey:@"empName"] == nil))
                    {
                        if ([empl_name isEqualToString: @""])
                        {
                            empl_name = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_name;
                        }
                    }
                    else
                    {
                        if ([empl_name isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"empName"]];
                            empl_name = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = empl_name;
                        }
                    }
                }
                else
                {
                    if([empl_name isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME", nil);
                                
                            }
                            else
                            {
                                empl_name = Cell.SearchTextField.text;
                            }
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = empl_name;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 2 ){
                SearchCell * Cell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                [Cell.SearchBtn addTarget:self action:@selector(RequestIDSearchAction) forControlEvents:UIControlEventTouchUpInside];
//                Cell.SearchTextField.text = req_id;
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestId"] isEqualToString:@""] || ([dict valueForKey:@"requestId"] == nil))
                    {
                        if ([req_id isEqualToString: @""])
                        {
                            req_id = Cell.SearchTextField.text;
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                    else
                    {
                        if ([req_id isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"requestId"]];
                            req_id = Cell.SearchTextField.text;
                        }
                        else
                        {
                            Cell.SearchTextField.text = req_id;
                        }
                    }
                }
                else
                {
                    if([req_id isEqualToString:@""])
                    {
                        if ([Cell.SearchTextField.text isEqualToString:@""])
                        {
                            Cell.SearchTextField.text = @"";
                            Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                        }
                        else
                        {
                            if (isClearSelected == YES)
                            {
                                Cell.SearchTextField.text = @"";
                                Cell.SearchTextField.placeholder = NSLocalizedString(@"REQUEST_ID", nil);
                                
                            }
                            else
                            {
                                req_id = Cell.SearchTextField.text;
                            }
                            
                        }
                    }
                    else
                    {
                        Cell.SearchTextField.text = req_id;
                    }
                }

                return Cell;
            }
            else if(indexPath.section == 3){
                CheckboxCell * Cell = (CheckboxCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
                Cell.CheckBoxBtn.tag = indexPath.row;
                Cell.BlanckBtn.tag = indexPath.row;
                [Cell.CheckBoxBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.BlanckBtn addTarget:self action:@selector(CheckUnCheckAction:) forControlEvents:UIControlEventTouchUpInside];
//                if(LocationNameArray.count>0){
//                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
//                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]]){
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
//                    }
//                    else{
//                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
//                    }
//                }
                if(LocationNameArray.count>0)
                {
                    Cell.LocationNameLabel.text = [LocationNameArray objectAtIndex:indexPath.row];
                }
                
                
                if ([dict valueForKey:@"locationId"] == nil)
                {
                    if(LocationNameArray.count>0)
                    {
                        if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%@",[SelectedArray objectAtIndex:indexPath.row]]])
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [Cell.CheckBoxBtn setImage:[UIImage imageNamed:@"un_check"] forState:UIControlStateNormal];
                    }
                }

                
                return Cell;
            }
            else if(indexPath.section == 4 ){
                SelectionCell * Cell = (SelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestStatusSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestStatusAction) forControlEvents:UIControlEventTouchUpInside];
//                if([request_status isEqualToString:@""]){
//                    Cell.SelectionLabel.text = @"Select Status";
//                }
//                else{
//                    Cell.SelectionLabel.text = request_status;
//                }
                if (dict != nil)
                {
                    if (([[dict valueForKey:@"status"] integerValue] == 10) || ([dict valueForKey:@"status"] == nil))
                    {
                        if (([[dict valueForKey:@"status"] integerValue]== 10) && [request_status isEqualToString: @""])
                        {
                            request_status = @"";
                            Cell.SelectionLabel.text = @"Select Status";
                        }
                        else
                        {
                            Cell.SelectionLabel.text = request_status;
                        }
                    }
                    else
                    {
                        if ([request_status isEqualToString:@""])
                        {
                            Cell.SelectionLabel.text = [self getRequestStatusFromRequestId:[[dict valueForKey:@"status"] integerValue]];
                            request_status = Cell.SelectionLabel.text;
                        }
                        else
                        {
                            Cell.SelectionLabel.text = request_status;
                        }
                    }
                }
                else
                {
                    if([request_status isEqualToString:@""])
                    {
                        Cell.SelectionLabel.text = @"Select Status";
                    }
                    else
                    {
                        Cell.SelectionLabel.text = request_status;
                    }
                }

                return Cell;
            }
            else  if(indexPath.section == 5 ){
                SelectionCell * Cell = (SelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(AttendanceStatusSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(AttendanceStatusAction) forControlEvents:UIControlEventTouchUpInside];
//                if([attendance_status isEqualToString:@""]){
//                    Cell.SelectionLabel.text = @"Select Attendance";
//                }
//                else{
//                    Cell.SelectionLabel.text = attendance_status;
//                }
                
                
                
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"attendanceStatus"] isEqualToString:@""] || ([dict valueForKey:@"attendanceStatus"] == nil))
                    {
                        if ([attendance_status isEqualToString:@""])
                        {
                            attendance_status = @"";
                            Cell.SelectionLabel.text = @"Select Attendance";
                        }
                        else
                        {
                            Cell.SelectionLabel.text = attendance_status;
                        }
                    }
                    else
                    {
                        if ([attendance_status isEqualToString:@""])
                        {
                            Cell.SelectionLabel.text = [dict valueForKey:@"attendanceStatus"];
                            attendance_status = Cell.SelectionLabel.text;
                        }
                        else
                        {
                            Cell.SelectionLabel.text = attendance_status;
                        }
                    }
                }
                else
                {
                    if([attendance_status isEqualToString:@""])
                    {
                        Cell.SelectionLabel.text = @"Select Attendance";
                    }
                    else
                    {
                        Cell.SelectionLabel.text = attendance_status;
                    }
                }
                return Cell;
            }
            else  if(indexPath.section == 6 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckInSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckInAction) forControlEvents:UIControlEventTouchUpInside];
//                if([check_in_time isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Check-in Time";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = check_in_time;
//                }
                
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkInMin"] isEqualToString:@""] || ([dict valueForKey:@"checkInMin"] == nil))
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            check_in_time = @"";
                            Cell.DateSelectionLabel.text = @"Check-in Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                    else
                    {
                        if ([check_in_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkInHour"],[dict valueForKey:@"checkInMin"],[dict valueForKey:@"checkInTimeType"]];
                            check_in_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_in_time;
                        }
                    }
                }
                else
                {
                    if([check_in_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Check-in Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_in_time;
                    }
                }

                
                return Cell;
            }
            else  if(indexPath.section == 7 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(CheckOutSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(CheckOutAction) forControlEvents:UIControlEventTouchUpInside];
//                if([check_out_time isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Checkout Time";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = check_out_time;
//                }
                
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"checkOutMin"] isEqualToString:@""] || ([dict valueForKey:@"checkOutMin"] == nil))
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            check_out_time = @"";
                            Cell.DateSelectionLabel.text = @"Checkout Time";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                    else
                    {
                        if ([check_out_time isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [NSString stringWithFormat:@"%@:%@ %@",[dict valueForKey:@"checkOutHour"],[dict valueForKey:@"checkOutMin"],[dict valueForKey:@"checkOutTimeType"]];
                            check_out_time = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = check_out_time;
                        }
                    }
                }
                else
                {
                    if([check_out_time isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Checkout Time";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = check_out_time;
                    }
                }
                
                return Cell;
            }
            
            else  if(indexPath.section == 8 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(RequestDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(RequestDateAction) forControlEvents:UIControlEventTouchUpInside];
//                if([request_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Request Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = request_date;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"requestDate"] isEqualToString:@""] || ([dict valueForKey:@"requestDate"] == nil))
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            request_date = @"";
                            Cell.DateSelectionLabel.text = @"Request Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                    else
                    {
                        if ([request_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"requestDate"]];
                            request_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = request_date;
                        }
                    }
                }
                else
                {
                    if([request_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Request Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = request_date;
                    }
                }

                return Cell;
            }
            else  if(indexPath.section == 9 ){
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(ActionDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(ActionDateAction) forControlEvents:UIControlEventTouchUpInside];
//                if([action_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"Action Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = action_date;
//                }
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"actionDate"] isEqualToString:@""] || ([dict valueForKey:@"actionDate"] == nil))
                    {
                        if ([action_date isEqualToString:@""])
                        {
                            action_date = @"";
                            Cell.DateSelectionLabel.text = @"Action Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = action_date;
                        }
                    }
                    else
                    {
                        if ([action_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"actionDate"]];
                            action_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = action_date;
                        }
                    }
                }
                else
                {
                    if([action_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"Action Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = action_date;
                    }
                }

                return Cell;
            }
            else {
                DateSelectionCell * Cell = (DateSelectionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                [Cell.SearchButton addTarget:self action:@selector(iWorkDateSearchAction) forControlEvents:UIControlEventTouchUpInside];
                [Cell.SelectionButton addTarget:self action:@selector(iWorkDateAction) forControlEvents:UIControlEventTouchUpInside];
                if (dict != nil)
                {
                    if ([[dict valueForKey:@"workDate"] isEqualToString:@""] || ([dict valueForKey:@"workDate"] == nil))
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            iwork_date = @"";
                            Cell.DateSelectionLabel.text = @"iWork Date";
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                    else
                    {
                        if ([iwork_date isEqualToString:@""])
                        {
                            Cell.DateSelectionLabel.text = [self dateToFormatedDateDDMMYYYY: [dict valueForKey:@"workDate"]];
                            iwork_date = Cell.DateSelectionLabel.text;
                        }
                        else
                        {
                            Cell.DateSelectionLabel.text = iwork_date;
                        }
                    }
                }
                else
                {
                    if([iwork_date isEqualToString:@""])
                    {
                        Cell.DateSelectionLabel.text = @"iWork Date";
                    }
                    else
                    {
                        Cell.DateSelectionLabel.text = iwork_date;
                    }
                }

//                if([iwork_date isEqualToString:@""]){
//                    Cell.DateSelectionLabel.text = @"iWork Date";
//                }
//                else{
//                    Cell.DateSelectionLabel.text = iwork_date;
//                }
                return Cell;
            }
        }
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == SelectTableView){
        if([Selection isEqualToString:@"RequestStatus"]){
            request_status = [RequestStatusArray objectAtIndex:indexPath.row];
        }
        else if([Selection isEqualToString:@"AttendanseStatus"]){
            attendance_status =  [AttendanceStatusArray objectAtIndex:indexPath.row];
        }
        SelectView.hidden = YES;
        [FilterTableView reloadData];
    }
    else{
        [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
        [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == SelectTableView){
        return 40;
    }
    else{
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
           return 44;
        }
        return 0;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(tableView == FilterTableView){
        return 44;
    }
    return 10;
}
#pragma mark - Creating View for TableView Section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *Views=[[UIView alloc]initWithFrame:CGRectMake(0, 0, FilterTableView.frame.size.width,0)];
    if(tableView != SelectTableView){
        UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, FilterTableView.frame.size.width,44)];
        sectionView.backgroundColor  = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
        sectionView.tag=section;
        UILabel *viewLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, FilterTableView.frame.size.width, 44)];
        viewLabel.backgroundColor=[UIColor clearColor];
        viewLabel.textAlignment = NSTextAlignmentLeft;
        viewLabel.textColor=[UIColor blackColor];
        viewLabel.font=delegate.ooredoo;
        viewLabel.text=[NSString stringWithFormat:@"%@",[FilterReportsArray objectAtIndex:section]];
        [sectionView addSubview:viewLabel];
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, FilterTableView.frame.size.width, 1)];
        separatorLineView.backgroundColor = [UIColor lightGrayColor];
        [sectionView addSubview:separatorLineView];
        
        UIImageView *ImageViews = [[UIImageView alloc] initWithFrame:CGRectMake(FilterTableView.frame.size.width-40, 17, 10, 10)];
        if ([[arrayForBool objectAtIndex:section] boolValue]){
            ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[MinusImageArray objectAtIndex:section]]];
        }
        else{
            ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[PlusImageArray objectAtIndex:section]]];
        }
        [sectionView addSubview:ImageViews];
        UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
        [sectionView addGestureRecognizer:headerTapped];
        return  sectionView;
    }
    return Views;
}
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[FilterReportsArray count]; i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
-(void)EmployeeIDSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    SearchCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    empl_id = cell.SearchTextField.text;
    [self FilterNofi];
}
-(void)EmployeeNameSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    SearchCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    empl_name = cell.SearchTextField.text;
    [self FilterNofi];
}
-(void)RequestIDSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
    SearchCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    req_id = cell.SearchTextField.text;
    [self FilterNofi];
}
-(void)RequestStatusAction{
    Selection = @"RequestStatus";
    SelectView.hidden = NO;
    [SelectTableView reloadData];
}
-(void)RequestStatusSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:4];
    SelectionCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    request_status = cell.SelectionLabel.text;
    if([request_status isEqualToString:@"Select Status"]){
        request_status = @"";
    }
    [self FilterNofi];
}
-(void)AttendanceStatusAction{
    Selection = @"AttendanseStatus";
    SelectView.hidden = NO;
    [SelectTableView reloadData];
}
-(void)AttendanceStatusSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:5];
    SelectionCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    attendance_status = cell.SelectionLabel.text;
    if([attendance_status isEqualToString:@"Select Attendence"]){
        attendance_status = @"";
    }
    [self FilterNofi];
}
-(void)CheckInAction{
    DateSelection = @"CheckIn";
  //  [DatePicker setLocale:[NSLocale systemLocale]];
    [DatePicker setDatePickerMode:UIDatePickerModeTime];
    DatePikerViewBg.hidden = NO;
    
}
-(void)CheckInSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:6];
    SelectionCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    check_in_time = cell.SelectionLabel.text;
    if([check_in_time isEqualToString:@"Check-in Time"]){
        check_in_time = @"";
    }
    [self FilterNofi];
}
-(void)CheckOutAction{
    DateSelection = @"CheckOut";
    //[DatePicker setLocale:[NSLocale systemLocale]];
    [DatePicker setDatePickerMode:UIDatePickerModeTime];
    DatePikerViewBg.hidden = NO;
}
-(void)CheckOutSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:7];
    SelectionCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    check_out_time = cell.SelectionLabel.text;
    if([check_out_time isEqualToString:@"Checkout Time"]){
        check_out_time = @"";
    }
    [self FilterNofi];
}
-(void)RequestDateAction{
    DateSelection = @"RequestDate";
    [DatePicker setMaximumDate:[NSDate date]];
    [DatePicker setLocale:nil];
    [DatePicker setDatePickerMode:UIDatePickerModeDate];
    DatePikerViewBg.hidden = NO;
}
-(void)RequestDateSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:8];
    DateSelectionCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    request_date = cell.DateSelectionLabel.text;
    if([request_date isEqualToString:@"Request Date"]){
        request_date = @"";
    }
    [self FilterNofi];
}
-(void)ActionDateAction{
    DateSelection = @"ActionDate";
    [DatePicker setMaximumDate:[NSDate date]];
    [DatePicker setLocale:nil];
    [DatePicker setDatePickerMode:UIDatePickerModeDate];
    DatePikerViewBg.hidden = NO;
}
-(void)ActionDateSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:9];
    DateSelectionCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    action_date = cell.DateSelectionLabel.text;
    if([action_date isEqualToString:@"Request Date"]){
        action_date = @"";
    }
    [self FilterNofi];
}
-(void)iWorkDateAction{
    DateSelection = @"iWorkDate";
    [DatePicker setMaximumDate:nil];
    [DatePicker setLocale:nil];
    [DatePicker setDatePickerMode:UIDatePickerModeDate];
    DatePikerViewBg.hidden = NO;
}
-(void)iWorkDateSearchAction{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:4];
    DateSelectionCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
    iwork_date = cell.DateSelectionLabel.text;
    if([iwork_date isEqualToString:@"iWork Date"]){
        iwork_date = @"";
    }
    [self FilterNofi];
}
-(void)FilterNofi{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if([isComeFrom isEqualToString:@"EmployeeDetail"]){
        
        // Request ID
        NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:0 inSection:0];
        SearchCell *cell2 = [FilterTableView cellForRowAtIndexPath:indexPath2];
        if (cell2.SearchTextField.text == nil)
        {
            if ([req_id isEqualToString:@""])
            {
                req_id = cell2.SearchTextField.text;
            }
        }
        else
        {
            req_id = cell2.SearchTextField.text;
        }

//        req_id = cell2.SearchTextField.text;
        
        // iWork Location
        // Location
        if(SelectedLocationIDArray.count>0){
            location_list_array = [SelectedLocationIDArray componentsJoinedByString:@","];
        }
        
        // Request Status
        NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:0 inSection:2];
        SelectionCell *cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
        if (cell4.SelectionLabel.text == nil)
        {
            if ([request_status isEqualToString:@""])
            {
                request_status = cell4.SelectionLabel.text;
            }
            
        }
        else
        {
            request_status = cell4.SelectionLabel.text;
        }

//        request_status = cell4.SelectionLabel.text;
        if([request_status isEqualToString:@"Select Status"])
        {
            request_status = @"";
        }
        
        // Attandeance Status
        NSIndexPath *indexPath5 = [NSIndexPath indexPathForRow:0 inSection:3];
        SelectionCell *cell5 = [FilterTableView cellForRowAtIndexPath:indexPath5];
        if (cell5.SelectionLabel.text == nil)
        {
            if ([attendance_status isEqualToString:@""])
            {
                attendance_status = cell5.SelectionLabel.text;
            }
            
        }
        else
        {
            attendance_status = cell5.SelectionLabel.text;
        }
//        attendance_status = cell5.SelectionLabel.text;
        if([attendance_status isEqualToString:@"Select Attendance"]){
            attendance_status = @"";
        }
        
        // CheckIn Time
        NSIndexPath *indexPath6 = [NSIndexPath indexPathForRow:0 inSection:4];
        DateSelectionCell *cell6 = [FilterTableView cellForRowAtIndexPath:indexPath6];
        if (cell6.DateSelectionLabel.text == nil)
        {
            if ([check_in_time isEqualToString:@""])
            {
                check_in_time = cell6.DateSelectionLabel.text;
            }
        }
        else
        {
            check_in_time = cell6.DateSelectionLabel.text;
        }
        
        if([check_in_time isEqualToString:@"Check-in Time"]){
            check_in_time = @"";
        }

        if ([check_in_time isEqualToString:@""])
        {
        }
        else
        {
            NSArray *Split = [check_in_time componentsSeparatedByString:@" "];
            NSString *CheckInHourMin = [Split objectAtIndex:0];
            CheckInAMPM = [Split objectAtIndex:1];
            NSArray *Split1 = [CheckInHourMin componentsSeparatedByString:@":"];
            CheckInHour = [Split1 objectAtIndex:0];
            CheckInMin = [Split1 objectAtIndex:1];
        }
        
//        check_in_time = cell6.DateSelectionLabel.text;
      
        
        // Checkout Time
        NSIndexPath *indexPath7 = [NSIndexPath indexPathForRow:0 inSection:5];
        DateSelectionCell *cell7 = [FilterTableView cellForRowAtIndexPath:indexPath7];
        if (cell7.DateSelectionLabel.text == nil)
        {
            if ([check_out_time isEqualToString:@""])
            {
                check_out_time = cell7.DateSelectionLabel.text;
            }
        }
        else
        {
            check_out_time = cell7.DateSelectionLabel.text;
        }
        if([check_out_time isEqualToString:@"Checkout Time"]){
            check_out_time = @"";
        }
        
        if ([check_out_time isEqualToString:@""])
        {
        }
        else
        {
            NSArray *Split = [check_out_time componentsSeparatedByString:@" "];
            NSString *CheckOutHourMin = [Split objectAtIndex:0];
            CheckOutAMPM = [Split objectAtIndex:1];
            NSArray *Split1 = [CheckOutHourMin componentsSeparatedByString:@":"];
            CheckOutHour = [Split1 objectAtIndex:0];
            CheckOutMin = [Split1 objectAtIndex:1];
        }
        
//        check_out_time = cell7.DateSelectionLabel.text;
        
        
        // Request Date
        NSIndexPath *indexPath8 = [NSIndexPath indexPathForRow:0 inSection:6];
        DateSelectionCell *cell8 = [FilterTableView cellForRowAtIndexPath:indexPath8];
        if (cell8.DateSelectionLabel.text == nil)
        {
            if ([request_date isEqualToString:@""])
            {
                request_date = cell8.DateSelectionLabel.text;
            }
        }
        else
        {
            request_date = cell8.DateSelectionLabel.text;
        }
//        request_date = cell8.DateSelectionLabel.text;
        if([request_date isEqualToString:@"Request Date"]){
            request_date = @"";
        }
        else {
            request_date = [self dateToFormatedDate:request_date];
        }
        
        // Action Date
        NSIndexPath *indexPath9 = [NSIndexPath indexPathForRow:0 inSection:7];
        DateSelectionCell *cell9 = [FilterTableView cellForRowAtIndexPath:indexPath9];
        if (cell9.DateSelectionLabel.text == nil)
        {
            if ([action_date isEqualToString:@""])
            {
                action_date = cell9.DateSelectionLabel.text;
            }
        }
        else
        {
            action_date = cell9.DateSelectionLabel.text;
        }
//        action_date = cell9.DateSelectionLabel.text;
        if([action_date isEqualToString:@"Action Date"]){
            action_date = @"";
        }
        else {
            action_date = [self dateToFormatedDate:action_date];
        }
        
        // iWork Date
        NSIndexPath *indexPath10 = [NSIndexPath indexPathForRow:0 inSection:8];
        DateSelectionCell *cell10 = [FilterTableView cellForRowAtIndexPath:indexPath10];
        if (cell10.DateSelectionLabel.text == nil)
        {
            if ([iwork_date isEqualToString:@""])
            {
                iwork_date = cell10.DateSelectionLabel.text;
            }
        }
        else
        {
            iwork_date = cell10.DateSelectionLabel.text;
        }
//        iwork_date = cell10.DateSelectionLabel.text;
        if([iwork_date isEqualToString:@"iWork Date"]){
            iwork_date = @"";
        }
        else {
            iwork_date = [self dateToFormatedDate:iwork_date];
        }
        
        NSMutableDictionary *UserDic = [[NSMutableDictionary alloc] init];
        if(![req_id isEqualToString:@""]){
            [UserDic setValue:req_id forKey:@"requestId"];
        }
        //if(![location_list_array isEqualToString:@""]){
            [UserDic setValue:SelectedLocationIDArray forKey:@"locationId"];
       // }
        if(![request_status isEqualToString:@""]){
            if([request_status isEqualToString:@"New"]){
                request_status = @"0";
            }
            else if ([request_status isEqualToString:@"Approved"]){
                request_status = @"1";
            }
            else if ([request_status isEqualToString:@"Rejected"]){
                request_status = @"2";
            }
            else if ([request_status isEqualToString:@"Cancelled"]){
                request_status = @"3";
            }
            else if ([request_status isEqualToString:@"Discarded"]){
                request_status = @"4";
            }
            else if ([request_status isEqualToString:@"Decliend"]){
                request_status = @"5";
            }
            [UserDic setValue:request_status forKey:@"status"];
        }
        if(![attendance_status isEqualToString:@""]){
            if([attendance_status isEqualToString:@"Present"]){
                attendance_status = @"Present";
            }
            else if([attendance_status isEqualToString:@"Absent"]){
                 attendance_status = @"Absent";
            }
            [UserDic setValue:attendance_status forKey:@"attendanceStatus"];
        }
        if(![request_date isEqualToString:@""]){
            [UserDic setValue:request_date forKey:@"requestDate"];
        }
        if(![iwork_date isEqualToString:@""]){
            [UserDic setValue:iwork_date forKey:@"workDate"];
        }
        if(![check_in_time isEqualToString:@""]){
            [UserDic setValue:CheckInHour forKey:@"checkInHour"];
            [UserDic setValue:CheckInMin forKey:@"checkInMin"];
            [UserDic setValue:CheckInAMPM forKey:@"checkInTimeType"];
        }
        if(![check_out_time isEqualToString:@""]){
            [UserDic setValue:CheckOutHour forKey:@"checkOutHour"];
            [UserDic setValue:CheckOutMin forKey:@"checkOutMin"];
            [UserDic setValue:CheckOutAMPM forKey:@"checkOutTimeType"];
        }
        if(![action_date isEqualToString:@""]){
            [UserDic setValue:action_date forKey:@"actionDate"];
        }
        if([request_status isEqualToString:@""]){
           [UserDic setValue:@10 forKey:@"status"];
        }
        [UserDic setValue:EmployeeID forKey:@"empId"];
        [UserDic setValue:[ApplicationState userId] forKey:@"userId"];
        [UserDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
        [UserDic setValue:[NSNumber numberWithInt:limit] forKey:@"limit"];
        [userDefault setObject:UserDic forKey:@"EmployeeDetail"];
        [userDefault synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EmployeeDetails" object:nil userInfo:UserDic];
    }
    else{
        // Employee ID
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        SearchCell *cell = [FilterTableView cellForRowAtIndexPath:indexPath];
        empl_id = [self setValueForApi:empl_id textField:cell.SearchTextField];
//        empl_id = cell.SearchTextField.text;
        
        // Employee Name
        NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:0 inSection:1];
        SearchCell *cell1 = [FilterTableView cellForRowAtIndexPath:indexPath1];
        empl_name = [self setValueForApi:empl_name textField:cell1.SearchTextField];
//        empl_name = cell1.SearchTextField.text;
        NSLog(@"empl_name===%@",empl_name);
        
        // Request ID
        NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:0 inSection:2];
        SearchCell *cell2 = [FilterTableView cellForRowAtIndexPath:indexPath2];
        req_id = [self setValueForApi:req_id textField:cell2.SearchTextField];
//        req_id = cell2.SearchTextField.text;
        
        if([isComeFrom isEqualToString:@"LineManagerRejected"]){
            // CheckIn Time
            NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:0 inSection:4];
            DateSelectionCell *cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
            check_in_time = [self setValueLabelForApi:check_in_time label:cell4.DateSelectionLabel];
//            check_in_time = cell4.DateSelectionLabel.text;
            if([check_in_time isEqualToString:@"Check-in Time"]){
                check_in_time = @"";
            }
            
            if ([check_in_time isEqualToString:@""])
            {
            }
            else
            {
                NSArray *Split = [check_in_time componentsSeparatedByString:@" "];
                NSString *CheckInHourMin = [Split objectAtIndex:0];
                CheckInAMPM = [Split objectAtIndex:1];
                NSArray *Split1 = [CheckInHourMin componentsSeparatedByString:@":"];
                CheckInHour = [Split1 objectAtIndex:0];
                CheckInMin = [Split1 objectAtIndex:1];
            }

            
            // Checkout Time
            NSIndexPath *indexPath5 = [NSIndexPath indexPathForRow:0 inSection:5];
            DateSelectionCell *cell5 = [FilterTableView cellForRowAtIndexPath:indexPath5];
            check_out_time = [self setValueLabelForApi:check_out_time label:cell5.DateSelectionLabel];
//            check_out_time = cell5.DateSelectionLabel.text;
            if([check_out_time isEqualToString:@"Checkout Time"]){
                check_out_time = @"";
            }
            if ([check_out_time isEqualToString:@""])
            {
            }
            else
            {
                NSArray *Split = [check_out_time componentsSeparatedByString:@" "];
                NSString *CheckOutHourMin = [Split objectAtIndex:0];
                CheckOutAMPM = [Split objectAtIndex:1];
                NSArray *Split1 = [CheckOutHourMin componentsSeparatedByString:@":"];
                CheckOutHour = [Split1 objectAtIndex:0];
                CheckOutMin = [Split1 objectAtIndex:1];
            }
            // Request Date
            NSIndexPath *indexPath6 = [NSIndexPath indexPathForRow:0 inSection:6];
            DateSelectionCell *cell6 = [FilterTableView cellForRowAtIndexPath:indexPath6];
            request_date = [self setValueLabelForApi:request_date label:cell6.DateSelectionLabel];
//            request_date = cell6.DateSelectionLabel.text;
            if([request_date isEqualToString:@"Request Date"]){
                request_date = @"";
            }
            else {
                request_date = [self dateToFormatedDate:request_date];
            }
            
            // Action Date
            NSIndexPath *indexPath7 = [NSIndexPath indexPathForRow:0 inSection:7];
            DateSelectionCell *cell7 = [FilterTableView cellForRowAtIndexPath:indexPath7];
            action_date = [self setValueLabelForApi:action_date label:cell7.DateSelectionLabel];
//            action_date = cell7.DateSelectionLabel.text;
            if([action_date isEqualToString:@"Action Date"]){
                action_date = @"";
            }
            else {
                action_date = [self dateToFormatedDate:action_date];
            }
            
            // iWork Date
            NSIndexPath *indexPath8 = [NSIndexPath indexPathForRow:0 inSection:8];
            DateSelectionCell *cell8 = [FilterTableView cellForRowAtIndexPath:indexPath8];
            iwork_date = [self setValueLabelForApi:iwork_date label:cell8.DateSelectionLabel];
//            iwork_date = cell8.DateSelectionLabel.text;
            if([iwork_date isEqualToString:@"iWork Date"]){
                iwork_date = @"";
            }
            else {
                iwork_date = [self dateToFormatedDate:iwork_date];
            }
        }
        else if([isComeFrom isEqualToString:@"LineManagerNew"]){
            // CheckIn Time
            NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:0 inSection:4];
            DateSelectionCell *cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
            check_in_time = [self setValueLabelForApi:check_in_time label:cell4.DateSelectionLabel];
//            check_in_time = cell4.DateSelectionLabel.text;
            if([check_in_time isEqualToString:@"Check-in Time"]){
                check_in_time = @"";
            }
            
            if ([check_in_time isEqualToString:@""])
            {
            }
            else
            {
                NSArray *Split = [check_in_time componentsSeparatedByString:@" "];
                NSString *CheckInHourMin = [Split objectAtIndex:0];
                CheckInAMPM = [Split objectAtIndex:1];
                NSArray *Split1 = [CheckInHourMin componentsSeparatedByString:@":"];
                CheckInHour = [Split1 objectAtIndex:0];
                CheckInMin = [Split1 objectAtIndex:1];
            }

            // Checkout Time
            NSIndexPath *indexPath5 = [NSIndexPath indexPathForRow:0 inSection:5];
            DateSelectionCell *cell5 = [FilterTableView cellForRowAtIndexPath:indexPath5];
            check_out_time = [self setValueLabelForApi:check_out_time label:cell5.DateSelectionLabel];
//            check_out_time = cell5.DateSelectionLabel.text;
            if([check_out_time isEqualToString:@"Checkout Time"]){
                check_out_time = @"";
            }
            if ([check_out_time isEqualToString:@""])
            {
            }
            else
            {
                NSArray *Split = [check_out_time componentsSeparatedByString:@" "];
                NSString *CheckOutHourMin = [Split objectAtIndex:0];
                CheckOutAMPM = [Split objectAtIndex:1];
                NSArray *Split1 = [CheckOutHourMin componentsSeparatedByString:@":"];
                CheckOutHour = [Split1 objectAtIndex:0];
                CheckOutMin = [Split1 objectAtIndex:1];
            }
            // Request Date
            NSIndexPath *indexPath6 = [NSIndexPath indexPathForRow:0 inSection:6];
            DateSelectionCell *cell6 = [FilterTableView cellForRowAtIndexPath:indexPath6];
            request_date = [self setValueLabelForApi:request_date label:cell6.DateSelectionLabel];
//            request_date = cell6.DateSelectionLabel.text;
            if([request_date isEqualToString:@"Request Date"]){
                request_date = @"";
            }
            else {
                request_date = [self dateToFormatedDate:request_date];
            }
            
            // iWork Date
            NSIndexPath *indexPath7 = [NSIndexPath indexPathForRow:0 inSection:7];
            DateSelectionCell *cell7 = [FilterTableView cellForRowAtIndexPath:indexPath7];
            iwork_date = [self setValueLabelForApi:iwork_date label:cell7.DateSelectionLabel];
//            iwork_date = cell7.DateSelectionLabel.text;
            if([iwork_date isEqualToString:@"iWork Date"]){
                iwork_date = @"";
            }
            else {
                iwork_date = [self dateToFormatedDate:iwork_date];
            }
        }
        else if([isComeFrom isEqualToString:@"LineManagerApproved"]){
            
            // Attandeance Status
            NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:0 inSection:4];
            SelectionCell *cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
            attendance_status = [self setValueLabelForApi:attendance_status label:cell4.SelectionLabel];
//            attendance_status = cell4.SelectionLabel.text;
            if([attendance_status isEqualToString:@"Select Attendance"]){
                attendance_status = @"";
            }
            
            // CheckIn Time
            NSIndexPath *indexPath5 = [NSIndexPath indexPathForRow:0 inSection:5];
            DateSelectionCell *cell5 = [FilterTableView cellForRowAtIndexPath:indexPath5];
            check_in_time = [self setValueLabelForApi:check_in_time label:cell5.DateSelectionLabel];
//            check_in_time = cell5.DateSelectionLabel.text;
            if([check_in_time isEqualToString:@"Check-in Time"]){
                check_in_time = @"";
            }
            
            if ([check_in_time isEqualToString:@""])
            {
            }
            else
            {
                NSArray *Split = [check_in_time componentsSeparatedByString:@" "];
                NSString *CheckInHourMin = [Split objectAtIndex:0];
                CheckInAMPM = [Split objectAtIndex:1];
                NSArray *Split1 = [CheckInHourMin componentsSeparatedByString:@":"];
                CheckInHour = [Split1 objectAtIndex:0];
                CheckInMin = [Split1 objectAtIndex:1];
            }

            
            // Checkout Time
            NSIndexPath *indexPath6 = [NSIndexPath indexPathForRow:0 inSection:6];
            DateSelectionCell *cell6 = [FilterTableView cellForRowAtIndexPath:indexPath6];
            check_out_time = [self setValueLabelForApi:check_out_time label:cell6.DateSelectionLabel];
//            check_out_time = cell6.DateSelectionLabel.text;
            if([check_out_time isEqualToString:@"Checkout Time"]){
                check_out_time = @"";
            }
            
            if ([check_out_time isEqualToString:@""])
            {
            }
            else
            {
                NSArray *Split = [check_out_time componentsSeparatedByString:@" "];
                NSString *CheckOutHourMin = [Split objectAtIndex:0];
                CheckOutAMPM = [Split objectAtIndex:1];
                NSArray *Split1 = [CheckOutHourMin componentsSeparatedByString:@":"];
                CheckOutHour = [Split1 objectAtIndex:0];
                CheckOutMin = [Split1 objectAtIndex:1];
            }
            // Request Date
            NSIndexPath *indexPath7 = [NSIndexPath indexPathForRow:0 inSection:7];
            DateSelectionCell *cell7 = [FilterTableView cellForRowAtIndexPath:indexPath7];
            request_date = [self setValueLabelForApi:request_date label:cell7.DateSelectionLabel];
//            request_date = cell7.DateSelectionLabel.text;
            if([request_date isEqualToString:@"Request Date"]){
                request_date = @"";
            }
            else {
                request_date = [self dateToFormatedDate:request_date];
            }
            
            // Action Date
            NSIndexPath *indexPath8 = [NSIndexPath indexPathForRow:0 inSection:8];
            DateSelectionCell *cell8 = [FilterTableView cellForRowAtIndexPath:indexPath8];
            action_date = [self setValueLabelForApi:action_date label:cell8.DateSelectionLabel];
//            action_date = cell8.DateSelectionLabel.text;
            if([action_date isEqualToString:@"Action Date"]){
                action_date = @"";
            }
            else {
                action_date = [self dateToFormatedDate:action_date];
            }
            
            // iWork Date
            NSIndexPath *indexPath9 = [NSIndexPath indexPathForRow:0 inSection:9];
            DateSelectionCell *cell9 = [FilterTableView cellForRowAtIndexPath:indexPath9];
            iwork_date = [self setValueLabelForApi:iwork_date label:cell9.DateSelectionLabel];
//            iwork_date = cell9.DateSelectionLabel.text;
            if([iwork_date isEqualToString:@"iWork Date"]){
                iwork_date = @"";
            }
            else {
                iwork_date = [self dateToFormatedDate:iwork_date];
            }
        }
        else{
            // Request Status
            NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:0 inSection:4];
            SelectionCell *cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
            request_status = [self setValueLabelForApi:request_status label:cell4.SelectionLabel];
//            request_status = cell4.SelectionLabel.text;
            if([request_status isEqualToString:@"Select Status"]){
                request_status = @"";
            }
            
            // Attandeance Status
            NSIndexPath *indexPath5 = [NSIndexPath indexPathForRow:0 inSection:5];
            SelectionCell *cell5 = [FilterTableView cellForRowAtIndexPath:indexPath5];
            attendance_status = [self setValueLabelForApi:attendance_status label:cell5.SelectionLabel];
//            attendance_status = cell5.SelectionLabel.text;
            if([attendance_status isEqualToString:@"Select Attendance"]){
                attendance_status = @"";
            }
            
            // CheckIn Time
            NSIndexPath *indexPath6 = [NSIndexPath indexPathForRow:0 inSection:6];
            DateSelectionCell *cell6 = [FilterTableView cellForRowAtIndexPath:indexPath6];
            check_in_time = [self setValueLabelForApi:check_in_time label:cell6.DateSelectionLabel];
//            check_in_time = cell6.DateSelectionLabel.text;
            if([check_in_time isEqualToString:@"Check-in Time"]){
                check_in_time = @"";
            }
            
            if ([check_in_time isEqualToString:@""])
            {
            }
            else
            {
                NSArray *Split = [check_in_time componentsSeparatedByString:@" "];
                NSString *CheckInHourMin = [Split objectAtIndex:0];
                CheckInAMPM = [Split objectAtIndex:1];
                NSArray *Split1 = [CheckInHourMin componentsSeparatedByString:@":"];
                CheckInHour = [Split1 objectAtIndex:0];
                CheckInMin = [Split1 objectAtIndex:1];
            }

            // Checkout Time
            NSIndexPath *indexPath7 = [NSIndexPath indexPathForRow:0 inSection:7];
            DateSelectionCell *cell7 = [FilterTableView cellForRowAtIndexPath:indexPath7];
            check_out_time = [self setValueLabelForApi:check_out_time label:cell7.DateSelectionLabel];
//            check_out_time = cell7.DateSelectionLabel.text;
            if([check_out_time isEqualToString:@"Checkout Time"]){
                check_out_time = @"";
            }
            
            if ([check_out_time isEqualToString:@""])
            {
            }
            else
            {
                NSArray *Split = [check_out_time componentsSeparatedByString:@" "];
                NSString *CheckOutHourMin = [Split objectAtIndex:0];
                CheckOutAMPM = [Split objectAtIndex:1];
                NSArray *Split1 = [CheckOutHourMin componentsSeparatedByString:@":"];
                CheckOutHour = [Split1 objectAtIndex:0];
                CheckOutMin = [Split1 objectAtIndex:1];
            }
            
            // Request Date
            NSIndexPath *indexPath8 = [NSIndexPath indexPathForRow:0 inSection:8];
            DateSelectionCell *cell8 = [FilterTableView cellForRowAtIndexPath:indexPath8];
            request_date = [self setValueLabelForApi:request_date label:cell8.DateSelectionLabel];
//            request_date = cell8.DateSelectionLabel.text;
            if([request_date isEqualToString:@"Request Date"]){
                request_date = @"";
            }
            else {
                request_date = [self dateToFormatedDate:request_date];
            }
            
            // Action Date
            NSIndexPath *indexPath9 = [NSIndexPath indexPathForRow:0 inSection:9];
            DateSelectionCell *cell9 = [FilterTableView cellForRowAtIndexPath:indexPath9];
            action_date = [self setValueLabelForApi:action_date label:cell9.DateSelectionLabel];
//            action_date = cell9.DateSelectionLabel.text;
            if([action_date isEqualToString:@"Action Date"]){
                action_date = @"";
            }
            else {
                action_date = [self dateToFormatedDate:action_date];
            }
            
            // iWork Date
            NSIndexPath *indexPath10 = [NSIndexPath indexPathForRow:0 inSection:10];
            DateSelectionCell *cell10 = [FilterTableView cellForRowAtIndexPath:indexPath10];
            iwork_date = [self setValueLabelForApi:iwork_date label:cell10.DateSelectionLabel];
//            iwork_date = cell10.DateSelectionLabel.text;
            if([iwork_date isEqualToString:@"iWork Date"]){
                iwork_date = @"";
            }
            else {
                iwork_date = [self dateToFormatedDate:iwork_date];
            }
        }
        NSMutableDictionary *UserDic = [[NSMutableDictionary alloc] init];
        if(![req_id isEqualToString:@""]){
            [UserDic setValue:req_id forKey:@"requestId"];
        }
//        if(![location_list_array isEqualToString:@""]){
            [UserDic setValue:SelectedLocationIDArray forKey:@"locationId"];
       // }
        if(![request_status isEqualToString:@""]){
            if([request_status isEqualToString:@"New"]){
                request_status = @"0";
            }
            else if ([request_status isEqualToString:@"Approved"]){
                request_status = @"1";
            }
            else if ([request_status isEqualToString:@"Rejected"]){
                request_status = @"2";
            }
            else if ([request_status isEqualToString:@"Cancelled"]){
                request_status = @"3";
            }
            else if ([request_status isEqualToString:@"Discarded"]){
                request_status = @"4";
            }
            else if ([request_status isEqualToString:@"Decliend"]){
                request_status = @"5";
            }
            else if ([request_status isEqualToString:@"Missed"]){
                request_status = @"6";
            }
            [UserDic setValue:request_status forKey:@"status"];
        }
        if(![attendance_status isEqualToString:@""]){
            if([attendance_status isEqualToString:@"Present"]){
                attendance_status = @"Present";
            }
            else if([attendance_status isEqualToString:@"Absent"]){
                attendance_status = @"Absent";
            }
            [UserDic setValue:attendance_status forKey:@"attendanceStatus"];
        }
        if(![request_date isEqualToString:@""]){
            [UserDic setValue:request_date forKey:@"requestDate"];
        }
        if(![iwork_date isEqualToString:@""]){
            [UserDic setValue:iwork_date forKey:@"workDate"];
        }
        if(![check_in_time isEqualToString:@""]){
            [UserDic setValue:CheckInHour forKey:@"checkInHour"];
            [UserDic setValue:CheckInMin forKey:@"checkInMin"];
            [UserDic setValue:CheckInAMPM forKey:@"checkInTimeType"];
        }
        if(![check_out_time isEqualToString:@""]){
            [UserDic setValue:CheckOutHour forKey:@"checkOutHour"];
            [UserDic setValue:CheckOutMin forKey:@"checkOutMin"];
            [UserDic setValue:CheckOutAMPM forKey:@"checkOutTimeType"];
        }
        if(![action_date isEqualToString:@""]){
            [UserDic setValue:action_date forKey:@"actionDate"];
        }
        if(![empl_id isEqualToString:@""]){
            [UserDic setValue:empl_id forKey:@"empId"];
        }
        if(![empl_name isEqualToString:@""]){
            [UserDic setValue:empl_name forKey:@"empName"];
        }
        if([request_status isEqualToString:@""]){
            [UserDic setValue:@10 forKey:@"status"];
        }
        NSLog(@"empl_name===%@",empl_name);
        [UserDic setValue:[ApplicationState userId] forKey:@"userId"];
        [UserDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
        [UserDic setValue:[NSNumber numberWithInt:limit] forKey:@"limit"];
       
        if([isComeFrom isEqualToString:@"LineManagerAll"]){
            if([request_status isEqualToString:@""]||[request_status isEqualToString:@"()"]|| request_status == nil){
                [UserDic setValue:@"10" forKey:@"status"];
            }
            [userDefault setObject:UserDic forKey:@"LineManagerAll"];
            [userDefault synchronize];
            NSLog(@"UserDic====%@",UserDic);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LineManagerAll" object:nil userInfo:UserDic];
        }
        else if([isComeFrom isEqualToString:@"LineManagerNew"]){
            if([request_status isEqualToString:@""]||[request_status isEqualToString:@"()"]|| request_status == nil){
                [UserDic setValue:@"0" forKey:@"status"];
            }
            [userDefault setObject:UserDic forKey:@"LineManagerNew"];
            [userDefault synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LineManagerNew" object:nil userInfo:UserDic];
        }

        if([isComeFrom isEqualToString:@"LineManagerApproved"]){
            if([request_status isEqualToString:@""]||[request_status isEqualToString:@"()"]|| request_status == nil){
                [UserDic setValue:@"1" forKey:@"status"];
            }
            [userDefault setObject:UserDic forKey:@"LineManagerApproved"];
            [userDefault synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LineManagerApproved" object:nil userInfo:UserDic];
        }
        else if([isComeFrom isEqualToString:@"LineManagerRejected"]){
            if([request_status isEqualToString:@""]||[request_status isEqualToString:@"()"]|| request_status == nil){
                [UserDic setValue:@"2" forKey:@"status"];
            }
            [userDefault setObject:UserDic forKey:@"LineManagerRejected"];
            [userDefault synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LineManagerRejected" object:nil userInfo:UserDic];
        }
        else if([isComeFrom isEqualToString:@"LineManagerMissed"]){
            if([request_status isEqualToString:@""]||[request_status isEqualToString:@"()"]|| request_status == nil){
                [UserDic setValue:@"6" forKey:@"status"];
            }
            [userDefault setObject:UserDic forKey:@"LineManagerMissed"];
            [userDefault synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LineManagerMissed" object:nil userInfo:UserDic];
        }
    }
}
-(void)LocationApi{
    SelectedIndex = [[NSMutableArray alloc] init];
    SelectedArray = [[NSMutableArray alloc] init];
    LocationIDArray = [[NSMutableArray alloc] init];
    LocationNameArray = [[NSMutableArray alloc] init];
    
    if(delegate.isInternetConnected){
        ResponseDic = [Api WebApi:nil Url:@"locationsList"];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            if([ResponseDic[@"object"] isKindOfClass:[NSDictionary class]]){
                NSArray * locations = [ResponseDic[@"object"] valueForKey:@"locationData"];
                if(locations.count>0){
                    for (int i = 0; i< locations.count; i++){
                        NSDictionary *Dic = [locations objectAtIndex:i];
                        NSString *LocID = [Dic valueForKey:@"iwork_locations_id"];
                        NSString *Name = @"";
                        if(IsSafeStringPlus(TrToString(Dic[@"office_name"]))){
                            Name = [Dic valueForKey:@"office_name"];
                        }
                        else{
                            Name = @"";
                        }
                        [LocationIDArray addObject:LocID];
                        [LocationNameArray addObject:Name];
                    }
                    for (int i = 0; i< [LocationIDArray count]; i++) {
                        NSString *TagValue = [NSString stringWithFormat:@"%d",i];
                        [SelectedArray addObject:TagValue];
                    }
                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                    dict = [defaults objectForKey:isComeFrom];
                    [self getLocationArray:dict];
                    [FilterTableView reloadData];
                }
            }
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:NSLocalizedString(@"NO_LOCATION_FROM_SERVER", nil) ButtonTitle:NSLocalizedString(@"OK", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
}

-(IBAction)DatePickerCancelAction:(id)sender{
    DatePikerViewBg.hidden = YES;
}
-(IBAction)DatePickerOKAction:(id)sender{
    DatePikerViewBg.hidden = YES;
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    
    if([DateSelection isEqualToString:@"CheckIn"]){
        df.dateFormat = @"hh:mm aa";
        check_in_time = [df stringFromDate:DatePicker.date];
       
        NSArray *Split = [check_in_time componentsSeparatedByString:@" "];
        NSString *CheckInHourMin = [Split objectAtIndex:0];
        CheckInAMPM = [Split objectAtIndex:1];
        NSArray *Split1 = [CheckInHourMin componentsSeparatedByString:@":"];
        CheckInHour = [Split1 objectAtIndex:0];
        CheckInMin = [Split1 objectAtIndex:1];
    
    }
    else if([DateSelection isEqualToString:@"CheckOut"]){
        df.dateFormat = @"hh:mm aa";
        check_out_time = [df stringFromDate:DatePicker.date];
        
        NSArray *Split = [check_out_time componentsSeparatedByString:@" "];
        NSString *CheckOutHourMin = [Split objectAtIndex:0];
        CheckOutAMPM = [Split objectAtIndex:1];
        NSArray *Split1 = [CheckOutHourMin componentsSeparatedByString:@":"];
        CheckOutHour = [Split1 objectAtIndex:0];
        CheckOutMin = [Split1 objectAtIndex:1];
      
    }
    else if([DateSelection isEqualToString:@"RequestDate"]){
        df.dateFormat = @"dd-MM-yyyy";
        request_date = [df stringFromDate:DatePicker.date];
    }
    else if([DateSelection isEqualToString:@"ActionDate"]){
        df.dateFormat = @"dd-MM-yyyy";
        action_date = [df stringFromDate:DatePicker.date];
    }
    else if([DateSelection isEqualToString:@"iWorkDate"]){
        df.dateFormat = @"dd-MM-yyyy";
        iwork_date = [df stringFromDate:DatePicker.date];
    }
    DateSelection = @"";
    [FilterTableView reloadData];
}

- (IBAction)CancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)FilterAction:(id)sender
{
    [self FilterNofi];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
//    if (isClearSelected == NO)
//    {
//        [self FilterNofi];
//    }
//    else
//    {
//        isClearSelected = NO;
//        [self ClearData];
//        if (clearFilterForEmployee == NO)
//        {
//            clearFilterForEmployee = nil;
//            [userDefault removeObjectForKey:isComeFrom];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"EmployeeDetailsClearFilterApi" object:nil userInfo:nil];
//        }
//        else
//        {
//            clearFilterForEmployee = nil;
//            [userDefault removeObjectForKey:isComeFrom];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"LMClearFilterApi" object:nil userInfo:nil];
//        }
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
-(void)CheckUnCheckAction:(UIButton*)sender{
    int Tagvalue = sender.tag;
    if([SelectedIndex containsObject:[NSString stringWithFormat:@"%d",Tagvalue]]){
        [SelectedIndex removeObject:[NSString stringWithFormat:@"%d",Tagvalue]];
        [SelectedLocationIDArray removeObject:[NSString stringWithFormat:@"%@",[LocationIDArray objectAtIndex:Tagvalue]]];
    }
    else{
        [SelectedIndex addObject:[NSString stringWithFormat:@"%d",Tagvalue]];
        [SelectedLocationIDArray addObject:[NSString stringWithFormat:@"%@",[LocationIDArray objectAtIndex:Tagvalue]]];
    }
    [FilterTableView reloadData];
    
}
-(void)ClearData{
    req_id = @"";
    request_status = @"";
    attendance_status = @"";
    request_date = @"";
    iwork_date = @"";
    location_list_array = @"";
    check_in_time = @"";
    check_out_time = @"";
    action_date = @"";
    empl_id = @"";
    empl_name = @"";
}
-(void)getLocationArray:(NSMutableDictionary *)dict{
    if ([dict valueForKey:@"locationId"] == nil)
    {
    }
    else{
        NSArray *locationIdArr = [dict valueForKey:@"locationId"];
        NSMutableArray *locationId = [[NSMutableArray alloc] init];
        for (int i = 0; i< LocationIDArray.count; i++)
        {
            for (int j = 0; j< locationIdArr.count; j++)
            {
                if (LocationIDArray[i] == [NSNumber numberWithInteger:[locationIdArr[j] intValue]])
                {
                    [locationId insertObject:locationIdArr[j] atIndex:i];
                    [SelectedIndex addObject:[NSString stringWithFormat:@"%d",i]];
                    break;
                }
                else
                {
                    [locationId insertObject:@"" atIndex:i];
                }
            }
        }
        SelectedLocationIDArray = [locationIdArr mutableCopy];
    }
}
-(IBAction)ClearFilterAction:(id)sender
{
    isClearSelected = YES;
    [self ClearData];
    [FilterTableView reloadData];
    if ([isComeFrom isEqualToString:@"EmployeeDetail"])
    {
        [userDefault removeObjectForKey:isComeFrom];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EmployeeDetailsClearFilterApi" object:nil userInfo:nil];
//        clearFilterForEmployee = NO;
    }
    else
    {
        [userDefault removeObjectForKey:isComeFrom];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LMClearFilterApi" object:nil userInfo:nil];
//        clearFilterForEmployee = YES;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(NSString *)dateToFormatedDate:(NSString *)dateStr {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:date];
}
-(NSString *)dateToFormatedDateDDMMYYYY:(NSString *)dateStr {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    return [dateFormatter stringFromDate:date];
}
-(NSString *)getRequestStatusFromRequestId:(NSInteger)requestId
{
    if ((int)requestId == 0)
    {
        return @"New";
    }
    else if ((int)requestId == 1)
    {
        return @"Approved";
    }
    else if ((int)requestId == 2)
    {
        return @"Rejected";
    }
    else if ((int)requestId == 3)
    {
        return @"Cancelled";
    }
    else if ((int)requestId == 4)
    {
        return @"Discarded";
    }
    else if ((int)requestId == 5)
    {
        return @"Decliend";
    }
    else
    {
        return @"Missed";
    }
}
-(NSString *)setValueForApi:(NSString *)value textField:(UITextField *)textField
{
    if (textField.text == nil)
    {
        if ([value isEqualToString:@""])
        {
            value = textField.text;
        }
        
    }
    else
    {
        value = textField.text;
    }
    return value;
}
-(NSString *)setValueLabelForApi:(NSString *)value label:(UILabel *)label
{
    if (label.text == nil)
    {
        if ([value isEqualToString:@""])
        {
            value = label.text;
        }
        
    }
    else
    {
        value = label.text;
    }
    return value;
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
@end
