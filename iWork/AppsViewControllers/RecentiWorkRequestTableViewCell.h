//
//  RecentiWorkRequestTableViewCell.h
//  iWork
//
//  Created by mac book pro on 06/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentiWorkRequestTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *upperLine;
@property (weak, nonatomic) IBOutlet UIView *circleView;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestID;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestDate;
@property (weak, nonatomic) IBOutlet UILabel *lblActionDate;
@property (weak, nonatomic) IBOutlet UIButton *btnStatus;
-(void)configureWith:(NSDictionary *)responseDict indexPath:(NSInteger)index;
@end
