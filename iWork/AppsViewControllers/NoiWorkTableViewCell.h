//
//  NoiWorkTableViewCell.h
//  iWork
//
//  Created by mac book pro on 17/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoiWorkTableViewCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UILabel *NoIworkRequestLabel;
@property(nonatomic,strong)IBOutlet UILabel *HorizentalLineLabel;
@property(nonatomic,strong)IBOutlet UIView *RedView;
@end
