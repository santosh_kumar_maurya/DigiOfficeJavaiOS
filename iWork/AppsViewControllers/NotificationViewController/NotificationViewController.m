//
//  NotificationViewController.m
//  iWork
//
//  Created by Fourbrick on 05/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import "NotificationViewController.h"
#import "Header.h"

@interface NotificationViewController (){
    IBOutlet UITableView *NotificationtableView;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UIImageView *NoDataImageView;
    AppDelegate *delegate;
    APIService *Api;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    NSMutableArray *NotificationArray;
    NSString *Message;
    UIRefreshControl *refreshControl;
    int offset;
    int limit;
}
@end

@implementation NotificationViewController

- (void)viewDidLoad{
    
    [super viewDidLoad];
    Api  =[[APIService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NotificationtableView.estimatedRowHeight = 500;
    NotificationtableView.rowHeight = UITableViewAutomaticDimension;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [NotificationtableView addSubview:refreshControl];
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetNotificationWebApi) withObject:nil afterDelay:1.0];
}
-(void)EmptyArray{
    offset = 0;
    limit = 10;
    NotificationArray = [[NSMutableArray alloc] init];
}
- (void)reloadData{
    [self EmptyArray];
    [self performSelector:@selector(GetNotificationWebApi) withObject:nil afterDelay:1.0];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
     return [NotificationArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NotificationCell * projectCell = (NotificationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    if(NotificationArray.count>0)
    {
        NSDictionary * responseData = NotificationArray[indexPath.row];
        if(IsSafeStringPlus(TrToString(responseData[@"message"]))){
            Message =  [responseData valueForKey:@"message"];
            //Message = [Message stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
            projectCell.NameLabel.text = Message;
        }
        else{
            projectCell.NameLabel.text = @" ";
        }
        if(indexPath.row == 0){
            projectCell.UpperLineLabel.hidden = YES;
        }
        else{
            projectCell.UpperLineLabel.hidden = NO;
        }
        
        if([Message containsString:@"approved"])
        {
            projectCell.CercilLabel.backgroundColor = [UIColor colorWithRed:64.0/255.0 green:179.0/255.0 blue:93.0/255.0 alpha:1];
            projectCell.CercilLabel.layer.borderWidth = 0.0;
            projectCell.CercilLabel.clipsToBounds = true ;
        }
        else if([Message containsString:@"rejected"])
        {
            projectCell.CercilLabel.backgroundColor = delegate.redColor;
            projectCell.CercilLabel.layer.borderWidth = 0.0;
            projectCell.CercilLabel.clipsToBounds = true ;
        }
        else
        {
            projectCell.CercilLabel.layer.borderColor = [UIColor lightGrayColor].CGColor;
            projectCell.CercilLabel.backgroundColor = [UIColor clearColor];
            projectCell.CercilLabel.layer.borderWidth = 1;
            projectCell.CercilLabel.clipsToBounds = true ;
            
        }
        [projectCell configureCell:NotificationArray[indexPath.row]];
    }
    return projectCell;
}
- (void)GetNotificationWebApi {
    
    if(delegate.isInternetConnected){
        params = @ {
            @"userId": [ApplicationState userId],
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
       ResponseDic = [Api WebApi:params Url:@"getNotificationList"];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self GetNotificationWebApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
            if(ResponseArrays.count>0){
                [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                NoDataLabel.hidden = YES;
                NoDataImageView.hidden = YES;
            }
            else if(NotificationArray.count == 0){
                [self NoIworkNotification];
            }
            [NotificationtableView reloadData];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
             [self NoIworkNotification];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoIworkNotification{
     [LoadingManager hideLoadingView:self.view];
    if([NotificationArray count]==0){
        NoDataImageView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = NSLocalizedString(@"NO_IWORK_NOTIFICATION_FOUND", nil);
       // NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
    if(ArraysValue.count>0){
        offset = offset + 10;
        [self GetNotificationWebApi];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [NotificationArray addObject:BindDataDic];
    }
}
- (IBAction)backDidClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)HomeAction:(id)sender {
    NSArray *array = [self.navigationController viewControllers];
    for (int i = 0 ; i < array.count; i++) {
        UIViewController *ViewController = [array objectAtIndex:i];
        if([ViewController isKindOfClass:[ContainerParentViewController class]]){
            [self.navigationController popToViewController:ViewController animated:YES];
        }
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
@end
