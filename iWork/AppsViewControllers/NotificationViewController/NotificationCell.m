//
//  NotificationCell.m
//  iWork
//
//  Created by Shailendra on 19/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "NotificationCell.h"
#import "Shared.h"

@implementation NotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
  
    self.CercilLabel.layer.cornerRadius = 7.5;
    self.CercilLabel.clipsToBounds = YES;
    
    
}
- (void)configureCell:(NSDictionary *)info{

    if(IsSafeStringPlus(TrToString(info[@"date_time"]))){
        NSNumber *datenumber = info[@"date_time"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _DateLabel.text  = @" ";
    }
    
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY | hh:mm aa"];
    return [dateformate stringFromDate:Newdate];
}

@end
