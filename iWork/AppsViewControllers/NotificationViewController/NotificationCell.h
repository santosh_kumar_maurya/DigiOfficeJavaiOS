//
//  NotificationCell.h
//  iWork
//
//  Created by Shailendra on 19/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
{
}
@property (strong, nonatomic) IBOutlet UILabel *NameLabel;
@property (strong, nonatomic) IBOutlet UILabel *DateLabel;
@property (strong, nonatomic) IBOutlet UILabel *UpperLineLabel;
@property (strong, nonatomic) IBOutlet UILabel *CercilLabel;
- (void)configureCell:(NSDictionary *)info;
@end
