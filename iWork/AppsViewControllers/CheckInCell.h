//
//  CheckInCell.h
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckInCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *CellBgView;
@property (strong, nonatomic) IBOutlet UIView *DateView;
@property (strong, nonatomic) IBOutlet UIView *RedView;
@property (strong, nonatomic) IBOutlet UILabel *DateLabel;
@property (strong, nonatomic) IBOutlet UILabel *TimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *AM_PM_Label;
@property (strong, nonatomic) IBOutlet UILabel *CongratsLabel;
@property (strong, nonatomic) IBOutlet UILabel *NoIWorkLabel;
@property (strong, nonatomic) IBOutlet UIButton *CheckInButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *checkInButtonHeightConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *checkInButtonBottonConstraints;

- (void)configureCell:(UIColor *)color backGround: (UIColor *)backGroundColor;

@end
