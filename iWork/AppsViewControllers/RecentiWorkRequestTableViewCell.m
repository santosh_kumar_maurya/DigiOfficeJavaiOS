//
//  RecentiWorkRequestTableViewCell.m
//  iWork
//
//  Created by mac book pro on 06/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "RecentiWorkRequestTableViewCell.h"
#import "Header.h"

@implementation RecentiWorkRequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self updateUI];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateUI
{
    self.btnStatus.layer.cornerRadius = 13.0;
    self.btnStatus.clipsToBounds = YES;
}

-(void)configureWith:(NSDictionary *)responseDict indexPath:(NSInteger)index
{
    if (index == 0)
    {
        self.upperLine.hidden = YES;
    }
    else
    {
        self.upperLine.hidden = NO;
    }
    
    if (IsSafeStringPlus(TrToString(responseDict[@"requestId"]))){
        NSNumber *requestIdNo = responseDict[@"requestId"];
        self.lblRequestID.text = [NSString stringWithFormat:@"%@",requestIdNo];
    }
    else
    {
        self.lblRequestID.text = @"-";
    }
    
    if (IsSafeStringPlus(TrToString(responseDict[@"location"]))){
        self.lblLocation.text = responseDict[@"location"];
    }
    else
    {
        self.lblLocation.text = @"-";
    }
    
    if (IsSafeStringPlus(TrToString(responseDict[@"actionDate"])))
    {
        NSNumber *Timenumber = responseDict[@"actionDate"];
        self.lblActionDate.text = [self GetDate:Timenumber];
    }
    else
    {
        self.lblActionDate.text = @"__:__";
    }
    
    if (IsSafeStringPlus(TrToString(responseDict[@"requestDate"])))
    {
        NSNumber *Timenumber = responseDict[@"requestDate"];
        self.lblRequestDate.text = [self GetDate:Timenumber];
    }
    else
    {
        self.lblRequestDate.text = @"__:__";
    }
    
    if (IsSafeStringPlus(TrToString(responseDict[@"recentStatus"])))
    {
        [_btnStatus setTitle:responseDict[@"recentStatus"] forState:UIControlStateNormal];
        if ([responseDict[@"recentStatus"] isEqualToString:@"Upcoming"])
        {
            self.circleView.layer.cornerRadius = self.circleView.frame.size.height/2;
            self.circleView.backgroundColor = [UIColor colorWithRed:(76.0/255.0) green:(194.0/255.0) blue:(255.0/255.0) alpha:1.0f];
            self.btnStatus.backgroundColor = [UIColor colorWithRed:(76.0/255.0) green:(194.0/255.0) blue:(255.0/255.0) alpha:1.0f];
            self.circleView.clipsToBounds = YES;
        }
        else
        {
            self.circleView.layer.cornerRadius = self.circleView.frame.size.height/2;
            self.circleView.backgroundColor = [UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f];
            self.btnStatus.backgroundColor = [UIColor colorWithRed:(76.0/255.0) green:(176.0/255.0) blue:(80.0/255.0) alpha:1.0f];
            self.circleView.clipsToBounds = YES;
        }
    }
    else{
        self.btnStatus.hidden = YES;
    }
    
}

-(NSString*)GetDate:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
@end
