//
//  LineManagerAllViewController.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LineManagerAllViewController.h"
#import "AllCell.h"
#import "RejectedCell.h"
#import "Header.h"

#define MAX_LENGTH 200
@interface LineManagerAllViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *ALLTableView;
    NSMutableArray *AllArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    NSString *cortex;
    NSString *user_requests_id;
    IBOutlet UILabel *NoDataLabel;
    
    IBOutlet UIView *RejectionOuterView;
    IBOutlet UIView *RejectionInnerView;
    IBOutlet UITextField *RejectionTextView;
    IBOutlet UILabel *RejectionTextViewLine;
    IBOutlet UIButton *RejectionYesBtn;
    IBOutlet UIButton *RejectionNoBtn;
    IBOutlet UIImageView *RequireImageView;
    
    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UIButton *YesBtn;
    IBOutlet UIButton *NoBtn;
    APIService *Api;
    NSDictionary *ResponseDic;
    NSString *requestsId;
    int offset;
    int limit;
    int TagValue;
    IBOutlet UIImageView *NoDataImageView;
}


@end

@implementation LineManagerAllViewController

- (void)viewDidLoad {
    CheckValue = @"ALL";
    offset = 0;
    limit = 10;
    TagValue = 0;
    Api = [[APIService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor whiteColor];
    ALLTableView.backgroundColor = [UIColor whiteColor];
    AllArray = [[NSMutableArray alloc] init];
    ALLTableView.estimatedRowHeight = 500;
    ALLTableView.rowHeight = UITableViewAutomaticDimension;
    ALLTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, ALLTableView.bounds.size.width, 0.0f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [ALLTableView addSubview:refreshControl];
    
    UINib *approvalNib = [UINib nibWithNibName:@"ApprovalsTableViewCell" bundle:nil];
    UINib *reportNib = [UINib nibWithNibName:@"ReportsTableViewCell" bundle:nil];
    [ALLTableView registerNib:approvalNib forCellReuseIdentifier:@"ApprovalsTableViewCell"];
    [ALLTableView registerNib:reportNib forCellReuseIdentifier:@"ReportsTableViewCell"];
    
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkLineManagerAll) withObject:nil afterDelay:0.5];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LineManageFilterApi:) name:@"LineManagerAll" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClearFilterApi:) name:@"LMClearFilterApi" object:nil];
    [self MessageContentView];
    [self RejectionContentView];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)reloadData{
    if ([CheckValue isEqualToString:@"FILTER"])
    {
        CheckValue = @"FILTER";
    }
    else{
        CheckValue = @"Refresh";
    }
    [self EmptyArray];
    [self GetiWorkLineManagerAll];
}
-(void)MessageContentView{
    MsgInnerView.layer.cornerRadius = 5;
    [YesBtn setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
    [YesBtn setBackgroundColor:delegate.redColor];
    [NoBtn setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
//    [NoBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: YesBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
    YesBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: NoBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    NoBtn.layer.mask = maskLayer2;
    
    
    
}
-(void)RejectionContentView{
    RejectionInnerView.layer.cornerRadius = 5;
    RejectionInnerView.clipsToBounds =YES;
    [RejectionYesBtn setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
    [RejectionYesBtn setBackgroundColor:delegate.redColor];
    [RejectionNoBtn setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
//    [RejectionNoBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: RejectionYesBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
    RejectionYesBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: RejectionNoBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    RejectionNoBtn.layer.mask = maskLayer2;
//    RejectionTextView.textColor = delegate.borderColor;
//    RejectionTextViewLine.backgroundColor = delegate.borderColor;
//    RejectionTextView.delegate = self;
//    RejectionTextView.layer.cornerRadius = 5.0;
//    RejectionTextView.layer.borderWidth = 1.0;
//    RejectionTextView.layer.borderColor = delegate.borderColor.CGColor;
//    RejectionTextView.textColor = delegate.borderColor;
}

-(void)EmptyArray{
    offset = 0;
    limit = 10;
    AllArray = [[NSMutableArray alloc]init];
}
#pragma mark  -UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [AllArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *Cell;
    if(AllArray.count>0){
       NSDictionary *Dic = AllArray[indexPath.section];
        if ([Dic[@"status"]  isEqualToString:@"PENDING"]){
            ApprovalsTableViewCell * projectCell = (ApprovalsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ApprovalsTableViewCell" forIndexPath:indexPath];
            projectCell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
            projectCell.tag = indexPath.section;
            [projectCell  setUserInteractionEnabled:YES];
            [projectCell addGestureRecognizer:Gesture];
            
            NSDictionary *responseData = AllArray [indexPath.section];
            [projectCell configureApprovalCell:responseData];
            projectCell.approveButton.tag = indexPath.section;
            projectCell.rejectButton.tag = indexPath.section;
            [projectCell.approveButton addTarget:self action:@selector(approveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [projectCell.rejectButton addTarget:self action:@selector(rejectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            if(indexPath.section == 0){
                projectCell.UpperLineLabel.hidden = YES;
            }
            else{
                projectCell.UpperLineLabel.hidden = NO;
            }
            return projectCell;
        }
        else if ([Dic[@"status"]  isEqualToString:@"REJECTED"]){

            RejectedCell *Cell = (RejectedCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
            Cell.tag = indexPath.section;
            [Cell  setUserInteractionEnabled:YES];
            
            [Cell addGestureRecognizer:Gesture];
            if(AllArray.count>0){
                NSDictionary * responseData = [AllArray objectAtIndex:indexPath.section];
                [Cell configureCell:responseData];
            }
            if(indexPath.section == 0){
                Cell.UpperLineLabel.hidden = YES;
            }
            else{
                Cell.UpperLineLabel.hidden = NO;
            }
            return Cell;
        }
        else if([Dic[@"status"] isEqualToString:@"MISSED"] || [Dic[@"status"] isEqualToString:@"DISCARDED"] || [Dic[@"status"] isEqualToString:@"DECLINED"]){
            
            MissedCell *Cell = (MissedCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
            Cell.tag = indexPath.section;
            [Cell  setUserInteractionEnabled:YES];
            
            [Cell addGestureRecognizer:Gesture];
            if(AllArray.count>0){
                NSDictionary * responseData = [AllArray objectAtIndex:indexPath.section];
                [Cell configureCell:responseData];
            }
            if([Dic[@"status"] isEqualToString:@"MISSED"] ){
                if ([Cell.ActionDateLabel.text isEqualToString:@" "]){
                    Cell.ActionDateStaticLabel.text = @" ";
                }
            }
            if(indexPath.section == 0){
                Cell.UpperLineLabel.hidden = YES;
            }
            else{
                Cell.UpperLineLabel.hidden = NO;
            }
            return Cell;
        }
        else{
            ReportsTableViewCell * projectCell = (ReportsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ReportsTableViewCell" forIndexPath:indexPath];
            projectCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [projectCell.ViewDetailsBtn addTarget:self action:@selector(GotoMapView:) forControlEvents:UIControlEventTouchUpInside];
            projectCell.ViewDetailsBtn.tag = indexPath.section;
            
            projectCell.tag  = indexPath.section;
            UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
            [projectCell  setUserInteractionEnabled:YES];
            [projectCell addGestureRecognizer:RequestIWork];
            if(AllArray.count>0){
                [projectCell configureReportCell:AllArray[indexPath.section]];
            }
            if(indexPath.section == 0){
                projectCell.UpperLineLabel.hidden = YES;
            }
            else{
                projectCell.UpperLineLabel.hidden = NO;
            }
            return projectCell;
        }
    }
    return Cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
    if(ArraysValue.count>0){
        offset = offset + 10;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self GetiWorkLineManagerAll];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [AllArray addObject:BindDataDic];
    }
}
-(void)MapPage{
    NSDictionary *Dic = [AllArray objectAtIndex:TagValue];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
    ObjViewController.MapDic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GotoMapView:(UIButton*)Sender{
    TagValue = Sender.tag;
    [self MapPage];
}
-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    TagValue = sender.view.tag;
    [self MapPage];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
   
    if (textField.text.length >= MAX_LENGTH && range.length == 0){
        [self ShowAlert:@"Character Limit Exceed" ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        return NO; // return NO to not change text
    }
    else{
        return YES;
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    RejectionTextView.text = @"";
    RequireImageView.hidden =YES;
    return YES;
}

#pragma mark -
- (void)approveButtonClicked:(UIButton *)sender {
    MsgLabel.text = NSLocalizedString(@"APPROVE_MSG", nil);
    TagValue = sender.tag;
    CheckValue = @"APPROVE";
    MsgOuterView.hidden = NO;
}
- (void)rejectButtonClicked:(UIButton *)sender {
    TagValue = sender.tag;
    CheckValue = @"REJECT";
    RejectionOuterView.hidden =NO;
}
- (void)GetiWorkLineManagerAll{
    if(delegate.isInternetConnected){
         if([CheckValue isEqualToString:@"ALL"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"status": @10,
                    @"userId": [ApplicationState userId],
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"viewTeamReport"];
            [refreshControl endRefreshing];
            [LoadingManager hideLoadingView:self.view];
             if(ResponseDic == nil){
                 [LoadingManager hideLoadingView:self.view];
                 [self GetiWorkLineManagerAll];
             }
             else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                NSLog(@"AllAllAllAllAllAllAll");
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
                if(ResponseArrays.count>0){
                     NoDataLabel.hidden = YES;
                     NoDataImageView.hidden = YES;
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"]];
                    [ALLTableView  reloadData];
                }
                else if(AllArray.count == 0){
                   [self NoIworkRequest];
                }
            }
            else{
                if(AllArray.count == 0){
                    [self NoIworkRequest];
                }
            }
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoIworkRequest{
    NoDataImageView.hidden = NO;
    NoDataLabel.hidden = NO;
    NoDataLabel.text = NSLocalizedString(@"NO_IWORK_REQUEST_FOUND", nil);
    //NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
     [ALLTableView  reloadData];
}
-(IBAction)YESBtnAction:(id)sender{
    NSDictionary * responseData = AllArray [TagValue];
    requestsId = [NSString stringWithFormat:@"%@",[responseData objectForKey:@"requestId"]];
    NSLog(@"requestsId==%@",requestsId);
    [self ApproveRejectWebApi];
}
-(void)ApproveRejectWebApi{
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"REJECT"]){
            NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
            NSString *RemarkStr = [RejectionTextView.text stringByAddingPercentEncodingWithAllowedCharacters:set];
            NSString *UrlStr = [NSString stringWithFormat:@"updateStatusOfRequest?status=2&requestId=%@&rejectionRemark=%@",requestsId,RemarkStr];
            NSLog(@"UrlStr==%@",UrlStr);
            ResponseDic = [Api WebApi:nil Url:UrlStr];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self ApproveRejectWebApi];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                MsgOuterView.hidden =YES;
                RejectionOuterView.hidden =YES;
                [self EmptyArray];
                [self GetiWorkLineManagerAll];
                
            }
            else{
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
        else if([CheckValue isEqualToString:@"APPROVE"]){
            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"updateStatusOfRequest?status=1&requestId=%@",requestsId]];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self ApproveRejectWebApi];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                MsgOuterView.hidden =YES;
                [self EmptyArray];
                [self GetiWorkLineManagerAll];
            }
            else{
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
}
-(IBAction)NoBtnAction:(id)sender{
    MsgOuterView.hidden =YES;
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)RejectionYesAction:(id)sender{
    [RejectionTextView resignFirstResponder];
    NSDictionary * responseData = AllArray [TagValue];
    requestsId = [NSString stringWithFormat:@"%@",[responseData objectForKey:@"requestId"]];
    if([RejectionTextView.text isEqualToString:@""]){
        RequireImageView.hidden =NO;
    }
    else if([RejectionTextView.text isEqualToString:@""]){
        RequireImageView.hidden =NO;
    }
    else{
        [self ApproveRejectWebApi];
    }
}
-(IBAction)RejectionNoCrossAction:(id)sender{
    RejectionTextView.text = @"";
    [RejectionTextView resignFirstResponder];
    CheckValue = @"";
    RejectionOuterView.hidden = YES;
    RequireImageView.hidden =YES;
}
-(void)LineManageFilterApi:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [self GetiWorkLineManagerAll];
}
-(void)ClearFilterApi:(NSNotification*)notification
{
    [self EmptyArray];
    CheckValue = @"ALL";
    [LoadingManager showLoadingView:self.view];
    [self GetiWorkLineManagerAll];
}
- (IBAction)FilterAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LineManagerFilterViewController *ObjLineManagerFilterViewController = [storyboard instantiateViewControllerWithIdentifier:@"LineManagerFilterViewController"];
    [self presentViewController:ObjLineManagerFilterViewController animated:YES completion:nil];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
