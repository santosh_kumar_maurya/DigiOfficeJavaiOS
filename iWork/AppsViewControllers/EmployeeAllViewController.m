//
//  EmployeeAllViewController.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeAllViewController.h"
#import "AllCell.h"
#import "RejectedCell.h"
#import "NewCell.h"
#import "Header.h"

@interface EmployeeAllViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *ALLTableView;
    NSMutableArray *AllArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    NSString *cortex;
    NSString *user_requests_id;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UIButton *YesBtn;
    IBOutlet UIButton *NoBtn;
    int TagValue;
    UIButton *btn;
    UIWindow *Window;
    NSDictionary *ResponseDic;
    APIService *Api;
    int offset;
    int limit;
    IBOutlet UIImageView *NoDataImageView;
}

@end

@implementation EmployeeAllViewController

- (void)viewDidLoad {
    offset = 0;
    limit = 10;
    CheckValue = @"ALL";
    Api = [[APIService alloc] init];
    AllArray = [[NSMutableArray alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor clearColor];
    ALLTableView.backgroundColor = [UIColor clearColor];

    ALLTableView.estimatedRowHeight = 500;
    ALLTableView.rowHeight = UITableViewAutomaticDimension;
    ALLTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, ALLTableView.bounds.size.width, 0.0f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [ALLTableView addSubview:refreshControl];
    [self MessageContentView];
    
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkUserAll) withObject:nil afterDelay:0.5];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AllApi:) name:@"All" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClearFilterApi:) name:@"EmployeeClearFilterApi" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)EmptyArray{
    offset = 0;
    limit = 10;
    AllArray = [[NSMutableArray alloc] init];
    [ALLTableView reloadData];
}
-(void)MessageContentView{
    MsgInnerView.layer.cornerRadius = 5;
    [YesBtn setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
    [YesBtn setBackgroundColor:delegate.redColor];
    [NoBtn setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
    [NoBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: YesBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
    YesBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: NoBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    NoBtn.layer.mask = maskLayer2;
}
- (void)reloadData{
    if ([CheckValue isEqualToString:@"FILTER"])
    {
        CheckValue = @"FILTER";
    }
    else{
        CheckValue = @"Refresh";
    }
    AllArray = [[NSMutableArray alloc] init];
    offset = 0;
    limit = 10;
    [self GetiWorkUserAll];
}

- (void)GetiWorkUserAll {
   /// @"locationId": @0,
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"ALL"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"attendanceStatus": @"",
                    @"requestDate": @"",
                    @"requestId": @0,
                    @"status": @10,
                    @"userId": [ApplicationState userId],
                    @"workDate": @"",
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]

                };
            }
            NSLog(@"%@",params);
            ResponseDic = [Api WebApi:params Url:@"myIworkRequest"];
            [refreshControl endRefreshing];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self GetiWorkUserAll];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
               [LoadingManager hideLoadingView:self.view];
               NoDataLabel.hidden = YES;
               NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                  [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    NoDataLabel.hidden = YES;
                    NoDataImageView.hidden = YES;
                }
                else if(ResponseArrays.count == 0 && AllArray.count == 0){
                     [self NoIworkRequest];
                }
                [ALLTableView reloadData];
            }
            else{
                if(AllArray.count == 0){
                    [self NoIworkRequest];
                }
            }
        }
        else if([CheckValue isEqualToString:@"CANCEL"]){
            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"updateStatusOfRequest?status=3&requestId=%@",user_requests_id]];
            if(ResponseDic == nil){
                [LoadingManager hideLoadingView:self.view];
                [self GetiWorkUserAll];
            }
            else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [self EmptyArray];
                CheckValue = @"ALL";
                MsgOuterView.hidden = YES;
                [self GetiWorkUserAll];
            }
            else{
                 [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
        else if ([CheckValue isEqualToString:@"CHECKIN"]||[CheckValue isEqualToString:@"CHECKOUT"]){
            if([delegate.userLat doubleValue] ==0.0 || [delegate.userLong doubleValue] == 0.0){
                [AlertManager showPopupMessageAlert:NSLocalizedString(@"TURN_ON_LOCATION", nil) withTitle:nil];
            }
           // else{
                delegate.userLat = @"";
                delegate.userLong = @"";
            //}
            NSString *Lat = [NSString stringWithFormat:@"%@",delegate.userLat];
            NSString *Logn = [NSString stringWithFormat:@"%@",delegate.userLong];
            if([Lat isEqualToString:@"(null)"]){
                Lat = @"";
                Logn = @"";
            }
//            else{
                if([CheckValue isEqualToString:@"CHECKIN"]){
                    params = @ {
                        @"mapping" : @"apis",
                        @"userId" : [ApplicationState userId],
                        @"requestId" : user_requests_id,
                        @"latitude" : Lat,
                        @"longitude" : Logn
                    };
                    ResponseDic = [Api WebApi:params Url:@"checkIn"];
                    NSLog(@"ResponseDic==%@",ResponseDic);
                    
                    if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                        MsgOuterView.hidden = YES;
                        CheckValue = @"ALL";
                        [self EmptyArray];
                        [self GetiWorkUserAll];
                    }
                    else{
                         [LoadingManager hideLoadingView:self.view];
                        [self ShowAlert:NSLocalizedString(ResponseDic[@"message"], nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                    }
                }
                else if([CheckValue isEqualToString:@"CHECKOUT"]){
                    params = @ {
                        @"mapping" : @"apis",
                        @"userId" : [ApplicationState userId],
                        @"requestId" : user_requests_id,
                        @"latitude" : Lat,
                        @"longitude" : Logn
                    };
                    ResponseDic = [Api WebApi:params Url:@"checkOut"];
                    NSLog(@"ResponseDic==%@",ResponseDic);
                    if(ResponseDic == nil){
                        [LoadingManager hideLoadingView:self.view];
                        [self GetiWorkUserAll];
                    }
                    else if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                        MsgOuterView.hidden = YES;
                        [self EmptyArray];
                        CheckValue = @"ALL";
                        [self GetiWorkUserAll];
                    }
                    else{
                         [LoadingManager hideLoadingView:self.view];
                        [self ShowAlert:NSLocalizedString(ResponseDic[@"message"], nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                    }
                }
            //}
        }

    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }

}
-(void)NoIworkRequest{
    [LoadingManager hideLoadingView:self.view];
    NoDataLabel.hidden = NO;
    NoDataImageView.hidden = NO;
    NoDataLabel.text = NSLocalizedString(@"NO_IWORK_REQUEST_FOUND", nil);
    
    //NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
}
- (void)checkInCheckOutClicked:(UIButton *)sender {
    TagValue = sender.tag;
    btn = sender;
    MsgOuterView.hidden =NO;
    CheckValue = @"CheckInCheckOut";
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CHECK_IN_BUTTON", nil)]){
       MsgLabel.text = NSLocalizedString(@"CHECKIN_MSG", nil);
    }
    else{
       MsgLabel.text = NSLocalizedString(@"CHECKOUT_MSG", nil);
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
    if(ArraysValue.count>0){
        offset = offset + 10;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self GetiWorkUserAll];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        NSLog(@"Dic===%@",Dic);
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [AllArray addObject:BindDataDic];
    }
    NSLog(@"AllArray==%@",AllArray);
}
- (void)CancelAction:(UIButton *)sender{
    CheckValue =@"CANCEL";
    MsgOuterView.hidden =NO;
    TagValue = sender.tag;
    MsgLabel.text = NSLocalizedString(@"CACNEL_MSG", nil);
}
-(IBAction)YESBtnAction:(id)sender{
   
    if([CheckValue isEqualToString:@"CANCEL"]){
        NSDictionary * responseData = AllArray[TagValue];
        if (responseData[@"requestId"] !=nil) {
            user_requests_id = responseData[@"requestId"];
        }
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(GetiWorkUserAll) withObject:nil afterDelay:0.4];
    }
    else if([CheckValue isEqualToString:@"CheckInCheckOut"]){
        if ([btn.titleLabel.text isEqualToString:NSLocalizedString(@"CHECK_IN_BUTTON", nil)]) {
            CheckValue = @"CHECKIN";
        }
        else{
            CheckValue = @"CHECKOUT";
        }
        user_requests_id = @" ";
        NSDictionary * responseData = AllArray[TagValue];
        if (responseData[@"requestId"] !=nil) {
            user_requests_id = responseData[@"requestId"];
        }
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(GetiWorkUserAll) withObject:nil afterDelay:0.4];
    }
   
}
-(IBAction)NoBtnAction:(id)sender{
    MsgOuterView.hidden =YES;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [AllArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *Cell;

    if(AllArray.count>0){
        NSDictionary * responseData = [AllArray objectAtIndex:indexPath.section];
        if([responseData[@"status"] isEqualToString:@"REJECTED"]){
            RejectedCell *Cell = (RejectedCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
            Cell.tag = indexPath.section;
            [Cell  setUserInteractionEnabled:YES];
            [Cell addGestureRecognizer:Gesture];
            [Cell configureCell:responseData];
            if(indexPath.section == 0){
                Cell.UpperLineLabel.hidden = YES;
            }
            else{
                Cell.UpperLineLabel.hidden = NO;
            }
            return Cell;
        }
        else if([responseData[@"status"] isEqualToString:@"MISSED"] || [responseData[@"status"] isEqualToString:@"DISCARDED"] || [responseData[@"status"] isEqualToString:@"DECLINED"]){
            
            MissedCell *Cell = (MissedCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
            UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
            Cell.tag = indexPath.section;
            [Cell  setUserInteractionEnabled:YES];
            [Cell addGestureRecognizer:Gesture];
            if(AllArray.count>0){
                NSDictionary * responseData = [AllArray objectAtIndex:indexPath.section];
                [Cell configureCell:responseData];
                if([responseData[@"status"] isEqualToString:@"MISSED"] ){
                    if ([Cell.ActionDateLabel.text isEqualToString:@" "]){
                        Cell.ActionDateStaticLabel.text = @" ";
                    }
                }
            }
            if(indexPath.section == 0){
                Cell.UpperLineLabel.hidden = YES;
            }
            else{
                Cell.UpperLineLabel.hidden = NO;
            }
            return Cell;
        }
        else if([responseData[@"status"] isEqualToString:@"PENDING"]){
            
            NewCell *Cell = (NewCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
            UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
            Cell.tag = indexPath.section;
            [Cell  setUserInteractionEnabled:YES];
            [Cell addGestureRecognizer:Gesture];
            Cell.CancelBtn.tag = indexPath.section;
            [Cell.CancelBtn addTarget:self action:@selector(CancelAction:) forControlEvents:UIControlEventTouchUpInside];
            if(AllArray.count>0){
                NSDictionary * responseData = [AllArray objectAtIndex:indexPath.section];
                [Cell configureCell:responseData];
                if([responseData[@"status"] isEqualToString:@"PENDING"]){
                    if ([Cell.ActionDateLabel.text isEqualToString:@" "]){
                        Cell.ActionDateStaticLabel.text = @" ";
                    }
                }
            }
            if(indexPath.section == 0){
                Cell.UpperLineLabel.hidden = YES;
            }
            else{
                Cell.UpperLineLabel.hidden = NO;
            }
            return Cell;
        }
        else{
            AllCell *Cell = (AllCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            Cell.CheckInButton.tag = indexPath.section;
            Cell.CancelButton.tag = indexPath.section;
            [Cell.CheckInButton addTarget:self action:@selector(checkInCheckOutClicked:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.CancelButton addTarget:self action:@selector(CancelAction:) forControlEvents:UIControlEventTouchUpInside];
            UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
            Cell.tag = indexPath.section;
            [Cell  setUserInteractionEnabled:YES];
            [Cell addGestureRecognizer:Gesture];
            if(AllArray.count>0){
                NSDictionary * responseData = [AllArray objectAtIndex:indexPath.section];
                [Cell configureCell:responseData];
            }
            if(indexPath.section == 0){
                Cell.UpperLineLabel.hidden = YES;
            }
            else{
                Cell.UpperLineLabel.hidden = NO;
            }
            return Cell;
        }
    }
    return Cell;
}

-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    if(delegate.isInternetConnected){
        TagValue = sender.view.tag;
        NSDictionary *Dics = [AllArray objectAtIndex:TagValue];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
        ObjViewController.MapDic = Dics;
        [[self navigationController] pushViewController:ObjViewController animated:YES];
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)AllApi:(NSNotification*)notification
{
    [self EmptyArray];
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkUserAll) withObject:nil afterDelay:0.4];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
-(void)ClearFilterApi:(NSNotification*)notification
{
    [self EmptyArray];
    CheckValue = @"ALL";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkUserAll) withObject:nil afterDelay:0.4];
}
@end
