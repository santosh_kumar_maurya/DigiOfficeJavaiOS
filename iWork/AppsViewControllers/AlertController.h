//
//  AlertController.h
//  iWork
//
//  Created by Shailendra on 27/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertController : UIViewController{
}
@property (strong, nonatomic)NSString *MessageTitleStr;
@property (strong, nonatomic)NSString *MessageBtnStr;
@property (strong, nonatomic)NSString *isCome;
@property (strong, nonatomic)IBOutlet UIView *YellowView;
@property (strong, nonatomic)IBOutlet UILabel *GraylineLabel;
@property (strong, nonatomic)IBOutlet UIButton *CancelBtn;
@property (strong, nonatomic)NSMutableAttributedString *AttributedString;
@end
