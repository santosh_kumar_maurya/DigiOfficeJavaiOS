//
//  MyWorkFilterViewController.h
//  iWork
//
//  Created by Shailendra on 20/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWorkFilterViewController : UIViewController

@property(nonatomic, strong)NSMutableDictionary *filterData;

@end
