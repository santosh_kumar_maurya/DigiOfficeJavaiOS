//
//  MapDetailTableViewCell.m
//  iWork
//
//  Created by mac book pro on 16/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MapDetailTableViewCell.h"
#import "Header.h"
#import "iWork-Swift.h"

@implementation MapDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCell:(NSDictionary *)info {
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    if(IsSafeStringPlus(TrToString(info[@"dateTime"]))) {
        NSNumber *datenumber = info[@"dateTime"];
        _RequestDateLabel.text = [self DateFormateChange:datenumber];
        
    } else {
        _RequestDateLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"task"]))) {
        _TaskLabel.text = info[@"task"];
    } else {
        _TaskLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"location"]))) {
        _iWorkLocationLabel.text = info[@"location"];
    } else {
        _iWorkLocationLabel.text = @" ";
    }
//    if(IsSafeStringPlus(TrToString(info[@"requestDate"]))) {
//        NSNumber *datenumber = info[@"requestDate"];
//        _RequestDateLabel.text = [self DateFormateChange:datenumber];
//    } else {
//        _RequestDateLabel.text = @" ";
//    }
    if(IsSafeStringPlus(TrToString(info[@"actionDate"]))) {
        NSNumber *datenumber = info[@"actionDate"];
        _ActionDateLabel.text = [self DateFormateChange:datenumber];
    } else {
        _ActionDateLabel.text = @" ";
        _ActionDateStaticLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"checkInTime"]))) {
        NSNumber *Timenumber = info[@"checkInTime"];
        _CheckInLabel.text = [self GetDate:Timenumber];
    } else {
        _CheckInLabel.text = @"- -:- -";
    }
    if(IsSafeStringPlus(TrToString(info[@"checkOutTime"]))) {
        NSNumber *Timenumber = info[@"checkOutTime"];
        _CheckoutLabel.text = [self GetDate:Timenumber];
    } else {
        _CheckoutLabel.text = @"- -:- -";
    }
    _HoursLabel.text = @" ";
    if(IsSafeStringPlus(TrToString(info[@"hour"]))) {
        _HoursLabel.text = info[@"hour"];
    }
    else{
        _HoursLabel.text = @"- -:- -";
    }
    if(IsSafeStringPlus(TrToString(info[@"maxCheckInTime"]))) {
        _MaxCheckInLabel.text = [self MaxDate:info[@"maxCheckInTime"]];
    }else{
        _MaxCheckInLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"locType"]))) {
        _iWorkLocationTypeLabel.text = info[@"locType"];
    } else {
        _iWorkLocationTypeLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"attendanceStatus"]))) {
        _AttandenceStatusLabel.text = info[@"attendanceStatus"];
        if([_AttandenceStatusLabel.text isEqualToString:@"-"]){
            _AttandenceStatusLabel.text = @" ";
        }
    }else{
        _AttandenceStatusLabel.text = @" ";
        _AttandenceStatusStaticLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"rejectionRemark"]))) {
        _rejectionRemarkVerticalConstraints.constant = 10.0;
        _rejectionRemark.textColor = [UIColor darkGrayColor];
        _rejectionRemark.text = [NSString stringWithFormat:@"%@",info[@"rejectionRemark"]];
//        NSMutableAttributedString *text = [[NSMutableAttributedString alloc]
//                                           initWithAttributedString: _rejectionRemark.attributedText];
//        
//        [text addAttribute:NSForegroundColorAttributeName
//                     value:[UIColor blackColor]
//                     range:NSMakeRange(0, 18)];
//        [_rejectionRemark setAttributedText: text];
        
    } else {
        _rejectionRemarkVerticalConstraints.constant = 0.0;
        _rejectionRemark.text = @" ";
        _rejectionRemarkStaticLabel.text = @" ";
        _rejectionRemark.textColor = [UIColor blackColor];
    }
    
    if(IsSafeStringPlus(TrToString(info[@"status"]))) {
        
        if ([info[@"status"] isEqualToString:@"PENDING"]) {
            _RequestStatusValueLabel.textColor = [UIColor ViewRequestOrangeColor];
            _RequestStatusValueLabel.text = @"New";
            _checkInVerticalContraints.constant = -10.0;
            _rejectionRemarkVerticalConstraints.constant = -10.0;
            _actionDateVerticalContraints.constant = -10.0;
        } else if ([info[@"status"] isEqualToString:@"APPROVED"]) {
            
            _RequestStatusValueLabel.textColor = [UIColor ViewRequestGreenColor];
            _RequestStatusValueLabel.text = @"Approved";
        }
        else if ([info[@"status"] isEqualToString:@"REJECTED"]) {
            _RequestStatusValueLabel.textColor = [UIColor ViewRequestRedColor];
            _RequestStatusValueLabel.text = @"Rejected";
        }
        else if ([info[@"status"] isEqualToString:@"CANCELLED"] ) {
            _RequestStatusValueLabel.textColor = [UIColor ViewRequestRedColor];
            _RequestStatusValueLabel.text = @"Cancelled";
            _checkInVerticalContraints.constant = 0.0;
        }
        else if ([info[@"status"] isEqualToString:@"DISCARDED"]) {
            _RequestStatusValueLabel.textColor = [UIColor ViewRequestRedColor];
            _RequestStatusValueLabel.text = @"Discarded";
        }
        else if ([info[@"status"]isEqualToString:@"DECLINED"] ) {
            _RequestStatusValueLabel.textColor = [UIColor ViewRequestRedColor];
            _RequestStatusValueLabel.text = @"Declined";
        }
        else if ([info[@"status"]isEqualToString:@"MISSED"] ) {
            _RequestStatusValueLabel.textColor = [UIColor ViewRequestRedColor];
            _RequestStatusValueLabel.text = @"Missed";
        }
        
        
        if([info[@"status"] isEqualToString:@"MISSED"] || [info[@"status"] isEqualToString:@"PENDING"]){
            if ([_ActionDateLabel.text isEqualToString:@" "]){
                _ActionDateStaticLabel.text = @" ";
            }
        }
    }

    if(IsSafeStringPlus(TrToString(info[@"status"]))){
        if([info[@"status"] isEqualToString:@"APPROVED"]){
            if(IsSafeStringPlus(TrToString(info[@"attendanceStatus"])))
            {
                _AttandenceStatusStaticLabel.text = NSLocalizedString(@"ATTENDANCE_STATUS", nil);
                if([info[@"attendanceStatus"] isEqualToString:@"Present"])
                {
                    _checkInVerticalContraints.constant = 10;
                    _CheckInStaticLabel.text = NSLocalizedString(@"Check_In", nil);
                    _CheckoutStaticLabel.text = NSLocalizedString(@"Check_Out", nil);
                    _HoursStaticLabel.text = NSLocalizedString(@"Hours", nil);
                    _HoursLabel.text = info[@"hour"];
                    if(IsSafeStringPlus(TrToString(info[@"checkOutTime"]))) {
                        NSNumber *Timenumber = info[@"checkOutTime"];
                        _CheckoutLabel.text = [self GetDate:Timenumber];
                    } else {
                        _CheckoutLabel.text = @"- -:- -";
                    }
                    if(IsSafeStringPlus(TrToString(info[@"checkInTime"]))) {
                        NSNumber *Timenumber = info[@"checkInTime"];
                        _CheckInLabel.text = [self GetDate:Timenumber];
                    } else {
                        _CheckInLabel.text = @"- -:- -";
                    }
                    
                }
                else{
                    _CheckInStaticLabel.text = @"";
                    _CheckoutStaticLabel.text = @"";
                    _HoursStaticLabel.text = @"";
                    _CheckInLabel.text = @"";
                    _CheckoutLabel.text = @"";
                    _HoursLabel.text = @"";
                    _checkInVerticalContraints.constant = 0;
                }
            }
            else
            {
                _CheckInStaticLabel.text = @"";
                _CheckoutStaticLabel.text = @"";
                _HoursStaticLabel.text = @"";
                _CheckInLabel.text = @"";
                _CheckoutLabel.text = @"";
                _HoursLabel.text = @"";
                _AttandenceStatusLabel.text = @"";
                _AttandenceStatusLabel.text = @"";
                _checkInVerticalContraints.constant = 0;
            }
        }
        else{
            _CheckInStaticLabel.text = @"";
            _CheckoutStaticLabel.text = @"";
            _HoursStaticLabel.text = @"";
            _CheckInLabel.text = @"";
            _CheckoutLabel.text = @"";
            _HoursLabel.text = @"";
            _AttandenceStatusLabel.text = @"";
            _AttandenceStatusLabel.text = @"";
            _checkInVerticalContraints.constant = 0;
        }
        
    }
    

    
    
//    if(IsSafeStringPlus(TrToString(MapDic[@"dateTime"]))){
//        NSNumber *datenumber = MapDic[@"dateTime"];
//        RequestDateValueLabel.text = [self DateFormateChange:datenumber];
//    }
//    else{
//        RequestDateValueLabel.text = @" ";
//    }
//    
//    if(IsSafeStringPlus(TrToString(MapDic[@"task"]))){
//        TaskValueLabel.text = MapDic[@"task"];
//    }
//    else{
//        TaskValueLabel.text = @" ";
//    }
//    
//    if(IsSafeStringPlus(TrToString(MapDic[@"locType"]))){
//        LocationTypeValueLabel.text = MapDic[@"locType"];
//    }
//    else{
//        LocationTypeValueLabel.text = @" ";
//    }
//    
//    
//    if(IsSafeStringPlus(TrToString(MapDic[@"actionDate"]))) {
//        NSNumber *datenumber = MapDic[@"actionDate"];
//        ActiontDateValueLabel.text = [self DateFormateChange:datenumber];
//    } else {
//        ActiontDateValueLabel.text = @" ";
//    }
//    
//    if(IsSafeStringPlus(TrToString(MapDic[@"checkInTime"]))) {
//        NSNumber *Timenumber = MapDic[@"checkInTime"];
//        CheckinValueLabel.text = [self GetDate:Timenumber];
//    } else {
//        CheckinValueLabel.text = @"- -:- -";
//    }
//    
//    if(IsSafeStringPlus(TrToString(MapDic[@"checkOutTime"]))) {
//        NSNumber *Timenumber = MapDic[@"checkOutTime"];
//        CheckoutvalueLabel.text = [self GetDate:Timenumber];
//    } else {
//        CheckoutvalueLabel.text = @"- -:- -";
//    }
//    
//    if(IsSafeStringPlus(TrToString(MapDic[@"hour"]))){
//        HoursValueLabel.text = MapDic[@"hour"];
//    }
//    else {
//        HoursValueLabel.text = @" " ;
//    }
//    
//    if(IsSafeStringPlus(TrToString(MapDic[@"attendanceStatus"]))){
//        AttandenceValueLabel.text = MapDic[@"attendanceStatus"];
//        if([AttandenceValueLabel.text isEqualToString:@"-"]){
//            AttandenceValueLabel.text = @ "";
//        }
//    }
//    else{
//        AttandenceValueLabel.text = @" ";
//    }
//    if(IsSafeStringPlus(TrToString(MapDic[@"maxCheckInTime"]))){
//        MaxTimeValueLabel.text = [self MaxDate:MapDic[@"maxCheckInTime"]];;
//    }
//    else{
//        MaxTimeValueLabel.text = @" ";
//        
//    }
//    if(IsSafeStringPlus(TrToString(MapDic[@"status"]))) {
//        
//        if ([MapDic[@"status"] isEqualToString:@"PENDING"]) {
//            StatusValueLabel.textColor = [UIColor colorWithRed:1.0 green:0.83 blue:00.0 alpha:1.0];
//            StatusValueLabel.text = @"New";
//            StatusImageView.image = [UIImage imageNamed:@"NewStatus"];
//            
//        } else if ([MapDic[@"status"] isEqualToString:@"APPROVED"]) {
//            
//            StatusValueLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:88.0/255.0 blue:45.0/255.0 alpha:1.0];
//            StatusValueLabel.text = @"Approved";
//            StatusImageView.image = [UIImage imageNamed:@"Approved"];
//            
//        }
//        else if ([MapDic[@"status"] isEqualToString:@"REJECTED"]) {
//            StatusValueLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
//            StatusValueLabel.text = @"Rejected";
//            StatusImageView.image = [UIImage imageNamed:@"Rejected"];
//            
//        }
//        else if ([MapDic[@"status"] isEqualToString:@"CANCELLED"] ) {
//            StatusValueLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
//            StatusValueLabel.text = @"Cancelled";
//            StatusImageView.image = [UIImage imageNamed:@"CancelledStatus"];
//            
//        }
//        else if ([MapDic[@"status"] isEqualToString:@"DISCARDED"]) {
//            StatusValueLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
//            StatusValueLabel.text = @"Discarded";
//            StatusImageView.image = [UIImage imageNamed:@"DiscardedStatus"];
//            
//        }
//        else if ([MapDic[@"status"]isEqualToString:@"DECLINED"] ) {
//            StatusValueLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
//            StatusValueLabel.text = @"Declined";
//            StatusImageView.image = [UIImage imageNamed:@"DeclinedStatus"];
//        }
//        else if ([MapDic[@"status"]isEqualToString:@"MISSED"] ) {
//            StatusValueLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
//            StatusValueLabel.text = @"Missed";
//            StatusImageView.image = [UIImage imageNamed:@"Missed"];
//        }
//        if([MapDic[@"status"] isEqualToString:@"MISSED"] || [MapDic[@"status"] isEqualToString:@"PENDING"]){
//            if ([ActiontDateValueLabel.text isEqualToString:@" "]){
//                ActionDateStaticLabel.text = @" ";
//            }
//        }
//    }

    
}

-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)GetDate:(NSNumber*)Timenumber{
    NSString *DateStr = [NSString stringWithFormat:@"%@",Timenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"HH:mm:ss"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"hh:mm a"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)MaxDate:(NSString*)DateString{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString: DateString];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *convertedString = [dateFormatter stringFromDate:date];
    return convertedString;
}

@end
