//
//  NetworkInterface.h
//  MangoApp
//
//  Created by Fourbrick on 17/11/16.
//    Copyright © 2017 Fourbrick. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NetworkInterfaceManagerSucessBlock)(id responseObject);
typedef void (^NetworkInterfaceManagerErrorBlock)(NSError* err);


@interface NetworkInterface : NSObject

+ (instancetype)sharedNetworkManager;
+ (instancetype)networkManager;


//Check tghe status of network
+ (BOOL)isNetworkReachible ;

/**
 *  Method for fecthing the data from the server
 *
 *  urlString
 *  resultBlock - success block
 *  failure     - failure block
 */
- (void)fetchDataWithRequestTypeGET:(NSString *)urlString successBlock:(NetworkInterfaceManagerSucessBlock)resultBlock failure:(NetworkInterfaceManagerErrorBlock)failure;

- (void)fetchDataWithRequestTypePOST:(NSString *)urlString parameters:(NSDictionary*)parms successBlock:(NetworkInterfaceManagerSucessBlock)sucessBlock failure:(NetworkInterfaceManagerErrorBlock)failure;
- (void)postQuizData:(NSString *)urlString parameters:(NSArray *)parms successBlock:(NetworkInterfaceManagerSucessBlock)sucessBlock failure:(NetworkInterfaceManagerErrorBlock)failure;
@end
