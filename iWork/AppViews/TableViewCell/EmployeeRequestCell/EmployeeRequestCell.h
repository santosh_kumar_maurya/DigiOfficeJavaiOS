//
//  EmployeeRequestCell.h
//  iWork
//
//  Created by Fourbrick on 18/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmployeeRequestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *requestIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *requestDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *iWorkDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *iWorkLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *lineManagerLabel;
@property (weak, nonatomic) IBOutlet UILabel *StatusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *StatusImageView;
@property (weak, nonatomic) IBOutlet UILabel *actionDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *hoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *checkInLabel;
@property (weak, nonatomic) IBOutlet UILabel *checkoutLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskLabel;
@property (weak, nonatomic) IBOutlet UILabel *AttandenceStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *MaxCheckInLabel;
@property (weak, nonatomic) IBOutlet UIButton *CheckInButton;

- (void)configureCell:(NSDictionary *)info;
@end
