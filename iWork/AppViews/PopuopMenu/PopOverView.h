//
//  PopOverView.h
//  
//
//  Created by Fourbrick on 17/03/16.
//
//

#import <UIKit/UIKit.h>

@protocol PopOverViewDelegate <NSObject>
- (void)popUpOptionSelected:(NSInteger)option;
@end

@interface PopOverView : UIView
+ (PopOverView *)popOverView;
- (void)createOptionTableView:(NSArray *)menuItems selectedIndex:(NSInteger)selIndex;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic)  id<PopOverViewDelegate> delegate;

@end
