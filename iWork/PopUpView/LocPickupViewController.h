//
//  LocPickupViewController.h
//  iWork
//
//  Created by Fourbrick on 04/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol LocationDataDelegate <NSObject>
- (void)locationDataSelected:(id)locData;
- (void)SearchData:(NSString*)Str;
@end

@interface LocPickupViewController : UIViewController
@property(assign)id <LocationDataDelegate>delegate;
@property (strong, nonatomic) NSString *SelectDateStr;
@property (strong, nonatomic) NSString *isComeFrom;
@end
