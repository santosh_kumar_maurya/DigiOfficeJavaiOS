//
//  WebApiService.h
//  iWork
//
//  Created by Shailendra on 06/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"
@interface WebApiService : NSObject{
    AppDelegate *delegate;
}

-(NSDictionary*)WebApi: (NSDictionary*)_params Url:(NSString*)Url;


@end
