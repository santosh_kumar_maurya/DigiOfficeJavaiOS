//
//  SubTaskBtnCell.swift
//  iWork
//
//  Created by Shailendra on 15/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class SubTaskBtnCell: UITableViewCell {

    @IBOutlet var SubtaskButton : UIButton!
    override func awakeFromNib() {
        SubtaskButton.backgroundColor = UIColor.ButtonSkyColor()
        super.awakeFromNib()
      
        SubtaskButton.layer.cornerRadius = 18.0
        SubtaskButton.clipsToBounds = true
        SubtaskButton.layer.shadowRadius = 3.0
        SubtaskButton.layer.shadowColor = UIColor.black.cgColor
        SubtaskButton.layer.shadowOffset = CGSize(width: -1.0, height: 2.0);
        SubtaskButton.layer.shadowOpacity = 0.7
        SubtaskButton.layer.masksToBounds = false;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
