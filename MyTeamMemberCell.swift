//
//  MyTeamMemberCell.swift
//  iWork
//
//  Created by Shailendra on 14/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class MyTeamMemberCell: UITableViewCell {

    @IBOutlet var UserNameLabel : UILabel!
    @IBOutlet var ProfileImageView : UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        ProfileImageView.layer.borderColor = UIColor.white.cgColor
        ProfileImageView.layer.borderWidth = 2
        ProfileImageView.layer.cornerRadius = 21
        ProfileImageView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
