//
//  KPIApprovalHistoryViewController.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class KPIApprovalHistoryViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var KPIHistoryTableView: UITableView!
    var delegate = AppDelegate()
    var ResponseDic : NSDictionary!
    var refreshControl = UIRefreshControl()
    var Api: WebApiService =  WebApiService()
    var ResponseArray = NSArray()
    var numberOfLine : Int = 1
    var btnMoreTitle: String!
    var selectedIndex: Int!
    var isSelected: Bool = false
    @IBOutlet var NoDataView : UIView!
    @IBOutlet var NoDataLabel : UILabel!
    var Dic : NSDictionary!
    var SelectedArray = NSMutableArray()
    var SelectedIndex = NSMutableArray()
    var GateChildArray = NSMutableArray()
    var GateChildArray1 = NSMutableArray()
    var GateParentArray = [NSMutableArray]()
    var GateParentArray1 = [NSMutableArray]()
    
    var GateStaticArrays = ["Gate 4","Gate 5","Gate 3","Gate 2","Gate 1"]
    var GateStaticArrays1 = ["Gate 1","Gate 2","Gate 3","Gate 4","Gate 5"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = UIApplication.shared.delegate as! AppDelegate
        self.view.backgroundColor = delegate.backgroudColor;
        KPIHistoryTableView.estimatedRowHeight = 50;
        KPIHistoryTableView.backgroundColor = UIColor.clear;
        KPIHistoryTableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: KPIHistoryTableView.bounds.size.width, height: 10.0))
        
        KPIHistoryTableView.rowHeight = UITableViewAutomaticDimension;
        refreshControl.addTarget(self, action: #selector(KPIApprovalHistoryViewController.reloadData), for: UIControlEvents.valueChanged)
        KPIHistoryTableView.addSubview(refreshControl)
        LoadingManager.showLoading(self.view)
        self.perform(#selector(KPIApprovalHistoryViewController.KPIHistoryWebApi), with: nil, afterDelay: 0.5)
        
    }
    func reloadData(){
        self.perform(#selector(KPIApprovalHistoryViewController.KPIHistoryWebApi), with: nil, afterDelay: 0.5)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ResponseArray.count;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell : KPIApprovalHistoryCell = tableView.dequeueReusableCell(
            withIdentifier: "CELL", for: indexPath) as! KPIApprovalHistoryCell
        Cell.btnViewMoreOrLess.tag = indexPath.row
        if(ResponseArray.count > 0){
            Dic = ResponseArray[indexPath.row] as! NSDictionary
            Cell.CellConfigure(info: Dic)
            
            if(SelectedIndex.contains(SelectedArray[indexPath.row])){
                Cell.GateStaticArray = GateStaticArrays1 as! NSMutableArray
                Cell.GateValueArray = GateParentArray1[indexPath.row]
                Cell.btnViewMoreOrLess.setImage(UIImage(named: "TargetUp"), for: UIControlState.normal)
                Cell.Hight.constant = CGFloat(35 * Cell.GateValueArray.count)
                Cell.TargetTableView.backgroundColor = UIColor.RedColor()
                
            }
            else{
                Cell.GateStaticArray = GateStaticArrays as! NSMutableArray
                Cell.GateValueArray = GateParentArray[indexPath.row]
                Cell.btnViewMoreOrLess.setImage(UIImage(named: "TargetDown"), for: UIControlState.normal)
                Cell.Hight.constant = 35
            }
            Cell.TargetTableView.reloadData()
            if(Dic["status"] as? String  == "Approved"){
                Cell.HightRejectionRemark.constant = 0
            }
            else{
                Cell.HightRejectionRemark.constant = 21
            }
            
        }
        return Cell
    }
    func KPIHistoryWebApi()
    {
        if (delegate.isInternetConnected()){
            let userId = UserDefaults.standard.string(forKey: "kUserId")
            let param : NSDictionary! = ["status": "10", "userId": userId!]
            ResponseDic = Api.webApi(param as! [AnyHashable : Any]!, url: "getKpiApprovalList") as NSDictionary! // status 1 for approval
            refreshControl.endRefreshing()
            if ResponseDic != nil
            {
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    LoadingManager.hideLoading(view)
                    print("ResponseDic==%@",ResponseDic);
                    ResponseArray = ResponseDic.value(forKey: "object") as! NSArray
                    if(ResponseArray.count==0){
                        NodataFound()
                    }
                    for i in 0 ..< ResponseArray.count {
                        SelectedArray.add("\(i)")
                        GateChildArray = NSMutableArray()
                        GateChildArray1 = NSMutableArray()
                        let Dic = ResponseArray[i] as! NSDictionary;
                        let Gete1 = Dic.value(forKey: "gate1") as! String
                        let Gete2 = Dic.value(forKey: "gate2") as! String
                        let Gete3 = Dic.value(forKey: "gate3") as! String
                        let Gete4 = Dic.value(forKey: "gate4") as! String
                        let Gete5 = Dic.value(forKey: "gate5") as! String
                        GateChildArray.add("\(Gete4)")
                        GateChildArray.add("\(Gete5)")
                        GateChildArray.add("\(Gete3)")
                        GateChildArray.add("\(Gete2)")
                        GateChildArray.add("\(Gete1)")
                        GateParentArray.append(GateChildArray)
                        
                        GateChildArray1.add("\(Gete1)")
                        GateChildArray1.add("\(Gete2)")
                        GateChildArray1.add("\(Gete3)")
                        GateChildArray1.add("\(Gete4)")
                        GateChildArray1.add("\(Gete5)")
                        GateParentArray1.append(GateChildArray1)
                    }

                    self.KPIHistoryTableView.reloadData()
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: ResponseDic.value(forKey: "message")))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(KPIApprovalHistoryViewController.KPIHistoryWebApi), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    func NodataFound(){
        if(ResponseArray.count == 0){
            NoDataView.isHidden = false
            NoDataLabel.isHidden = false
            NoDataLabel.text = "No record found"
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: true, completion: nil)
    }
   @IBAction func btnViewMoreOrLessTapped(sender: UIButton)
    {
        let Tagvalue = sender.tag;
        if(SelectedIndex.contains("\(Tagvalue)")){
            SelectedIndex.remove("\(Tagvalue)")
        }
        else{
            SelectedIndex.add("\(Tagvalue)")
        }
       KPIHistoryTableView.reloadData()
    }
}

