#import "NewTaskScreen.h"
#import "Header.h"
#import "iWork-Swift.h"

@interface NewTaskScreen ()<UITableViewDelegate,UITableViewDataSource,FCAlertViewDelegate>
{
    AppDelegate *delegate;
    IBOutlet UITableView *tab;
    IBOutlet UILabel *headerlab;
    IBOutlet UILabel *NoDataLabel;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    NSMutableArray *newArray;
    int TagValue;
    UIRefreshControl *refreshControl;
    WebApiService *Api;
    NSString *handelId;
    NSString* taskID, *actionType;
    NSString *CheckValue;
    NSString *Status;
    int offset;
    int limit;
    IBOutlet UIView *NoDataView;
    IBOutlet UIView *ConfirmationView;
    IBOutlet UILabel *ConfirmationTitle;
    
    IBOutlet UIView *ConfirmedPopupView;
    IBOutlet UILabel *ConfirmedDiscripTionLabel;
    
    __weak IBOutlet CustomShadowView *viewDetailPopUpInnerView;
    IBOutlet UIView *ViewDetailsView;
    IBOutlet UILabel *ApprovedLabel;
    IBOutlet UILabel *TaskNameLabel;
    IBOutlet UILabel *TaskDetailsLabel;
    IBOutlet UILabel *KPINameLabel;
    IBOutlet UILabel *CreationDateLabel;
    IBOutlet UILabel *EndDateLabel;
    IBOutlet UILabel *DurationLabel;
    IBOutlet UILabel *StakeHolderLabel;
    IBOutlet UILabel *TaskDetailsStaticLabel;
}
@end
@implementation NewTaskScreen
@synthesize taskType;


- (void)viewDidLoad{
    [super viewDidLoad];
    [self EmplyArray];
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    handelId = [NSString stringWithFormat:@"%@",[delegate.dataFull objectForKey:@"handsetId"]];
    TagValue = 0;
    Api = [[WebApiService alloc] init];
    tab.estimatedRowHeight = 50;
    tab.backgroundColor = [UIColor whiteColor];
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tab.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
    if (1 == taskType)
        headerlab.text=NSLocalizedString(@"MY_TASK", nil);
    else
        headerlab.text=NSLocalizedString(@"MY_ASSIGNMENT", nil);
//    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
//    [tab registerNib:Nib forCellReuseIdentifier:@"CELL"];
    self.view.backgroundColor = [UIColor whiteColor];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TaskAndAssignmentWebApi) withObject:nil afterDelay:0.5];
    ConfirmationTitle.textColor = [UIColor TextBlueColor];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self EmplyArray];
    [self TaskAndAssignmentWebApi];
}
-(void)EmplyArray{
    offset = 0;
    limit = 50;
    newArray = [[NSMutableArray alloc] init];
}
-(void) TaskAndAssignmentWebApi{
    if(delegate.isInternetConnected){
        if(taskType == 1){
           Status = @"me";
        }
        else{
           Status = @"linemanager";
        }
        params = @ {
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"MeLineManagerCountList?employeeID=%@&status=%@",[ApplicationState userId],Status]];
        [refreshControl endRefreshing];
        if(ResponseDic == nil){
            [self performSelector:@selector(TaskAndAssignmentWebApi) withObject:nil afterDelay:0.5]; 
        }
        if([[ResponseDic valueForKey:@"statusCode"]intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                }
                else if(newArray.count == 0){
                   [self NoDataFound];
                }
                [tab reloadData];
            }
            else{
                [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(newArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Data Found";
    }
    [tab reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
    if(ArraysValue.count>0){
        offset = offset + 50;
        [self TaskAndAssignmentWebApi];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [newArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [newArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AskForProgressCell *Cell = (AskForProgressCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.StartCancelBtn.tag = indexPath.row;
    Cell.DetailsBtn.tag = indexPath.row;
    if(newArray.count>0){
        NSDictionary * responseData = [newArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
        
    }
    return Cell;
}
-(IBAction)StartTaskAction:(UIButton*)sender{
    ConfirmationView.hidden = NO;
}
-(IBAction)CancelAction:(UIButton*)sender{
    ConfirmationView.hidden = YES;
    ViewDetailsView.hidden = YES;
}
-(IBAction)StartAction:(UIButton*)sender{
    TagValue = sender.tag;
    ConfirmationView.hidden = YES;
    taskID = [[newArray objectAtIndex:TagValue] objectForKey:@"taskId"];

    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(setStatus) withObject:nil afterDelay:0.5];
}
-(IBAction)ConfirmationOkayAndCrossAction:(UIButton*)sender{
    ConfirmedPopupView.hidden = YES;
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)ViewDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;

     NSDictionary * info= [newArray objectAtIndex:TagValue];
    TaskDetailsStaticLabel.textColor = [UIColor TextBlueColor];
    ApprovedLabel.backgroundColor = [UIColor ApproveGreenColor];
    ApprovedLabel.layer.cornerRadius = 15;
    ApprovedLabel.clipsToBounds = YES;
    if(IsSafeStringPlus(TrToString(info[@"taskNAme"]))){
        TaskNameLabel.text  = info[@"taskNAme"];
    }
    else{
        TaskNameLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskDetail"]))){
        TaskDetailsLabel.text  = info[@"taskDetail"];
    }
    else{
        TaskDetailsLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"kpi"]))){
        KPINameLabel.text  = info[@"kpi"];
    }
    else{
        KPINameLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"creationOn"]))){
        NSNumber *datenumber = info[@"creationOn"];
        CreationDateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        CreationDateLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"endDate"]))){
        NSNumber *datenumber = info[@"endDate"];
        EndDateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        EndDateLabel.text  = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"duration"]))){
        DurationLabel.text  = info[@"duration"];
    }
    else{
        DurationLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"stakeholder"]))){
        NSArray *StakeHolderName = [[info valueForKey:@"stakeholder"] valueForKey:@"stakeholderName"];
        StakeHolderLabel.text = [StakeHolderName componentsJoinedByString:@", "];
    }
    else{
        StakeHolderLabel.text  = @" ";
    }
    ViewDetailsView.hidden = NO;
}
- (void) setStatus{
    if(delegate.isInternetConnected){
         ResponseDic =  [Api WebApi:nil Url:[NSString stringWithFormat:@"startButton?employeeId=%@&taskId=%@",[ApplicationState userId],taskID]];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self performSelector:@selector(TaskAndAssignmentWebApi) withObject:nil afterDelay:0.1];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            ConfirmedPopupView.hidden = NO;
            ConfirmedDiscripTionLabel.text = [ResponseDic valueForKey:@"message"];
            [self EmplyArray];
            [self performSelector:@selector(TaskAndAssignmentWebApi) withObject:nil afterDelay:0.1];
        }
        else{
             [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
      [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(IBAction)ConfirmedViewOKCrossAction:(id)sender{
    ConfirmedPopupView.hidden = YES;
   // [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
