//
//  NegativeStackedBarChartViewController.h
//  ChartsDemo
//
//  Copyright 2015 Daniel Cohen Gindi & Philipp Jahoda
//  A port of MPAndroidChart for iOS
//  Licensed under Apache License 2.0
//
//  https://github.com/danielgindi/Charts
//

#import <UIKit/UIKit.h>
//#import <Charts/Charts.h>
#import "AppDelegate.h"

@interface ChartViewController : UIViewController <UITextFieldDelegate>
{
    //ChartViewDelegate, IChartAxisValueFormatter
    AppDelegate *delegate;
    NSMutableArray *graphData,*oldData;
    NSMutableDictionary *filterData;
    UIView *filterView, *dimView, *screenView;
    UITextField *fromDate,*toDate;
    UIButton *resetButton;
    UIDatePicker *dateTimePicker;
    BOOL isFromDate;
    UINavigationBar *NavBar;
}

@property (nonatomic, retain) NSMutableArray *graphData;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
@end
