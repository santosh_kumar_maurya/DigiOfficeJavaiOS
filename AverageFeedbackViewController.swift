//
//  AverageFeedbackViewController.swift
//  iWork
//
//  Created by Shailendra on 14/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

class AverageFeedbackViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var AverageFeedbackTableView: UITableView!
    var delegate = AppDelegate()
    var ResponseDic : NSDictionary!
    var refreshControl = UIRefreshControl()
    var Api: WebApiService =  WebApiService()
    var averageFeedback : NSNumber!
    var recommendationScore : String! = ""
    var EmployeeID : String! = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = UIApplication.shared.delegate as! AppDelegate
        AverageFeedbackTableView.estimatedRowHeight = 150;
        AverageFeedbackTableView.rowHeight = UITableViewAutomaticDimension;
        AverageFeedbackTableView.addSubview(refreshControl)
        LoadingManager.showLoading(self.view)
        self.perform(#selector(AverageFeedbackViewController.GetAverageFeedback), with: nil, afterDelay: 0.5)
    }
    func reloadData(){
        self.perform(#selector(AverageFeedbackViewController.GetAverageFeedback), with: nil, afterDelay: 0.5)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let Cell : EmployeeAverageFeedbacksCell = tableView.dequeueReusableCell(
                withIdentifier: "CELL", for: indexPath) as! EmployeeAverageFeedbacksCell
            if(averageFeedback != nil){
                let FloatValue = averageFeedback
                Cell.RatingView.value = FloatValue as! CGFloat
                Cell.RatingView.tintColor = UIColor.NeedRevisionBtnColor()
            }
            else{
                Cell.RatingView.value = 0
            }
            return Cell
        }
        else {
            let Cell : RecommendationScoreCell = tableView.dequeueReusableCell(
                withIdentifier: "CELL1", for: indexPath) as! RecommendationScoreCell
            if(recommendationScore != nil || recommendationScore == ""){
               Cell.AverageNumberLabel.text = "\(recommendationScore!)"
            }
            else{
               Cell.AverageNumberLabel.text = " "
               Cell.ScoreLabel.text = " "
            }
            return Cell
        }
    }
    func  GetAverageFeedback(){
        
        if (delegate.isInternetConnected()){
           // let userId = UserDefaults.standard.string(forKey: "kUserId")
            let param : NSDictionary! = ["employeeId": "\(EmployeeID!)","status": "1"]// status 1 for avarage feedback
            ResponseDic = Api.webApi(param as! [AnyHashable : Any]!, url: "getTeamAverageFeedback") as NSDictionary!
            refreshControl.endRefreshing()
            if ResponseDic != nil{
                LoadingManager.hideLoading(view)
                if(ResponseDic.value(forKey: "statusCode") as! Int == 5){
                    let ObjectArray = ResponseDic.value(forKey: "object") as! NSArray
                    let Dic = ObjectArray[0] as! NSDictionary
                    averageFeedback = Dic.value(forKey: "averageFeedback") as! NSNumber
                    recommendationScore = Dic.value(forKey: "recommendationScore") as! String
                    self.AverageFeedbackTableView.reloadData()
                }
                else{
                    LoadingManager.hideLoading(view)
                    ShowAlert(MsgTitle: "\(String(describing: ResponseDic.value(forKey: "message")))" as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
                }
            }
            else{
                LoadingManager.showLoading(self.view)
                self.perform(#selector(AverageFeedbackViewController.GetAverageFeedback), with: nil, afterDelay: 0.5)
            }
        }
        else{
            LoadingManager.hideLoading(view)
            ShowAlert(MsgTitle: NSLocalizedString("NO_INTERNET", comment: "") as NSString, BtnTitle: NSLocalizedString("CLOSE", comment: "")as NSString)
        }
    }
    @IBAction func BackAction(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func HomeAction(){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContainerParentViewController") as! ContainerParentViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    func ShowAlert(MsgTitle:NSString,BtnTitle:NSString){
        let ObjAlertController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertController") as! AlertController
        ObjAlertController.messageBtnStr = BtnTitle as String!;
        ObjAlertController.messageTitleStr = MsgTitle as String!;
        ObjAlertController.view.backgroundColor = UIColor.clear
        ObjAlertController.providesPresentationContextTransitionStyle = true
        ObjAlertController.definesPresentationContext = true
        ObjAlertController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(ObjAlertController, animated: true, completion: nil)
    }
}
