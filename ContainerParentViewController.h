//
//  ContainerParentViewController.h
//  iWork
//
//  Created by Shailendra on 21/09/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerParentViewController : UIViewController

-(void)buttonPressed:(id) sender;
@end
