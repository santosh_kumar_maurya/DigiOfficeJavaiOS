#import "NotificationScreen.h"
#import "Header.h"
#import "iWork-Swift.h"
@interface NotificationScreen ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource, FCAlertViewDelegate>
{
    UIRefreshControl *refreshControl;
    WebApiService *Api;
    NSDictionary *ResponseDic;
    int offset;
    int limit;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UILabel *headerlab;
    IBOutlet UIView *NoDataView;
    
    int tagValue;
    NSString *taskType;
    NSString *SelectBtnString;
  
    IBOutlet UITableView *tab;
    
    AppDelegate *delegate;
    NSMutableArray *dataArray;
    NSString *taskID,*taskFeedback;
    
    int taskRating;
    
    IBOutlet UIView *NeedRevisionView;
    IBOutlet UITextView *CommentTextView;
    IBOutlet UILabel *NeedRevisionStaticLabel;
    
    IBOutlet UIView *OuterView;
    IBOutlet HCSStarRatingView *RatingView;
    IBOutlet UILabel *ApproveTaskStaticLabel;
    IBOutlet UILabel *RatingLabel;
    IBOutlet UITextView *FeedTextView;
    float RateValue;

    IBOutlet UIView *ConfirmationView;
    IBOutlet UILabel *ConfirmationTitle;
    IBOutlet UILabel *ConfirmationDescription;
    
    IBOutlet UIView *ConfirmationConfirmView;
    IBOutlet UILabel *ConfirmationConfirmTitle;
    IBOutlet UILabel *ConfirmationConfirmDescription;
    IBOutlet UIImageView *ConfirmationConfirmImageView;
 
    NSString *EmployeeID;
    IBOutlet UIButton *ApproveRejectBtn;
    IBOutlet UIButton *CancelBtn;
    
    NSString *CommingSoon;
    NSMutableAttributedString *attributedString;
}
@end

@implementation NotificationScreen

- (void)viewDidLoad{
    [super viewDidLoad];
    offset = 0;
    limit = 50;
    taskType = @"";
    SelectBtnString = @"";
    EmployeeID = @"";
    dataArray = [[NSMutableArray alloc] init];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    headerlab.text=NSLocalizedString(@"NOTIFICATIONS", nil);
    taskRating = 0;
    taskFeedback = @"";
    tab.estimatedRowHeight = 250;
    tab.backgroundColor = [UIColor whiteColor];
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tab.bounds.size.width, 8.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];

    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(NotificationWebApi) withObject:nil afterDelay:0.5];
    
    ConfirmationTitle.textColor = [UIColor TextBlueColor];
    [CancelBtn setTitleColor:[UIColor RejectBtnColor] forState:UIControlStateNormal];
    [ApproveRejectBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    
    FeedTextView.layer.cornerRadius = 6;
    FeedTextView.layer.borderWidth = 1;
    FeedTextView.layer.borderColor = delegate.borderColor.CGColor;
    CommentTextView.layer.cornerRadius = 6;
    CommentTextView.layer.borderWidth = 1;
    CommentTextView.layer.borderColor = delegate.borderColor.CGColor;
    ApproveTaskStaticLabel.textColor = [UIColor TextBlueColor];
    NeedRevisionStaticLabel.textColor = [UIColor TextBlueColor];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
    
}
-(void) NotificationWebApi{
    if(delegate.isInternetConnected){
        params = @ {
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit],
            @"userId":[ApplicationState userId]
        };
        NSLog(@"params==%@",params);
        ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"getNotification?userId=%@",[ApplicationState userId]]];
        [refreshControl endRefreshing];
        
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self NotificationWebApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            NSLog(@"NotificationResponseDic==%@",ResponseDic);
            NoDataView.hidden = YES;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"notification"]))){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"notification"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"notification"]];
                    [tab reloadData];
                }
                else if(ResponseArrays.count == 0 && dataArray.count == 0){
                    [self NoDataFound];
                }
            }
            else{
                 [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(dataArray.count == 0){
        NoDataView.hidden = NO;
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Record Found";
    }
    [tab reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"notification"];
    if(ArraysValue.count>0){
        offset = offset + 50;
        [self NotificationWebApi];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [dataArray addObject:BindDataDic];
    }
}
- (void)reloadData{
    offset = 0;
    limit = 50;
    dataArray = [[NSMutableArray alloc] init];
    [self NotificationWebApi];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
      return [dataArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"notiStatus"] isEqualToString:@"READ"]){
        NotiCell *Cell2 = (NotiCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        NSLog(@"READ");
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
        Cell2.tag = indexPath.row;
        [Cell2 addGestureRecognizer:gestureRecognizer];
        
        if(dataArray.count>0){
            NSDictionary * responseData = [dataArray objectAtIndex:indexPath.row];
            [Cell2 configureCell:responseData];
        }
        return Cell2;
    }
    else{
        NeedRevisionCell *Cell = (NeedRevisionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        Cell.RjectBtn.tag = indexPath.row;
        Cell.ApproveBtn.tag = indexPath.row;
        Cell.NeedRevisionBtn.tag = indexPath.row;
        Cell.CompleteApproveBtn.tag = indexPath.row;
        Cell.StartTaskBtn.tag = indexPath.row;
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
        Cell.tag = indexPath.row;
        [Cell addGestureRecognizer:gestureRecognizer];
        
        NSDictionary * responseData = [dataArray objectAtIndex:indexPath.row];
        [Cell configureCell:responseData];
        
        if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"APPROVE"]||[[[dataArray objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"Assigned"]){
            Cell.StartTaskBtn.hidden = NO;
            Cell.ApproveBtn.hidden = YES;
            Cell.RjectBtn.hidden = YES;
            Cell.NeedRevisionBtn.hidden = YES;
            Cell.CompleteApproveBtn.hidden = YES;
            Cell.LineLabel.hidden = YES;
            NSLog(@"APPROVE");
            [Cell.StartTaskBtn setTitle:[NSLocalizedString(@"START_TASK", nil) uppercaseString] forState:UIControlStateNormal];
            return Cell;
        }
        else if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"NEWTASK"]){
            NSLog(@"NEWTASK");
            Cell.StartTaskBtn.hidden = YES;
            Cell.ApproveBtn.hidden = NO;
            Cell.RjectBtn.hidden = NO;
            Cell.LineLabel.hidden = NO;
            Cell.NeedRevisionBtn.hidden = YES;
            Cell.CompleteApproveBtn.hidden = YES;
            [Cell.RjectBtn setTitle:[NSLocalizedString(@"REJECT_BTN_CAPS", nil) uppercaseString] forState:UIControlStateNormal];
            [Cell.ApproveBtn setTitle:[NSLocalizedString(@"APPROVE_BTN_CAPS", nil) uppercaseString] forState:UIControlStateNormal];
            return Cell;
            
        }
        else if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"COMPLETE"]){
            NSLog(@"COMPLETE");
            Cell.StartTaskBtn.hidden = YES;
            Cell.ApproveBtn.hidden = YES;
            Cell.RjectBtn.hidden = YES;
            Cell.LineLabel.hidden = NO;
            Cell.NeedRevisionBtn.hidden = NO;
            Cell.CompleteApproveBtn.hidden = NO;
            [Cell.NeedRevisionBtn setTitle:[NSLocalizedString(@"NEED_REVISION", nil) uppercaseString] forState:UIControlStateNormal];
            [Cell.CompleteApproveBtn setTitle:[NSLocalizedString(@"APPROVE_BTN_CAPS", nil) uppercaseString] forState:UIControlStateNormal];
            return Cell;
        }
        else{
            NotiCell *Cell1 = (NotiCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            NSLog(@"OtherREAD");
            UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
            Cell1.tag = indexPath.row;
            [Cell1 addGestureRecognizer:gestureRecognizer];
            NSDictionary * responseData = [dataArray objectAtIndex:indexPath.row];
            [Cell1 configureCell:responseData];
            return Cell1;
        }
    }
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer{
        tagValue = recognizer.view.tag;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
        if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"LM_NEWCOMMENT"]){
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
            if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Approved"]
                || [[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Rejected"]
                ){
                HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
                viewController.taskID = str;
                viewController.isManager = TRUE;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
            else{
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
                TaskDetailManagerScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailManagerScreen"];
                viewController.taskID = str;
                viewController.isManager = TRUE;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"NEW_KPI"]){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
            KPIApprovalParentViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"KPIApprovalParentViewController"];
            [[self navigationController] pushViewController:ObjController animated:YES];
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"EMP_NEWCOMMENT"]){
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
            if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Approved"]){
                HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
                viewController.taskID = str;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
            else{
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
                TaskDetailScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailScreen"];
                viewController.taskID = str;
                viewController.isComeFrom = @"NOTIFICATION";
                [[self navigationController] pushViewController:viewController animated: YES];
            
            }
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"S_CLOSE"]){
            if([[[dataArray objectAtIndex:tagValue] objectForKey:@"notiStatus"] isEqualToString:@"READ"]){
                HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
                NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
                viewController.taskID = str;
                viewController.isManager = TRUE;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
            else{
                TaskDetailStakeHolderScreen *viewController = [[TaskDetailStakeHolderScreen alloc] init];
                NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
                viewController.taskID = str;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
            
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"STACKFEED"]){
            HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
            viewController.taskID = str;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"LM_STACKFEED"]){
            HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
            viewController.taskID = str;
            viewController.isManager = TRUE;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"START"]){
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
          if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Approved"]
                || [[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Rejected"]
                ){
                HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
                viewController.taskID = str;
                viewController.isManager = TRUE;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
            else{
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
                TaskDetailManagerScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailManagerScreen"];
                viewController.taskID = str;
                viewController.isManager = TRUE;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
        }
    
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"CLOSE"] ||
                 [[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"REJECT"]||
                 [[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"CANCEL"]){
            HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
            viewController.taskID = str;
            viewController.isManager = false;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"COMPLETE"]){
            if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"notiStatus"] isEqualToString:@"READ"]){
                HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
                NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
                viewController.taskID = str;
                viewController.isManager = false;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
            else{
                TaskRequestScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskRequestScreen"];
                ObjViewController.isNewReq = FALSE;
                [[self navigationController] pushViewController:ObjViewController animated:YES];
            }
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"NEWTASK"]){
            if(![[[dataArray objectAtIndex:tagValue] objectForKey:@"notiStatus"] isEqualToString:@"READ"]){
                TaskRequestScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskRequestScreen"];
                ObjViewController.isNewReq = YES;
                [[self navigationController] pushViewController:ObjViewController animated:YES];
            }
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"APPROVE"]){
            if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
                MyTaskScreenParentViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTaskScreenParentViewController"];
                [[self navigationController] pushViewController:ObjViewController animated:YES];
            }
            else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
                if(![[[dataArray objectAtIndex:tagValue] objectForKey:@"notiStatus"] isEqualToString:@"READ"]){
                    NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
                    TaskDetailScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailScreen"];
                    viewController.taskID = str;
                    [[self navigationController] pushViewController:viewController animated: YES];
                }
            }
            else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Submitted"]||
                     [[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Rejected"]||
                     [[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Cancelled"]){

                NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
                HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
                viewController.taskID = str;
                viewController.isManager = FALSE;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"NEED REVISION"]){
            if([[[dataArray objectAtIndex:tagValue] objectForKey:@"notiStatus"] isEqualToString:@"READ"]){
                
            }
            else{
                MyTaskScreenParentViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTaskScreenParentViewController"];
                ObjViewController.isComeFrom = @"PROGRESS";
                [[self navigationController] pushViewController:ObjViewController animated:YES];
            }
        }

}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)StartTaskBtnAction:(UIButton*)sender{
    tagValue = sender.tag;
    ConfirmationView.hidden = NO;
    ConfirmationTitle.text = @"Start Task";
    SelectBtnString = @"START";
    [ApproveRejectBtn setTitle:@"OK" forState:UIControlStateNormal];
    [ApproveRejectBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    ConfirmationDescription.text = NSLocalizedString(@"ARE_YOU_SURE_START", nil);
}
-(IBAction)RejectBtnAction:(UIButton*)sender{
    tagValue = sender.tag;
    taskType = @"2";
    ConfirmationView.hidden = NO;
    [ApproveRejectBtn setTitle:@"OK" forState:UIControlStateNormal];
    [ApproveRejectBtn setTitleColor:[UIColor RedColor] forState:UIControlStateNormal];
    ConfirmationTitle.text = @"Reject Task";
    SelectBtnString = @"REJECT";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    ConfirmationDescription.text = NSLocalizedString(@"ARE_YOU_SURE_REJECT", nil);
    //[self showRejectAlertForNew];
    OuterView.hidden = YES;
}
-(IBAction)ApproveBtnAction:(UIButton*)sender{
    tagValue = sender.tag;
    taskType = @"1";
    ConfirmationView.hidden = NO;
    [ApproveRejectBtn setTitle:@"OK" forState:UIControlStateNormal];
    [ApproveRejectBtn setTitleColor:[UIColor ApproveGreenColor] forState:UIControlStateNormal];
    ConfirmationTitle.text = @"Approve Task";
    SelectBtnString = @"APPROVE";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    ConfirmationDescription.text = NSLocalizedString(@"ARE_YOU_SURE_APPROVE", nil);
    //[self showApproveAlertForNew];
    OuterView.hidden = YES;
}
-(IBAction)NeedRevisionBtnAction:(UIButton*)sender{
     [self FeedBackClear];
    SelectBtnString = @"NEED REVISION";
    tagValue = sender.tag;
    taskType = @"3";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    NeedRevisionView.hidden = NO;
    OuterView.hidden = YES;
    
}
-(IBAction)CompleteApproveBtnAction:(UIButton*)sender{
   
    SelectBtnString = @"";
    tagValue = sender.tag;
    taskType = @"1";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    EmployeeID = [[dataArray objectAtIndex:tagValue] objectForKey:@"empId"];
    OuterView.hidden = NO;
}
-(IBAction)ConfirmViewCancelAction:(id)sender{
     ConfirmationView.hidden = YES;
     NeedRevisionView.hidden = YES;
}
-(IBAction)ConfirmedViewOKCrossAction:(id)sender{
   ConfirmationConfirmView.hidden = YES;
   /// [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)ApproveRejectStartNeedRevisionCompleteApproveAction:(id)sender{
    ConfirmationView.hidden = YES;
    
    if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
         [LoadingManager showLoadingView:self.view];
      [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.5];
    }
    else if([SelectBtnString isEqualToString:@"START"]){
         [LoadingManager showLoadingView:self.view];
       [self performSelector:@selector(TaskStartWebApi) withObject:nil afterDelay:0.5];
    }
    else if([SelectBtnString isEqualToString:@"NEED REVISION"]){
        if([CommentTextView.text isEqualToString:@""]||[CommentTextView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)]){
            [self ShowAlert:NSLocalizedString(@"PLEASE_GIVE_THE_FEEDBACK", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else{
            NeedRevisionView.hidden = YES;
             [LoadingManager showLoadingView:self.view];
           [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.5];
        }
    }
    else{
        [self RegcognitionsYes];
    }
}
-(void) startTheBgForStatus{
    if(delegate.isInternetConnected){
      
        if(OuterView.hidden == NO){
            int ConvertRating = (int)RateValue;
            params = @ {
                @"status": taskType,
                @"taskId": taskID,
                @"employeeId": [ApplicationState userId],
                @"rating" : [NSNumber numberWithInt:ConvertRating],
                @"feedback" : FeedTextView.text
            };
        }
        else{
            NSString *Comment = @"";
            Comment = CommentTextView.text;
            if(Comment == nil){
                Comment = @"";
            }
            if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
                params = @ {
                    @"status": taskType,
                    @"taskId": taskID,
                    @"user_id": [ApplicationState userId],
                    @"comment" : Comment
                };
            }
            else{
                params = @ {
                    @"status": taskType,
                    @"taskId": taskID,
                    @"userId": [ApplicationState userId],
                    @"comment" : Comment
                };
            }
        }
        NSLog(@"params==%@",params);
        if(OuterView.hidden == NO){
            ResponseDic = [Api WebApi:params Url:@"taskApproval"];
        }
        else{
            if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
                ResponseDic = [Api WebApi:params Url:@"updateTaskStatus"];
            }
            else{
                ResponseDic = [Api WebApi:params Url:@"needRevision"];
            }
        }
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self startTheBgForStatus];
        }
        else if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            if(OuterView.hidden == NO){
                FeedTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
                FeedTextView.textColor = [UIColor lightGrayColor];
                RatingLabel.hidden = NO;
                RatingLabel.text = @"";
                RatingView.value = 0.0;
                if([[[ResponseDic valueForKey:@"object"] valueForKey:@"recognitationStatus"] intValue]==1){
                    ConfirmationView.hidden = NO;
                    ConfirmationTitle.text = @"Give Recognition";
                    ConfirmationDescription.text = @"Would you link to give recognitions?";
                    [ApproveRejectBtn setTitle:@"YES" forState:UIControlStateNormal];
                }
                else{
                    ConfirmationConfirmTitle.text = @"RATING SENT";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"KPISubmit"];
                    ConfirmationConfirmView.hidden = NO;
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                }
            }
            else{
                if([SelectBtnString isEqualToString:@"APPROVE"]){
                    ConfirmationConfirmTitle.text = @"APPROVED";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"ApprovedKpi"];
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                    ConfirmationConfirmView.hidden = NO;
                }
                else if([SelectBtnString isEqualToString:@"REJECT"]){
                    ConfirmationConfirmTitle.text = @"REJECTED";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"RejectedKpi"];
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                    ConfirmationConfirmView.hidden = NO;
                }
                else{
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                    CommentTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
                    CommentTextView.textColor = [UIColor lightGrayColor];

                    ConfirmationConfirmView.hidden = NO;
                    ConfirmationConfirmTitle.text = @"NEED REVISION";
                    ConfirmationConfirmImageView.image = [UIImage imageNamed:@"ApprovedKpi"];
                    ConfirmationConfirmDescription.text = [[ResponseDic valueForKey:@"object"] valueForKey:@"message"];
                }
            }
            OuterView.hidden = YES;
            [self reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert: [ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
}
- (void) TaskStartWebApi{
    if(delegate.isInternetConnected){
        ResponseDic =  [Api WebApi:nil Url:[NSString stringWithFormat:@"startButton?employeeId=%@&taskId=%@",[ApplicationState userId],taskID]];
        if(ResponseDic == nil){
            [LoadingManager hideLoadingView:self.view];
            [self TaskStartWebApi];
        }
        else if([[ResponseDic valueForKey:@"statusCode"]intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            ConfirmationConfirmView.hidden = NO;
            ConfirmationConfirmTitle.text = @"TASK STARTED";
            ConfirmationConfirmImageView.image = [UIImage imageNamed:@"ApprovedKpi"];
            ConfirmationConfirmDescription.text = [ResponseDic valueForKey:@"message"];
            [self reloadData];
        }
        else{
            [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)FeedbackSubmitAction:(id)sender{
    if (RateValue == 0.0){
        [self ShowAlert:NSLocalizedString(@"PLEASE_FILL_THE_STAR", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if([FeedTextView.text isEqualToString:@""]||[FeedTextView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_GIVE_THE_FEEDBACK", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        taskFeedback = FeedTextView.text;
        taskRating = RateValue;
        [FeedTextView resignFirstResponder];
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.3];
    }
}
-(IBAction)FeedbackCancelAction:(id)sender{
    OuterView.hidden = YES;
    [self FeedBackClear];
    [FeedTextView resignFirstResponder];
}
-(void)FeedBackClear{
    RatingView.value = 0.0;
    RatingLabel.text = @"";
    CommentTextView.textColor = [UIColor lightGrayColor];
    FeedTextView.textColor = [UIColor lightGrayColor];
    FeedTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    CommentTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
}
- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    RateValue = sender.value;
    [RatingView setTintColor:[UIColor NeedRevisionBtnColor]];
    if(RateValue == 0.0){
        RatingLabel.hidden = YES;
    }
    else if(RateValue == 1.0){
        RatingLabel.text = [@"Below Expectation" uppercaseString];
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 2.0){
        RatingLabel.text = [@"Meet Expectation" uppercaseString];
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 3.0){
        RatingLabel.text = [@"Above Expectation" uppercaseString];
        RatingLabel.hidden = NO;
    }
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    if([textView isEqual:CommentTextView]){
        if([CommentTextView.text isEqualToString: NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)]){
            CommentTextView.text = @"";
            CommentTextView.textColor = [UIColor blackColor];
        }
    }
    else if([textView isEqual:FeedTextView]){
        if([FeedTextView.text isEqualToString: NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)]){
            FeedTextView.text = @"";
            FeedTextView.textColor = [UIColor blackColor];
        }
    }
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if([textView isEqual:CommentTextView]){
        if([CommentTextView.text isEqualToString:@""]){
            CommentTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
            CommentTextView.textColor = [UIColor lightGrayColor];
        }
    }
    else if([textView isEqual:FeedTextView]){
        if([FeedTextView.text isEqualToString:@""]){
            FeedTextView.text =  NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
            FeedTextView.textColor = [UIColor lightGrayColor];
        }
    }
}
-(void)RegcognitionsYes{
    ConfirmationConfirmView.hidden = YES;
    if([[ApplicationState GetRecognitionEnable] intValue] == 0){ // Not Able to give recognition
        CommingSoon = @"COMMINGSOON";
        NSString *normalText = @"Comming soon...\n Please give recognition using iConnect.";
        attributedString = [[NSMutableAttributedString alloc] initWithString:normalText];
        [self ShowAlert:@"" ButtonTitle:@"Close"];
    }
    else{
        NSString *RegcognitionsUrl = [NSString stringWithFormat:@"iconnect://?token=%@&user_key=%@&givenTo=%@&taskId=%@",[ApplicationState GetToken],[ApplicationState userId],EmployeeID,taskID];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:RegcognitionsUrl] options:@{} completionHandler:nil];
    }
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    if([CommingSoon isEqualToString:@"COMMINGSOON"]){
        ObjAlertController.isCome = @"KPI";
        ObjAlertController.AttributedString = attributedString;
    }
    else{
        ObjAlertController.MessageTitleStr = MsgTitle;
    }
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}

@end
