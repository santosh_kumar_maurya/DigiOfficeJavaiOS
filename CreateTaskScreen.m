#import "CreateTaskScreen.h"
#import "Header.h"
#import "iWork-Swift.h"

@interface CreateTaskScreen ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,FCAlertViewDelegate>{
   
    NSString *StackHolderID;
    IBOutlet UIView *DatePickerViewBg;
    IBOutlet UIDatePicker *DatePicker;
    NSString *DatePickerSelectionStr;
    NSString *StartDateStr;
    NSString *EndDateStr;
    NSString *NumberOfDaysStr;
    NSString *KPIStr;
    NSString *StakeHolderStr;
    NSMutableArray *StakeHolderNameArray;
    NSMutableArray *StakeHolderIDArray;
    NSDateFormatter *dateFormat;
    NSMutableArray *EMPIDArray;
    NSString *EMPIDStr;
    NSDictionary *ResponseDic;
    NSDictionary *param;
    WebApiService *Api;
    NSString *HandsetID;
    NSString *CheckValue;
    NSDictionary *params;
    NSString *SubTaskStr;
    NSString *TaskStr;
    NSString *TaskDetailStr;
    NSString *SubTask0;
    NSString *SubTask1;
    NSString *SubTask2;
    NSString *SubTask3;
    NSString *SubTask4;
    
    AppDelegate *delegate;
    IBOutlet UITableView *tab;
    NSMutableDictionary *dataDict;
    UITextField *taskNameTxt, *taskDetailTxt, *kpiTxt, *empTxt,*stakeHolderTxt,*StartDate,*EndDate;
    int subTaskCount;
    IBOutlet UIPickerView *areaSelect;
    IBOutlet UIView *CustomView;
    UITextField *subTaskTxt0, *subTaskTxt1, *subTaskTxt2, *subTaskTxt3, *subTaskTxt4;
    NSString *SelectTextField;
    NSMutableArray *EMPArray;
    NSString *EMPStr;
    NSString *KMPCodeStr;
    IBOutlet UILabel *headerlab;
    IBOutlet UIView *PopupView;
    IBOutlet UILabel *PopupMsgLabel;
    CustomPopView *ObjCustomPopView;
    NSMutableArray *SubtaskArray;
    int Count;
    NSString *isOpenStakeHolder;
    SelectStakeHolderCell *StakeHolderCell;
    
    TaskCell *TaskNameCell;
    SubTaskCell *SubTaskNameCell;
    TaskDetailsCell *TaskDetailsNameCell;
    
    
}
@end

@implementation CreateTaskScreen
@synthesize isAssignTask;

- (void)viewDidLoad{
    [super viewDidLoad];

    Count = 0;
    ObjCustomPopView = [[CustomPopView alloc] init];
    SubtaskArray = [[NSMutableArray alloc] init];
    EMPStr = @"";
    EMPIDStr = @"";
    SelectTextField = @"";
    DatePickerSelectionStr = @"";
    TaskStr = @"";
    TaskDetailStr = @"";
    SubTask0 = @"";
    SubTask1 = @"";
    SubTask2 = @"";
    SubTask3 = @"";
    SubTask4 = @"";
    Api = [[WebApiService alloc] init];
    tab.estimatedRowHeight = 100.0;
    [tab setRowHeight:UITableViewAutomaticDimension];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager]setEnable:YES];
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    dataDict=[[NSMutableDictionary alloc] init];
    EMPArray = [[NSMutableArray alloc] init];
    EMPIDArray = [[NSMutableArray alloc] init];
    [DatePicker setMinimumDate:[NSDate date]];
    if(isAssignTask ==YES){
        CheckValue = @"Employee";
        headerlab.text=NSLocalizedString(@"ASSING_TASK", nil);
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(EmployeeListAndAssignTaskApi) withObject:nil afterDelay:0.5];
    }
    else{
       headerlab.text=NSLocalizedString(@"TASK_CREATION", nil);
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetStackHolderData:) name:@"StackHolder" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetkpiData:) name:@"KPINOTI" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DeleteTagData:) name:@"DELETETAG" object:nil];
    
}
-(void)EmployeeListAndAssignTaskApi{
    
    if(delegate.isInternetConnected==YES){
        if(isAssignTask==YES){
            if([CheckValue isEqualToString:@"Employee"]){
                ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getEmployeeList?user_id=%@",[ApplicationState userId]]];
                if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                    [LoadingManager hideLoadingView:self.view];
                    EMPArray = [[[ResponseDic valueForKey:@"object"] valueForKey:@"empData"] valueForKey:@"empName"];
                    EMPIDArray =  [[[ResponseDic valueForKey:@"object"] valueForKey:@"empData"] valueForKey:@"empId"];
                    [areaSelect reloadAllComponents];
                }
            }
            else if([CheckValue isEqualToString:@"Assignment"]){
                params = @ {
                    @"assignedEmployeeId": EMPIDStr,
                    @"employeeName": EMPStr,
                    @"endDate": EndDateStr,
                    @"kpi": KPIStr,
                    @"stakeholderId": StackHolderID,
                    @"startDate": StartDateStr,
                    @"status": @"New",
                    @"subtask": SubTaskStr,
                    @"taskDetail": TaskDetailStr,
                    @"taskName": TaskStr,
                    @"user_id": [ApplicationState userId]
                };
                NSLog(@"params==%@",params);
                
                ResponseDic =  [Api WebApi:params Url:@"giveAssignment"];
                if(ResponseDic==nil){
                    [LoadingManager hideLoadingView:self.view];
                    [self EmployeeListAndAssignTaskApi];
                }
                else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
                    [LoadingManager hideLoadingView:self.view];
                    PopupView.hidden = NO;
                    PopupMsgLabel.text = [NSString stringWithFormat:@"%@",[ResponseDic objectForKey:@"message"]];
                    //[self alertViewFC:[ResponseDic objectForKey:@"message"]];
                    
                     [ObjCustomPopView AddCustomPopupWithCustomView:PopupView];
                }
                else{
                    [LoadingManager hideLoadingView:self.view];
                  [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                }
            }
        }
        else {
            if([CheckValue isEqualToString:@"Create"]){
                params = @ {
                    @"eEndDate": EndDateStr,
                    @"eStartDate": StartDateStr,
                    @"kpi": KPIStr,
                    @"stackholderId": StackHolderID,
                    @"subtask": SubTaskStr,
                    @"taskDetail": TaskDetailStr,
                    @"taskName": TaskStr,
                    @"userId": [ApplicationState userId]
                };
                NSLog(@"params==%@",params);
                ResponseDic =  [Api WebApi:params Url:@"taskCreation"];
                if(ResponseDic==nil){
                    [LoadingManager hideLoadingView:self.view];
                    [self EmployeeListAndAssignTaskApi];
                }
                else if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
                    [LoadingManager hideLoadingView:self.view];
                    PopupView.hidden = NO;
                    PopupMsgLabel.text = [ResponseDic objectForKey:@"message"];
                    [ObjCustomPopView AddCustomPopupWithCustomView:PopupView];
                }
                else{
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                }
            }
        }
    }
    else {
        [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(IBAction)ClearBtn:(UIButton*)sender{
    int TagValue = sender.tag;
    if(Count != 0)
    {
        [SubtaskArray removeLastObject];
        Count = Count - 1;
        
        [dataDict removeObjectForKey:[NSString stringWithFormat:@"subTask%d", TagValue]];
        for(int i = 0; i <dataDict.count;i++){
            NSString *key =  [NSString stringWithFormat:@"subTask%d", i];
            if(key !=nil){
                if([dataDict objectForKey:key]){
                }
                else{
                    NSString *Value =  [dataDict objectForKey:[NSString stringWithFormat:@"subTask%d", i+1]];
                    if(Value !=nil){
                        [dataDict removeObjectForKey:[NSString stringWithFormat:@"subTask%d", i+1]];
                        [dataDict setObject:Value forKey:[NSString stringWithFormat:@"subTask%d", i]];
                    }
                }
            }
        }
        
        
//        [tab beginUpdates];
        NSArray *arr = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:TagValue inSection:2]];
        [tab deleteRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationAutomatic];
        
        
        [tab reloadData];
//        [tab endUpdates];
    }
    
    
    
//    [dataDict removeObjectForKey:[NSString stringWithFormat:@"subTask%d", TagValue]];
//    for(int i = 0; i <dataDict.count;i++){
//        NSString *key =  [NSString stringWithFormat:@"subTask%d", i];
//        if(key !=nil){
//            if([dataDict objectForKey:key]){
//            }
//            else{
//                NSString *Value =  [dataDict objectForKey:[NSString stringWithFormat:@"subTask%d", i+1]];
//                if(Value !=nil){
//                    [dataDict removeObjectForKey:[NSString stringWithFormat:@"subTask%d", i+1]];
//                    [dataDict setObject:Value forKey:[NSString stringWithFormat:@"subTask%d", i]];
//                }
//            }
//        }
//    }
//    subTaskCount = subTaskCount - 1;
//    [tab reloadData];
    
}
-(IBAction)SubTaskBtnAction:(id)sender
{
    if(Count == 5){
       [self ShowAlert:NSLocalizedString(@"MAX_LIMIT", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else
    {
        [SubtaskArray addObject:@"1"];
        [tab beginUpdates];
        NSArray *arr = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:Count inSection:2]];
        [tab insertRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationAutomatic];
        Count = Count + 1;
        [tab endUpdates];
    }
}
-(IBAction)SubmitAction:(id)sender{

    if([TaskStr length] < 1){
        [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if([TaskDetailStr length] < 1){
        [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if (StakeHolderNameArray.count == 0 ){
        [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if ([KPIStr isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if([EMPStr isEqualToString:@""] && TRUE == isAssignTask){
        [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if([StartDateStr isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if([EndDateStr isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if ([NumberOfDaysStr containsString:@"-"]){
        [self ShowAlert:@"Number of days should be in postive value." ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if ([NumberOfDaysStr containsString:@""]){
        [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        [self getSubTaskData];
        if(isAssignTask == YES){
            CheckValue = @"Assignment";
        }
        else{
            CheckValue = @"Create";
        }
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(EmployeeListAndAssignTaskApi) withObject:nil afterDelay:0.5];
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    if (isAssignTask  == YES){
        return 8;
    }
    else{
        return 7;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==2)
    {
        if(SubtaskArray.count == 0)
        {
            return 0;
        }
        else
        {
            return SubtaskArray.count;
        }
    }
    return 1;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isAssignTask  == YES){
        if(indexPath.section == 0){
            TaskCell *Cell = (TaskCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            TaskStr = Cell.TaskTextField.text;
            taskNameTxt.text = Cell.TaskTextField.text;
            return  Cell;
        }
        else if(indexPath.section == 1){
            TaskDetailsCell *Cell = (TaskDetailsCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            TaskDetailStr = Cell.TaskDetailsTextField.text;
            taskDetailTxt.text = Cell.TaskDetailsTextField.text;
            
            return  Cell;
        }
        else if(indexPath.section == 2){
            SubTaskCell *Cell = (SubTaskCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
            [Cell.SubTaskTextField becomeFirstResponder];
            Cell.DeleteButton.tag = indexPath.row;
            if (indexPath.row == 0) {
                SubTask0 = Cell.SubTaskTextField.text;
                subTaskTxt0.text = Cell.SubTaskTextField.text;
                //Cell.SubTaskTextField.text = [dataDict objectForKey:@"subTask0"];
            }
            else if (indexPath.row == 1) {
                SubTask1 = Cell.SubTaskTextField.text;
                subTaskTxt1.text = Cell.SubTaskTextField.text;
                //Cell.SubTaskTextField.text = [dataDict objectForKey:@"subTask1"];
            }
            else if (indexPath.row == 2) {
                SubTask2 = Cell.SubTaskTextField.text;
                subTaskTxt2.text = Cell.SubTaskTextField.text;
                //Cell.SubTaskTextField.text = [dataDict objectForKey:@"subTask2"];
            }
            else if (indexPath.row == 3) {
                SubTask3 = Cell.SubTaskTextField.text;
                subTaskTxt3.text = Cell.SubTaskTextField.text;
                //Cell.SubTaskTextField.text = [dataDict objectForKey:@"subTask3"];
            }
            else if (indexPath.row == 4) {
                SubTask4 = Cell.SubTaskTextField.text;
                subTaskTxt4.text = Cell.SubTaskTextField.text;
                //Cell.SubTaskTextField.text = [dataDict objectForKey:@"subTask0"];
            }
            return Cell;
            
        }
        else if(indexPath.section == 3){
            SubTaskBtnCell *Cell = (SubTaskBtnCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
            [Cell.SubtaskButton addTarget:self action:@selector(SubTaskBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            return  Cell;
            
        }
        else if(indexPath.section == 4){
            SelectEmployeeCell *Cell = (SelectEmployeeCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL5" forIndexPath:indexPath];
            Cell.EmployeeNameTextField.text = EMPStr;
            return  Cell;
        }
        else if(indexPath.section == 5){
            SelectKpiCell *Cell = (SelectKpiCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL4" forIndexPath:indexPath];
            Cell.SelectKPITextField.text = KPIStr;
            return  Cell;
        }
        else if(indexPath.section == 6){
            StartEndDateCell *Cell = (StartEndDateCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL6" forIndexPath:indexPath];
             Cell.StartDateTextField.text = StartDateStr;
             Cell.EndDateTextField.text = EndDateStr;
            return  Cell;
        }
        else{
            SelectStakeHolderCell *Cell = (SelectStakeHolderCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL7" forIndexPath:indexPath];
            if (StakeHolderNameArray.count>0){
                Cell.tagViewHeightConstraints.constant = 40.0;
            }
            else{
                Cell.tagViewHeightConstraints.constant = 0.0;
            }
            return  Cell;

        }
    }
    else{
        if(indexPath.section == 0){
            TaskCell *Cell = (TaskCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            TaskStr = Cell.TaskTextField.text;
            taskNameTxt.text = Cell.TaskTextField.text;
            return  Cell;
        }
        else if(indexPath.section == 1){
            TaskDetailsCell *Cell = (TaskDetailsCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            TaskDetailStr = Cell.TaskDetailsTextField.text;
            taskDetailTxt.text = Cell.TaskDetailsTextField.text;
            return  Cell;
        }
        else if(indexPath.section == 2)
        {
            SubTaskCell *Cell = (SubTaskCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
            Cell.DeleteButton.tag = indexPath.row;
            [Cell.SubTaskTextField becomeFirstResponder];
            if (indexPath.row == 0) {
                SubTask0 = Cell.SubTaskTextField.text;
//                subTaskTxt0 = [[UITextField alloc] init];
                subTaskTxt0.text = Cell.SubTaskTextField.text;
               // Cell.SubTaskTextField.text = [dataDict objectForKey:@"subTask0"];
            }
            else if (indexPath.row == 1) {
                SubTask1 = Cell.SubTaskTextField.text;
                //subTaskTxt1 = [[UITextField alloc] init];
                subTaskTxt1.text = Cell.SubTaskTextField.text;
                //Cell.SubTaskTextField.text = [dataDict objectForKey:@"subTask1"];
            }
            else if (indexPath.row == 2) {
                SubTask2 = Cell.SubTaskTextField.text;
                //subTaskTxt2 = [[UITextField alloc] init];
                subTaskTxt2.text = Cell.SubTaskTextField.text;
                //Cell.SubTaskTextField.text = [dataDict objectForKey:@"subTask2"];
            }
            else if (indexPath.row == 3) {
                SubTask3 = Cell.SubTaskTextField.text;
                //subTaskTxt3 = [[UITextField alloc] init];
                subTaskTxt3.text = Cell.SubTaskTextField.text;
                //Cell.SubTaskTextField.text = [dataDict objectForKey:@"subTask3"];
            }
            else if (indexPath.row == 4) {
                SubTask4 = Cell.SubTaskTextField.text;
               // subTaskTxt4 = [[UITextField alloc] init];
                subTaskTxt4.text = Cell.SubTaskTextField.text;
               // Cell.SubTaskTextField.text = [dataDict objectForKey:@"subTask0"];
            }
            return Cell;
            
        }
        else if(indexPath.section == 3){
            SubTaskBtnCell *Cell = (SubTaskBtnCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
            
            [Cell.SubtaskButton addTarget:self action:@selector(SubTaskBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            return  Cell;
            
        }
        else if(indexPath.section == 4){
            SelectKpiCell *Cell = (SelectKpiCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL4" forIndexPath:indexPath];
            Cell.SelectKPITextField.text = KPIStr;
            return  Cell;
        }
        else if(indexPath.section == 5){
            StartEndDateCell *Cell = (StartEndDateCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL6" forIndexPath:indexPath];
            Cell.StartDateTextField.text = StartDateStr;
            Cell.EndDateTextField.text = EndDateStr;
            return  Cell;
        }
        else{
            SelectStakeHolderCell *Cell = (SelectStakeHolderCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL7" forIndexPath:indexPath];
            if (StakeHolderNameArray.count>0){
                Cell.tagViewHeightConstraints.constant = 40.0;
            }
            else{
                Cell.tagViewHeightConstraints.constant = 0.0;
            }
            return  Cell;
        }
    }
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component; {
    if([EMPArray count]>0){
        return [EMPArray count];
    }
    return 0;
}
-(NSString*) pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if([EMPArray count]>0){
        return [EMPArray objectAtIndex:row];
    }
    return @"";
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;{
    if([EMPArray count]>0){
        EMPStr = [EMPArray objectAtIndex:row];
        EMPIDStr = [EMPIDArray objectAtIndex:row];
    }
    else{
        EMPStr = @"";
        EMPIDStr = @"";
    }
    [tab reloadData];
}
-(IBAction)SelectEmployeePickerView{
    [self KeyBoardHide];
    CustomView.hidden = NO;
    [areaSelect reloadAllComponents];
}
-(IBAction)PickerDoneAction:(id)sender{
    CustomView.hidden = YES;
    if([EMPStr isEqualToString:@""]){
      EMPStr = [EMPArray objectAtIndex:0];
        EMPIDStr = [EMPIDArray objectAtIndex:0];
    }
    [tab reloadData];
}
-(IBAction)OpenKpiAction:(id)sender{
   [self KeyBoardHide];
    [self KeyBoardHide];
    if(isAssignTask == YES){
        if([EMPIDStr isEqualToString:@""]){
            [self ShowAlert:NSLocalizedString(@"SELECT_EMPLOYEE", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else{
            [self PersentKPIPopup];
        }
    }
    else{
       [self PersentKPIPopup];
    }
}
-(void)PersentKPIPopup{
    SelectTextField = @"KPI";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyKPIScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyKPIScreen"];
    if(isAssignTask == YES){
        ObjViewController.EMP_ID = EMPIDStr;
        ObjViewController.isComeFrom = @"ASSIGN";
    }
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:ObjViewController contentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    [self presentViewController:popupViewController animated:NO completion:nil];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    DatePickerViewBg.hidden = YES;
    CustomView.hidden = YES;
    if([textField  isEqual:subTaskTxt0]){
        NSLog(@"subTaskTxt0");
    }
    else if([textField  isEqual:subTaskTxt1]){
         NSLog(@"subTaskTxt1");
    }
//    if([SubTask0 isEqualToString:@""]){
//        [dataDict setObject:SubTask0 forKey:@"subTask0"];
//    }
//    else if([SubTask1 isEqualToString:@""]){
//        [dataDict setObject:SubTask1 forKey:@"subTask1"];
//    }
//    else if([SubTask2 isEqualToString:@""]){
//        [dataDict setObject:SubTask2 forKey:@"subTask2"];
//    }
//    else if([SubTask3 isEqualToString:@""]){
//         [dataDict setObject:SubTask3 forKey:@"subTask3"];
//    }
//    else if([SubTask4 isEqualToString:@""]){
//         [dataDict setObject:SubTask4 forKey:@"subTask4"];
//    }
    
    if(textField == kpiTxt){
        [kpiTxt resignFirstResponder];
    }
    else if(textField== empTxt){
         [empTxt resignFirstResponder];
         SelectTextField = @"EMP";
         areaSelect.hidden = NO;
         [tab reloadData];
    }
    else if (textField == stakeHolderTxt){
        [stakeHolderTxt resignFirstResponder];
    }
}
-(void)DeleteTagData:(NSNotification*)notifcation{
    
    NSDictionary *Dic = notifcation.userInfo;
    NSLog(@"Dic====%@",Dic);
    NSString *StakeHolderTagValue = [Dic valueForKey:@"TAG"];
    [self GetCellObject];
    
    if(StakeHolderNameArray.count>0){
        [StakeHolderCell.tagsView deleteTagAtIndex:[StakeHolderTagValue intValue]];
        [StakeHolderNameArray removeObjectAtIndex:[StakeHolderTagValue intValue]];
        [StakeHolderIDArray removeObjectAtIndex:[StakeHolderTagValue intValue]];
        
    }
    if(StakeHolderNameArray.count == 0){
        StakeHolderStr = nil;
        StackHolderID = nil;
    }
    else{
        StakeHolderStr = @"";
        StackHolderID = @"";
        StakeHolderStr = [StakeHolderNameArray componentsJoinedByString:@","];
        StackHolderID = [StakeHolderIDArray componentsJoinedByString:@","];
    }

    [tab reloadData];
    
    
}
-(IBAction)OpenDatePicker:(UIButton*)sender{
    switch (sender.tag) {
        case 1:
            EndDateStr = @"";
            DatePickerViewBg.hidden = NO;
            DatePickerSelectionStr = @"Start";
            break;
        case 2:
            DatePickerViewBg.hidden = NO;
            DatePickerSelectionStr = @"End";
            break;
        default:
            break;
    }
}
-(IBAction)DatePickerDoneAction:(id)sender{
    
    DatePickerViewBg.hidden = YES;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    if([DatePickerSelectionStr isEqualToString:@"Start"]){
        StartDateStr = [dateFormat stringFromDate: DatePicker.date];
        EndDateStr = @"";
        NumberOfDaysStr = @"";
        
    }
    else if([DatePickerSelectionStr isEqualToString:@"End"]){
         EndDateStr = [dateFormat stringFromDate: DatePicker.date];
    }
    
   
    if(![EndDateStr isEqualToString:@""]){
        NSComparisonResult result;
        NSDate *startDate = [dateFormat dateFromString:StartDateStr];
        NSDate *endDate = [dateFormat dateFromString:EndDateStr];
        result = [startDate compare:endDate];
        
        if(result==NSOrderedAscending){
            [self DaysCount];
        }
        else if(result==NSOrderedDescending){
            EndDateStr = @"";
            NumberOfDaysStr = @"";
            [self ShowAlert:NSLocalizedString(@"DATE_VALIEDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else{
           // [self DaysCount];
//            EndDateStr = @"";
//            NumberOfDaysStr = @"";
//            [self ShowAlert:NSLocalizedString(@"DATE_VALIEDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
     [tab reloadData];
}
-(void)DaysCount{
    NSString *start = StartDateStr;
    NSString *end = EndDateStr;
    NSDateComponents *components;
    if([StartDateStr isEqualToString:EndDateStr]){
        NSDate *startDate = [dateFormat dateFromString:start];
        NSDate *endDate = [dateFormat dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        components = [gregorianCalendar components:NSCalendarUnitDay
                                          fromDate:startDate
                                            toDate:endDate
                                           options:0];
        components.day = 1;
        NSDate *nextDate = [gregorianCalendar dateByAddingComponents:components toDate:[NSDate date] options:0];
        EndDateStr = [dateFormat stringFromDate: nextDate];
    }
    else{
        NSDate *startDate = [dateFormat dateFromString:start];
        NSDate *endDate = [dateFormat dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        components = [gregorianCalendar components:NSCalendarUnitDay
                                          fromDate:startDate
                                            toDate:endDate
                                           options:0];
    }
    NumberOfDaysStr = [NSString stringWithFormat:@"%ld",(long)[components day]];
    [tab reloadData];
}
-(IBAction)SelectStackHolderAction{
    [self KeyBoardHide];

    
    if(isAssignTask == YES){
        if([EMPIDStr isEqualToString:@""]){
            [self ShowAlert:NSLocalizedString(@"SELECT_EMPLOYEE", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else{
            [self PersentStakeHolderPopup];
        }
    }
    else{
           [self PersentStakeHolderPopup];
    }
}
-(void)PersentStakeHolderPopup{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    StackHolderViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"StackHolderViewController"];
    if(isAssignTask == YES){
        ObjViewController.EMP_ID = EMPIDStr;
        ObjViewController.isComeFrom = @"ASSIGN";
    }
    ObjViewController.StakeHolderIDStr = StackHolderID;
    ObjViewController.StakeHolderNameStr = StakeHolderStr;
    ObjViewController.view.backgroundColor = [UIColor clearColor];
    ObjViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjViewController animated:NO completion:nil];
}
-(void)GetStackHolderData:(NSNotification*)notification{
    [self GetCellObject];
    if([[StakeHolderCell.tagsView tags] count]>0){
        for (int i = (int)StakeHolderNameArray.count - 1; i >= 0; i--){
            [StakeHolderCell.tagsView deleteTagAtIndex:i];
        }
    }
    NSDictionary *Dic = notification.userInfo;
    if(Dic.count>0){
        StakeHolderStr = [Dic valueForKey:@"StackHolderName"];
        StakeHolderNameArray = [[NSMutableArray alloc] init];
        StakeHolderIDArray  = [[NSMutableArray alloc] init];
        StakeHolderNameArray = [[StakeHolderStr componentsSeparatedByString:@","] mutableCopy];
        [StakeHolderNameArray mutableCopy];
        StackHolderID = [Dic valueForKey:@"StackHolderID"];
        StakeHolderIDArray = [[StackHolderID componentsSeparatedByString:@","] mutableCopy];
        [StakeHolderIDArray mutableCopy];
        for (int i = 0; i< StakeHolderNameArray.count; i++){
            [StakeHolderCell.tagsView addTag:StakeHolderNameArray[i]];
        }
    }
    else{
        [StakeHolderNameArray removeAllObjects];
        [StakeHolderIDArray removeAllObjects];
        StackHolderID = nil;
        StakeHolderStr = nil;
    }
    [tab reloadData];
}
-(void)GetCellObject{
   
    if(isAssignTask == YES){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:7];
        StakeHolderCell = [tab cellForRowAtIndexPath:indexPath];
    }
    else{
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:6];
        StakeHolderCell = [tab cellForRowAtIndexPath:indexPath];
    }
}
-(void)viewWillLayoutSubviews{
    [self KeyBoardHide];
    [self.view endEditing:YES];
    [super viewWillLayoutSubviews];
}
-(void)viewDidAppear:(BOOL)animated{
    [self KeyBoardHide];
}
-(void)KeyBoardHide{
    [taskNameTxt resignFirstResponder];
    [taskDetailTxt resignFirstResponder];
    [StartDate resignFirstResponder];
    [EndDate resignFirstResponder];
    [subTaskTxt0 resignFirstResponder];
    [subTaskTxt1 resignFirstResponder];
    [subTaskTxt2 resignFirstResponder];
    [subTaskTxt3 resignFirstResponder];
    [subTaskTxt4 resignFirstResponder];
    [kpiTxt resignFirstResponder];
    [empTxt resignFirstResponder];
    [stakeHolderTxt resignFirstResponder];
    DatePickerViewBg.hidden = YES;
    CustomView.hidden = YES;
    [tab reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return TRUE;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
        if(textField == taskNameTxt)
            [dataDict setObject:taskNameTxt.text forKey:@"taskName"];
        else if(textField == taskDetailTxt)
            [dataDict setObject:taskDetailTxt.text forKey:@"taskDetail"];
        else if(textField == kpiTxt)
            [dataDict setObject:kpiTxt.text forKey:@"kpiTxt"];
        else if(textField==subTaskTxt0)
            [dataDict setObject:subTaskTxt0.text forKey:@"subTask0"];
        else if(textField==subTaskTxt1)
            [dataDict setObject:subTaskTxt1.text forKey:@"subTask1"];
        else if(textField==subTaskTxt2)
            [dataDict setObject:subTaskTxt2.text forKey:@"subTask2"];
        else if(textField==subTaskTxt3)
            [dataDict setObject:subTaskTxt3.text forKey:@"subTask3"];
        else if(textField==subTaskTxt4)
            [dataDict setObject:subTaskTxt4.text forKey:@"subTask4"];
        else if(textField==stakeHolderTxt)
            [dataDict setObject:stakeHolderTxt.text forKey:@"stakeHolders"];
}

-(void)GetkpiData:(NSNotification*)notification;{
    KPIStr = [notification.userInfo valueForKey:@"DESC"];
    KMPCodeStr = [notification.userInfo valueForKey:@"CODE"];
    [tab reloadData];
}
-(void)getSubTaskData{
    
    NSMutableArray *SubTaskArray = [[NSMutableArray alloc] init];
    if (![SubTask0 isEqualToString:@""])
    {
        [SubTaskArray addObject:SubTask0];
    }
    else if (![SubTask1 isEqualToString:@""])
    {
        [SubTaskArray addObject:SubTask1];
    }
    else if (![SubTask2 isEqualToString:@""])
    {
        [SubTaskArray addObject:SubTask2];
    }
    else if (![SubTask3 isEqualToString:@""])
    {
        [SubTaskArray addObject:SubTask3];
    }
    else if (![SubTask4 isEqualToString:@""])
    {
        [SubTaskArray addObject:SubTask4];
    }
    SubTaskStr = [SubTaskArray componentsJoinedByString:@","];
}
-(IBAction)PopupCrossAndOkAction:(id)sender {
    PopupView.hidden = YES;
    [[self navigationController] popViewControllerAnimated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction{
//    [self GetCellObject];
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    TaskNameCell = [tab cellForRowAtIndexPath:indexPath];
//    
//    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:0 inSection:1];
//    SubTaskNameCell = [tab cellForRowAtIndexPath:indexPath1];
//    
//    NSIndexPath *indexPath3 = [NSIndexPath indexPathForRow:0 inSection:3];
//    TaskDetailsNameCell = [tab cellForRowAtIndexPath:indexPath3];
//    
//    [TaskNameCell.TaskTextField resignFirstResponder];
//    [TaskDetailsNameCell.TaskDetailsTextField resignFirstResponder];
//    [SubTaskNameCell.SubTaskTextField resignFirstResponder];

    
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContainerParentViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContainerParentViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:NO completion:nil];
}
@end
