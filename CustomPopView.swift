//
//  CustomPopView.swift
//  iWork
//
//  Created by Shailendra on 12/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

public class CustomPopView: NSObject {
   
    override init() {}
    func AddCustomPopup(CustomView:UIView){
        let frontToBackWindows  =  UIApplication.shared.windows
        for window: UIWindow in frontToBackWindows {
            let windowOnMainScreen: Bool = window.screen == UIScreen.main
            let windowIsVisible: Bool = !window.isHidden && window.alpha > 0
            let windowLevelNormal: Bool = window.windowLevel == UIWindowLevelNormal
            if windowOnMainScreen && windowIsVisible && windowLevelNormal {
                window.addSubview(CustomView)
                break
            }
        }
    }
}
