//
//  CustomView.swift
//  iWork
//
//  Created by Shailendra on 10/10/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

import UIKit

public class CustomShadowView: UIView {
    
    override open func layoutSubviews() {
        layer.cornerRadius = 2.0
        mask?.layer.cornerRadius = 7.0
        layer.shadowRadius = 3.0;
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: -1.0, height: 2.0)
        layer.shadowOpacity = 0.7
        layer.masksToBounds = false
    }
}
